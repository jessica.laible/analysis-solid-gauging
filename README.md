# ANALYSIS SOLID GAUGING
ANALYSIS SOLID GAUGING aims to analyse and present data from suspended sand samples and ADCP gaugings.

## Licence
ANALYSIS SOLID GAUGING

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


## Requirements and Dependencies
ANALYSIS SOLID GAUGING is currently being developed using Python 3.9.0 and makes use of the following packages:  
contourpy==1.0.7  
cycler==0.11.0  
fonttools==4.39.3  
importlib-resources==5.12.0  
kiwisolver==1.4.4  
matplotlib==3.7.1  
mycolorpy==1.5.1  
numpy==1.24.2  
packaging==23.0  
pandas==1.5.3  
Pillow==9.4.0  
pyparsing==3.0.9  
PyQt5==5.15.9  
PyQt5-Qt5==5.15.2  
PyQt5-sip==12.11.1  
python-dateutil==2.8.2  
pytz==2023.3  
scipy==1.10.1  
six==1.16.0  
sortedcontainers==2.4.0  
utm==0.7.0  
xmltodict==0.13.0  
zipp==3.15.0  

## Authors
Jessica Laible - PhD Student  
Blaise Calmel - Research Engineer   
Thibault Vassor - Intern  
INRAE Lyon-Grenoble   
jessica.laible@inrae.fr

## Citation
Laible, J., Calmel, B., Le Coz, J., Camenen, B., Dramais, G., & Vassor, T. (2023). Analysis solid gauging. In River suspended-sand flux computation with uncertainty estimation, using water samples and high-resolution ADCP measurements. Zenodo. https://doi.org/10.5281/zenodo.13973594