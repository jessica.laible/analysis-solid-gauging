cat("\014") # Clear console
rm(list=ls())# Clean workspace

args <- commandArgs(trailingOnly = F)
cwd <- args[length(args)-1]
cwd <- sub("--","", cwd)
cwd <- gsub("§20%", " ", cwd)

packages <- c("ggplot2", "devtools", "RBaM")
missing_packages <- setdiff(packages, rownames(installed.packages()))

install.packages(missing_packages[missing_packages != "RBaM"],
                 repos = "http://cran.us.r-project.org")

if ("RBaM" %in% missing_packages){
  devtools::install_github('BaM-tools/RBaM', 
                           repos = "http://cran.us.r-project.org")
}

library(RBaM)
# install.packages('ggplot2', dependencies = TRUE)
library(ggplot2)

path_data <- read.table(paste0(cwd, '/Info_BaM.csv'),sep = ';',header = T)
sampling_date = path_data[2,1]
sampler = path_data[3,1]


file_pathh = path_data[1,1]
path_temps <- setwd(paste0(gsub('\\\\', '/', file_pathh)))
path_temps <- setwd(paste0(gsub('\\\\', '/', file_pathh)))


file_concentration <- read.table(paste0(path_temps, '/', sampling_date, '_Point_errors_', sampler ,'.csv'),sep = ';',header = T)

est_params <- read.table(paste0(path_temps, '/', sampling_date, '_Estimated_a_priori_params_RBaM_', sampler, '.csv'),sep = ';',header = T)

f1='lnCR+alpha*Z'


# get number of abscissas
no_verticals = unique(file_concentration$Abscissa_Name)

# Ks=vector(mode='list',length=5)

for (i in 1:length(no_verticals)) {
  
  workspace_temp=paste0(path_temps,'/BaM_workspace/','Abscissa_',i)
  
  # Define a priori parameters (estimated in error_analysis.py )
  p1=parameter(name='lnCR',init= est_params[i,'lnCr_est'],prior.dist='Gaussian',prior.par=c(est_params[i,'lnCr_est'],as.double(est_params[3,'Info'])))
  p2=parameter(name='alpha',init=est_params[i,'alpha_est'],prior.dist='Gaussian',prior.par=c(est_params[i,'alpha_est'],as.double(est_params[4,'Info'])))
  
  # Define data for each vertical
  data_vert = file_concentration[file_concentration$Abscissa_Name ==i,]
  # print(data_vert)
  
  # plot z - lnCs
  # plot(y=data_vert$z,x=data_vert$lnCs,col='blue',pch=19)
  # plot(x=data_vert$z,y=data_vert$lnCs,col='blue',pch=19)
  
  # use xtraModelInfo to pass the names of the inputs and the formulas
  xtra=xtraModelInfo(object=list(inputs=c('Z'),formulas=c(f1)))
  # model
  mod=model(ID='TextFile',nX=1,nY=1,par=list(p1,p2),xtra=xtra)
  
  X_temp <- data.frame(data_vert[,2])
  colnames(X_temp) <- "Z"
  
  Y_temp <- data.frame(data_vert[,3])
  colnames(Y_temp) <- "lnCs"
  
  Yu_temp <- data.frame(data_vert[,4])
  colnames(Yu_temp) <- "uCs"
  # Input data. WARNING: make sure column order is consistent with inputs declared in xtraModelInfo
  X= X_temp
  # Output data. WARNING: make sure column order is consistent with the order of formulas
  Y=Y_temp
  Yu=Yu_temp
  # dataset object
  data=dataset(X=X,Y=Y,Yu=Yu,data.dir=workspace_temp)
  
  remnant_error <- list(remnantErrorModel(funk = "Constant",
                                          par = list(parameter(name="intercept",
                                                               init=mean(data_vert$lnCs),
                                                               prior.dist = "Uniform",
                                                               prior.par = c(0,max(data_vert$lnCs))))))
  
  BaM(mod=mod,data=data,workspace=workspace_temp,remnant = remnant_error )
  
  mcmc=readMCMC(file=file.path(workspace_temp,'Results_Cooking.txt'))
  # g=tracePlot(mcmc)
  # gridExtra::grid.arrange(grobs=g,ncol=2)
  
  # g=densityPlot(mcmc)
  # gridExtra::grid.arrange(grobs=g,ncol=2)
  
  res=read.table(file=file.path(workspace_temp,'Results_Residuals.txt'),header=TRUE)
  res[res==-9999] <- NA
  
  Y1_sim_exp <- exp(res$Y1_sim) 
  Y1_obs_exp <- exp(res$Y1_obs)
  
  g=ggplot(res,aes(y=X1_obs))+theme_bw()+xlab('Concentration')+ylab('bed level')
  g=g+geom_line(aes(x=Y1_sim_exp),color='blue')+geom_point(aes(x=Y1_obs_exp),color='blue')
  g=g+coord_trans(x="log10")
  print(g)
  
  pred_ParamU=prediction(X=X_temp,
                         spagFiles=c('lnCs_ParamU.spag'),
                         data.dir=workspace_temp,
                         fname='Config_Pred_ParamU.txt',
                         doParametric=TRUE,
                         doStructural=c(FALSE))
  # # propagate total uncertainty 
  
  pred_totalU=prediction(X=X_temp,
                         spagFiles=c('lnCs_TotalU.spag'),
                         data.dir=workspace_temp,
                         fname='Config_Pred_TotalU.txt',
                         doParametric=TRUE,
                         doStructural=c(TRUE))
  
  BaM(mod=mod,data=data,workspace=workspace_temp,remnant = remnant_error,pred=list(pred_ParamU,pred_totalU),doCalib=FALSE,doPred=TRUE)
  
  # spag =utils::read.table(file.path(workspace,'lnCs_TotalU.spag'))
  # spag_temps<- exp(spag)
  # matplot(y=X,x=spag_temps,type='l',col='gray',log ="x")
  # lines(y=t(X),x=Y1_sim_exp,col='red',pch=19)
  # points(y=t(X),x=Y1_obs_exp,col='blue',pch=19)
  
  
  # spag =utils::read.table(file.path(workspace,'lnCs_ParamU.spag'))
  # spag_temps<- exp(spag)
  # matplot(y=X,x=spag_temps,type='l',col='gray',log ="x")
  # lines(y=t(X),x=Y1_sim_exp,col='red',pch=25)
  # points(y=t(X),x=Y1_obs_exp,col='blue',pch=19)
  
  
  # Necessary outputs per vertical:
  # graph with spaghetti, obs with error, simulated with error, mean uncertainty
  # df with total and parametric uncertainty 
  # regression equation
  # Ks[i] = i+1
}

# create one df with total and parametric uncertainty for each vertical
# export df