# -*- coding: utf-8 -*-

"""
ANALYSIS SOLID GAUGING

Created on Mon Apr 19 11:51:02 2021

@authors: Jessica Laible - Blaise Calmel - Thibault Vassor (INRAE)

The purpose of this script is to analyse and present data from suspended sand samples and ADCP gaugings

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Packages
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import os

# To find R.exe path, open RStudio and type file.path(R.home("bin"), "R")
path_R = 'C:/Users/jessica.laible/AppData/Local/Programs/R/R-4.2.1/bin/x64/Rscript.exe'
# Vertical distance from the diver to the sampler nozzle (P6 / P72)
# vertical distance above the bottom (BD)
position_diver = 0.12  # (m) 
HADCP_option = True     # Choose if HADCP section or not

# Add paths to system
cwd = os.getcwd()
dirnamecwd = os.path.dirname(cwd)
classes_path = cwd + os.sep + 'classes'
classes_MAP_Qrev_path = cwd + os.sep + 'qrevint_21-03-30'
sys.path.insert(0, classes_path)
sys.path.insert(0, classes_MAP_Qrev_path)

# Local impot
from Choice_unit import choice_unit  
from Message_error_no_prep_file import message_error_no_prep_file   
from User_data_choice import user_data_choice
from User_input import user_input

from Hydrosedimentary_station_data import hydrosedimentary_data
from Field_data_initialisation import field_data_initialisation
from Determine_sampling_depth import determine_sampling_depth, define_sampling_depth
from Stats_granulo import stats_granulo
from Velocity_tools import map_treatment
from MAP_user_input import map_user_input
from Run_MAP import run_MAP
import Plot_point_data

from User_data_choice import ask_float
from Sediment_flux_BD_single import sediment_flux_BD          
from Sediment_flux_BD_C_single import sediment_flux_BD_C                    
from Calculate_Concentration_single import calculate_concentration, calculate_concentration_S

from Nearest_neighbor import nearest_neighbor_method
from ISO4363 import ISO_method
from SDC_method import SDC_method
from SDC_Rouse_method import SDC_Rouse_method
from Segment_method import segment_method
from SDC_fines_method import SDC_fines_method
from GSD_interpolation import GSD_interpolation, calculate_mean_GSD

from Uncertainty_estimation import uncertainty_analysis
from HADCP_function import HADCP_function, HADCP_function_BD
from Export_data import Export_data

#%% Initialization
stage_sampling = None
spm_sampling = None
Q_sampling = None
summary_NN = None 
summary_ISO = None 
summary_Rouse = None 
summary_SDC = None
summary_segment = None
summary_fine = None
summary_GSD_SDC = None
Conc_SDC_cell_export = None
sand_flux_SDC_kg_s_export = None
Conc_Rouse_cell_export = None
sand_flux_Rouse_kg_s_export = None
ISO_mean_gsd = None
ISO_mean_gsd_number = None
ISO_classes = None
Conc_fine_cell_export = None
fine_flux_kg_s_export = None
ratio_cell = None
D50_SDC_cell_export = None 
GSD_SDC_cell_export = None
mean_gsd_weighted = None 
u_point_sand = None
u_point_fines = None
comparison_methods = None
HADCP_export = None
HADCP_export_BD = None
uncertainty = None
map_data = None
 
#%% User inputs
unit = choice_unit()    # Choose units used for volumes and masses
if os.path.isfile(cwd + os.sep + "path_files.txt"):
    with open(cwd + os.sep + "path_files.txt") as f:
        txt = f.readlines()
    path_field_data = txt[0]
else:
    path_filed_data = cwd
    with open(cwd + os.sep + "path_files.txt", "w") as f:
        f.write(path_filed_data)

#%%
# Paths
user_dat = user_data_choice()
choice = user_dat.choice_list

# Check if initial data are available
if choice[0] == 0 :
    message_error_no_prep_file()
    sys.exit('Samples file (Prep_data) is missing')
    
# Load data
path_prep_data, path_diver, path_grainsize, path_ADCP, path_Q, path_stage, path_spm = user_input(path_field_data, choice)
if len(path_prep_data) > 0:
    with open(cwd + os.sep + 'path_files.txt', 'w') as f:
        f.write(os.path.abspath(os.path.dirname(path_prep_data)))

# MAP
if choice[3] == 1:
    keys = [elem.split('.')[-1].split('.')[0] for elem in path_ADCP]
    if 'mmt' in keys or 'mat' in keys:
        tk_map = map_user_input()
        tk_map.open()
        input_getted = tk_map.input_map
        if 'mmt' in keys:
            type_meas = 'TRDI'
            fileName = path_ADCP[0]
        elif 'mat' in keys:
            type_meas = 'SonTek'
            ind_meas = [i for i, s in enumerate(keys) if "mat" in s]
            fileName = [(path_ADCP[x]) for x in ind_meas]

        path_map_result = run_MAP(input_getted, fileName, type_meas)
    else:
        path_map_result = path_ADCP[0]

    with open(path_map_result, 'rb') as f:
        map_data = pickle.load(f)
        

#%% Data treatment
# Prepare data
analysis_data, outpath, outpath_figures, sampling_date = field_data_initialisation(path_prep_data, map_data)

# Hydrosedimentary data
if choice[4] == 1:
    Q_sampling = hydrosedimentary_data(analysis_data, path_Q) #* functions class
if choice[5] == 1:
    stage_sampling = hydrosedimentary_data(analysis_data, path_stage)
if choice[6] == 1:
    spm_sampling = hydrosedimentary_data(analysis_data, path_spm)

# Determine sampling depths
if choice[1] == 1: # using the diver
    analysis_data = determine_sampling_depth(path_diver, analysis_data, choice, position_diver)
if choice[1] == 0: # using field data
    analysis_data = define_sampling_depth(analysis_data)
    
# Delete data without C or dry mass
if 'Dry_mass_sand' in analysis_data.columns: # and 'Dry_mass_total' in analysis_data.columns:
    analysis_data = analysis_data.dropna(subset = ['Dry_mass_sand'], how = 'any')
    analysis_data.reset_index(drop=True, inplace=True)
if 'Sand_concentration' in analysis_data.columns:
    analysis_data = analysis_data.dropna(subset = ['Sand_concentration'])
    analysis_data.reset_index(drop=True, inplace=True)
analysis_data = analysis_data.sort_values(['Abscissa', 'Sampling_depth_final'])
analysis_data.reset_index(drop=True, inplace=True)
# supposing the deepest sample at 0.1 m above the bottom:
most_frequent_sampler = max(set(list(analysis_data['Sampler'])), key=list(analysis_data['Sampler']).count).strip()
if choice[3] == 0 and most_frequent_sampler != 'BD':
    max_depth_vertical = []
    for k in np.unique(analysis_data['Abscissa_Name']):
        f = analysis_data[analysis_data['Abscissa_Name'] == k]
        f.reset_index(drop=True, inplace=True)
        cc = np.max(f['Sampling_depth_final']) + 0.1
        ccc = [cc] * len(f)
        max_depth_vertical.append(ccc)
    analysis_data['vertical_depth'] = [item for sublist in max_depth_vertical for item in sublist]
    
#analysis_data = pd.read_csv(outpath + sampling_date + '_Analysis_data_with_1.csv', sep = ';')

# Get velocity data
if choice[3] == 1:
    track, track_depth, v2_d_index, analysis_data = map_treatment(analysis_data, map_data)
    
# Get grain size distribution and statistics
if choice[2] == 1:   
    analysis_data, cumsum, size_classes_cumsum, size_classes_classified, number_gsd = stats_granulo(analysis_data, path_grainsize)
            
# Plot data
if choice[3] == 1 and choice[2] == 1:
    Plot_point_data.plot_data_A(analysis_data, outpath_figures, sampling_date, track, track_depth, cumsum)
elif choice[3] == 0 and choice[2] == 1:
    Plot_point_data.plot_data_B(analysis_data, outpath_figures, sampling_date, cumsum)
elif choice[3] == 1 and choice[2] == 0:
    Plot_point_data.plot_data_C(analysis_data, outpath_figures, sampling_date, track, track_depth)
elif choice[3] == 0 and choice[2] == 0:
    Plot_point_data.plot_data_D(analysis_data, outpath_figures, sampling_date)

#%% Calculate point fluxes or concentration
most_frequent_sampler = max(set(list(analysis_data['Sampler'])), key=list(analysis_data['Sampler']).count).strip()
 
if choice[2] == 0 and most_frequent_sampler == 'BD':
    D50_BD_alpha = ask_float(text='D50 for all samples', title='Estimate D50 (µm)', default_value='100')
    analysis_data['D50'] = [D50_BD_alpha.value]*len(analysis_data)
    
if choice[3] == 0:
    if most_frequent_sampler == 'BD':
        velocity_BD_alpha = ask_float(text='Velocity for all samples', title='Estimate velocity at sampling point (m/s)', default_value='1')
        analysis_data['Velocity_sampling_point'] = [np.nan] *len(analysis_data)
        velocity_BD = [velocity_BD_alpha.value]*len(analysis_data[analysis_data['Sampler']=='BD'])
        analysis_data['Velocity_sampling_point'][analysis_data['Sampler'] == 'BD'] = velocity_BD 
        
    analysis_data['Sampling_duration_s'] = analysis_data['Filling_time_field']
    sand_flux_point = []
    concentration_sand_point = []
    concentration_fines = []
    
    for i in range(len(analysis_data)):
        meas = analysis_data.iloc[i,:]    
        if meas['Sampler'] == 'BD':           
            sand_flux_point_i = sediment_flux_BD(outpath_figures, meas, choice, unit)
            sand_flux_point.append(sand_flux_point_i)  
            concentration_sand_point.append(np.nan)            
        elif meas['Sampler'] == 'BD_C':
            sand_flux_point_i = sediment_flux_BD_C(outpath_figures, meas, choice, unit)
            sand_flux_point.append(sand_flux_point_i)   
            concentration_sand_point.append(np.nan)
            concentration_fines.append(np.nan)        
        elif meas['Sampler'] == 'P6' or meas['Sampler'] == 'P72' or meas['Sampler'] == 'Pump' or meas['Sampler'] == 'Niskin':
            concentration_sand_i, concentration_fines_i = calculate_concentration(meas, unit)
            sand_flux_point.append(np.nan)           
            concentration_sand_point.append(concentration_sand_i)
            concentration_fines.append(concentration_fines_i)
            #concentration_sand_point = [item for sublist in concentration_sand_point for item in sublist]
        elif meas['Sampler'] == 'S':
            concentration_sand_i, concentration_fines_i = calculate_concentration_S(meas, unit)
            sand_flux_point.append(np.nan)           
            concentration_sand_point.append(concentration_sand_i)
            concentration_fines.append(concentration_fines_i)
        else:
            sand_flux_point.append(np.nan)           
            concentration_sand_point.append(np.nan) 
            concentration_fines.append(np.nan)
    
    #concentration_sand_point = [item for sublist in concentration_sand_point for item in sublist]
    analysis_data['Sand_flux_point_kg_s_m2'] = sand_flux_point
    if np.count_nonzero(~np.isnan(np.array(concentration_sand_point))) >= 1:
        analysis_data['Concentration_sand_g_l'] = concentration_sand_point 
    if np.count_nonzero(~np.isnan(np.array(concentration_fines))) >= 1:
        analysis_data['Concentration_fine_g_l'] = [item for sublist in concentration_fines for item in sublist]
        analysis_data['Concentration_sand_g_l'] = [item for sublist in concentration_sand_point for item in sublist]
    
        # Calculate relative different of point fine concentrations
        analysis_data['Rel_diff_fines'] = [(analysis_data['Concentration_fine_g_l'][i]-
                                            np.mean(analysis_data['Concentration_fine_g_l']))/
                                           np.mean(analysis_data['Concentration_fine_g_l'])
                          for i in range(len(analysis_data))]
        analysis_data['Cfine_Csand'] = analysis_data['Concentration_fine_g_l']/analysis_data['Concentration_sand_g_l']
    
    # Plot concentration / flux and grain size data 
    Plot_point_data.plot_conc_flux_D50_B(analysis_data, outpath_figures, sampling_date, choice)
    

elif choice[3] == 1:
    sand_flux_point = []
    concentration_sand_point = []
    concentration_fines = []
    
    for i in range(len(analysis_data)):
        meas = analysis_data.iloc[i,:]    
        if meas['Sampler'] == 'BD':
            sand_flux_point_i, c_sand_point_g_l_from_flux_i = sediment_flux_BD(outpath_figures, meas, choice, unit)
            sand_flux_point.append(sand_flux_point_i)   
            concentration_sand_point.append(c_sand_point_g_l_from_flux_i) 
            concentration_fines.append(np.nan)
        elif meas['Sampler'] == 'BD_C':
            sand_flux_point_i, c_sand_point_g_l_from_flux_i = sediment_flux_BD_C(outpath_figures, meas, choice, unit)
            sand_flux_point.append(sand_flux_point_i)   
            concentration_sand_point.append(c_sand_point_g_l_from_flux_i)
            concentration_fines.append(np.nan)        
        elif meas['Sampler'] == 'P6' or meas['Sampler'] == 'P72' or meas['Sampler'] == 'Pump' or meas['Sampler'] == 'Niskin':
            concentration_sand_i, concentration_fines_i = calculate_concentration(meas, unit)
            sand_flux_point.append(np.nan)           
            concentration_sand_point.append(concentration_sand_i)
            concentration_fines.append(concentration_fines_i)
            #concentration_sand_point = [item for sublist in concentration_sand_point for item in sublist]
        elif meas['Sampler'] == 'S':
            concentration_sand_i, concentration_fines_i = calculate_concentration_S(meas, unit)
            sand_flux_point.append(np.nan)           
            concentration_sand_point.append(concentration_sand_i)
            concentration_fines.append(concentration_fines_i)
        else:
            sand_flux_point.append(np.nan)           
            concentration_sand_point.append(np.nan) 
            concentration_fines.append(np.nan)
    
    #concentration_sand_point = [item for sublist in concentration_sand_point for item in sublist]
    analysis_data['Sand_flux_point_kg_s_m2'] = sand_flux_point
    analysis_data['Concentration_sand_g_l'] = concentration_sand_point 
    if np.count_nonzero(~np.isnan(np.array(concentration_fines))) >= 1:
        analysis_data['Concentration_fine_g_l'] = [item for sublist in concentration_fines for item in sublist]
        analysis_data['Concentration_sand_g_l'] = [item for sublist in concentration_sand_point for item in sublist]
    
        # Calculate relative different of point fine concentrations
        analysis_data['Rel_diff_fines'] = [(analysis_data['Concentration_fine_g_l'][i]-
                                            np.mean(analysis_data['Concentration_fine_g_l']))/
                                           np.mean(analysis_data['Concentration_fine_g_l'])
                          for i in range(len(analysis_data))]
        analysis_data['Cfine_Csand'] = analysis_data['Concentration_fine_g_l']/analysis_data['Concentration_sand_g_l']
        analysis_data['Perc_sand'] = analysis_data['Concentration_sand_g_l']/(analysis_data['Concentration_sand_g_l']+
                                                     analysis_data['Concentration_fine_g_l'])*100
    # Plot concentration / flux and grain size data 
    Plot_point_data.plot_conc_flux_D50(analysis_data, outpath_figures, sampling_date, track, track_depth)


#%% Inter- / Extrapolation methods
if choice[3] == 1: # if ADCP data available
    # Nearest neighbor method
    analysis_data, summary_NN = nearest_neighbor_method(outpath_figures, analysis_data, choice, unit, sampling_date, Q_sampling, map_data)
    
    if 'Concentration_sand_g_l' in analysis_data.columns:
        if choice[5] == 0: # if no Q from hydrometric station available
            summary_segment = segment_method(analysis_data, outpath_figures, sampling_date, choice, Q_sampling = None)
        if choice[5] == 1: # if Q from hydrometric station available
            summary_segment = segment_method(analysis_data, outpath_figures, sampling_date, choice, Q_sampling)

    # ISO 4363 method
    if choice[2] == 1: # if GSD data available
        analysis_data, summary_ISO, ISO_classes, ISO_mean_gsd, ISO_mean_gsd_number = ISO_method(
            analysis_data, map_data, choice, sampling_date, outpath_figures, Q_sampling = None)
    if choice[2] == 0: # if no GSD data available
        analysis_data, summary_ISO = ISO_method(
            analysis_data, map_data, choice, sampling_date, outpath_figures, Q_sampling = None)

    # Rouse vertical profile + lateral interpolation
    analysis_data, summary_Rouse, Conc_Rouse_cell_export, sand_flux_Rouse_kg_s_export = SDC_Rouse_method(analysis_data, map_data, outpath, outpath_figures, sampling_date)
    
    # SDC method
    analysis_data, summary_SDC, Conc_SDC_cell_export, sand_flux_SDC_kg_s_export = SDC_method(analysis_data, map_data, outpath, outpath_figures, sampling_date)

    # Fine sediments
    if np.count_nonzero(~np.isnan(np.array(concentration_fines))) >= 1: 
        analysis_data, summary_fine, Conc_fine_cell_export, fine_flux_kg_s_export, ratio_cell = SDC_fines_method(
            analysis_data, map_data, outpath, outpath_figures, sampling_date)
                
        
if choice[3] == 0: # if no ADCP-data available
    # Nearest neighbor method 
    if choice[5] == 0: # if no Q from hydrometric station available
        analysis_data, summary_NN = nearest_neighbor_method(outpath_figures, analysis_data, choice, unit, sampling_date)
    if choice[5] == 1: # if Q from hydrometric station available
        analysis_data, summary_NN = nearest_neighbor_method(outpath_figures, analysis_data, choice, unit, sampling_date, Q_sampling)
   
    # Segment method
    if 'Concentration_sand_g_l' in analysis_data.columns:
        if choice[5] == 0: # if no Q from hydrometric station available
            summary_segment = segment_method(analysis_data, outpath_figures, sampling_date, choice, Q_sampling = None)
        if choice[5] == 1: # if Q from hydrometric station available
            summary_segment = segment_method(analysis_data, outpath_figures, sampling_date, choice, Q_sampling)

# Inter- and extrapolate GSD
if choice[2] == 1 and choice[3] == 1 and number_gsd >= 2:
    analysis_data, summary_GSD_SDC, D50_SDC_cell_export, GSD_SDC_cell_export = GSD_interpolation(
        analysis_data, map_data, outpath, outpath_figures, sampling_date)
if choice[2] == 1 and choice[3] == 0 and number_gsd >= 2: # if no ADCP-data available 
    # calculate mean D50 or GSD weighted by flux or C at point
    mean_gsd_weighted = calculate_mean_GSD(analysis_data, outpath, outpath_figures, sampling_date)

#%% Uncertainty estimation 
analysis_data, U_F, uncertainty = uncertainty_analysis(analysis_data, outpath, outpath_figures, sampling_date,
                      choice, v2_d_index, summary_ISO, summary_fine, map_data, cwd, path_R)

#%% Compare results from different methods
if choice[3] == 1:
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    methods_names = ['NN', 'ISO', 'Rouse', 'SDC']
    comparison_methods = pd.DataFrame([[summary_NN['NN_mean_sand_conc_g_l'][0], summary_ISO['mean_sand_conc_g_l'][0],
                         summary_Rouse['Conc_mean_Rouse'][0], summary_SDC['Conc_mean_SDC_g_l'][0]], [summary_NN['NN_total_sand_flux_kg_s'][0],
                        summary_ISO['total_sand_flux_kg_s'][0], summary_Rouse['total_sand_flux_Rouse_kg_s'][0], summary_SDC['total_sand_flux_SDC_kg_s'][0]]]).transpose()
    comparison_methods.index = methods_names
    comparison_methods.columns = ['Mean_sand_conc_g_l', 'total_sand_flux_kg_s']
     
    fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
    
    ax2 = ax.twinx()
    width = 0.4
    
    comparison_methods.Mean_sand_conc_g_l.plot(kind='bar', color='sienna', edgecolor = 'black',
                        ax=ax, width=width, position=1, rot= 0 )
    comparison_methods.total_sand_flux_kg_s.plot(kind='bar', color='darkorange', edgecolor = 'black',
                        ax=ax2, width=width, position=0, rot= 0 )
    
    ax.set_xlim(-0.5,3.5)
    ax.set_ylabel('$\mathregular{\overline{C}_{sand}}$ (g/l)', fontsize = 14, color = 'sienna', weight = 'bold')
    ax2.set_ylabel('$\mathregular{\Sigma \Phi_{sand}}$ (kg/s)', fontsize = 14, color = 'darkorange', weight = 'bold')
    ax.set_xlabel('Method', fontsize = 14, weight = 'bold')
    ax.grid(linewidth = 0.2)
    ax.tick_params(axis='both', which='major', labelsize = 12) 
    fig.tight_layout()
    figname = '_Mean_concentration_total_flux_methods_'
    fig.savefig(outpath_figures + str(sampling_date) + figname + most_frequent_sampler + '.png', dpi = 200, bbox_inches='tight')
    
#%% Determine concentration, flux and GSD along HADCP beam
if HADCP_option == True:
    if choice[3] == 1 and stage_sampling is not None:
        if most_frequent_sampler != 'BD':
            HADCP_export = HADCP_function(analysis_data, map_data, stage_sampling, outpath, sampling_date, 
                                      Conc_SDC_cell_export, sand_flux_SDC_kg_s_export, Conc_fine_cell_export, 
                                      fine_flux_kg_s_export, ratio_cell, GSD_SDC_cell_export, D50_SDC_cell_export)
   
        if most_frequent_sampler == 'BD':
            HADCP_export_BD = HADCP_function_BD(analysis_data, map_data, stage_sampling, outpath, sampling_date, 
                              Conc_SDC_cell_export, sand_flux_SDC_kg_s_export, 
                              GSD_SDC_cell_export, D50_SDC_cell_export)
    
#%%  Export results and data
# analysis_data.to_csv(r'C:\Users\jessica.laible\Documents\Analyses\Flux_concentration\Error_analysis\BD_orientation\\' + 
#                      sampling_date + '_Analysis_data_diver_RG.csv', sep = ';')

most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
analysis_data.to_csv(outpath + sampling_date + '_Analysis_data_' + most_frequent_sampler +'.csv', sep = ';')
if choice[3] == 1:
    comparison_methods.to_csv(outpath + sampling_date + '_Comparison_methods_' + most_frequent_sampler +'.csv', sep = ';')
#discharge.to_csv(outpath + sampling_date + '_Discharge_' + most_frequent_sampler +'.csv', sep = ';')
         
test = Export_data(analysis_data, outpath, stage_sampling, spm_sampling, Q_sampling,summary_NN,summary_ISO, 
             summary_Rouse, summary_SDC, summary_segment, summary_fine, summary_GSD_SDC, Conc_SDC_cell_export, 
             sand_flux_SDC_kg_s_export, Conc_Rouse_cell_export, sand_flux_Rouse_kg_s_export, ISO_classes, ISO_mean_gsd, ISO_mean_gsd_number, 
             Conc_fine_cell_export, fine_flux_kg_s_export, ratio_cell, D50_SDC_cell_export, GSD_SDC_cell_export, 
             mean_gsd_weighted,u_point_sand, u_point_fines, comparison_methods, HADCP_export, HADCP_export_BD, uncertainty)
test.save_data(sampling_date, most_frequent_sampler)            
            
# path_folder = r'C:\Users\jessica.laible\Documents\Mesures\Manips&Sites\Grenoble_Campus\20210511_Campus'
# with open(path_folder + '\\' +'20210511_Analysis_solid_gauging_P72.txt', "rb") as fp:   
#     export_data = pickle.load(fp)            

