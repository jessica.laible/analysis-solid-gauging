"""
Calculates grain size distribution statistics and 
relates the grain size measurements with samples in analysis_data

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import pandas as pd
from scipy.stats import skew
from scipy.stats import kurtosis
from itertools import accumulate
from datetime import date, timedelta, datetime, time

def stats_granulo(analysis_data, path_grainsize):

    #%% Load grain size data
    grain_size_data = pd.read_csv(path_grainsize, sep=';')
    
    # Use only mean results    
    grain_size_mean = grain_size_data.iloc[3::4, :]
    grain_size_mean.reset_index(drop=True, inplace=True) #*
      
    # Sort data after ascending abscissa and depth
    grain_size_mean = grain_size_mean.sort_values(
        by=['Abscissa', 'Depth', 'Height'], ascending=True)
    grain_size_mean.reset_index(drop=True, inplace=True)
      
    # Find double grain size measurements on the same sample and average them
    grain_size_mean.drop(['Sample','Name', 'Series'], axis=1, inplace=True)
    grain_size_mean = grain_size_mean.groupby(['Sampler','Time']).mean().reset_index()

    # Get size classes 
    colnames = grain_size_mean.columns.values
    index_num = []
    for name in colnames:
        index_num.append(not any([c.isalpha() for c in name]))
    size_classes = colnames[index_num]
    size_classes = [float(i) for i in size_classes]
    
    # Size classes for cumulated and classified distribution
    size_classes_cumsum = size_classes[1:]
    size_lower = np.array(size_classes[0:-1])* 1e-6 / 2
    size_upper = np.array(size_classes[1:])* 1e-6 / 2
    size_classes_classified = [10**((np.log10(size_lower[i]) + np.log10(size_upper[i]))/2) 
                         for i in range(len(size_lower))]
    size_classes_classified= np.array(size_classes_classified)
    
    # Cumulative distribution
    cumsum = [np.nancumsum(grain_size_mean.iloc[i,np.where(index_num)[0][0]:np.where(index_num)[0][-1]]) 
              for i in range(len(grain_size_mean))]
    cumsum = pd.DataFrame(cumsum)
    
    # Convert to phi
    size_classes_cumsum_phi = [- np.log2(size_classes_cumsum[i]/1000)
                               for i in range(len(size_classes_cumsum))]
    
    # Calculate indices
    grain_size_mean['D5'] = [np.interp(5, cumsum.iloc[i,:], size_classes_cumsum)
           for i in range(len(cumsum))]
    grain_size_mean['D16'] = [np.interp(16, cumsum.iloc[i,:], size_classes_cumsum)
           for i in range(len(cumsum))]
    grain_size_mean['D25'] = [np.interp(25, cumsum.iloc[i,:], size_classes_cumsum)
           for i in range(len(cumsum))]
    grain_size_mean['D75'] = [np.interp(75, cumsum.iloc[i,:], size_classes_cumsum)
           for i in range(len(cumsum))]
    grain_size_mean['D84'] = [np.interp(84, cumsum.iloc[i,:], size_classes_cumsum)
           for i in range(len(cumsum))]
    grain_size_mean['D95'] = [np.interp(95, cumsum.iloc[i,:], size_classes_cumsum)
           for i in range(len(cumsum))]
    
    D5_phi = [np.interp(5, cumsum.iloc[i,:], size_classes_cumsum_phi)
           for i in range(len(cumsum))]
    D16_phi = [np.interp(16, cumsum.iloc[i,:], size_classes_cumsum_phi)
           for i in range(len(cumsum))]
    D25_phi = [np.interp(25, cumsum.iloc[i,:], size_classes_cumsum_phi)
           for i in range(len(cumsum))]
    D50_phi = [np.interp(50, cumsum.iloc[i,:], size_classes_cumsum_phi)
           for i in range(len(cumsum))]
    D75_phi = [np.interp(75, cumsum.iloc[i,:], size_classes_cumsum_phi)
           for i in range(len(cumsum))]
    D84_phi = [np.interp(84, cumsum.iloc[i,:], size_classes_cumsum_phi)
           for i in range(len(cumsum))]
    D95_phi = [np.interp(95, cumsum.iloc[i,:], size_classes_cumsum_phi)
           for i in range(len(cumsum))]
    
    # #%% 
    # # Calculate Folk and Ward statistics (1957)
    # sorting = np.round(np.sqrt(D25/D75),3)
    grain_size_mean['Mean_grain_size'] = [(D16_phi[i] + 
                        D50_phi[i] + D84_phi[i])/3
                       for i in range(len(D16_phi))]
    grain_size_mean['Sorting'] = [(D16_phi[i] + D84_phi[i])/4 + 
               (D95_phi[i] - D5_phi[i])/6.6
                       for i in range(len(D16_phi))]
    grain_size_mean['Skewness'] = [(D16_phi[i] + D84_phi[i]-
                                    2*D50_phi[i])/
                                   (2*D16_phi[i] + D84_phi[i]) + 
                                   (D5_phi[i] + D95_phi[i]-
                                               2*D50_phi[i])/
                                              (2*D5_phi[i] + D95_phi[i])
                       for i in range(len(D16_phi))]
    grain_size_mean['Kurtosis'] = [(D95_phi[i] - D5_phi[i])/ 
                                   (2.44*(D75_phi[i] - D25_phi[i]))
                       for i in range(len(D16_phi))]
        
    sorting_classification = []
    for i in range(len(grain_size_mean)):
        if grain_size_mean['Sorting'][i] < 0.35:
            sorting_classification.append('very well sorted')
        elif grain_size_mean['Sorting'][i] >= 0.35 and grain_size_mean['Sorting'][i] < 0.5:
            sorting_classification.append(' well sorted')
        elif grain_size_mean['Sorting'][i] >= 0.5 and grain_size_mean['Sorting'][i] < 0.71:
            sorting_classification.append('moderately well sorted')
        elif grain_size_mean['Sorting'][i] >= 0.71 and grain_size_mean['Sorting'][i] < 1:
            sorting_classification.append('moderately sorted')
        elif grain_size_mean['Sorting'][i] >= 1 and grain_size_mean['Sorting'][i] < 2:
            sorting_classification.append('poorly sorted')
        elif grain_size_mean['Sorting'][i] >= 2 and grain_size_mean['Sorting'][i] < 4:
            sorting_classification.append('very poorly sorted')
        elif grain_size_mean['Sorting'][i] >= 4:
            sorting_classification.append('extremely poorly sorted')
  
    grain_size_mean['Sorting_classification']  = sorting_classification 
    

    # Convert time for comparison
    if len(grain_size_mean['Time'][0]) == 8:
        Time_datetime_gsd = [datetime.strptime(grain_size_mean['Time'][i],'%H:%M:%S').time()
                     for i in range(len(grain_size_mean))]
    if len(grain_size_mean['Time'][0]) == 5:
        Time_datetime_gsd = [datetime.strptime(grain_size_mean['Time'][i],'%H:%M').time()
                     for i in range(len(grain_size_mean))]
    if len(grain_size_mean['Time'][0]) == 4:
         Time_datetime_gsd = [datetime.strptime(grain_size_mean['Time'][i],'%H:%M').time()
                      for i in range(len(grain_size_mean))]
    if len(analysis_data['Time'][0]) == 8:
        Time_datetime_analysis_data = [datetime.strptime(analysis_data['Time'][i],'%H:%M:%S').time()
                     for i in range(len(analysis_data))]
    if len(analysis_data['Time'][0]) == 5:
        Time_datetime_analysis_data = [datetime.strptime(analysis_data['Time'][i],'%H:%M').time()
                     for i in range(len(analysis_data))]
    if len(analysis_data['Time'][0]) == 4:
         Time_datetime_analysis_data = [datetime.strptime(analysis_data['Time'][i],'%H:%M').time()
                      for i in range(len(analysis_data))]
    
    #%% Relate grain size measurements with analysis_data #*add == with sampler (deleted by grouby.mean in grain_size_mean)
    m = 0
    grain_size_dis = []
    for k in range(len(grain_size_mean)):
        for i in range(len(analysis_data)):
            if grain_size_mean['Sampler'][k] == analysis_data['Sampler'][i] and grain_size_mean['Depth'][k] == \
                analysis_data['Depth_field'][i] and grain_size_mean['Abscissa'][k] == \
                    analysis_data['Abscissa'][i] and Time_datetime_gsd[k] == Time_datetime_analysis_data[i]:
                grain_size_dis.append(grain_size_mean.iloc[k])
                grain_size_dis[m]['ID'] = i
                m = m + 1
            elif grain_size_mean['Sampler'][k] == analysis_data['Sampler'][i] and grain_size_mean['Height'][k] == \
                analysis_data['Height_field'][i] and grain_size_mean['Abscissa'][k] == \
                    analysis_data['Abscissa'][i] and Time_datetime_gsd[k] == Time_datetime_analysis_data[i]:
                grain_size_dis.append(grain_size_mean.iloc[k])
                grain_size_dis[m]['ID'] = i
                m = m + 1
    grain_size_df = pd.DataFrame(grain_size_dis)
    # Warning: A value is trying to be set on a copy of a slice from a DataFrame (two times)
               
    # Management of data
    grain_size_df.set_index('ID', inplace=True)
    loc_D10 = grain_size_df.columns.get_loc('D10')
    grain_size_df.drop(grain_size_df.iloc[:, 0:loc_D10], axis=1, inplace=True)
    analysis_data = pd.concat([analysis_data, grain_size_df], axis=1)
    
    number_gsd = len(grain_size_df)
    #%%
    return (analysis_data, cumsum, size_classes_cumsum, size_classes_classified, number_gsd)












