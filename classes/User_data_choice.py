"""
Choose data used for analysis

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import tkinter as tk
from tkinter import messagebox

class user_data_choice():
    def __init__(self):
        self.choice_list = []
        self.root = tk.Tk()
        self.root.geometry('250x300')
        self.root.title('Choose your data :')
        self.root.wm_attributes("-topmost", 1)
        self.root.bind("<Return>", self.input_choice)
        self.root.bind("<Control-Return>", self.select_all)

        data = ('Prep_data', 'Diver', 'Grainsize', 'ADCP-folder',
                        'Water discharge', 'Water stage', 'Concentration')
        
        self.choice1 = tk.IntVar()
        self.choice2 = tk.IntVar()
        self.choice3 = tk.IntVar()
        self.choice4 = tk.IntVar()
        self.choice5 = tk.IntVar()
        self.choice6 = tk.IntVar()
        self.choice7 = tk.IntVar()
     
        Checkbutton_1 = tk.Checkbutton(self.root, text=data[0], variable=self.choice1)
        Checkbutton_1.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        Checkbutton_1.select()

        Checkbutton_2 = tk.Checkbutton(self.root, text=data[1], variable=self.choice2)
        Checkbutton_2.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        Checkbutton_3 = tk.Checkbutton(self.root, text=data[2], variable=self.choice3)
        Checkbutton_3.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        Checkbutton_4 = tk.Checkbutton(self.root, text=data[3], variable=self.choice4)
        Checkbutton_4.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        Checkbutton_5 = tk.Checkbutton(self.root, text=data[4], variable=self.choice5)
        Checkbutton_5.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        Checkbutton_6 = tk.Checkbutton(self.root, text=data[5], variable=self.choice6)
        Checkbutton_6.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
        Checkbutton_7 = tk.Checkbutton(self.root, text=data[6], variable=self.choice7)
        Checkbutton_7.pack(side=tk.TOP, anchor=tk.W, ipadx=15)
    

        self.button = tk.Button(self.root, text='Ok [Return]', command=self.input_choice, padx=50, pady=10)
        self.button.pack()

        self.button_all = tk.Button(self.root, text='Check all [Ctrl+Return]', command=self.select_all, padx=20, pady=10)
        self.button_all.pack()

        self.root.mainloop()

    def input_choice(self, event=None):
        self.choice_list = [self.choice1.get(), self.choice2.get(), self.choice3.get(), self.choice4.get(), 
                            self.choice5.get(), self.choice6.get(), self.choice7.get()]
        self.root.destroy()
        print(self.choice_list)

    def select_all(self, event=None):
        self.choice_list = [1, 1, 1, 1, 1, 1, 1]
        self.root.destroy()
        print(self.choice_list)

# test = user_data_choice()

class ask_float():
    def __init__(self, text: str = '', title: str = '', default_value:str=''):
        self.value = None
        self.ws = tk.Tk()
        self.ws.title(title)
        self.ws.geometry("250x100")
        self.ws.wm_attributes("-topmost", 1)
        self.ws.bind("<Return>", self.generate)
        self.num1 = tk.StringVar(value=default_value)
        self.label = tk.Label(self.ws, text=text)
        self.label.pack()
        self.entry1 = tk.Entry(self.ws, textvariable=self.num1)
        self.entry1.pack()
        self.button = tk.Button(self.ws, text='Ok [Return]', command=self.generate, padx=50, pady=10)
        self.button.pack()
        self.ws.mainloop()

    def generate(self, event=None):
        try:
            result = float(self.num1.get())
            self.value = result
            self.ws.destroy()
            print(self.value)
        except Exception as ex:
            messagebox.showwarning('warning', ex)

# test = ask_float(text='D50 for all samples', title='D50 user', default_value='100')
# test.value