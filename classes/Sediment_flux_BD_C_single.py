"""
Determines solid flux per point for Delft bottle samples depending on the nozzle used 
and based on point D50 and velocity, if available, otherwise based on user inputs. 
Converts to point concentration if ADCP-data (MAP) are available. 
Only for deployment without the structure e.g. on a cableway.

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import numpy as np
import pandas as pd
from Sed_flux_small_straight import sed_flux_small_straight
from Sed_flux_small_bend import sed_flux_small_bend
from Sed_flux_big_straight import sed_flux_big_straight
from Sed_flux_big_bend import sed_flux_big_bend
from Functions import find_nearest, get_sec

def sediment_flux_BD_C(outpath_figures, meas, choice, unit) :

    # %%################### Calculate sediment flux ###########################
    unit_mass = unit.unit_masses
    unit_vol = unit.unit_volume
    
    BD_grain_size_steps = [75, 90, 100, 110, 130, 150, 210]

    # Create Alpha graphs
    sed_flux_small_straight(outpath_figures)
    alpha_small_straight = sed_flux_small_straight.alpha_small_straight

    sed_flux_small_bend(outpath_figures)
    alpha_small_bend = sed_flux_small_bend.alpha_small_bend

    sed_flux_big_straight(outpath_figures)
    alpha_big_straight = sed_flux_big_straight.alpha_big_straight

    sed_flux_big_bend(outpath_figures)
    alpha_big_bend = sed_flux_big_bend.alpha_big_bend

    # %%################### Determine alpha and nozzle surface for samples #########

    # Define measurement range by reel
    xy = np.round(np.arange(0.001, 4.001, 0.001), 3)
    vel_possiblerange = np.round(np.arange(0.01, 4.01, 0.01), 3)
    velocity_point_2digits = np.round(meas['Velocity_sampling_point'], 2)

    # Find nearest grain size
    nearest_grain_size = BD_grain_size_steps.index(find_nearest(BD_grain_size_steps, meas['D50']))
    vel_ind = np.where(vel_possiblerange == velocity_point_2digits)
    
    
    alpha = alpha_big_bend[nearest_grain_size][vel_ind]
  

    # Determine the used nozzle (small or big)
    if meas['Small_nozzle'] == 1:
        alpha = alpha_small_straight[nearest_grain_size][vel_ind]
    if meas['Small_nozzle'] == 0:
        alpha = alpha_big_straight[nearest_grain_size][vel_ind]

    alpha_l = [l.tolist() for l in alpha]
    alpha_list = [item for sublist in alpha_l for item in sublist]

    if meas['Small_nozzle'] == 1:
         S = 0.000188692
    if meas['Small_nozzle'] == 0:
         S = 0.000380133
         
    if pd.isna(meas['Sampling_duration_s']):
        meas['Sampling_duration_s'] = meas['Filling_time_field'] 
    # %%################### Calculate point solid flux #######################
    if unit_mass == 'mg':
        sand_flux_point_i = alpha * meas['Dry_mass_sand'] * \
                  0.001 * 0.001 / (S * meas['Sampling_duration_s'])   
        sand_flux_point_i = (np.float(sand_flux_point_i))
    if unit_mass == 'g':
        sand_flux_point_i = alpha * meas['Dry_mass_sand'] * \
                  0.001 / (S * meas['Sampling_duration_s'])
        sand_flux_point_i = (np.float(sand_flux_point_i))
    
    # %%####### Convert point solid flux to concentration #####################
    if choice[3] == 1:
        if unit_mass == 'mg':
            c_sand_point_g_l_from_flux_i = (meas['Dry_mass_sand']*0.001*0.001)/(meas['Velocity_sampling_point']*S*meas['Filling_time_field'])
        if unit_mass == 'g':
            c_sand_point_g_l_from_flux_i = (meas['Dry_mass_sand']*0.001)/(meas['Velocity_sampling_point']*S*meas['Filling_time_field'])

    
    return (sand_flux_point_i, c_sand_point_g_l_from_flux_i)
