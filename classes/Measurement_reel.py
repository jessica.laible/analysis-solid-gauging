"""
The purpose of this scrit is to calculate the speed taken by mill, particulary when no ADCP datas

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import os
import numpy as np
import pandas as pd


def measurement_reel(outpath, number_outpath, analysis_data) :

    # Test if reel datas exist
    if os.path.isfile(outpath[:-1] + 'feuille_moulin_vitesse_' + number_outpath + '.csv'):
        reel_speed_data = pd.read_csv(outpath[:-1] + 'feuille_moulin_vitesse_' + number_outpath + '.csv', sep=';')

        # Datas management
        reel_speed_data = reel_speed_data[reel_speed_data['Abscisse'].notna()]
        reel_speed_data['Hauteur au dessus du sol (m)'] = reel_speed_data['Hauteur au dessus du sol (m)'].fillna('0')
        reel_speed_data['Vitesse (m/s)'] = reel_speed_data['Vitesse (m/s)'].fillna('0')
        reel_speed_data['Profondeur de la surface (m)'] = reel_speed_data['Profondeur de la surface (m)'].fillna('0')

        for i in range(len(reel_speed_data['Hauteur au dessus du sol (m)'])) :
            reel_speed_data['Hauteur au dessus du sol (m)'][i] = reel_speed_data['Hauteur au dessus du sol (m)'][i].replace(',', '.')
            reel_speed_data['Vitesse (m/s)'][i] = reel_speed_data['Vitesse (m/s)'][i].replace(',', '.')
            reel_speed_data['Profondeur de la surface (m)'][i] = reel_speed_data['Profondeur de la surface (m)'][i].replace(',', '.')

        reel_speed_data['Vitesse (m/s)'] = reel_speed_data['Vitesse (m/s)'].astype(float)
        reel_speed_data['Hauteur au dessus du sol (m)'] = reel_speed_data['Hauteur au dessus du sol (m)'].astype(float)
        reel_speed_data['Profondeur de la surface (m)'] = reel_speed_data['Profondeur de la surface (m)'].astype(float)

        # Speed of reel
        reel_speed = []
        for k in range(len(analysis_data)) :
            for i in range(len(reel_speed_data)) :
                if reel_speed_data['Abscisse'][i] == analysis_data['Abscissa'][k] and reel_speed_data['Hauteur au dessus du sol (m)'][i] == analysis_data['Height_field'][k] :
                    reel_speed.append(reel_speed_data['Vitesse (m/s)'][i])
                elif reel_speed_data['Abscisse'][i] == analysis_data['Abscissa'][k] and reel_speed_data['Profondeur de la surface (m)'][i] == analysis_data['Depth_field'][k] :
                    reel_speed.append(reel_speed_data['Vitesse (m/s)'][i])

        return (reel_speed)