"""
a) calculate_mean_GSD: Calculates mean grain size distribution weighted by concentration 
or flux per point ; applied if no ADCP-data are available 

b) GSD_interpolation: Determines the grain size distribution and D50 for each MAP-cell, 
inter- and extrapolation are based on the SDC-method

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from UI.MplCanvas import MplCanvas
from scipy import stats
from Common_functions import show_figure


def calculate_mean_GSD(analysis_data, outpath, outpath_figures, sampling_date):
    #%%
    analysis_data_gsd = analysis_data.dropna(subset=['D10'])
    analysis_data_gsd.reset_index(drop=True, inplace=True)
    
    abscissa_values_gsd = analysis_data_gsd['Abscissa'].unique().tolist()    
    analysis_data_gsd.insert(2, 'Abscissa_Name_gsd', [abscissa_values_gsd.index(i) + 1
                                              for i in analysis_data_gsd['Abscissa']])
    
    colnames = analysis_data_gsd.columns.values
    index_num = []
    for name in colnames:
        index_num.append(not any([c.isalpha() for c in name]))
    size_classes = colnames[index_num]
    size_classes = [float(i) for i in size_classes]
    
    # Size classes for cumulated and classified distribution
    size_classes_cumsum = size_classes[1:]
    size_lower = np.array(size_classes[0:-1])* 1e-6
    size_upper = np.array(size_classes[1:])* 1e-6
    size_classes_classified = [10**((np.log10(size_lower[i]) + np.log10(size_upper[i]))/2) 
                         for i in range(len(size_lower))]
    size_classes_classified= np.array(size_classes_classified)
    
    # Cumulative distribution
    cumsum = [np.nancumsum(analysis_data_gsd.iloc[i,np.where(index_num)[0][0]:np.where(index_num)[0][-1]]) 
              for i in range(len(analysis_data_gsd))]
    cumsum = pd.DataFrame(cumsum)
        
    # Calculate mean grain size distribution weighted per point sediment concentration or flux
    if 'Sand_flux_point_kg_s_m2' in analysis_data_gsd.columns:
        weight = 'Flux'
        mean_gsd_weighted = np.nansum([cumsum.iloc[i,:]*analysis_data_gsd['Sand_flux_point_kg_s_m2'][i] 
                       for i in range(len(analysis_data_gsd))], axis = 0)/np.nansum(
                               analysis_data_gsd['Sand_flux_point_kg_s_m2'])
       
    if 'Concentration_sand_g_l' in analysis_data_gsd.columns:
        weight = 'C'
        mean_gsd_weighted = np.nansum([cumsum.iloc[i,:]*analysis_data_gsd['Concentration_sand_g_l'][i] 
                      for i in range(len(analysis_data_gsd))], axis = 0)/np.nansum(
                              analysis_data_gsd['Concentration_sand_g_l'])
                             
                              
    # # Get grain size indices
    # d10 = np.interp(10, mean_gsd_weighted, size_classes_cumsum)
    # d50 = np.interp(50, mean_gsd_weighted, size_classes_cumsum)
    # d90 = np.interp(90, mean_gsd_weighted, size_classes_cumsum)
          
    # xlims
    xlim_left = next(x-2 for x, val in enumerate(mean_gsd_weighted) if val > 2)
    if xlim_left < 0:
        xlim_left = 0
    xlim_right = next(x+4 for x, val in enumerate(mean_gsd_weighted) if val > 99.9)
    if xlim_right >= len(size_classes_cumsum):
        xlim_right = len(size_classes_cumsum)-1        
    
    mean_gsd_weighted = pd.concat([pd.DataFrame(size_classes_cumsum), pd.DataFrame(mean_gsd_weighted)], axis = 1)
    mean_gsd_weighted.columns = ['Sizes', 'Cumulative_fraction']
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 12
    fontsize_legend_title = 14
    fontsize_ticks = 12 
        
    #%% Plot mean grain size distribution 
    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)
                          
    ax.plot(size_classes_cumsum, mean_gsd_weighted.iloc[:,1], '-', lw = 2, color = 'darkred', 
            zorder = 30, label = 'cross-sectional mean GSD')
    for i in range(len(cumsum)):
        ax.plot(size_classes_cumsum, cumsum.iloc[i,:], '-', lw = 1, color = 'lightgrey', zorder = 1)
    ax.plot(size_classes_cumsum, cumsum.iloc[0,:], '-', lw = 1, color = 'lightgrey', 
            zorder = 1, label = 'GSD per sample')
    
    ax.set_ylim(-2, 102)
    ax.set_xlim(size_classes_cumsum[xlim_left], size_classes_cumsum[xlim_right])
    legend = plt.legend(fontsize=fontsize_legend, loc = 'lower right')
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    ax.grid(linewidth = 0.2)
    ax.grid(which = 'minor', axis = 'both', linewidth = 0.1)
    ax.set_xscale('log')
    ax.set_xlabel('Particle diameter (µm)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Cumulative fraction (%)', fontsize=fontsize_axis, weight = 'bold')
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    
    fig.tight_layout()
    figname = sampling_date+ '_Mean_GSD_cross-section_weighted_by_' + weight + '_' + str(analysis_data['Sampler'][0])
    fig.savefig(outpath_figures + figname + '.png', dpi=300, bbox_inches='tight')
     
  #%%
    return mean_gsd_weighted

#%% 

def GSD_interpolation(analysis_data, map_data, outpath, outpath_figures, sampling_date): 
    #%%
    analysis_data_gsd = analysis_data.dropna(subset=['D10'])
    analysis_data_gsd.reset_index(drop=True, inplace=True)
    
    abscissa_values_gsd = analysis_data_gsd['Abscissa'].unique().tolist()    
    analysis_data_gsd.insert(2, 'Abscissa_Name_gsd', [abscissa_values_gsd.index(i) + 1
                                              for i in analysis_data_gsd['Abscissa']])
 
    abscissa_values = (analysis_data_gsd['Abscissa']).unique().tolist()
    depth_verticals = analysis_data_gsd['max_depth_vertical'].unique()
   
    # Discharge
    discharge = pd.concat([pd.DataFrame(map_data.left_cells_discharge), pd.DataFrame(map_data.middle_cells_discharge),
                           pd.DataFrame(map_data.right_cells_discharge)], axis = 1)
    discharge.columns = np.arange(0,len(discharge.columns),1)
    
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S']
    forms_sampler = ['o', 'o','D', 's', 'P', '*']  
    size_sampler = [6, 6, 6, 6, 7, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                     'darkorange', 'darkred']
    samplers_used = np.unique(analysis_data_gsd['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    id_sampler_used = [sampler.index(analysis_data_gsd['Sampler'][i])
          for i in range(len(analysis_data_gsd))]
    most_frequent_sampler= analysis_data_gsd['Sampler'].value_counts().idxmax()
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 14
    fontsize_legend_title = 14
    fontsize_text = 14
    fontsize_ticks = 12
    
    #%% SDC profile     
    stats_D50_SDC_profile = []
    x_ranges_vertical = []
    
    for i in range(len(abscissa_values)):
        x = np.array(analysis_data_gsd['z_h'][analysis_data_gsd['Abscissa'] == abscissa_values[i]])
        Y = np.log(np.array(analysis_data_gsd['D50'][analysis_data_gsd['Abscissa'] == abscissa_values[i]]))
        array_x_vertical = np.linspace(0,1, num=100)
        x_ranges_vertical.append(array_x_vertical)
        
        res = stats.linregress(x, Y)
        # correct slope (increasing C with depth)
        res = list(res)
        if res[0] >= 0:
            res[0] = -0.2
            res[1] = np.max(Y)
            res[2] = np.nan
            print('Slope and intercept of the D50 profile corrected for vertical i = ' + str(i))
        stats_D50_SDC_profile.append(res)
        
    x_ranges_vertical = pd.DataFrame(x_ranges_vertical) 
    stats_D50_SDC_profile = pd.DataFrame(stats_D50_SDC_profile)
    stats_D50_SDC_profile.columns = ['slope', 'intercept','rvalue', 'pvalue', 'stderr']
    stats_D50_SDC_profile['Cr_vertical'] = np.exp(stats_D50_SDC_profile['intercept'])
    stats_D50_SDC_profile['Crh_vertical'] = stats_D50_SDC_profile['Cr_vertical']/depth_verticals
    stats_D50_SDC_profile['alphah_vertical'] = -(stats_D50_SDC_profile['slope'])
    stats_D50_SDC_profile['alpha_vertical'] = stats_D50_SDC_profile['alphah_vertical']/depth_verticals
    stats_D50_SDC_profile['R2'] = stats_D50_SDC_profile['rvalue']**2

    D50_vertical_profile = pd.DataFrame([np.exp(stats_D50_SDC_profile['intercept'][i])*np.exp(stats_D50_SDC_profile['slope'][i]*x_ranges_vertical.iloc[i,:])
             for i in range(len(stats_D50_SDC_profile))])
 
           
#%% Plot all vertical profiles in one graph
    markerss = ['o', 's', 'D', 'p', 'X', 'P', 'v', '<', '*']
    cmap = plt.cm.get_cmap('nipy_spectral')
    colors = cmap(np.linspace(0,1,len(abscissa_values)))
    
    fig, ax = plt.subplots(figsize=(10, 6), dpi = 100)
    
    for i in range(len(abscissa_values)):
        ax.plot(D50_vertical_profile.iloc[i,:], x_ranges_vertical.iloc[i,:],
                linestyle = '-', linewidth = 2, color = colors[i],
                label = str(abscissa_values[i]))
           
        ax.plot(analysis_data_gsd['D50'][analysis_data_gsd['Abscissa']== abscissa_values[i]], 
                   analysis_data_gsd['z_h'][analysis_data_gsd['Abscissa']== abscissa_values[i]],
                   color = colors[i], linestyle = ' ',
                   markersize = size_sampler[id_sampler_used[i]]*1.5, marker = markerss[i],
                   markeredgewidth = 0.2, markeredgecolor = 'black')
    
    legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa', 
              loc = 'upper right',  facecolor = 'white', framealpha = 1, bbox_to_anchor = (1.17, 1))
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    
    ax.set_ylim(-0.05,1.05)
    ax.set_xlim(np.min(analysis_data_gsd['D50']) - 20, np.max(analysis_data_gsd['D50']) + 20)
    ax.set_ylabel('z/h (-)', fontsize = fontsize_axis, weight = 'bold')
    ax.set_xlabel('$\mathregular{D_{50}\; (\mu m)}$', fontsize = fontsize_axis, weight = 'bold')
    ax.grid(linewidth = 0.2)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    fig.tight_layout()
    figname = '_D50_profiles_' + most_frequent_sampler
    fig.savefig(outpath_figures + sampling_date + figname + '.png', dpi = 200, bbox_inches='tight')
           
#%% Define MAP grid
    cell_height = map_data.depth_cells_border[1][0]-map_data.depth_cells_border[0][0]
    depth_cells = np.arange(map_data.depth_cells_border[0][0], (len(map_data.depth_cells_border)*cell_height), cell_height)
   
    # Calculate cell properties
    cell_height = map_data.border_depth[1] - map_data.border_depth[0]
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
    depth_cells = map_data.border_depth
    
    # Length and depth of transect (center of each distance cell, plus start (0) and end)
    # TODO shouldnt add 0 at the end/start : track = mid distance
    (map_data.depths - map_data.depth_cells_center).shape
    map_data.left_vertical_depth
    # Center of cell + max edges
    track = (border_cells[1:] + border_cells[:-1]) / 2
    # track = np.append(np.insert(track, 0, 0), border_cells[-1])

    # Depth on middle of the cell
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    # track_depth = np.append(np.insert(track_depth, 0, 0), 0)

    # Create height above bottom grid
    # TODO too short : not representative of depth (length should be 9)
    height_midpoints_cells = []
    for i in range(len(track_depth)):
        heh = int(track_depth[i]//cell_height)
        height_last_cell = (track_depth[i]-depth_cells[heh])
        midpoint_last_cell = np.array([height_last_cell/2])   # use half-distance between bottom and bottom of lowest complete cell for bottom cell
        midpoint_height_cells = np.arange(0, heh)*cell_height + (height_last_cell + cell_height/2)
        height_midpoints_cells1 = np.concatenate([midpoint_last_cell, midpoint_height_cells], axis=0)
        height_midpoints_cells1 = np.flip(height_midpoints_cells1)
        height_midpoints_cells.append(height_midpoints_cells1)
       
    height_midpoints_cells = pd.DataFrame(height_midpoints_cells).transpose()
    
    depth_midpoints_cells = [np.round((depth_cells[i+1]+depth_cells[i])/2, 3)
                             for i in range(len(depth_cells)-1)]        
       
    #%% Determine slope and intercept for the whole transect
    
    # Interpolate between sampling abscissas
    id_start_middle_track = next(x-1 for x, val in enumerate(track) if val > abscissa_values[0])
    id_end_middle_track = next(x+1 for x, val in enumerate(track) if val > abscissa_values[-1])
    track_middle = track[id_start_middle_track:id_end_middle_track]
    
    Crh_interp = [np.interp(track_middle[i], abscissa_values, stats_D50_SDC_profile['Crh_vertical'])
            for i in range(len(track_middle))]
    alpha_interp = [np.interp(track_middle[i], abscissa_values, stats_D50_SDC_profile['alpha_vertical'])
            for i in range(len(track_middle))]  
   
    Cr_middle = Crh_interp*track_depth[id_start_middle_track:id_end_middle_track]
    alphah_middle = alpha_interp*track_depth[id_start_middle_track:id_end_middle_track]

    # Extrapolate coefficients (constant)
    Cr_left = [Cr_middle[0]]*(id_start_middle_track-1)
    alphah_left =[alphah_middle[0]]*(id_start_middle_track-1)   
    Cr_right = [Cr_middle[-1]]*(len(track)-id_end_middle_track+1)
    alphah_right =[alphah_middle[-1]]*(len(track)-id_end_middle_track+1)
      
    # Concatenate coefficients for the entire cross section
    Cr = np.concatenate((Cr_left, Cr_middle, Cr_right))
    alphah = np.concatenate((alphah_left, alphah_middle, alphah_right))
    
    #%% Calculate D50 in cells           
    D50_SDC_cell = []
    for j in range(len(track)):
        ddd = []
        hh = np.max(height_midpoints_cells.iloc[:,j])
        zz_hh = height_midpoints_cells.iloc[:,j]/hh
        #zz_hh.iloc[-1] = 0
        for i in range(len(zz_hh)):            
            dd = Cr[j]*np.exp(-alphah[j]*zz_hh[i])
            ddd.append(dd)
        D50_SDC_cell.append(ddd)   
    D50_SDC_cell = pd.DataFrame(D50_SDC_cell).transpose()
    D50_SDC_cell_arr = np.array(D50_SDC_cell)
    
    # Calculate mean D50
    mean_D50 = np.nansum(D50_SDC_cell*discharge)/map_data.total_discharge
       
#%% Plot D50 per MAP cell 
    # Define attributs
    plot_data = D50_SDC_cell_arr                
    max_limit=0
    
    x_plt = np.tile(np.nan, (2 * (depth_cells.shape[0]-1), 2 * (plot_data.shape[1])))
    x_pand = np.array([val for val in border_cells for _ in (0, 1)][1:-1])
    for n in range(len(x_pand)):
        x_plt[:, n] = x_pand[n]

    cell_plt = np.tile(np.nan, (2 * (depth_cells.shape[0]-1), 2 * (plot_data.shape[1]))) #depth_cells.shape
    cell_pand = np.array([val for val in depth_cells for _ in (0, 1)][1:-1])  # 1:-1
    for p in range(cell_pand.shape[0]):
        cell_plt[p, :] = cell_pand[p]

    speed_xpand = np.tile(np.nan, (depth_cells.shape[0]-1, 2 * (plot_data.shape[1])))
    for j in range(plot_data.shape[1]):
        # speed_j = np.array([val for val in plot_data[:, j] for _ in (0, 1)])
        len_plot = len(plot_data[:, j])
        speed_xpand[:len_plot, 2 * j] = plot_data[:, j]
        speed_xpand[:len_plot, 2 * j + 1] = plot_data[:, j]
    
    Conc_plt = np.repeat(speed_xpand, 2, axis=0)
    
    # Main parameters
    plt.rcParams.update({'font.size': 14})
    
    ## Canvas
    main_wt_contour_canvas = MplCanvas(width=10, height=6, dpi=240)
    canvas = main_wt_contour_canvas
    fig = canvas.fig
    
    # Configure axis
    fig.ax = fig.add_subplot(1, 1, 1)
    fig.subplots_adjust(left=0.08, bottom=0.2, right=1, top=0.97, wspace=0.1, hspace=0)
    if max_limit == 0:
        if np.sum(Conc_plt[Conc_plt > -900]) > 0:
            max_limit = np.percentile(Conc_plt[Conc_plt > -900] , 99)
        else:
            max_limit = 1
                    
    x_fill = np.insert(track,0, -1)
    x_fill = np.append(x_fill, x_fill[-1]+1)
    depth_fill = np.insert(track_depth,0,0)
    depth_fill = np.append(depth_fill, 0)
    import matplotlib.colors as colors

    # Create color map
    import matplotlib.cm as cm
    cmap = cm.get_cmap('YlOrBr')
    cmap.set_under('white')
    # Generate color contour
    c = fig.ax.pcolormesh(x_plt, cell_plt, Conc_plt, cmap=cmap, norm = colors.LogNorm()) 
    
    # Add color bar and axis labels
    cb = fig.colorbar(c, pad=0.02, shrink=0.7, anchor=(0, 0.1))
    cb.ax.set_ylabel(canvas.tr('$\mathregular{D_{50} (\mu m)}$'), fontsize = fontsize_axis, weight = 'bold')
    cb.ax.yaxis.label.set_fontsize(12)
    cb.ax.tick_params(labelsize=12)
    fig.ax.invert_yaxis()
    fig.ax.fill_between(x_fill, np.nanmax(track_depth)+2, depth_fill, color='w') # below bathy
    fig.ax.plot(track, track_depth, color='k', linewidth=1.5) # bathy
    
    fig.ax.set_xlabel(canvas.tr('Distance (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.set_ylabel(canvas.tr('Depth (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.xaxis.label.set_fontsize(12)
    fig.ax.yaxis.label.set_fontsize(12)
    fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
    fig.ax.set_ylim(top=0, bottom=np.nanmax(track_depth)+0.3)
    lower_limit = track[0]-0.5
    upper_limit = track[-1]+0.5
    
    fig.ax.set_xlim(left=lower_limit, right=upper_limit)
    
    for i in index_samplers_used:
        fig.ax.plot(analysis_data_gsd['Abscissa'][analysis_data_gsd['Sampler']== sampler[i]], 
                analysis_data_gsd['Sampling_depth_final'][analysis_data_gsd['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
    legend = fig.ax.legend(loc = 'upper right', fontsize=fontsize_legend, 
                       title = 'Sampler', bbox_to_anchor=(1.18, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
    fig.ax.text(2.52, 0.12, '$\overline{D_{50}}$ = ' + str(round(mean_D50)) + ' $\mu m$', transform = ax.transAxes,
            fontsize = fontsize_text)
    
    canvas.draw()   
    show_figure(fig)    
    fig.savefig(outpath_figures + sampling_date + r'_Cross-sectional_D50_SDC_' + most_frequent_sampler+ '.png', dpi=300, bbox_inches='tight')

#%% # Export results
    D50_SDC_cell_export = D50_SDC_cell
    D50_SDC_cell_export.columns = [np.round(track,3)]
    D50_SDC_cell_export.index = [depth_midpoints_cells]
    D50_SDC_cell_export.to_csv(outpath + str(sampling_date) + '_D50_SDC_g_l_cell_' + most_frequent_sampler +'.csv', sep = ';')
         
    summary_GSD_SDC = pd.DataFrame([mean_D50]).transpose()
    summary_GSD_SDC.columns = ['Mean_D50_SDC']

################################################################################  

# Inter- and extrapolate all grain sizes

################################################################################  


    #%% Prepare size classes
    # Get size classes 
    colnames = analysis_data.columns.values
    index_num = []
    for name in colnames:
        index_num.append(not any([c.isalpha() for c in name]))
    size_classes = colnames[index_num]
    size_classes_str = size_classes[1:] # colnames
    size_classes = [float(i) for i in size_classes]
     
    # Size classes for cumulated and classified distribution
    size_classes_cumsum = size_classes[1:]
    size_lower = np.array(size_classes[0:-1])* 1e-6 / 2
    size_upper = np.array(size_classes[1:])* 1e-6 / 2
    size_classes_classified = [10**((np.log10(size_lower[i]) + np.log10(size_upper[i]))/2) 
                         for i in range(len(size_lower))]
    size_classes_classified= np.array(size_classes_classified)
    
     
    start_GSD_idx = int(np.where(colnames==str(size_classes[1]))[0])
    end_GSD_idx = int(np.where(colnames==str(int(size_classes[-1])))[0])
    array_x_vertical = np.linspace(0,1, num=100)
    
    #%% Apply exponential profile
    all_stats_GSD_SDC_profile = []
    x_ranges_vertical = []
    
    for i in range(len(abscissa_values)):
        slp_i = []    
        x = np.array(analysis_data_gsd['z_h'][analysis_data_gsd['Abscissa'] == abscissa_values[i]])
        array_x_vertical = np.linspace(0,1, num=90)
        x_ranges_vertical.append(array_x_vertical)
        for j in range(len(size_classes_classified)):           
            Y = np.log(np.array(analysis_data_gsd[size_classes_str[j]][analysis_data_gsd['Abscissa'] == abscissa_values[i]]))
                                    
            res = stats.linregress(x, Y)
            # correct slope (increasing C with depth)
            res = list(res)
            if res[0] >= 0:
                res[0] = -0.2
                res[1] = np.max(Y)
                res[2] = np.nan
                #print('Slope and intercept of the D50 profile corrected for vertical i = ' + str(i))
            slp_i.append(res)
            
        slp_i = pd.DataFrame(slp_i)
        slp_i.columns = ['slope', 'intercept','rvalue', 'pvalue', 'stderr']
        slp_i['Cr_vertical'] = np.exp(slp_i['intercept'])
        slp_i['Crh_vertical'] = slp_i['Cr_vertical']/depth_verticals[i]
        slp_i['alphah_vertical'] = -(slp_i['slope'])
        slp_i['alpha_vertical'] = slp_i['alphah_vertical']/depth_verticals[i]
        slp_i['R2'] = slp_i['rvalue']**2
    
        GSD_vertical_profile_i = pd.DataFrame([np.exp(slp_i['intercept'][l])*np.exp(slp_i['slope'][l]*array_x_vertical[20])
                 for l in range(len(slp_i))])
        
    x_ranges_vertical = pd.DataFrame(x_ranges_vertical) 
    
    
    #%%
    all_stats_D50_SDC_profiles = []
    all_D50_vertical_profiles = []
    for l in range(len(abscissa_values)):
        slp_i = []    
        for j in range(len(size_classes_classified)):
            x = np.array(analysis_data['z_h'][analysis_data['Abscissa'] == abscissa_values[l]])
            Y = np.array(analysis_data[size_classes_str[j]][analysis_data['Abscissa'] == abscissa_values[l]])
             
            res = stats.linregress(x, Y)
            slp_i.append(res)
            
        slp_i = pd.DataFrame(slp_i)
        slp_i['slope_h'] = slp_i['slope']/depth_verticals[0]
        slp_i['intercept_h'] = slp_i['intercept']/depth_verticals[0]
        slp_i['R2'] = slp_i['rvalue']**2
        D50_vertical_profile = pd.DataFrame([slp_i['intercept'][i]+(slp_i['slope'][i]*array_x_vertical[:])
                  for i in range(len(slp_i))]).transpose()
        all_stats_D50_SDC_profiles.append(slp_i)
        all_D50_vertical_profiles.append(D50_vertical_profile)
        
          
#%% Plot all vertical profiles in one graph
    # cmap = plt.cm.get_cmap('nipy_spectral')
    # colors = cmap(np.linspace(0,1,len(abscissa_values)))
    
    # fig, ax = plt.subplots(figsize=(10, 6), dpi = 100)
    
    # for i in range(len(abscissa_values)):
    #     ax.plot(all_D50_vertical_profiles[i].iloc[:,70], x_ranges_vertical.iloc[i,:],
    #             linestyle = '-', linewidth = 2, color = colors[i],
    #             label = str(abscissa_values[i]))
           
    #     ax.plot(analysis_data[size_classes_str[70]][analysis_data['Abscissa']== abscissa_values[i]], 
    #                analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
    #                color = colors[i], linestyle = ' ',
    #                markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
    #                markeredgewidth = 0.2, markeredgecolor = 'black')
    
    # legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa', 
    #           loc = 'upper right',  facecolor = 'white', framealpha = 1)
    # plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    
    # ax.set_ylim(-0.05,1.05)
    # #ax.set_xlim(0)
    # ax.set_ylabel('z/h (-)', fontsize = fontsize_axis)
    # ax.set_xlabel(str(size_classes_str[70]) + ' $\mu m$', fontsize = fontsize_axis)
    # ax.grid(linewidth = 0.2)
    # ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    # # fig.tight_layout()
    # # figname = '_D50_vertical_profiles_' + most_frequent_sampler
    # # fig.savefig(outpath_figures + sampling_date + figname + '.png', dpi = 200, bbox_inches='tight')  
           
    #%%
    # Determine slope and intercept for each sizeclass and abscissa
    slopeh_GSD = []
    intercepth_GSD = []
    for j in range(len(size_classes_classified)):
        sh = [all_stats_D50_SDC_profiles[i].iloc[j,5]
              for i in range(len(all_stats_D50_SDC_profiles))]
        slopeh_GSD.append(sh)
        ih = [all_stats_D50_SDC_profiles[i].iloc[j,6]
              for i in range(len(all_stats_D50_SDC_profiles))]
        intercepth_GSD.append(ih)
         
    slopeh_GSD = pd.DataFrame(slopeh_GSD).transpose()
    intercepth_GSD = pd.DataFrame(intercepth_GSD).transpose()
    
    # Interpolate slope and intercept for each sizeclass
    slopeh_interp_GSD = []
    intercepth_interp_GSD = []
    for j in range(len(size_classes_classified)):
        shh = [np.interp(track_middle[i], abscissa_values, slopeh_GSD.iloc[:,j])
                for i in range(len(track_middle))]
        slopeh_interp_GSD.append(shh)
        ihh = [np.interp(track_middle[i], abscissa_values, intercepth_GSD.iloc[:,j])
                for i in range(len(track_middle))]
        intercepth_interp_GSD.append(ihh)
        
    slopeh_interp_GSD = pd.DataFrame(slopeh_interp_GSD).transpose()
    intercepth_interp_GSD = pd.DataFrame(intercepth_interp_GSD).transpose()
    
    slope_middle_GSD = pd.DataFrame([slopeh_interp_GSD.iloc[:,i]*track_depth[id_start_middle_track:id_end_middle_track]
                        for i in range(len(size_classes_classified))]).transpose()
    intercept_middle_GSD = pd.DataFrame([intercepth_interp_GSD.iloc[:,i]*track_depth[id_start_middle_track:id_end_middle_track]
                        for i in range(len(size_classes_classified))]).transpose()
    
    # Extrapolate slope and intercept constant)
    slope_left_GSD = pd.DataFrame([slope_middle_GSD.iloc[0,:]]*(id_start_middle_track-1))
    intercept_left_GSD = pd.DataFrame([intercept_middle_GSD.iloc[0,:]]*(id_start_middle_track-1))
    slope_right_GSD = pd.DataFrame([slope_middle_GSD.iloc[-1,:]]*(len(track)-id_end_middle_track+1))
    intercept_right_GSD = pd.DataFrame([intercept_middle_GSD.iloc[-1,:]]*(len(track)-id_end_middle_track+1))
   
    # Concatenate coefficients for the entire cross section
    slope_GSD = pd.DataFrame(np.concatenate((slope_left_GSD, slope_middle_GSD, slope_right_GSD)))
    intercept_GSD = pd.DataFrame(np.concatenate((intercept_left_GSD, intercept_middle_GSD, intercept_right_GSD)))
   
    #%% Calculate GSD in cells 
    
    # List of dataframes ()    # verify! 
    GSD_SDC_cell_all = []
    for l in range(len(size_classes_classified)):      
        GSD_SDC_cell = []
        for j in range(len(track)):
            ddd = []
            hh = np.max(height_midpoints_cells.iloc[:,j])
            zz_hh = height_midpoints_cells.iloc[:,j]/hh
            #zz_hh.iloc[-1] = 0
            for i in range(len(zz_hh)):            
                dd = slope_GSD.iloc[j,l]*zz_hh[i]+intercept_GSD.iloc[j,l]
                ddd.append(dd)
            GSD_SDC_cell.append(ddd)   
        GSD_SDC_cell = pd.DataFrame(GSD_SDC_cell).transpose()
        GSD_SDC_cell_arr = np.array(GSD_SDC_cell)
        GSD_SDC_cell_all.append(GSD_SDC_cell)
    
#%% Get GSD for each cell 
# list at each df cell 
    GSD_cells = []
    for j in range(len(track)):
        inner = []
        for l in range(len(height_midpoints_cells)):
            ff = [GSD_SDC_cell_all[i].iloc[l,j] for i in range(len(GSD_SDC_cell_all))]
            ff = [0 if i < 0 else i for i in ff]
            inner.append(ff)
        GSD_cells.append(inner)
      
    GSD_cells = pd.DataFrame(GSD_cells).transpose()

#%% Get index of samples in MAP grid (depth_midpoints_cells and track)
    # # no 0: (11,0)
    # # 12:  (34, 1)
    # # 11: (25, 5)
    
    # f = GSD_cells.iloc[4, 25]
    
    # #%% 
    # # Grain size distributions
    
    # from matplotlib.ticker import FixedLocator, FixedFormatter
    # from matplotlib.ticker import FuncFormatter
    
    # fig, ax = plt.subplots(figsize=(10, 6), dpi=100)
    
    # ax.axvline(63, color='darkgrey', linewidth=1, linestyle='--')
    # ax.plot(size_classes_classified*2/1e-6, analysis_data.iloc[10,(start_GSD_idx-1):end_GSD_idx])
    # ax.plot(size_classes_classified*2/1e-6, f, 'red')
    
    # ax.set_xlabel('Diameter (\u03BCm)', fontsize = fontsize_axis)
    # ax.set_ylabel('Density (%)', fontsize = fontsize_axis)
    # ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    # x_formatter = FixedFormatter(['10', '50', '100', '500', '1000'])
    # x_locator = FixedLocator([10, 50, 100, 500, 1000])
    # ax.xaxis.set_major_formatter(x_formatter)
    # ax.xaxis.set_major_locator(x_locator)
    # ax.set_ylim(0,)
    # ax.set_xlim(0,1000)
    # #plt.legend()
    
    # # plt.tight_layout()
    # # figname = sampling_date + '_Grain_size_distributions_' + most_frequent_sampler
    # # fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')
    #%%
    GSD_SDC_cell_export = GSD_cells
    GSD_SDC_cell_export.columns = [np.round(track,3)]
    GSD_SDC_cell_export.index = [depth_midpoints_cells]
    GSD_SDC_cell_export.to_csv(outpath + str(sampling_date) + '_GSD_SDC_cell_' + most_frequent_sampler +'.csv', sep = ';')
   
    
    return analysis_data, summary_GSD_SDC, D50_SDC_cell_export, GSD_SDC_cell_export
