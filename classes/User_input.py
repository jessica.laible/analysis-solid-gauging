"""
Choose the files and folders to be used for analysis

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# Load packages
import tkinter as tk
import tkinter.filedialog
import os
import sys

def numbers_extrac(outpath) :
    copie_outpath = outpath.replace('\\', ' ').replace('_', ' ').replace('/', ' ')
    numbers_extrac = [int(s) for s in copie_outpath.split() if s.isdigit()]
    numbers_extrac = str(numbers_extrac)
    numbers_extrac = numbers_extrac.replace('[', '').replace(']', '')
    return (numbers_extrac)

def user_input(path_field_data, choice) :

    # Prep data
    root = tk.Tk()
    root.withdraw()
    root.attributes("-topmost", True)
    filepath_prep_data = str(tk.filedialog.askopenfilenames(title='Select Prep_data (.csv)', initialdir=path_field_data))
    filepath_prep_data = filepath_prep_data.replace('(', '').replace(")", '').replace("'", '').replace(
        ",", '')
    root.destroy()

    path_field_data = os.path.dirname(filepath_prep_data)

    if choice[1] == 1:
        # Diver
        root = tk.Tk()
        root.withdraw()
        root.attributes("-topmost", True)
        filepath_diver = str(tk.filedialog.askopenfilenames(title='Select diver (.csv)', initialdir=path_field_data))
        filepath_diver = filepath_diver.replace('(', '').replace(")", '').replace("'", '').replace(",", '')
        root.destroy()
    else:
        filepath_diver = ''

    if choice[2] == 1 :
        # Grainsize
        root = tk.Tk()
        root.withdraw()
        root.attributes("-topmost", True)
        filepath_grainsize = str(tk.filedialog.askopenfilenames(title='Select Grainsize  (.csv)'
                                                                      , initialdir=path_field_data))
        filepath_grainsize = filepath_grainsize.replace('(', '').replace(")", '').replace("'", '').replace(",", '')
        root.destroy()
    else :
        filepath_grainsize = ''

    if choice[3] == 1 :
        # ADCP
        root = tk.Tk()
        root.withdraw()
        root.attributes("-topmost", True)
        # filepath_ADCP = str(tk.filedialog.askdirectory(title='Select ADCP folder (MAP)'
        #                                                               , initialdir=path_field_data))
        filepath_ADCP = tk.filedialog.askopenfilenames(title='Select ADCP meas or results',
                                                           initialdir=path_field_data,
                            filetypes=[('ADCP', '*.mmt'),
                                       ('ADCP', '*.mat'),
                                       ('ADCP', '*.txt')])
        # filepath_ADCP = filepath_ADCP.replace('(', '').replace(")", '').replace("'", '').replace(",", '')
        root.destroy()
    else :
        filepath_ADCP = ''
        
    # if choice[4] == 1 :
    #     # ADCP stationnary verticals
    #     root = tk.Tk()
    #     root.withdraw()
    #     root.attributes("-topmost", True)
    #     filepath_ADCP_verticals = str(tk.filedialog.askdirectory(title='Select folder of stationnary ADCP measurements at sampling verticals'
    #                                                                   , initialdir=filepath_ADCP))
    #     filepath_ADCP_verticals = filepath_ADCP_verticals.replace('(', '').replace(")", '').replace("'", '').replace(",", '')
    #     filepath_ADCP_verticals = filepath_ADCP_verticals + '/'
    #     root.destroy()
    # else :
    #     filepath_ADCP_verticals = ''
        
    if choice[4] == 1 :
        root = tk.Tk()
        root.withdraw()
        root.attributes("-topmost", True)
        filepath_Q = str(tk.filedialog.askopenfilenames(title='Select water discharge data'
                                                                      , initialdir=path_field_data))
        filepath_Q = filepath_Q.replace('(', '').replace(")", '').replace("'", '').replace(",", '')
        root.destroy()
    else :
        filepath_Q = ''

    if choice[5] == 1 :
        root = tk.Tk()
        root.withdraw()
        root.attributes("-topmost", True)
        filepath_stage = str(tk.filedialog.askopenfilenames(title='Select water stage data'
                                                                      , initialdir=filepath_Q))
        filepath_stage = filepath_stage.replace('(', '').replace(")", '').replace("'", '').replace(",", '')
        root.destroy()
    else :
        filepath_stage = ''
        
    if choice[6] == 1 :
        root = tk.Tk()
        root.withdraw()
        root.attributes("-topmost", True)
        filepath_spm = str(tk.filedialog.askopenfilenames(title='Select concentration data'
                                                                      , initialdir=filepath_Q))
        filepath_spm = filepath_spm.replace('(', '').replace(")", '').replace("'", '').replace(",", '')
        root.destroy()
    else :
        filepath_spm = ''

    # Verification of dates
    number_prep = numbers_extrac(filepath_prep_data)
    number_diver = numbers_extrac(filepath_diver)
    number_grainsize = numbers_extrac(filepath_grainsize)
       
     # Simple error test
    # if number_prep is not number_diver: # is not number_grainsize:
    #      sys.exit(
    #          "Error message: Dates do not correspond")
    
        
    return (filepath_prep_data, filepath_diver, filepath_grainsize, filepath_ADCP, filepath_Q, filepath_stage, filepath_spm)

