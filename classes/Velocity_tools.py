"""
Determines the velocity at the MAP-cell surrounding each sampling point, 
the bathymetry (track_depth) and the length of the MAP-transect (track, borders of each MAP-cell)

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import pandas as pd


def map_treatment(analysis_data, map_data):
    #%%
    abscissa_values = np.unique(analysis_data['Abscissa']).tolist()

    # Calculate cell properties
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
   
    # Length of transect (center of each distance cell, plus start (0) and end)
    track = (border_cells[1:] + border_cells[:-1]) / 2
    track = np.append(np.insert(track, 0, 0), border_cells[-1])
   
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    track_depth = np.append(np.insert(track_depth, 0, 0), 0)

    # Max depth of each abscissas
    # Value interpolated of track_depth at each abscissa
    index_value = []
    value_near = []
    value_near_nearly = []
    depth_max_abscisse = []
    depth_max_abscisse_nearly = []
    max_depth_abs = []

    for i in range(len(abscissa_values)):
        # Find closest map vertical
        value_near.append(min(track, key=lambda x: abs(x - abscissa_values[i])))
        index_value.append(np.where(track == value_near[i]))
        # Depth of the map vertical
        depth_max_abscisse.append(track_depth[index_value[i][0][0]])

        # Frame abscissa with map verticals
        if value_near[i] > abscissa_values[i]:
            depth_max_abscisse_nearly.append(track_depth[index_value[i][0][0] - 1])
            value_near_nearly.append(track[index_value[i][0][0] - 1])
        elif value_near[i] < abscissa_values[i]:
            depth_max_abscisse_nearly.append(track_depth[index_value[i][0][0] + 1])
            value_near_nearly.append(track[index_value[i][0][0] + 1])

        # Interpolate map depth for x=abscissa
        depth_interp = pd.Series([depth_max_abscisse_nearly[i], np.nan, depth_max_abscisse[i]],
                                 index=[value_near_nearly[i], abscissa_values[i], value_near[i]])
        depth_interp = depth_interp.interpolate(method='index')

        max_depth_abs.append(depth_interp.iloc[1])

    analysis_data['max_depth_vertical'] = [max_depth_abs[analysis_data['Abscissa_Name'][i] - 1]
                                           for i in range(len(analysis_data['Abscissa_Name']))]

    #%%############################################################################
    # # Normalized sampling depth
    # if analysis_data['Height_field'] is not np.nan:
    #     analysis_data['vertical_depth'] = analysis_data['Sampling_depth_final'] + analysis_data['Height_field']
    #     for id in analysis_data.loc[analysis_data['vertical_depth'].isna()].index:
    #         x = analysis_data.iloc[id]['Abscissa']
    #         depth_value = analysis_data[analysis_data['Abscissa'] == x]['vertical_depth'].mean()
    #         analysis_data.loc[id, 'vertical_depth'] = depth_value
    
    # Calculation of sampling height above bottom
    analysis_data.insert(6, 'Sampling_height_final',
                          [analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
                           for i in range(len(analysis_data))])
    if np.count_nonzero(~np.isnan(np.array(analysis_data['Height_field']))) >= len(analysis_data['Height_field'])/2:
        analysis_data['vertical_depth'] = analysis_data['Sampling_depth_final'] + analysis_data['Height_field']
    else:
        analysis_data['vertical_depth'] = analysis_data['max_depth_vertical']
                
    analysis_data['z_h'] = analysis_data['Sampling_height_final']/analysis_data['max_depth_vertical']
    z_h = analysis_data['Sampling_height_final']/analysis_data['max_depth_vertical']
    depth = analysis_data['Sampling_depth_final']
    print('Depths adapted on ID:')
    for i in range(len(analysis_data)):
        if analysis_data['Sampling_height_final'][i] < 0 and analysis_data['Sampling_height_final'][i] >= -0.1:
            print(i, 'corrected difference: 0.1')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.1
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.1 and analysis_data['Sampling_height_final'][i] >= -0.2:
            print(i, 'corrected difference: 0.2')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.2
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.2 and analysis_data['Sampling_height_final'][i] >= -0.3:
            print(i, 'corrected difference: 0.3')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.3
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.3 and analysis_data['Sampling_height_final'][i] >= -0.4:
            print(i, 'corrected difference: 0.4')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.4
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.4 and analysis_data['Sampling_height_final'][i] >= -0.5:
            print(i, 'corrected difference: 0.5')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.5
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.5 and analysis_data['Sampling_height_final'][i] >= -0.6:
            print(i, 'corrected difference: 0.6')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.6
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.6 and analysis_data['Sampling_height_final'][i] >= -0.7:
            print(i, 'corrected difference: 0.7')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.7
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.7 and analysis_data['Sampling_height_final'][i] >= -0.8:
            print(i, 'corrected difference: 0.8')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.8
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.8 and analysis_data['Sampling_height_final'][i] >= -0.9:
            print(i, 'corrected difference: 0.9')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 0.9
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -0.9 and analysis_data['Sampling_height_final'][i] >= -1:
            print(i, 'corrected difference: 1')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 1
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
        if analysis_data['Sampling_height_final'][i] <= -1:
            print(i, 'corrected depth difference: 1.4')
            analysis_data['Sampling_depth_final'][i] = analysis_data['Sampling_depth_final'][i] - 1.4
            analysis_data['Sampling_height_final'][i] = analysis_data['max_depth_vertical'][i] - analysis_data['Sampling_depth_final'][i]
            analysis_data['z_h'].iloc[i] = analysis_data['Sampling_height_final'][i]/analysis_data['max_depth_vertical'][i]
      
    norm_depth = analysis_data['Sampling_depth_final']/analysis_data['max_depth_vertical']
   
    #%%##########################################################
    map_velocity = np.c_[
        map_data.left_primary_velocity, map_data.extrap_primary_velocity, map_data.right_primary_velocity]

    map_center_depth = np.c_[
        map_data.left_mid_cells_y, map_data.depth_cells_center, map_data.right_mid_cells_y]

    map_vertical_depth = np.tile(np.nan, (map_velocity.shape))
    len_left = map_data.left_primary_velocity.shape[1]
    len_mid = map_data.extrap_primary_velocity.shape[1]
    map_vertical_depth[:, 0:len_left] = map_data.left_vertical_depth
    map_vertical_depth[:, len_left:len_left + len_mid] = np.tile(map_data.depths, (map_velocity.shape[0], 1))
    map_vertical_depth[:, len_left + len_mid:] = map_data.right_vertical_depth

    map_norm_depth = map_center_depth / map_vertical_depth

    # Calculate index of MAP cell (abscissa) where the sample is located
    cell_idx_sampling_point = []
    for k in range(len(analysis_data)):
        cc = [i for i in range(len(border_cells))
              if border_cells[i] <= analysis_data['Abscissa'][k]]
        ccc = cc[-1]
        cell_idx_sampling_point.append(ccc)

    # Calculate index of cell (depth) where the sample is located
    # Supposing MAP_depth is top of each cell
    depth_idx_sampling_point = []
    for k in range(len(analysis_data)):
        norm_depth_vertical = map_norm_depth[:, cell_idx_sampling_point[k]]
        closest_depth = np.nanargmin(np.abs(norm_depth_vertical - norm_depth[k]))
        depth_idx_sampling_point.append(closest_depth)

    # Determine velocity and discharge in cell at sampling point 
    velocity_point = []
    discharge_point = []
    for gg in range(len(cell_idx_sampling_point)):
        dt = map_velocity[depth_idx_sampling_point[gg],
                          cell_idx_sampling_point[gg]]
        dd = map_data.middle_cells_discharge[depth_idx_sampling_point[gg],
                          cell_idx_sampling_point[gg]]
        velocity_point.append(np.round(dt, 3))
        discharge_point.append(np.round(dd, 3))

    analysis_data['Discharge_sampling_point'] = discharge_point
    analysis_data['Velocity_sampling_point'] = velocity_point
    map_velocity = pd.DataFrame(map_velocity)
    analysis_data['Mean_velocity_vertical'] = [np.nanmean(map_velocity.iloc[:,cell_idx_sampling_point[i]]) 
              for i in range(len(cell_idx_sampling_point))]
   
    # Determine v²/d index
    v2 = np.mean(map_velocity).mean()**2
    d = np.mean(track_depth)
    v2_d_ratio = v2/d
    
    v2_d_vertical = analysis_data['Mean_velocity_vertical']*analysis_data['Mean_velocity_vertical']/analysis_data['max_depth_vertical']
    v2_d_index = np.max(v2_d_vertical)/v2_d_ratio
    
    #%%###################### Graphs #################################

    # if graph_option == True :
    #     hydrometric_graphs(MAP_distance, MAP_depth, track, track_depth, real_date, 
    #                            analysis_data, abscissa_values, outpath_figures)
    analysis_data = analysis_data.sort_values(['Abscissa', 'Sampling_depth_final'])
    
    return (track, track_depth, v2_d_index, analysis_data)
