"""
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
from scipy import stats
from UI.MplCanvas import MplCanvas
import matplotlib.cm as cm
from Common_functions import show_figure

def vertical_profiles(analysis_data, map_data, outpath, outpath_figures, sampling_date): 
    
    abscissa_values = np.unique(analysis_data['Abscissa']).tolist()
    
    # Discharge
    discharge = pd.concat([pd.DataFrame(map_data.left_cells_discharge), pd.DataFrame(map_data.middle_cells_discharge),
                           pd.DataFrame(map_data.right_cells_discharge)], axis = 1)
    discharge.columns = np.arange(0,len(discharge.columns),1)
   
    #%%
    depth_verticals = analysis_data['max_depth_vertical'].unique() # list_h    
    
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S', 'Niskin']
    forms_sampler = ['o', 'o','D', 's', 'P', '*', 'v']  
    size_sampler = [6, 6, 6, 6, 7, 8, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                     'darkorange', 'darkred', 'yellowgreen']
    samplers_used = np.unique(analysis_data['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    id_sampler_used = [sampler.index(analysis_data['Sampler'][i])
          for i in range(len(analysis_data))]
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 14
    fontsize_legend_title = 14
    fontsize_text = 14
    fontsize_ticks = 12
    
    #%% Exponential profile     
    # using elevation above bed 
    stats_exp_ele_profile = []
    x_ranges_exp_ele = []
    
    for i in range(len(abscissa_values)):
        x = np.array(analysis_data['Sampling_height_final'][analysis_data['Abscissa'] == abscissa_values[i]])
        Y = np.log(np.array(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]))
        array_x_exp_ele = np.linspace(np.max(x) - 0.02, 0.02, num=100) #* rather analysis_data['max_depth_vertical'].unique()[i] instead of max(x)?
        x_ranges_exp_ele.append(array_x_exp_ele)
        
        res = stats.linregress(x, Y)
        stats_exp_ele_profile.append(res)
        C = [np.exp(res[1])*np.exp(-res[0]*array_x_exp_ele[j])
             for j in range(len(array_x_exp_ele))]       

    x_ranges_exp_ele = pd.DataFrame(x_ranges_exp_ele) 
    stats_exp_ele_profile = pd.DataFrame(stats_exp_ele_profile)
    stats_exp_ele_profile['Cr_exp_ele'] = np.exp(stats_exp_ele_profile['intercept'])
    stats_exp_ele_profile['beta_exp_ele'] = -(stats_exp_ele_profile['slope'])
    stats_exp_ele_profile['R2'] = stats_exp_ele_profile['rvalue']**2
    # Conc_exp_ele_vertical_profile = pd.DataFrame([stats_exp_ele_profile['Cr_exp_ele'][i]*np.exp(-stats_exp_ele_profile['beta_exp_ele'][i]*x_ranges_exp_ele.iloc[i,:])
    #          for i in range(len(stats_exp_ele_profile))])
    Conc_exp_ele_vertical_profile = pd.DataFrame([np.exp(stats_exp_ele_profile['intercept'][i])*np.exp(stats_exp_ele_profile['slope'][i]*x_ranges_exp_ele.iloc[i,:])
             for i in range(len(stats_exp_ele_profile))])
    
  ############################################################################
  
  # using z/h
    stats_exp_zh_profile = []
    x_ranges_exp_zh = []
    
    for i in range(len(abscissa_values)):
        x = np.array(analysis_data['z_h'][analysis_data['Abscissa'] == abscissa_values[i]])
        Y = np.log(np.array(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]))
        array_x_exp_zh = np.linspace(1, 0, num=100)
        x_ranges_exp_zh.append(array_x_exp_zh)
        
        res = stats.linregress(x, Y)
        stats_exp_zh_profile.append(res)
        C = [np.exp(res[1])*np.exp(-res[0]*array_x_exp_zh[j])
             for j in range(len(array_x_exp_zh))]       
        
    x_ranges_exp_zh = pd.DataFrame(x_ranges_exp_zh) 
    stats_exp_zh_profile = pd.DataFrame(stats_exp_zh_profile)
    stats_exp_zh_profile['Cr_exp_zh'] = np.exp(stats_exp_zh_profile['intercept'])
    stats_exp_zh_profile['beta_exp_zh'] = -(stats_exp_zh_profile['slope'])
    stats_exp_zh_profile['R2'] = stats_exp_zh_profile['rvalue']**2
    Conc_exp_zh_vertical_profile = pd.DataFrame([np.exp(stats_exp_zh_profile['intercept'][i])*np.exp(stats_exp_zh_profile['slope'][i]*x_ranges_exp_zh.iloc[i,:])
              for i in range(len(stats_exp_zh_profile))])
    # Conc_exp_zh_vertical_profile = pd.DataFrame([stats_exp_zh_profile['Cr_exp_zh'][i]*np.exp(-stats_exp_zh_profile['beta_exp_zh'][i]*x_ranges_exp_zh.iloc[i,:])
    #          for i in range(len(stats_exp_zh_profile))])
 
   # plt.plot(x,Y)
   # plt.xlabel('z/h (-)')
   # plt.ylabel('log(C)')
   
#%% Rouse Profile 
    Za = 0.05
    
    # using elevation 
    z_ranges_Rouse_ele = []
    stats_Rouse_ele_profile = []
    list_z_log = []
    list_y_log = []
    Conc_Rouse_ele = []
    Ca_Rouse_ele = []
    Rouse_number = []
    for i in range(len(abscissa_values)) :
        z = np.array(analysis_data['Sampling_height_final'][analysis_data['Abscissa'] == abscissa_values[i]])
        Y = np.log10(np.array(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]))
        array_z_Rouse_ele = np.linspace(0.05, np.max(z) - 0.05, num=100) 
        z_ranges_Rouse_ele.append(array_z_Rouse_ele)
        
        for j in range(len(z)):
            if z[j] == depth_verticals[i]:
                z[j] = depth_verticals[i] - 0.1
        zlog = np.log10((depth_verticals[i] - z) / z)
        list_z_log.append(zlog)
        list_y_log.append(Y)

        res = stats.linregress(zlog, Y)        
        stats_Rouse_ele_profile.append(res)
       
        Ca = 10 ** (res[1] - res[0] * np.log10(Za / (depth_verticals[i] - Za)))
        Ca_Rouse_ele.append(Ca)
        C = Ca * ((((depth_verticals[i] - array_z_Rouse_ele) / array_z_Rouse_ele) * (Za / (depth_verticals[i] - Za))) ** (res[0]))
        Conc_Rouse_ele.append(C)
        rouse = [res[0]]*len(z)
        Rouse_number.append(rouse)
        
    analysis_data['Rouse_number'] = [item for sublist in Rouse_number for item in sublist]
    z_ranges_Rouse_ele = pd.DataFrame(z_ranges_Rouse_ele) 
    stats_Rouse_ele_profile = pd.DataFrame(stats_Rouse_ele_profile)
    stats_Rouse_ele_profile['R2'] = stats_Rouse_ele_profile['rvalue']**2
    Ca_Rouse_ele = pd.DataFrame(Ca_Rouse_ele)
    Conc_Rouse_ele = pd.DataFrame(Conc_Rouse_ele)
        
    
    ############################################################################
    
    # using z/h 
    z_ranges_Rouse_zh = []
    stats_Rouse_zh_profile = []
    list_z_log = []
    list_y_log = []
    Conc_Rouse_zh = []
    Ca_Rouse_zh = []
    for i in range(len(abscissa_values)) :
        z = np.array(analysis_data['z_h'][analysis_data['Abscissa'] == abscissa_values[i]])
        Y = np.log10(np.array(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]))
        array_z_Rouse_zh = np.linspace(0.01,1, num=100) 
        z_ranges_Rouse_zh.append(array_z_Rouse_zh)
        
        for j in range(len(z)):
            if z[j] == depth_verticals[i]:
                z[j] = depth_verticals[i] - 0.1
        zlog = np.log10((depth_verticals[i] - z) / z)
        list_z_log.append(zlog)
        list_y_log.append(Y)

        res = stats.linregress(zlog, Y)        
        stats_Rouse_zh_profile.append(res)
       
        Ca = 10 ** (res[1] - res[0] * np.log10(Za / (depth_verticals[i] - Za)))
        Ca_Rouse_zh.append(Ca)
        C = Ca * ((((depth_verticals[i] - array_z_Rouse_zh) / array_z_Rouse_zh) * (Za / (depth_verticals[i] - Za))) ** (res[0]))
        Conc_Rouse_zh.append(C)
        
    z_ranges_Rouse_zh = pd.DataFrame(z_ranges_Rouse_zh) 
    stats_Rouse_zh_profile = pd.DataFrame(stats_Rouse_zh_profile)
    stats_Rouse_zh_profile['R2'] = stats_Rouse_zh_profile['rvalue']**2
    Ca_Rouse_zh = pd.DataFrame(Ca_Rouse_zh)
    Conc_Rouse_zh = pd.DataFrame(Conc_Rouse_zh)
    
    
############################################################################

# Plot vertical profiles 

############################################################################
    
#%% Plot all exponential and Rouse profiles using elevation in one graph
    cmap = plt.cm.get_cmap('copper')
    colors = cmap(np.linspace(0,1,len(abscissa_values)))
    
    fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
    
    for i in range(len(abscissa_values)):
        ax.plot(Conc_exp_ele_vertical_profile.iloc[i,:], x_ranges_exp_ele.iloc[i,:],
                linestyle = '-', linewidth = 2, color = colors[i],
                label = str(abscissa_values[i]) + '; exp')
        ax.plot(Conc_Rouse_ele.iloc[i,:], z_ranges_Rouse_ele.iloc[i,:],
                linestyle = ':', linewidth = 2, color = colors[i],
                label = str(abscissa_values[i]) + '; Rouse')
    
        ax.plot(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
                   analysis_data['Sampling_height_final'][analysis_data['Abscissa']== abscissa_values[i]],
                   color = colors[i], linestyle = ' ',
                   markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                   markeredgewidth = 0.2, markeredgecolor = 'black')
    
    legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa & \nInterpolation', 
              loc = 'upper right',  facecolor = 'white', framealpha = 1)
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    ax.set_ylim(0,)
    ax.set_xlim(0)
    ax.set_ylabel('Elevation above bed (m)', fontsize = fontsize_axis)
    ax.set_xlabel('Concentration (g/l)', fontsize = fontsize_axis) #* mg/l?
    ax.grid(linewidth = 0.2)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    fig.tight_layout()
    figname = sampling_date + '_Conc_height_exp_Rouse_profile_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi = 200, bbox_inches='tight')
    
    # log-scale
    fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
    
    for i in range(len(abscissa_values)):
        ax.plot(Conc_exp_ele_vertical_profile.iloc[i,:], x_ranges_exp_ele.iloc[i,:],
                linestyle = '-', linewidth = 2, color = colors[i],
                label = str(abscissa_values[i]) + '; exp')
        ax.plot(Conc_Rouse_ele.iloc[i,:], z_ranges_Rouse_ele.iloc[i,:],
                linestyle = ':', linewidth = 2, color = colors[i],
                label = str(abscissa_values[i]) + '; Rouse')
    
        ax.plot(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
                   analysis_data['Sampling_height_final'][analysis_data['Abscissa']== abscissa_values[i]],
                   color = colors[i], linestyle = ' ',
                   markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                   markeredgewidth = 0.2, markeredgecolor = 'black')
    
    legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa & \nInterpolation', 
              loc = 'upper right',  facecolor = 'white', framealpha = 1)
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    
    ax.set_xscale('log')
    ax.set_ylim(0,)
    #ax.set_xlim(0)
    ax.set_ylabel('Elevation above bed (m)', fontsize = fontsize_axis)
    ax.set_xlabel('Concentration (g/l)', fontsize = fontsize_axis) #* mg/l?
    ax.grid(linewidth = 0.2)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    fig.tight_layout()
    figname = sampling_date + '_Conc_height_exp_Rouse_profile_log_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi = 200, bbox_inches='tight')
  
    #%% Plot exp and Rouse profiles using elevation for each abscissa   
    for i in range(len(abscissa_values)):
        fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
        ax.plot(Conc_exp_ele_vertical_profile.iloc[i,:], x_ranges_exp_ele.iloc[i,:],
                linestyle = '-', linewidth = 2, color = colors[i],
                label = 'exp')
        ax.plot(Conc_Rouse_ele.iloc[i,:], z_ranges_Rouse_ele.iloc[i,:],
                linestyle = ':', linewidth = 2, color = colors[i],
                label = 'Rouse')
        
        ax.plot(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
                   analysis_data['Sampling_height_final'][analysis_data['Abscissa']== abscissa_values[i]],
                   color = colors[i], linestyle = ' ',
                   markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                   markeredgewidth = 0.2, markeredgecolor = 'black')
        
        ax.text(0.77, 0.68, '${R²_{exp}}$ = ' + str(np.round(stats_exp_ele_profile['R2'][i], 2)), 
                transform=ax.transAxes, fontsize = fontsize_text)
        ax.text(0.77, 0.62, '${R²_{Rouse}}$ = ' + str(np.round(stats_Rouse_ele_profile['R2'][i], 2)), 
                transform=ax.transAxes, fontsize = fontsize_text)
        
        legend = ax.legend(fontsize = fontsize_legend, title = 'Interpolation', 
                  loc = 'upper right', facecolor = 'white', framealpha = 1)
        plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
        ax.set_ylim(0,)
        #ax.set_xlim(0)
        ax.set_ylabel('Elevation above bed (m)', fontsize = fontsize_axis)
        ax.set_xlabel('Concentration (g/l)', fontsize = fontsize_axis) #* mg/l?
        ax.grid(linewidth = 0.2)
        ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
        fig.tight_layout()
        figname = sampling_date + '_Conc_height_profiles_'
        fig.savefig(outpath_figures + figname + str(abscissa_values[i]) + '_'+ most_frequent_sampler + '.png', dpi = 200, bbox_inches='tight')
       
        
       #%% Plot all exponential and Rouse profiles using z/h in one graph
        fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)

        for i in range(len(abscissa_values)):
            ax.plot(Conc_exp_zh_vertical_profile.iloc[i,:], x_ranges_exp_zh.iloc[i,:],
                    linestyle = '-', linewidth = 2, color = colors[i],
                    label = str(abscissa_values[i]) + '; exp')
            ax.plot(Conc_Rouse_zh.iloc[i,:], z_ranges_Rouse_zh.iloc[i,:],
                    linestyle = ':', linewidth = 2, color = colors[i],
                    label = str(abscissa_values[i]) + '; Rouse')
        
            ax.plot(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
                        analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
                        color = colors[i], linestyle = ' ',
                        markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                        markeredgewidth = 0.2, markeredgecolor = 'black')
                     
        legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa & \nInterpolation', 
                  loc = 'upper right',  facecolor = 'white', framealpha = 1) # , bbox_to_anchor = (1.4, 1.1)
        plt.setp(legend.get_title(), fontsize=fontsize_legend_title)

        ax.set_ylim(0,1)
        ax.set_xlim(0, np.max(analysis_data['Concentration_sand_g_l'])+ np.mean(analysis_data['Concentration_sand_g_l']))
        ax.set_ylabel('z/h (-)', fontsize = fontsize_axis)
        ax.set_xlabel('Concentration (g/l)', fontsize = fontsize_axis) #* mg/l?
        ax.grid(linewidth = 0.2)
        ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
        fig.tight_layout()
        figname = sampling_date + '_Conc_zh_exp_Rouse_profiles_' + most_frequent_sampler
        fig.savefig(outpath_figures + figname + '.png', dpi = 200, bbox_inches='tight')
        
    # log-scale
        fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)

        for i in range(len(abscissa_values)):
            ax.plot(Conc_exp_zh_vertical_profile.iloc[i,:], x_ranges_exp_zh.iloc[i,:],
                    linestyle = '-', linewidth = 2, color = colors[i],
                    label = str(abscissa_values[i]) + '; exp')
            ax.plot(Conc_Rouse_zh.iloc[i,:], z_ranges_Rouse_zh.iloc[i,:],
                    linestyle = ':', linewidth = 2, color = colors[i],
                    label = str(abscissa_values[i]) + '; Rouse')
        
            ax.plot(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
                        analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
                        color = colors[i], linestyle = ' ',
                        markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                        markeredgewidth = 0.2, markeredgecolor = 'black')
                     
        legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa & \nInterpolation', 
                  loc = 'upper right',  facecolor = 'white', framealpha = 1) # , bbox_to_anchor = (1.4, 1.1)
        plt.setp(legend.get_title(), fontsize=fontsize_legend_title)

        ax.set_xscale('log')
        ax.set_ylim(0,1)
        ax.set_ylabel('z/h (-)', fontsize = fontsize_axis)
        ax.set_xlabel('Concentration (g/l)', fontsize = fontsize_axis) #* mg/l?
        ax.grid(linewidth = 0.2)
        ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
        fig.tight_layout()
        figname = sampling_date + '_Conc_zh_exp_Rouse_profiles_log_' + most_frequent_sampler
        fig.savefig(outpath_figures + figname + '.png', dpi = 200, bbox_inches='tight')
                                        
        #%% Plot exp and Rouse profiles using z/h for each abscissa   
        for i in range(len(abscissa_values)):
            fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
            ax.invert_yaxis()
            ax.text(0.77, 0.68, '${R²_{exp}}$ = ' + str(np.round(stats_exp_zh_profile['R2'][i], 2)), 
                    transform=ax.transAxes, fontsize = fontsize_text)
            ax.text(0.77, 0.62, '${R²_{Rouse}}$ = ' + str(np.round(stats_Rouse_zh_profile['R2'][i], 2)), 
                    transform=ax.transAxes, fontsize = fontsize_text)
           
            ax.plot(Conc_exp_zh_vertical_profile.iloc[i,:], x_ranges_exp_zh.iloc[i,:],
                    linestyle = '-', linewidth = 2, color = colors[i],
                    label = 'exp')
            ax.plot(Conc_Rouse_zh.iloc[i,:], z_ranges_Rouse_zh.iloc[i,:],
                    linestyle = ':', linewidth = 2, color = colors[i],
                    label = 'Rouse')
            
            ax.plot(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
                       analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
                       color = colors[i], linestyle = ' ',
                       markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                       markeredgewidth = 0.2, markeredgecolor = 'black')
             
            legend = ax.legend(fontsize = fontsize_legend, title = 'Interpolation', 
                      loc = 'upper right', facecolor = 'white', framealpha = 1)
            plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
            ax.set_ylim(0,1)
            #ax.set_xlim(0)
            ax.set_ylabel('z/h (-)', fontsize = fontsize_axis)
            ax.set_xlabel('Concentration (g/l)', fontsize = fontsize_axis) #* mg/l?
            ax.grid(linewidth = 0.2)
            ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
            fig.tight_layout()
            figname = sampling_date + '_Conc_zh_profiles_'
            fig.savefig(outpath_figures + figname + str(abscissa_values[i]) + '_' + most_frequent_sampler + '.png', dpi = 200, bbox_inches='tight')
    

#%% # Define MAP grid
    cell_height = map_data.depth_cells_border[1][0]-map_data.depth_cells_border[0][0]  
    depth_cells = np.arange(map_data.depth_cells_border[0][0], (len(map_data.depth_cells_border)*cell_height), cell_height)
   
    # Calculate cell properties
    cell_height = map_data.border_depth[1] - map_data.border_depth[0]
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
    depth_cells = map_data.border_depth
    
    # Length and depth of transect (center of each distance cell, plus start (0) and end)
    track = (border_cells[1:] + border_cells[:-1]) / 2
    track = np.append(np.insert(track, 0, 0), border_cells[-1])
    
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    track_depth = np.append(np.insert(track_depth, 0, 0), 0)

    # Create height above bottom grid
    height_midpoints_cells=[]
    for i in range(len(track_depth)):
        heh = int(track_depth[i]//cell_height)
        height_last_cell = (track_depth[i]-depth_cells[heh])
        midpoint_last_cell = np.array([height_last_cell/2])   # use half-distance between bottom and bottom of lowest complete cell for bottom cell
        midpoint_height_cells = np.arange(0, heh)*cell_height + (height_last_cell + cell_height/2)
        height_midpoints_cells1 = np.concatenate([midpoint_last_cell,midpoint_height_cells], axis = 0)
        height_midpoints_cells1 = np.flip(height_midpoints_cells1)
        height_midpoints_cells.append(height_midpoints_cells1)
       
    height_midpoints_cells = pd.DataFrame(height_midpoints_cells).transpose()
    
    depth_midpoints_cells = [np.round((depth_cells[i+1]+depth_cells[i])/2,3) 
                             for i in range(len(depth_cells)-1)]
        
    
    #%% Determine Rouse slope and intercept for the whole transect
    
    # Interpolate between sampling abscissas
    id_start_middle_track = next(x-1 for x, val in enumerate(track) if val > abscissa_values[0])
    id_end_middle_track = next(x+1 for x, val in enumerate(track) if val > abscissa_values[-1])
    track_middle = track[id_start_middle_track:id_end_middle_track]
    
    intercept_middle = [np.interp(track_middle[i], abscissa_values, stats_Rouse_ele_profile['intercept'])
            for i in range(len(track_middle))]
    slope_middle = [np.interp(track_middle[i], abscissa_values, stats_Rouse_ele_profile['slope'])
            for i in range(len(track_middle))]
 
    # Extrapolate coefficients (constant)
    intercept_left = [intercept_middle[0]]*(id_start_middle_track-1)
    slope_left =[slope_middle[0]]*(id_start_middle_track-1)   
    intercept_right = [intercept_middle[-1]]*(len(track)-id_end_middle_track+1)
    slope_right =[slope_middle[-1]]*(len(track)-id_end_middle_track+1)
      
    # Concatenate coefficients for the entire interceptoss section
    intercept = np.concatenate((intercept_left, intercept_middle, intercept_right))
    slope = np.concatenate((slope_left, slope_middle, slope_right))
    
    #%% Calculate concentration per grid cell
    Ca_interp = []
    for i in range(len(track)):
        ca = 10**(intercept[i]-slope[i]*np.log10(
                    Za/(track_depth[i]-Za)))
        Ca_interp.append(ca) 
        
    Ca_interp = pd.DataFrame(Ca_interp).transpose()
    
    Conc_Rouse_cell = []
    for i in range(len(track)):  
        ccc = []
        for j in range(len(height_midpoints_cells)):
            cc = np.float(Ca_interp[i]*((
            (track_depth[i] - height_midpoints_cells.iloc[j,i])/ height_midpoints_cells.iloc[j,i])*
            (Za / (track_depth[i]- Za)))**slope[i])
            ccc.append(cc)
        Conc_Rouse_cell.append(ccc)
        
    Conc_Rouse_cell = pd.DataFrame(Conc_Rouse_cell).transpose() 
    Conc_Rouse_cell_arr = np.array(Conc_Rouse_cell)
    Conc_Rouse_cell.loc[:,0] = np.nan
    Conc_Rouse_cell.iloc[:,-1] = np.nan    
       
    # Calculate flux per cell 
    sand_flux_Rouse_kg_s = Conc_Rouse_cell*discharge
    sand_flux_Rouse_kg_s_arr = np.array(sand_flux_Rouse_kg_s)
    
    # Calculate total flux
    total_sand_flux_Rouse_kg_s = np.nansum(sand_flux_Rouse_kg_s)
    
    # Calculate mean concentration
    Conc_mean_Rouse = total_sand_flux_Rouse_kg_s/map_data.total_discharge       
    
    plt.plot(track, intercept)
    plt.plot(track, slope, '*')
    
#%% Plot Concentration per MAP cell 
    # Define attributs
    plot_data = Conc_Rouse_cell_arr
                
    max_limit=0
    
    x_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    x_pand = np.array([val for val in track for _ in (0, 1)])           
    for n in range(len(x_pand)):
        x_plt[:,n] = x_pand[n]
    
    cell_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    cell_pand = np.array([val for val in depth_cells for _ in (0, 1)][1:-1]) #1:-1
    for p in range(cell_pand.shape[0]):
        cell_plt[p,:] = cell_pand[p]
    
    speed_xpand = np.tile(np.nan, (plot_data.shape[0], 2 * (plot_data.shape[1])))
    
    for j in range(plot_data.shape[1]):
        speed_xpand[:,2*j] = plot_data[:,j]
        speed_xpand[:,2*j+1] = plot_data[:,j]
    
    Conc_plt = np.repeat(speed_xpand, 2, axis=0)
    
    # Main parameters
    plt.rcParams.update({'font.size': 14})
    
    ## Canvas
    main_wt_contour_canvas = MplCanvas(width=10, height=3, dpi=240)
    canvas = main_wt_contour_canvas
    fig = canvas.fig
    
    # Configure axis
    fig.ax = fig.add_subplot(1, 1, 1)
    fig.subplots_adjust(left=0.08, bottom=0.2, right=1, top=0.97, wspace=0.1, hspace=0)
    if max_limit == 0:
        if np.sum(Conc_plt[Conc_plt > -900]) > 0:
            max_limit = np.percentile(Conc_plt[Conc_plt > -900] , 99)
        else:
            max_limit = 1
                    
    x_fill = np.insert(track,0, -1)
    x_fill = np.append(x_fill, x_fill[-1]+1)
    depth_fill = np.insert(track_depth,0,0)
    depth_fill = np.append(depth_fill, 0)

    # Create color map
    import matplotlib.colors as colors
    cmap = cm.get_cmap('YlOrBr')
    cmap.set_under('white')
    # Generate color contour
    c = fig.ax.pcolormesh(x_plt, cell_plt, Conc_plt, cmap=cmap, norm = colors.LogNorm()) 
    
    # Add color bar and axis labels
    cb = fig.colorbar(c, pad=0.02, shrink=0.5, anchor=(0, 0.1))
    cb.ax.set_ylabel(canvas.tr('$C_{sand}$ (g/l)'))
    cb.ax.yaxis.label.set_fontsize(12)
    cb.ax.tick_params(labelsize=12)
    fig.ax.invert_yaxis()
    fig.ax.fill_between(x_fill, np.nanmax(track_depth)+2, depth_fill, color='w') # below bathy
    fig.ax.plot(track, track_depth, color='k', linewidth=1.5) # bathy
    
    fig.ax.set_xlabel(canvas.tr('Distance (m)'))
    fig.ax.set_ylabel(canvas.tr('Depth (m)'))
    fig.ax.xaxis.label.set_fontsize(12)
    fig.ax.yaxis.label.set_fontsize(12)
    fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
    fig.ax.set_ylim(top=0, bottom=np.nanmax(track_depth)+0.3)
    lower_limit = track[0]-0.5
    upper_limit = track[-1]+0.5
    
    fig.ax.set_xlim(left=lower_limit, right=upper_limit)
    
    for i in index_samplers_used:
        fig.ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
    legend = fig.ax.legend(loc = 'upper right', fontsize=fontsize_legend, 
                       title = 'Sampler', bbox_to_anchor=(1.15, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
    fig.ax.text(2.55, -0.03, '$\overline{C}_{sand}$ = ' + str(np.round(Conc_mean_Rouse,3)) + ' g/l', 
            transform = ax.transAxes, fontsize = fontsize_text)
    fig.ax.text(2.55, -0.13, '$\Phi_{total}$ = ' + str(np.round(total_sand_flux_Rouse_kg_s,0)) + ' kg/s', transform = ax.transAxes,
            fontsize = fontsize_text)
    
    canvas.draw()   
    show_figure(fig)    
    fig.savefig(outpath_figures + sampling_date + r'_Rouse_cross-sectional_concentration_' + most_frequent_sampler +'.png', dpi=300, bbox_inches='tight')

#%% Plot sand flux per MAP cell 
     # Calculate flux per cell 
    no_boundary_discharge = discharge = pd.concat([pd.DataFrame(map_data.no_boundary_left_cells_discharge), pd.DataFrame(map_data.no_boundary_middle_cells_discharge),
                           pd.DataFrame(map_data.no_boundary_right_cells_discharge)*(-1)], axis = 1)
    no_boundary_discharge.columns = np.arange(0,len(no_boundary_discharge.columns),1)
    sand_flux_Rouse_kg_s_plot = Conc_Rouse_cell*no_boundary_discharge
    sand_flux_Rouse_kg_s_arr_plot = np.array(sand_flux_Rouse_kg_s_plot)
  
    # Define attributs
    plot_data = sand_flux_Rouse_kg_s_arr_plot
                
    max_limit=0
    
    x_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    x_pand = np.array([val for val in track for _ in (0, 1)])           
    for n in range(len(x_pand)):
        x_plt[:,n] = x_pand[n]
    
    cell_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    cell_pand = np.array([val for val in depth_cells for _ in (0, 1)][1:-1])
    for p in range(cell_pand.shape[0]):
        cell_plt[p,:] = cell_pand[p]
    
    speed_xpand = np.tile(np.nan, (plot_data.shape[0], 2 * (plot_data.shape[1])))
    
    for j in range(plot_data.shape[1]):
        speed_xpand[:,2*j] = plot_data[:,j]
        speed_xpand[:,2*j+1] = plot_data[:,j]
    
    Conc_plt = np.repeat(speed_xpand, 2, axis=0)
    
    # Main parameters
    plt.rcParams.update({'font.size': 14})
    
    ## Canvas
    main_wt_contour_canvas = MplCanvas(width=10, height=3, dpi=240)
    canvas = main_wt_contour_canvas
    fig = canvas.fig
    
    # Configure axis
    fig.ax = fig.add_subplot(1, 1, 1)
    fig.subplots_adjust(left=0.08, bottom=0.2, right=1, top=0.97, wspace=0.1, hspace=0)
    if max_limit == 0:
        if np.sum(Conc_plt[Conc_plt > -900]) > 0:
            max_limit = np.percentile(Conc_plt[Conc_plt > -900] , 99)
        else:
            max_limit = 1
                    
    x_fill = np.insert(track,0, -1)
    x_fill = np.append(x_fill, x_fill[-1]+1)
    depth_fill = np.insert(track_depth,0,0)
    depth_fill = np.append(depth_fill, 0)
   
    # Create color map
    import matplotlib.colors as colors
    cmap = cm.get_cmap('pink_r')
    cmap.set_under('white')
    # Generate color contour
    c = fig.ax.pcolormesh(x_plt, cell_plt, Conc_plt, cmap=cmap, norm = colors.LogNorm()) 
    
    # Add color bar and axis labels
    cb = fig.colorbar(c, pad=0.02, shrink=0.5, anchor=(0, 0.1))
    cb.ax.set_ylabel(canvas.tr('$\Phi_{sand}$ (kg/s)'))
    cb.ax.yaxis.label.set_fontsize(12)
    cb.ax.tick_params(labelsize=12)
    fig.ax.invert_yaxis()
    fig.ax.fill_between(x_fill, np.nanmax(track_depth)+2, depth_fill, color='w') # below bathy
    fig.ax.plot(track, track_depth, color='k', linewidth=1.5) # bathy
    
    fig.ax.set_xlabel(canvas.tr('Distance (m)'))
    fig.ax.set_ylabel(canvas.tr('Depth (m)'))
    fig.ax.xaxis.label.set_fontsize(12)
    fig.ax.yaxis.label.set_fontsize(12)
    fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
    fig.ax.set_ylim(top=0, bottom=np.nanmax(track_depth)+0.3)
    lower_limit = track[0]-0.5
    upper_limit = track[-1]+0.5
    
    fig.ax.set_xlim(left=lower_limit, right=upper_limit)
    
    for i in index_samplers_used:
        fig.ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = 'P72')
        
    legend = fig.ax.legend(loc = 'upper right', fontsize=fontsize_legend, 
                        title = 'Sampler', bbox_to_anchor=(1.15, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
    fig.ax.text(2.55, -0.1, '$\Sigma \Phi_{sand}$ = ' + str(np.round(total_sand_flux_Rouse_kg_s,0)) + ' kg/s', transform = ax.transAxes,
            fontsize = fontsize_text)
    
    canvas.draw()   
    show_figure(fig)    
    fig.savefig(outpath_figures + sampling_date + r'_Rouse_cross-sectional_flux_' + most_frequent_sampler + '.png', dpi=300, bbox_inches='tight')

#%% # Export results
    Conc_Rouse_cell_export = Conc_Rouse_cell
    Conc_Rouse_cell_export.columns = [np.round(track,3)]
    Conc_Rouse_cell_export.index = [depth_midpoints_cells]
    Conc_Rouse_cell_export.to_csv(outpath + str(sampling_date) + '_Conc_Rouse_g_l_cell_' + most_frequent_sampler + '.csv', sep = ';')
    
    sand_flux_Rouse_kg_s_export = sand_flux_Rouse_kg_s
    sand_flux_Rouse_kg_s_export.columns = [np.round(track,3)]
    sand_flux_Rouse_kg_s_export.index = [depth_midpoints_cells]
    sand_flux_Rouse_kg_s_export.to_csv(outpath + str(sampling_date) + '_Sand_flux_Rouse_kg_s_cell_' + most_frequent_sampler + '.csv', sep = ';')
        
    summary_Rouse = pd.DataFrame([Conc_mean_Rouse, total_sand_flux_Rouse_kg_s]).transpose()
    summary_Rouse.columns = ['Conc_mean_Rouse', 'total_sand_flux_Rouse_kg_s']

    return analysis_data, summary_Rouse












