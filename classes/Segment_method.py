
"""
Calculate the mean cross-sectional sand concentration by averaging first vertically, 
laterally by attributing a wet surface. Calculate the total sand flux if discharge
data from an adjacent hydrometric station are available

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import pandas as pd
import numpy as np

def segment_method(analysis_data, outpath_figures, sampling_date, choice, Q_sampling = None):
    #%%
    if 'Sampling_height_final' in analysis_data.columns:
        pass
    else:
        analysis_data.insert(6, 'Sampling_height_final',
                              [analysis_data['vertical_depth'][i] - analysis_data['Sampling_depth_final'][i]
                                for i in range(len(analysis_data))])
       
    # Vertical integration (calculate mean concentration per vertical segment)
    mean_concentration_verticals = []
    wet_surface = []
    for j in np.unique(analysis_data['Abscissa_Name']):
        # Calculate height (vertical length) of each segment
        aa = analysis_data[analysis_data['Abscissa_Name'] == j]
        aa.reset_index(drop=True, inplace=True)
        
        height_segment = [aa['Sampling_height_final'][i]-aa['Sampling_height_final'][i+1]
                          for i in range(len(aa)-1)]
        #height_segment.insert(0,aa['max_depth_vertical'][0]-aa['Sampling_height_final'][0])
        
        # Perform vertical integration
        vert_integration = [height_segment[i] * (aa['Concentration_sand_g_l'][i]+aa['Concentration_sand_g_l'][i])/2
                            for i in range(len(height_segment))]
        mean_conc_vertical = np.nansum(vert_integration)/np.nansum(height_segment)
        mean_concentration_verticals.append(mean_conc_vertical)
     
        # Calculate wet surface per segment and cross-section 
        wet_surfacee = np.max((aa['NN_field_length']*aa['vertical_depth']))#.unique()
        wet_surface.append(wet_surfacee)
        
    wet_surface[0] = (wet_surface[0] + wet_surface[0]/2) # add left edge
    wet_surface[-1] = (wet_surface[-1] + wet_surface[-1]/2) # add right edge
    total_wet_surface = np.sum(wet_surface)
    
    # Calculate unity sand flux (g/l/m²) per segment and cross-section 
    sand_flux_segment = [mean_concentration_verticals[i]*wet_surface[i] for i in range(len(wet_surface))]
    total_sand_flux = np.sum(sand_flux_segment)
    
    # Calculate cross-sectional mean sand concentration 
    Conc_mean_segment = total_sand_flux/total_wet_surface
    
    #%% Calculate cross-sectional total sand flux
    if choice[5]==1:
        total_sand_flux_segment_kg_s = Conc_mean_segment*np.mean(Q_sampling['Value'])
        total_sand_flux_segment_t_h = total_sand_flux_segment_kg_s*3.6
        
    # Export data
    if choice[5]==1:
        summary_segment = pd.DataFrame([Conc_mean_segment, total_sand_flux_segment_kg_s, total_sand_flux_segment_t_h]).transpose()
        summary_segment.columns = ['Conc_mean_segment','total_sand_flux_segment_kg_s', 'total_sand_flux_segment_t_h']
    else:     
        summary_segment = pd.DataFrame([Conc_mean_segment]).transpose()
        summary_segment.columns = ['Conc_mean_segment']
    #%%
    return summary_segment
    




