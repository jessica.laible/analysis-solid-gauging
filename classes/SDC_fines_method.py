
"""
Inter- and extrapolates the concentration of fine sediments and the fine sediments/fine ratio
based on the SDC-method for each ADCP-cell (MAP)

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from UI.MplCanvas import MplCanvas
from scipy import stats
import matplotlib.cm as cm
from Common_functions import show_figure
import matplotlib.colors as colors
import matplotlib as mpl
from matplotlib.colors import ListedColormap as mpl_colors_ListedColormap 


def SDC_fines_method(analysis_data, map_data, outpath, outpath_figures, sampling_date): 
    
    abscissa_values = (analysis_data['Abscissa']).unique().tolist()
    depth_verticals = analysis_data['max_depth_vertical'].unique()
    
    #%%
    # Discharge
    discharge = pd.concat([pd.DataFrame(map_data.left_cells_discharge), pd.DataFrame(map_data.middle_cells_discharge),
                           pd.DataFrame(map_data.right_cells_discharge)], axis = 1)
    discharge.columns = np.arange(0,len(discharge.columns),1)
    
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S', 'Niskin']
    forms_sampler = ['o', 'o','D', 's', 'P', '*', 'v']  
    size_sampler = [6, 6, 6, 6, 7, 8, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                 'darkorange', 'darkred', 'yellowgreen']
    samplers_used = np.unique(analysis_data['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    id_sampler_used = [sampler.index(analysis_data['Sampler'][i])
          for i in range(len(analysis_data))]
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 14
    fontsize_legend_title = 14
    fontsize_text = 14
    fontsize_ticks = 12
    
    #%% SDC_fines profile     
    stats_SDC_fines_profile = []
    x_ranges_vertical = []
    
    for i in range(len(abscissa_values)):
        x = np.array(analysis_data['z_h'][analysis_data['Abscissa'] == abscissa_values[i]])
        Y = np.log(np.array(analysis_data['Concentration_fine_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]))
        array_x_vertical = np.linspace(0,1, num=100)
        x_ranges_vertical.append(array_x_vertical)
        
        res = stats.linregress(x, Y)
        # correct slope (increasing C with depth)
        res = list(res)
        if res[0] >= 0:
            res[0] = -0.01
            res[1] = np.mean(Y)
            res[2] = np.nan
            print('Slope and intercept corrected for vertical i = ' + str(i))        
        stats_SDC_fines_profile.append(res)
        
    x_ranges_vertical = pd.DataFrame(x_ranges_vertical) 
    stats_SDC_fines_profile = pd.DataFrame(stats_SDC_fines_profile)
    stats_SDC_fines_profile.columns = ['slope', 'intercept','rvalue', 'pvalue', 'stderr']
    stats_SDC_fines_profile['Cr_vertical'] = np.exp(stats_SDC_fines_profile['intercept'])
    stats_SDC_fines_profile['Crh_vertical'] = stats_SDC_fines_profile['Cr_vertical']/depth_verticals
    stats_SDC_fines_profile['alphah_vertical'] = -(stats_SDC_fines_profile['slope'])
    stats_SDC_fines_profile['alpha_vertical'] = stats_SDC_fines_profile['alphah_vertical']/depth_verticals
    stats_SDC_fines_profile['R2'] = stats_SDC_fines_profile['rvalue']**2
    Conc_vertical_profile = pd.DataFrame([np.exp(stats_SDC_fines_profile['intercept'][i])*np.exp(stats_SDC_fines_profile['slope'][i]*x_ranges_vertical.iloc[i,:])
              for i in range(len(stats_SDC_fines_profile))])
    
       
#%% Plot all vertical profiles in one graph
    markerss = ['o', 's', 'D', 'p', 'X', 'P', 'v', '<', '*']
    cmap = plt.cm.get_cmap('nipy_spectral')
    colorss = cmap(np.linspace(0,1,len(abscissa_values)))
    
    fig, ax = plt.subplots(figsize=(10, 6), dpi = 100)
    
    for i in range(len(abscissa_values)):
        ax.plot(Conc_vertical_profile.iloc[i,:], x_ranges_vertical.iloc[i,:],
                linestyle = '-', linewidth = 2, color = colorss[i],
                label = str(abscissa_values[i]))
           
        ax.plot(analysis_data['Concentration_fine_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
                   analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
                   color = colorss[i], linestyle = ' ',
                   markersize = size_sampler[id_sampler_used[i]]*1.5, marker = markerss[i],
                   markeredgewidth = 0.2, markeredgecolor = 'black')

    legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa', 
              loc = 'upper right',  facecolor = 'white', framealpha = 1, bbox_to_anchor = (1.17, 1))
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    
    ax.set_ylim(0,1)
    #ax.set_xlim(0)
    ax.set_ylabel('z/h (-)', fontsize = fontsize_axis, weight = 'bold')
    ax.set_xlabel(r'$\mathregular{C_{fine}}$ (g/l)', fontsize = fontsize_axis, weight = 'bold')
    ax.grid(linewidth = 0.2)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    fig.tight_layout()
    figname = '_Fine_conc_profiles_SDC_' + most_frequent_sampler
    fig.savefig(outpath_figures + sampling_date + figname + '.png', dpi = 200, bbox_inches='tight')
           
       
#%% # Define MAP grid
    cell_height = map_data.depth_cells_border[1][0]-map_data.depth_cells_border[0][0]  
    depth_cells = np.arange(map_data.depth_cells_border[0][0], (len(map_data.depth_cells_border)*cell_height), cell_height)
   
    # Calculate cell properties
    cell_height = map_data.border_depth[1] - map_data.border_depth[0]
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
    depth_cells = map_data.border_depth
  
    # Length and depth of transect (center of each distance cell, plus start (0) and end)
    # TODO shouldnt add 0 at the end/start : track = mid distance
    (map_data.depths - map_data.depth_cells_center).shape
    map_data.left_vertical_depth
    # Center of cell + max edges
    track = (border_cells[1:] + border_cells[:-1]) / 2
    # track = np.append(np.insert(track, 0, 0), border_cells[-1])

    # Depth on middle of the cell
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    # track_depth = np.append(np.insert(track_depth, 0, 0), 0)

    # Create height above bottom grid
    # TODO too short : not representative of depth (length should be 9)
    height_midpoints_cells = []
    for i in range(len(track_depth)):
        heh = int(track_depth[i]//cell_height)
        height_last_cell = (track_depth[i]-depth_cells[heh])
        midpoint_last_cell = np.array([height_last_cell/2])   # use half-distance between bottom and bottom of lowest complete cell for bottom cell
        midpoint_height_cells = np.arange(0, heh)*cell_height + (height_last_cell + cell_height/2)
        height_midpoints_cells1 = np.concatenate([midpoint_last_cell, midpoint_height_cells], axis=0)
        height_midpoints_cells1 = np.flip(height_midpoints_cells1)
        height_midpoints_cells.append(height_midpoints_cells1)
       
    height_midpoints_cells = pd.DataFrame(height_midpoints_cells).transpose()
    
    depth_midpoints_cells = [np.round((depth_cells[i+1]+depth_cells[i])/2, 3)
                             for i in range(len(depth_cells)-1)]

        
    #%% Determine Cr and alphah for the whole transect
    
    # Interpolate between sampling abscissas
    id_start_middle_track = next(x-1 for x, val in enumerate(track) if val > abscissa_values[0])
    id_end_middle_track = next(x+1 for x, val in enumerate(track) if val > abscissa_values[-1])
    track_middle = track[id_start_middle_track:id_end_middle_track]
    
    Crh_interp = [np.interp(track_middle[i], abscissa_values, stats_SDC_fines_profile['Crh_vertical'])
            for i in range(len(track_middle))]
    alpha_interp = [np.interp(track_middle[i], abscissa_values, stats_SDC_fines_profile['alpha_vertical'])
            for i in range(len(track_middle))]  
   
    Cr_middle = Crh_interp*track_depth[id_start_middle_track:id_end_middle_track]
    alphah_middle = alpha_interp*track_depth[id_start_middle_track:id_end_middle_track]

    # Extrapolate coefficients (constant)
    Cr_left = [Cr_middle[0]]*(id_start_middle_track-1)
    alphah_left =[alphah_middle[0]]*(id_start_middle_track-1)   
    Cr_right = [Cr_middle[-1]]*(len(track)-id_end_middle_track+1)
    alphah_right =[alphah_middle[-1]]*(len(track)-id_end_middle_track+1)
      
    # Concatenate coefficients for the entire cross section
    Cr = np.concatenate((Cr_left, Cr_middle, Cr_right))
    alphah = np.concatenate((alphah_left, alphah_middle, alphah_right))
       
    #%% Calculate concentration and flux
    
    # Calculate concentration in cells       
    Conc_SDC_cell = []
    for j in range(len(track)):
        ddd = []
        hh = np.max(height_midpoints_cells.iloc[:,j])
        zz_hh = height_midpoints_cells.iloc[:,j]/hh
        #zz_hh.iloc[-1] = 0
        for i in range(len(zz_hh)):            
            dd = Cr[j]*np.exp(-alphah[j]*zz_hh[i])
            ddd.append(dd)
        Conc_SDC_cell.append(ddd)   
    Conc_SDC_cell = pd.DataFrame(Conc_SDC_cell).transpose()
    Conc_SDC_cell_arr = np.array(Conc_SDC_cell)
    
    # Calculate flux per cell 
    fine_flux_SDC_kg_s = Conc_SDC_cell*discharge
    
    # Calculate total flux
    total_fine_flux_SDC_kg_s = np.nansum(fine_flux_SDC_kg_s)
    
    # Calculate mean concentration
    Conc_mean_SDC_fines = total_fine_flux_SDC_kg_s/map_data.total_discharge   
       
#%% Plot Concentration per MAP cell 
    # Define attributs
    plot_data = Conc_SDC_cell_arr                
    max_limit = 0

    x_plt = np.tile(np.nan, (2 * (depth_cells.shape[0]-1), 2 * (plot_data.shape[1])))
    x_pand = np.array([val for val in border_cells for _ in (0, 1)][1:-1])
    for n in range(len(x_pand)):
        x_plt[:, n] = x_pand[n]

    cell_plt = np.tile(np.nan, (2 * (depth_cells.shape[0]-1), 2 * (plot_data.shape[1]))) #depth_cells.shape
    cell_pand = np.array([val for val in depth_cells for _ in (0, 1)][1:-1])  # 1:-1
    for p in range(cell_pand.shape[0]):
        cell_plt[p, :] = cell_pand[p]

    speed_xpand = np.tile(np.nan, (depth_cells.shape[0]-1, 2 * (plot_data.shape[1])))
    for j in range(plot_data.shape[1]):
        # speed_j = np.array([val for val in plot_data[:, j] for _ in (0, 1)])
        len_plot = len(plot_data[:, j])
        speed_xpand[:len_plot, 2 * j] = plot_data[:, j]
        speed_xpand[:len_plot, 2 * j + 1] = plot_data[:, j]
    
    Conc_plt = np.repeat(speed_xpand, 2, axis=0)
    
    # Main parameters
    plt.rcParams.update({'font.size': 14})
    
    ## Canvas
    main_wt_contour_canvas = MplCanvas(width=10, height=6, dpi=240)
    canvas = main_wt_contour_canvas
    fig = canvas.fig
    
    # Configure axis
    fig.ax = fig.add_subplot(1, 1, 1)
    fig.subplots_adjust(left=0.08, bottom=0.2, right=1, top=0.97, wspace=0.1, hspace=0)
    if max_limit == 0:
        if np.sum(Conc_plt[Conc_plt > -900]) > 0:
            max_limit = np.percentile(Conc_plt[Conc_plt > -900] , 99)
        else:
            max_limit = 1
                    
    x_fill = np.insert(track,0, -1)
    x_fill = np.append(x_fill, x_fill[-1]+1)
    depth_fill = np.insert(track_depth,0,0)
    depth_fill = np.append(depth_fill, 0)

    # Create color map
    cmap = cm.get_cmap('YlOrBr')
    cmap.set_under('white')
    # Generate color contour
    c = fig.ax.pcolormesh(x_plt, cell_plt, Conc_plt, cmap=cmap, norm=colors.LogNorm())
    
    # Add color bar and axis labels
    cb = fig.colorbar(c, pad=0.02, shrink=0.7, anchor=(0, 0.1))
    cb.ax.set_ylabel(canvas.tr('$\mathregular{C_{fine}}$ (g/l)'), weight = 'bold')
    cb.ax.yaxis.label.set_fontsize(12)
    cb.ax.tick_params(labelsize=12)
    fig.ax.invert_yaxis()
    fig.ax.fill_between(x_fill, np.nanmax(track_depth)+2, depth_fill, color='w') # below bathy
    fig.ax.plot(track, track_depth, color='k', linewidth=1.5) # bathy
    
    fig.ax.set_xlabel(canvas.tr('Distance (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.set_ylabel(canvas.tr('Depth (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.xaxis.label.set_fontsize(12)
    fig.ax.yaxis.label.set_fontsize(12)
    fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
    fig.ax.set_ylim(top=0, bottom=np.nanmax(track_depth)+0.3)
    lower_limit = track[0]-0.5
    upper_limit = track[-1]+0.5
    
    fig.ax.set_xlim(left=lower_limit, right=upper_limit)
    
    for i in index_samplers_used:
        fig.ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
    legend = fig.ax.legend(loc = 'upper right', fontsize=fontsize_legend, 
                       title = 'Sampler', bbox_to_anchor=(1.17, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
    fig.ax.text(1.9, 0.12, '$\overline{C}_{fine}$ = ' + str(np.round(Conc_mean_SDC_fines,3)) + ' g/l', 
            transform = ax.transAxes, fontsize = fontsize_text)
    fig.ax.text(2.7, 0.12, '$\Phi_{total}$ = ' + str(np.round(total_fine_flux_SDC_kg_s,0)) + ' kg/s', transform = ax.transAxes,
            fontsize = fontsize_text)
    
    canvas.draw()   
    show_figure(fig)    
    fig.savefig(outpath_figures + sampling_date + r'_Cross-sectional_fine_concentration_SDC_' + most_frequent_sampler + '.png', dpi=300, bbox_inches='tight')
 
    #%% Plot fine flux per MAP cell 
     # Calculate flux per cell 
    no_boundary_discharge = discharge = pd.concat([pd.DataFrame(map_data.no_boundary_left_cells_discharge), pd.DataFrame(map_data.no_boundary_middle_cells_discharge),
                           pd.DataFrame(map_data.no_boundary_right_cells_discharge)*(-1)], axis = 1)
    no_boundary_discharge.columns = np.arange(0,len(no_boundary_discharge.columns),1)
    fine_flux_linear_kg_s_plot = pd.DataFrame(Conc_SDC_cell.values*no_boundary_discharge.values, columns=Conc_SDC_cell.columns, index=Conc_SDC_cell.index)
    #Conc_SDC_cell*no_boundary_discharge
    fine_flux_linear_kg_s_arr_plot = np.array(fine_flux_linear_kg_s_plot)

    # Define attributs
    plot_data = fine_flux_linear_kg_s_arr_plot
                
    max_limit=0
    
    x_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    x_pand = np.array([val for val in track for _ in (0, 1)])           
    for n in range(len(x_pand)):
        x_plt[:,n] = x_pand[n]
    
    cell_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    cell_pand = np.array([val for val in depth_cells for _ in (0, 1)][1:-1]) # 1:-1
    for p in range(cell_pand.shape[0]):
        cell_plt[p,:] = cell_pand[p]
    
    speed_xpand = np.tile(np.nan, (plot_data.shape[0], 2 * (plot_data.shape[1])))
    
    for j in range(plot_data.shape[1]):
        speed_xpand[:,2*j] = plot_data[:,j]
        speed_xpand[:,2*j+1] = plot_data[:,j]
    
    Conc_plt = np.repeat(speed_xpand, 2, axis=0)
    
    # Main parameters
    plt.rcParams.update({'font.size': 14})
    
    ## Canvas
    main_wt_contour_canvas = MplCanvas(width=10, height=6, dpi=240)
    canvas = main_wt_contour_canvas
    fig = canvas.fig
    
    # Configure axis
    fig.ax = fig.add_subplot(1, 1, 1)
    fig.subplots_adjust(left=0.08, bottom=0.2, right=1, top=0.97, wspace=0.1, hspace=0)
    if max_limit == 0:
        if np.sum(Conc_plt[Conc_plt > -900]) > 0:
            max_limit = np.percentile(Conc_plt[Conc_plt > -900] , 99)
        else:
            max_limit = 1
                    
    x_fill = np.insert(track,0, -1)
    x_fill = np.append(x_fill, x_fill[-1]+1)
    depth_fill = np.insert(track_depth,0,0)
    depth_fill = np.append(depth_fill, 0)
    
    # Create color map
    cmap = cm.get_cmap('YlOrBr')
    cmap.set_under('white')
    # Generate color contour
    c = fig.ax.pcolormesh(x_plt, cell_plt, Conc_plt, cmap=cmap, norm = colors.LogNorm()) 
    
    # Add color bar and axis labels
    cb = fig.colorbar(c, pad=0.02, shrink=0.7, anchor=(0, 0.1))
    cb.ax.set_ylabel(canvas.tr('$\mathregular{\Phi_{fine}}$ (g/l)'),fontsize = fontsize_axis, weight = 'bold')
    cb.ax.yaxis.label.set_fontsize(12)
    cb.ax.tick_params(labelsize=12)
    fig.ax.invert_yaxis()
    fig.ax.fill_between(x_fill, np.nanmax(track_depth)+2, depth_fill, color='w') # below bathy
    fig.ax.plot(track, track_depth, color='k', linewidth=1.5) # bathy
    
    fig.ax.set_xlabel(canvas.tr('Distance (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.set_ylabel(canvas.tr('Depth (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.xaxis.label.set_fontsize(12)
    fig.ax.yaxis.label.set_fontsize(12)
    fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
    fig.ax.set_ylim(top=0, bottom=np.nanmax(track_depth)+0.3)
    lower_limit = track[0]-0.5
    upper_limit = track[-1]+0.5
    
    fig.ax.set_xlim(left=lower_limit, right=upper_limit)
    
    for i in index_samplers_used:
        fig.ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
    legend = fig.ax.legend(loc = 'upper right', fontsize=fontsize_legend, 
                       title = 'Sampler', bbox_to_anchor=(1.18, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
    fig.ax.text(2.52,0.12, '$\Sigma \Phi_{fine}$ = ' + str(np.round(total_fine_flux_SDC_kg_s,3)) + ' kg/s', transform = ax.transAxes,
            fontsize = fontsize_text)
    
    canvas.draw()   
    show_figure(fig)    
    fig.savefig(outpath_figures + sampling_date + r'_Cross-sectional_fine_flux_SDC_ ' + most_frequent_sampler +'.png', dpi=300, bbox_inches='tight') 
    
    
#%% # Export results
    Conc_fine_cell_export = Conc_SDC_cell
    Conc_fine_cell_export.columns = [np.round(track,3)]
    Conc_fine_cell_export.index = [depth_midpoints_cells]
    Conc_fine_cell_export.to_csv(outpath + str(sampling_date) + '_Fine_conc_SDC_g_l_cell_' + most_frequent_sampler +'.csv', sep = ';')
    
    fine_flux_kg_s_export = fine_flux_SDC_kg_s
    fine_flux_kg_s_export.columns = [np.round(track,3)]
    fine_flux_kg_s_export.index = [depth_midpoints_cells]
    fine_flux_kg_s_export.to_csv(outpath + str(sampling_date) + '_Fine_flux_SDC_kg_s_cell_' + most_frequent_sampler +'.csv', sep = ';')
        
    summary_fine = pd.DataFrame([Conc_mean_SDC_fines, total_fine_flux_SDC_kg_s]).transpose()
    summary_fine.columns = ['Conc_mean_SDC_fines_g_l', 'total_fine_flux_SDC_kg_s']

################################################################################  
    
    #%% ratio profile     
    stats_ratio_profile = []
    x_ranges_ratio = []
    
    for i in range(len(abscissa_values)):
        x = np.array(analysis_data['z_h'][analysis_data['Abscissa'] == abscissa_values[i]])
        Y = np.log(np.array(analysis_data['Cfine_Csand'][analysis_data['Abscissa'] == abscissa_values[i]]))
        array_x_ratio = np.linspace(0,1, num=100)
        x_ranges_ratio.append(array_x_ratio)
        
        res = stats.linregress(x, Y)
        # correct slope
        res = list(res)
        if res[0] <= 0:
            res[0] = 0.2
            res[1] = np.mean(Y)
            res[2] = np.nan
            print('Slope and intercept of the ratio profile corrected for vertical i = ' + str(i))
        stats_ratio_profile.append(res)
                
    x_ranges_ratio = pd.DataFrame(x_ranges_ratio) 
    stats_ratio_profile = pd.DataFrame(stats_ratio_profile)
    stats_ratio_profile.columns = ['slope', 'intercept','rvalue', 'pvalue', 'stderr']
    stats_ratio_profile['Cr_ratio'] = np.exp(stats_ratio_profile['intercept'])
    stats_ratio_profile['Crh_ratio'] = stats_ratio_profile['Cr_ratio']/depth_verticals
    stats_ratio_profile['alphah_ratio'] = -(stats_ratio_profile['slope'])
    stats_ratio_profile['alpha_ratio'] = stats_ratio_profile['alphah_ratio']/depth_verticals
    stats_ratio_profile['R2'] = stats_ratio_profile['rvalue']**2
    Conc_ratio_ratio_profile = pd.DataFrame([np.exp(stats_ratio_profile['intercept'][i])*np.exp(stats_ratio_profile['slope'][i]*x_ranges_ratio.iloc[i,:])
              for i in range(len(stats_ratio_profile))])
       
       
#%% Plot all ratio profiles in one graph
    cmap = plt.cm.get_cmap('nipy_spectral')
    colorss = cmap(np.linspace(0,1,len(abscissa_values)))
    
    fig, ax = plt.subplots(figsize=(10, 6), dpi = 100)
    
    for i in range(len(abscissa_values)):
        ax.plot(Conc_ratio_ratio_profile.iloc[i,:], x_ranges_ratio.iloc[i,:],
                linestyle = '-', linewidth = 2, color = colorss[i],
                label = str(abscissa_values[i]))
           
        ax.plot(analysis_data['Cfine_Csand'][analysis_data['Abscissa']== abscissa_values[i]], 
                   analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
                   color = colorss[i], linestyle = ' ',
                   markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                   markeredgewidth = 0.2, markeredgecolor = 'black')
    
    legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa', 
              loc = 'upper right',  facecolor = 'white', framealpha = 1,bbox_to_anchor = (1.2, 1))
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    
    ax.set_ylim(0,1)
    ax.set_xlim(0)
    ax.set_ylabel('z/h (-)', fontsize = fontsize_axis, weight = 'bold')
    ax.set_xlabel('$\mathregular{C_{fine}/C_{sand}}$', fontsize = fontsize_axis, weight = 'bold')
    ax.grid(linewidth = 0.2)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    fig.tight_layout()
    figname = '_Ratio_profiles_' + most_frequent_sampler
    fig.savefig(outpath_figures + sampling_date + figname + '.png', dpi = 200, bbox_inches='tight')
        
        #%% log-scale
    cmap = plt.cm.get_cmap('nipy_spectral')
    colorss = cmap(np.linspace(0,1,len(abscissa_values)))
    
    fig, ax = plt.subplots(figsize=(10, 6), dpi = 100)
    
    for i in range(len(abscissa_values)):
        ax.plot(Conc_ratio_ratio_profile.iloc[i,:], x_ranges_ratio.iloc[i,:],
                linestyle = '-', linewidth = 2, color = colorss[i],
                label = str(abscissa_values[i]))
           
        ax.plot(analysis_data['Cfine_Csand'][analysis_data['Abscissa']== abscissa_values[i]], 
                   analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
                   color = colorss[i], linestyle = ' ',
                   markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
                   markeredgewidth = 0.2, markeredgecolor = 'black')
    
    legend = ax.legend(fontsize = fontsize_legend, title = 'Abscissa', 
              loc = 'upper right',  facecolor = 'white', framealpha = 1, bbox_to_anchor = (1.2, 1))
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    
    ax.set_xscale('log')
    ax.set_ylim(0,1)
    #ax.set_xlim(0)
    ax.set_ylabel('z/h (-)', fontsize = fontsize_axis, weight = 'bold')
    ax.set_xlabel('$\mathregular{C_{fine}/C_{sand}}$', fontsize = fontsize_axis, weight = 'bold')
    ax.grid(linewidth = 0.2)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    fig.tight_layout()
    figname = '_Ratio_profiles_log_' + most_frequent_sampler
    fig.savefig(outpath_figures + sampling_date + figname + '.png', dpi = 200, bbox_inches='tight')
                             
    #%% Plot ratio profiles for each abscissa   
    # for i in range(len(abscissa_values)):
    #     fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
    #     ax.invert_yaxis()
    #     ax.plot(Conc_ratio_ratio_profile.iloc[i,:], x_ranges_ratio.iloc[i,:],
    #             linestyle = '-', linewidth = 2, color = colorss[i])
        
    #     ax.plot(analysis_data['Cfine_Cfine'][analysis_data['Abscissa']== abscissa_values[i]], 
    #                analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
    #                color = colorss[i], linestyle = ' ',
    #                markersize = size_sampler[id_sampler_used[i]]*1.5, marker = forms_sampler[id_sampler_used[i]],
    #                markeredgewidth = 0.2, markeredgecolor = 'black')
        
    #     a = np.round(stats_ratio_profile['Cr_ratio'][i], 2)
    #     b = np.round(stats_ratio_profile['alphah_ratio'][i], 2)
    #     ax.text(0.75, 0.92, '$y=%3.7s*e^{%3.7sx}$'%(a, b),
    #             transform=ax.transAxes, fontsize = fontsize_text)
    #     ax.text(0.75, 0.85, '${R²}$ = ' + str(np.round(stats_ratio_profile['R2'][i], 2)), 
    #             transform=ax.transAxes, fontsize = fontsize_text)       
        
    #     ax.set_ylim(0,1)
    #     #ax.set_xlim(0)
    #     ax.set_ylabel('z/h (-)', fontsize = fontsize_axis)
    #     ax.set_xlabel('$C_{fine}/C_{fine}$', fontsize = fontsize_axis)
    #     ax.grid(linewidth = 0.2)
    #     ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    #     fig.tight_layout()
    #     figname = sampling_date + '_Ratio_profiles_' 
    #     fig.savefig(outpath_figures + figname + str(abscissa_values[i]) + '_' + most_frequent_sampler +'.png', dpi = 200, bbox_inches='tight') 
                
    #%% Determine Cr and alphah for the whole transect
    
    # Interpolate between sampling abscissas
    id_start_middle_track = next(x-1 for x, val in enumerate(track) if val > abscissa_values[0])
    id_end_middle_track = next(x+1 for x, val in enumerate(track) if val > abscissa_values[-1])
    track_middle = track[id_start_middle_track:id_end_middle_track]
    
    Crh_interp = [np.interp(track_middle[i], abscissa_values, stats_ratio_profile['Crh_ratio'])
            for i in range(len(track_middle))]
    alpha_interp = [np.interp(track_middle[i], abscissa_values, stats_ratio_profile['alpha_ratio'])
            for i in range(len(track_middle))]  
   
    Cr_middle = Crh_interp*track_depth[id_start_middle_track:id_end_middle_track]
    alphah_middle = alpha_interp*track_depth[id_start_middle_track:id_end_middle_track]

    # Extrapolate coefficients (constant)
    Cr_left = [Cr_middle[0]]*(id_start_middle_track-1)
    alphah_left =[alphah_middle[0]]*(id_start_middle_track-1)   
    Cr_right = [Cr_middle[-1]]*(len(track)-id_end_middle_track+1)
    alphah_right =[alphah_middle[-1]]*(len(track)-id_end_middle_track+1)
      
    # Concatenate coefficients for the entire cross section
    Cr = np.concatenate((Cr_left, Cr_middle, Cr_right))
    alphah = np.concatenate((alphah_left, alphah_middle, alphah_right))
       
    #%% Calculate concentration and flux
    
    # Calculate concentration in cells       
    ratio_cell = []
    for j in range(len(track)):
        ddd = []
        hh = np.max(height_midpoints_cells.iloc[:,j])
        zz_hh = height_midpoints_cells.iloc[:,j]/hh
        #zz_hh.iloc[-1] = 0
        for i in range(len(zz_hh)):            
            dd = Cr[j]*np.exp(-alphah[j]*zz_hh[i])
            ddd.append(dd)
        ratio_cell.append(ddd)   
    ratio_cell = pd.DataFrame(ratio_cell).transpose()
    
    # Change index and colnames
    index_list = list(Conc_SDC_cell.index.values)
    colnames_list = list(Conc_SDC_cell.columns)  
    colnames_list = [item for t in colnames_list for item in t]
   
    ratio_cell['Index'] = index_list #[item for t in index_list for item in t]
    ratio_cell = ratio_cell.set_index(['Index'])    
    ratio_cell.set_axis(colnames_list, axis=1,inplace=True)
    ratio_cell_arr = np.array(ratio_cell)
       
    # Calculate mean ratio
    mean_ratio = ratio_cell.mean().mean()
       
#%% Plot Concentration per MAP cell 
    # Define attributs
    plot_data = ratio_cell_arr
                
    max_limit=0
    
    x_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    x_pand = np.array([val for val in track for _ in (0, 1)])          
    for n in range(len(x_pand)):
        x_plt[:,n] = x_pand[n]
    
    cell_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
    cell_pand = np.array([val for val in depth_cells for _ in (0, 1)][1:-1])
    for p in range(cell_pand.shape[0]):
        cell_plt[p,:] = cell_pand[p]
    
    speed_xpand = np.tile(np.nan, (plot_data.shape[0], 2 * (plot_data.shape[1])))
    
    for j in range(plot_data.shape[1]):
        speed_xpand[:,2*j] = plot_data[:,j]
        speed_xpand[:,2*j+1] = plot_data[:,j]
    
    Conc_plt = np.repeat(speed_xpand, 2, axis=0)
    
    # Main parameters
    plt.rcParams.update({'font.size': 14})
    
    ## Canvas
    main_wt_contour_canvas = MplCanvas(width=10, height=6, dpi=240)
    canvas = main_wt_contour_canvas
    fig = canvas.fig
    
    # Configure axis
    fig.ax = fig.add_subplot(1, 1, 1)
    fig.subplots_adjust(left=0.08, bottom=0.2, right=1, top=0.97, wspace=0.1, hspace=0)
    if max_limit == 0:
        if np.sum(Conc_plt[Conc_plt > -900]) > 0:
            max_limit = np.percentile(Conc_plt[Conc_plt > -900] , 99)
        else:
            max_limit = 1
                    
    x_fill = np.insert(track,0, -1)
    x_fill = np.append(x_fill, x_fill[-1]+1)
    depth_fill = np.insert(track_depth,0,0)
    depth_fill = np.append(depth_fill, 0)
   
    # Create color map
    nb_couleurs = 14  
    viridis = cm.get_cmap('RdBu_r', nb_couleurs) 
    echelle_colorimetrique = viridis(np.linspace(0, 1, nb_couleurs))
    vecteur_blanc = np.array([1, 1, 1, 1])
    echelle_colorimetrique[5:6,:] = vecteur_blanc  
    cmap = mpl_colors_ListedColormap(echelle_colorimetrique)
    cmap.set_under('black')
    cmap.set_over('saddlebrown')  # 
    cbounds = [0.01, 0.1, 0.2, 0.4, 0.6, 0.8, 1.2, 2, 3, 4, 5, 6, 7, 10]
    norm = mpl.colors.BoundaryNorm(cbounds, cmap.N)
    # Generate color contour
    c = fig.ax.pcolormesh(x_plt, cell_plt, Conc_plt, cmap=cmap, norm = norm) 
    
    # Add color bar and axis labels
    cb = fig.colorbar(c, pad=0.02, shrink = 0.8, anchor=(0, -0.1), ticks=[0.01, 0.1, 0.2, 0.4, 0.6, 0.8, 1.2, 2, 3, 4, 5, 6, 7, 10])
    cb.ax.set_yticklabels(['0.01','0.1','0.2','0.4','0.6','0.8','1.2','2.0','3.0', '4.0', '5.0','6.0','7.0', '10.0'],ha='right')
    cb.ax.yaxis.set_tick_params(pad=35) 
    cb.ax.set_ylabel(canvas.tr('$\mathregular{C_{fine}/C_{sand}}$'), fontsize = fontsize_axis, weight = 'bold')
    cb.ax.yaxis.label.set_fontsize(12)
    cb.ax.tick_params(labelsize=12)
    fig.ax.invert_yaxis()
    fig.ax.fill_between(x_fill, np.nanmax(track_depth)+2, depth_fill, color='w') # below bathy
    fig.ax.plot(track, track_depth, color='k', linewidth=1.5) # bathy
    
    fig.ax.set_xlabel(canvas.tr('Distance (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.set_ylabel(canvas.tr('Depth (m)'), fontsize = fontsize_axis, weight = 'bold')
    fig.ax.xaxis.label.set_fontsize(12)
    fig.ax.yaxis.label.set_fontsize(12)
    fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
    fig.ax.set_ylim(top=0, bottom=np.nanmax(track_depth)+0.3)
    lower_limit = track[0]-0.5
    upper_limit = track[-1]+0.5
    
    fig.ax.set_xlim(left=lower_limit, right=upper_limit)
    
    for i in index_samplers_used:
        fig.ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
    legend = fig.ax.legend(loc = 'upper right', fontsize=fontsize_legend, 
                        title = 'Sampler', bbox_to_anchor=(1.17, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
    fig.ax.text(2, 0.12, '$\overline{C_{fine}/C_{sand}}$ = ' + str(np.round(mean_ratio,3)), 
            transform = ax.transAxes, fontsize = fontsize_text)
    
    canvas.draw()   
    show_figure(fig)    
    fig.savefig(outpath_figures + sampling_date + r'_Ratio_cross-section_' + most_frequent_sampler +'.png', dpi=300, bbox_inches='tight')

#%%
    return analysis_data, summary_fine, Conc_fine_cell_export, fine_flux_kg_s_export, ratio_cell
