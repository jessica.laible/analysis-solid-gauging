"""
Calculate the concentration for each line in the prep_data-file
for P6, P72 samplers, buckets and pumps using given data 
(dry mass, volume or concentration) separately for fine sediments 
and sands

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
# Load packages
import numpy as np

def calculate_concentration(meas, unit):
    if 'Volume_rinsing_sand' in meas.index:
        meas['Volume_rinsing_sand'] =  meas['Volume_rinsing_sand'].fillna(0)
    if 'Volume_rinsing_fines' in meas.index:   
        meas['Volume_rinsing_fines'] =  meas['Volume_rinsing_fines'].fillna(0)
    unit_mass = unit.unit_masses
    unit_vol = unit.unit_volume
    
    concentration_sand_i = []
    concentration_fine_i = []
    if meas['Sand_concentration'] == np.nan:
        if unit_mass == 'mg': # mass in mg
            if unit_vol == 'mL': # volume in ml
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
                cf = (meas['Dry_mass_fines'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_fines'])))
                concentration_fine_i.append(cf)
            if unit_vol == 'L': # volume in l
                cs = (meas['Dry_mass_sand']*0.001)/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
                cf = (meas['Dry_mass_fines']*0.001)/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_fines'])))
                concentration_fine_i.append(cf)
        if unit_mass == 'g': # mass in g
            if unit_vol == 'mL': # volume in ml
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*0.001*
                     (meas['Sample_volume']*0.001/(meas['Sample_volume']*0.001+
                                                         meas['Volume_rinsing_sand']*0.001)))
                concentration_sand_i.append(cs)
                cf = (meas['Dry_mass_fines'])/(meas['Sample_volume']*0.001*
                     (meas['Sample_volume']*0.001/(meas['Sample_volume']*0.001+
                                                         meas['Volume_rinsing_fines']*0.001)))
            concentration_fine_i.append(cf)
            if unit_vol == 'L': # volume in l
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
                cf = (meas['Dry_mass_fines'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_fines'])))
                concentration_fine_i.append(cf)
    # else:
    #     concentration_sand_i.append(np.nan) 
    else:
        if unit_mass == 'mg': # mass in mg
            if unit_vol == 'mL': # volume in ml
                concentration_sand_i.append(meas['Sand_concentration'])
                concentration_fine_i.append(meas['Fine_concentration'])
            if unit_vol == 'L': # volume in l
                concentration_sand_i.append(meas['Sand_concentration']*0.001)
                concentration_fine_i.append(meas['Fine_concentration']*0.001)
        if unit_mass == 'g': # mass in g
            if unit_vol == 'mL': # volume in ml
                concentration_sand_i.append(meas['Sand_concentration']*1000)
                concentration_fine_i.append(meas['Fine_concentration']*1000)
            if unit_vol == 'L': # volume in l
                concentration_sand_i.append(meas['Sand_concentration'])
                concentration_fine_i.append(meas['Fine_concentration'])   
    
    return (concentration_sand_i, concentration_fine_i)

#%% Calculate concentration for surface samples (S)

def calculate_concentration_S(meas, unit):
    if 'Volume_rinsing_sand' in meas.index:
        meas['Volume_rinsing_sand'] =  meas['Volume_rinsing_sand'].fillna(0)
    else:
        meas['Volume_rinsing_sand'] = 0
    if 'Volume_rinsing_fines' in meas.index:   
        meas['Volume_rinsing_fines'] =  meas['Volume_rinsing_fines'].fillna(0)
    unit_mass = unit.unit_masses
    unit_vol = unit.unit_volume
    indexx = meas.index.values.tolist()
    
    # if Concentration_sand given
    concentration_sand_i = []
    concentration_fine_i = []
    if 'Sand_concentration' in indexx and meas['Sand_concentration'] == np.nan:
        if unit_mass == 'mg': # mass in mg
            if unit_vol == 'mL': # volume in ml
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
            if unit_vol == 'L': # volume in l
                cs = (meas['Dry_mass_sand']*0.001)/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
        if unit_mass == 'g': # mass in g
            if unit_vol == 'mL': # volume in ml
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*0.001*
                     (meas['Sample_volume']*0.001/(meas['Sample_volume']*0.001+
                                                         meas['Volume_rinsing_sand']*0.001)))
                concentration_sand_i.append(cs)
            if unit_vol == 'L': # volume in l
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
    elif 'Fine_concentration' in indexx and meas['Fine_concentration'] == np.nan:
        if unit_mass == 'mg': # mass in mg
            if unit_vol == 'mL': # volume in ml                
                cf = (meas['Dry_mass_fines'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_fines'])))
                concentration_fine_i.append(cf)
            if unit_vol == 'L': # volume in l
                cf = (meas['Dry_mass_fines']*0.001)/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_fines'])))
                concentration_fine_i.append(cf)
        if unit_mass == 'g': # mass in g
            if unit_vol == 'mL': # volume in ml
                cf = (meas['Dry_mass_fines'])/(meas['Sample_volume']*0.001*
                     (meas['Sample_volume']*0.001/(meas['Sample_volume']*0.001+
                                                         meas['Volume_rinsing_fines']*0.001)))
            concentration_fine_i.append(cf)
            if unit_vol == 'L': # volume in l
                cf = (meas['Dry_mass_fines'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_fines'])))
                concentration_fine_i.append(cf)
    elif 'Dry_mass_sand' in indexx: 
        if unit_mass == 'mg': # mass in mg
            if unit_vol == 'mL': # volume in ml
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
            if unit_vol == 'L': # volume in l
                cs = (meas['Dry_mass_sand']*0.001)/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
        if unit_mass == 'g': # mass in g
            if unit_vol == 'mL': # volume in ml
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*0.001*
                     (meas['Sample_volume']*0.001/(meas['Sample_volume']*0.001+
                                                         meas['Volume_rinsing_sand']*0.001)))
                concentration_sand_i.append(cs)
            if unit_vol == 'L': # volume in l
                cs = (meas['Dry_mass_sand'])/(meas['Sample_volume']*
                     (meas['Sample_volume']/(meas['Sample_volume']+
                                                         meas['Volume_rinsing_sand'])))
                concentration_sand_i.append(cs)
                
    if concentration_fine_i == []:
        concentration_fine_i = np.nan
    concentration_sand_i = np.float(concentration_sand_i[0])
    
    # Convert to flux if BD is most frequent sampler and no ADCP used
    time_sampling = 2 # s
    surface_sampling = 0.2 * 0.1 # m²
    sand_flux_i = concentration_sand_i/1000 
    
    return (concentration_sand_i, concentration_fine_i)






























