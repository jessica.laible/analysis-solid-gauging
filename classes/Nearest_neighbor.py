"""
Calculating fine sediment and sand concentration or flux per field around each sampling point
and the total sediment flux or mean concentration if ADCP-data or water discharge data
from an adjacent station are available

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import pandas as pd
from mycolorpy import colorlist as mcp
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import copy
import tkinter as tk
from tkinter import messagebox
import sys

def nearest_neighbor_method(outpath_figures, analysis_data, choice, unit, sampling_date, Q_sampling, map_data = None): 
    #%%    
    ### Define NN fields
    # Calculate half distances between the verticals
    analysis_data = analysis_data.sort_values(['Abscissa', 'Sampling_depth_final'])
    analysis_data.reset_index(drop=True, inplace=True)
    abscissa_values = analysis_data['Abscissa'].unique().tolist()
    half_dis = [sum(abscissa_values[i:i + 2]) / 2
                for i in range(len(abscissa_values) - 1)]
    abscissa_field = half_dis[:]
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    
    # Add left and right bank
    if choice[3] == 1:
        # Calculate cell properties
        cell_height = map_data.border_depth[1] - map_data.border_depth[0]
        mid = map_data.borders_ens + map_data.left_distance
        border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
        depth_cells = map_data.border_depth

        # Length and depth of transect (center of each distance cell, plus start (0) and end)
        track = (border_cells[1:] + border_cells[:-1]) / 2
        track = np.append(np.insert(track, 0, 0), border_cells[-1])
       
        track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                                map_data.right_vertical_depth[0, :])
        track_depth = np.append(np.insert(track_depth, 0, 0), 0)
        
        abscissa_field.insert(0, abscissa_values[0]-(abscissa_field[0]-abscissa_values[0]))                              
        abscissa_field.append(abscissa_values[-1]+ abscissa_values[-1]- abscissa_field[-1])
    else:
        abscissa_field.insert(0, 0)
        
        class ask_float():
            def __init__(self, text: str = '', title: str = '', default_value:str=''):
                self.value = None
                self.ws = tk.Tk()
                self.ws.title(title)
                self.ws.geometry("250x100")
                self.ws.wm_attributes("-topmost", 1)
                self.ws.bind("<Return>", self.generate)
                self.num1 = tk.StringVar(value=default_value)
                self.label = tk.Label(self.ws, text=text)
                self.label.pack()
                self.entry1 = tk.Entry(self.ws, textvariable=self.num1)
                self.entry1.pack()
                self.button = tk.Button(self.ws, text='Ok [Return]', command=self.generate, padx=50, pady=10)
                self.button.pack()
                self.ws.mainloop()
    
            def generate(self, event=None):
                try:
                    result = float(self.num1.get())
                    self.value = result
                    self.ws.destroy()
                    print(self.value)
                except Exception as ex:
                    messagebox.showwarning('warning', ex)
    
        test = ask_float(text='Channel width (m)', title='Channel width (m)', default_value = str(abscissa_values[-1]+10))
        abscissa_field.append(test.value)
    
    # Calculate left and right limit (abscissa) for each field
    for i in np.unique(analysis_data['Abscissa_Name']):
        analysis_data.loc[analysis_data['Abscissa_Name'] ==
                          i, 'min_abscissa_NN_field'] = abscissa_field[i - 1]
    for i in np.unique(analysis_data['Abscissa_Name']):
        analysis_data.loc[analysis_data['Abscissa_Name']
                          == i, 'max_abscissa_NN_field'] = abscissa_field[i]
        
    # Define height, top and bottom of the fields
    field_top = []
    field_bottom = []
    field_height = []
    for k in np.unique(analysis_data['Abscissa_Name']):
        f = analysis_data[analysis_data['Abscissa_Name'] == k]
        f.reset_index(drop=True, inplace=True)
        l = len(f)

        # Calculate half height between sampling points
        half_height_per_vert = [(f['Sampling_depth_final'][i + 1] - f['Sampling_depth_final'][i]) / 2
                                for i in range(len(f['Sampling_depth_final']) - 1)]
        half_height_per_vert.insert(0, f['Sampling_depth_final'][0])
        
        if choice[3] == 0:
            #if analysis_data['Height_field'] is not np.nan:
            # analysis_data['vertical_depth'] = analysis_data['Sampling_depth_final'] + analysis_data['Height_field']
            # for id in analysis_data.loc[analysis_data['vertical_depth'].isna()].index:
            #     x = analysis_data.iloc[id]['Abscissa']
            #     depth_value = analysis_data[analysis_data['Abscissa'] == x]['vertical_depth'].mean()
            #     analysis_data.loc[id, 'vertical_depth'] = depth_value
                
            half_height_per_vert.append(
                f['vertical_depth'][0] - f['Sampling_depth_final'][l - 1])

          
        if choice[3] == 1:
            if f['max_depth_vertical'][0] < f['Sampling_depth_final'][l - 1]:
                sys.exit(
                    "Error message: Deepest sampling point below ADCP bottom depth! Adjust last field depth for analysis")
    
            half_height_per_vert.append(
                f['max_depth_vertical'][0] - f['Sampling_depth_final'][l - 1])


        # Calculating height of field around each sampling point
        field_height_vert = [round((half_height_per_vert[i + 1] + half_height_per_vert[i]), 2)
                             for i in range(len(half_height_per_vert) - 1)]
        field_height.append(field_height_vert)

        # Field top
        field_top_vert = [f['Sampling_depth_final'][i] - half_height_per_vert[i]
                          for i in range(len(f))]
        field_top.append(field_top_vert)

        # Field bottom
        field_bot_vert = [f['Sampling_depth_final'][i] + half_height_per_vert[i + 1]
                          for i in range(len(f))]
        field_bottom.append(field_bot_vert)

    analysis_data['NN_field_height'] = [np.round(item, 2)
                                     for sublist in field_height for item in sublist]
    analysis_data['NN_field_top'] = [np.round(item, 2)
                                  for sublist in field_top for item in sublist]
    analysis_data['NN_field_bottom'] = [np.round(item, 2)
                                     for sublist in field_bottom for item in sublist]
    analysis_data['NN_field_length'] = analysis_data['max_abscissa_NN_field'] - analysis_data['min_abscissa_NN_field']
    analysis_data['NN_area_m2'] = analysis_data['NN_field_height'] * analysis_data['NN_field_length']     
        
     #%%    Calculate sand concentration and flux
    # Calculate concentration and flux if MAP-data existent
    if choice[3] == 1:
        # Determine liquid discharge in NN fields
        # Load MAP data and create section profile (left/mid/right)
        map_discharge = np.c_[
            map_data.left_cells_discharge, map_data.middle_cells_discharge, map_data.right_cells_discharge]
        map_area = np.c_[map_data.left_area, map_data.mid_area, map_data.right_area]
        map_borders = np.append(np.insert(map_data.borders_ens[1:] + map_data.left_distance, 0, map_data.left_borders),
                                map_data.right_borders[1:] + map_data.borders_ens[-1] + map_data.left_distance)

        left_depth_border = np.tile(np.vstack(map_data.border_depth), (1, map_data.left_cells_discharge.shape[1]))
        right_depth_border = np.tile(np.vstack(map_data.border_depth), (1, map_data.right_cells_discharge.shape[1]))
        depth_cells_border = np.c_[left_depth_border, map_data.depth_cells_border, right_depth_border]

        list_discharge_sample_cells = []
        for m in range(len(analysis_data)):
            q_sum = 0
            # Find verticals between min and max abscissa
            id_x = np.where((map_borders[1:] >= analysis_data['min_abscissa_NN_field'][m]) & (
                    map_borders[:-1] <= analysis_data['max_abscissa_NN_field'][m]))[0]
            x_values = map_borders[np.append(id_x, id_x[-1]+1)]
            # Change borders to NN min/max values
            x_values[0] = analysis_data['min_abscissa_NN_field'][m]
            x_values[-1] = analysis_data['max_abscissa_NN_field'][m]

            for k in range(len(id_x)):
                # Find correct depth in each vertical
                map_vertical = depth_cells_border[:, id_x[k]]
                id_y = np.where((map_vertical[1:] >= analysis_data['NN_field_top'][m]) & (
                        map_vertical[:-1] <= analysis_data['NN_field_bottom'][m]))[0]
                if len(id_y) > 0:
                    # Change borders to NN top/bottom values
                    y_values = map_vertical[np.append(id_y, id_y[-1]+1)]
                    y_values[0] = analysis_data['NN_field_top'][m]
                    y_values[-1] = analysis_data['NN_field_bottom'][m]

                    # Add discharge for each vertical in NN cell
                    # !TODO Adapt for edge's cells
                    area = [min(1, i) for i in (y_values[1:] - y_values[:-1]) * (x_values[k + 1] - x_values[k])]
                    q_sum += np.nansum(map_discharge[id_y, id_x[k]] * area / map_area[id_y, id_x[k]])

            list_discharge_sample_cells.append(q_sum)

        analysis_data['Discharge_NN_field_m3_s'] = list_discharge_sample_cells

        mid_cell_x = (map_data.borders_ens[:-1] + (map_data.borders_ens[1:] - map_data.borders_ens[:-1])/2) + map_data.left_distance
        mid_cell_y = (map_data.depth_cells_border[:-1] + (map_data.depth_cells_border[1:] - map_data.depth_cells_border[:-1])/2)

        # Determine liquid discharge in edges close to the bank
        id_left_triangle = next(x-1 for x, val in enumerate(mid_cell_x) if val > analysis_data['min_abscissa_NN_field'][0]) # to be adjusted?
        discharge_left =  np.nansum(pd.DataFrame(map_data.middle_cells_discharge).iloc[:,0:id_left_triangle]) + map_data.left_cells_discharge
        id_right_triangle = next(x+1 for x, val in enumerate(mid_cell_x) if val > analysis_data['max_abscissa_NN_field'].iloc[-1]) # to be adjusted?
        discharge_right =  np.nansum(pd.DataFrame(map_data.middle_cells_discharge).iloc[:,id_right_triangle:]) + map_data.right_cells_discharge
               
        # Calculate flux and concentration if flux data are given
        if np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1: 
            # Calculate solid flux per field
            analysis_data['NN_field_sand_flux_kg_s'] = analysis_data['Sand_flux_point_kg_s_m2'] * analysis_data['NN_area_m2']
            flux_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['NN_field_sand_flux_kg_s']
            
            # Calculate total sand flux
            NN_total_sand_flux_kg_s = np.nansum(analysis_data['NN_field_sand_flux_kg_s'])
            + analysis_data['Sand_flux_point_kg_s_m2'][0] + flux_last_vertical.iloc[0]   # add edges    
            NN_total_sand_flux_t_h = NN_total_sand_flux_kg_s * 3.6
            
            # Calculate concentration per field
            analysis_data['NN_field_sand_concentration_g_l'] = analysis_data['NN_field_sand_flux_kg_s']/analysis_data['Discharge_NN_field_m3_s']
            C_left = analysis_data['Sand_flux_point_kg_s_m2'][0]/map_data.left_discharge 
            C_right = flux_last_vertical.iloc[0]/map_data.right_discharge 
            
            # Calculate mean concentration
            NN_mean_sand_concentration_g_l = (np.nansum(analysis_data['NN_field_sand_concentration_g_l'])+ C_left + C_right)/(
                    analysis_data['NN_field_sand_concentration_g_l'].count() + 2)
            
        elif 'Concentration_sand_g_l' in analysis_data.columns:
            # Calculate cross-sectional discharge weighted mean concentration 
            # NN_mean_sand_concentration_g_l1 = np.nansum(analysis_data['Concentration_sand_g_l']*
            #                                            (analysis_data['NN_area_m2']/total_surface)) # surface weighted
            C_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['Concentration_sand_g_l']
            
            NN_mean_sand_concentration_g_l = np.nansum(analysis_data['Concentration_sand_g_l']*
            (analysis_data['Discharge_NN_field_m3_s']/map_data.total_discharge)) # discharge weighted
            + analysis_data['Concentration_sand_g_l'][0] * (discharge_left/map_data.total_discharge)
            + C_last_vertical.iloc[0] *(discharge_right / map_data.total_discharge)            
                    
            # Calculate solid flux per field using MAP discharge
            analysis_data['NN_field_sand_flux_kg_s'] = analysis_data['Concentration_sand_g_l']*analysis_data['Discharge_NN_field_m3_s']
            
            # Calculate total sand flux
            NN_total_sand_flux_kg_s = np.nansum(analysis_data['NN_field_sand_flux_kg_s'])
            + discharge_left*analysis_data['Concentration_sand_g_l'][0] # add left surface sample plus triangle discharge
            + discharge_right*C_last_vertical.iloc[0]   # add right surface sample 
            NN_total_sand_flux_t_h = NN_total_sand_flux_kg_s*3.6
            
    elif choice[5] == 1 and choice[3] == 0: 
                     
        # Calculate flux and concentration if flux data are given        
        if 'Sand_flux_point_kg_s_m2' in analysis_data.columns and np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1:
            # Calculate solid flux per field            
            analysis_data['NN_field_sand_flux_kg_s'] = analysis_data['Sand_flux_point_kg_s_m2'] * analysis_data['NN_area_m2']
            flux_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['NN_field_sand_flux_kg_s']
            
            # Calculate total sand flux
            NN_total_sand_flux_kg_s = np.nansum(analysis_data['NN_field_sand_flux_kg_s'])
            + analysis_data['Sand_flux_point_kg_s_m2'][0]+ flux_last_vertical.iloc[0]  # add edges   
            NN_total_sand_flux_t_h = NN_total_sand_flux_kg_s * 3.6
            
            # Calculate mean concentration  
            Q_samplingg = Q_sampling
            NN_mean_sand_concentration_g_l = np.nanmean(NN_total_sand_flux_kg_s) / np.mean(Q_samplingg['Value'])
          
        elif 'Concentration_sand_g_l' in analysis_data.columns:
            print(5)
            # Calculate surface
            # left_edge_surface = (analysis_data['min_abscissa_NN_field'][0]*
            #                      np.max(analysis_data[analysis_data['Abscissa'] == abscissa_values[0]]['NN_field_bottom']))/2
            # maxx = analysis_data['max_abscissa_NN_field']
            # right_edge_surface = 0# (maxx.iloc[-1]*
            #                      np.max(analysis_data[analysis_data['Abscissa'] == abscissa_values[0]]['NN_field_bottom']))/2
            total_surface = np.nansum(analysis_data['NN_area_m2']) #+ left_edge_surface + right_edge_surface) #* Edges???
           
            # Calculate cross-sectional discharge weighted mean concentration 
            C_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['Concentration_sand_g_l']             
            NN_mean_sand_concentration_g_l = np.nansum(analysis_data['Concentration_sand_g_l']*
                                                        (analysis_data['NN_area_m2']/total_surface)) 
            #+ analysis_data['Concentration_sand_g_l'][0] * (left_edge_surface / total_surface) # surface weighted
            #+ C_last_vertical *(right_edge_surface / total_surface)
             
            # Calculate total sand flux
            Q_samplingg = Q_sampling
            NN_total_sand_flux_kg_s = NN_mean_sand_concentration_g_l * np.mean(Q_samplingg['Value']) 
            NN_total_sand_flux_t_h = NN_total_sand_flux_kg_s *3.6
    
    else:
        if np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1:
            flux_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['NN_field_sand_flux_kg_s']
            
            # Calculate total sand flux
            NN_total_sand_flux_kg_s = np.nansum(analysis_data['NN_field_sand_flux_kg_s'])
            + analysis_data['Sand_flux_point_kg_s_m2'][0]+ flux_last_vertical.iloc[0]  # add edges   
            NN_total_sand_flux_t_h = NN_total_sand_flux_kg_s * 3.6
     
        elif 'Concentration_sand_g_l' in analysis_data.columns:
            C_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['Concentration_sand_g_l']             
            NN_mean_sand_concentration_g_l = np.nanmean([analysis_data['NN_field_sand_concentration_g_l'],
            analysis_data['NN_field_sand_concentration_g_l'][0], C_last_vertical])
             
    #%% Calculate fine concentration and flux 
    # Calculate concentration and flux if MAP-data existent
    if choice[3] == 1: 
        # Determine liquid discharge in NN fields
        mid_cell_x = (map_data.borders_ens[:-1] + (map_data.borders_ens[1:] - map_data.borders_ens[:-1])/2) + map_data.left_distance
        mid_cell_y = (map_data.depth_cells_border[:-1] + (map_data.depth_cells_border[1:] - map_data.depth_cells_border[:-1])/2)

        list_discharge_sample_cells = []
        for m in range(len(analysis_data)) :
            list_interm_discharge_sample_cells = []
            for i in range(mid_cell_y.shape[1]) :
                for k in range(mid_cell_y.shape[0]) :
                    if mid_cell_y[k][i] > analysis_data['NN_field_top'][m] and mid_cell_y[k][i] <\
                    analysis_data['NN_field_bottom'][m] and mid_cell_x[i] > analysis_data['min_abscissa_NN_field'][m] \
                        and mid_cell_x[i] < analysis_data['max_abscissa_NN_field'][m]:
                        list_interm_discharge_sample_cells.append(map_data.middle_cells_discharge[k][i])
            list_discharge_sample_cells.append(sum(list_interm_discharge_sample_cells))
        analysis_data['Discharge_NN_field_m3_s'] = list_discharge_sample_cells
        
        # Determine liquid discharge in edges close to the bank
        discharge_left = np.nansum(map_data.left_cells_discharge)
        discharge_right = np.nansum(map_data.right_cells_discharge)
        
        # Calculate flux and concentration if flux data are given
        if 'fine_flux_point_kg_s_m2' in analysis_data.columns:
            # Calculate solid flux per field
            analysis_data['NN_field_fine_flux_kg_s'] = analysis_data['fine_flux_point_kg_s_m2'] * analysis_data['NN_area_m2']
            flux_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['NN_field_fine_flux_kg_s']
            
            # Calculate total fine flux
            NN_total_fine_flux_kg_s = np.nansum(analysis_data['NN_field_fine_flux_kg_s'])
            + analysis_data['fine_flux_point_kg_s_m2'][0] + flux_last_vertical.iloc[0]   # add edges    
            NN_total_fine_flux_t_h = NN_total_fine_flux_kg_s * 3.6
            
            # Calculate concentration per field
            analysis_data['NN_field_fine_concentration_g_l'] = analysis_data['NN_field_fine_flux_kg_s']/analysis_data['Discharge_NN_field_m3_s']
            C_left = analysis_data['fine_flux_point_kg_s_m2'][0]/map_data.left_discharge 
            C_right = flux_last_vertical.iloc[0]/map_data.right_discharge 
            
            # Calculate mean concentration
            NN_mean_fine_concentration_g_l = np.nanmean([analysis_data['NN_field_fine_concentration_g_l'],
                                                         C_left, C_right]) 
            
        elif 'Concentration_fine_g_l' in analysis_data.columns:
            # Calculate cross-sectional discharge weighted mean concentration 
            C_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['Concentration_fine_g_l']
            
            NN_mean_fine_concentration_g_l = np.nansum(analysis_data['Concentration_fine_g_l']*
            (analysis_data['Discharge_NN_field_m3_s']/map_data.total_discharge)) # discharge weighted
            + analysis_data['Concentration_fine_g_l'][0] * (discharge_left/map_data.total_discharge)
            + C_last_vertical.iloc[0] *(discharge_right / map_data.total_discharge)
            
            # Calculate solid flux per field using MAP discharge
            analysis_data['NN_field_fine_flux_kg_s'] = analysis_data['Concentration_fine_g_l']*analysis_data['Discharge_NN_field_m3_s']
            
            # Calculate total fine flux
            NN_total_fine_flux_kg_s = np.nansum(analysis_data['NN_field_fine_flux_kg_s'])
            + discharge_left*analysis_data['Concentration_fine_g_l'][0] # add left surface sample plus triangle discharge
            + discharge_right*C_last_vertical.iloc[0]   # add right surface sample 
            NN_total_fine_flux_t_h = NN_total_fine_flux_kg_s * 3.6
            
    elif choice[5] == 1 and choice[3] == 0:              
        # Calculate flux and concentration if flux data are given
        if 'fine_flux_point_kg_s_m2' in analysis_data.columns:
            # Calculate solid flux per field
            analysis_data['NN_field_fine_flux_kg_s'] = analysis_data['fine_flux_point_kg_s_m2'] * analysis_data['NN_area_m2']
            flux_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['NN_field_fine_flux_kg_s']
            
            # Calculate total fine flux
            NN_total_fine_flux_kg_s = np.nansum(analysis_data['NN_field_fine_flux_kg_s'])
            + analysis_data['fine_flux_point_kg_s_m2'][0]+ flux_last_vertical.iloc[0]  # add edges   
            NN_total_fine_flux_t_h = NN_total_fine_flux_kg_s * 3.6
             
            # Calculate mean concentration
            Q_samplingg = Q_sampling
            NN_mean_fine_concentration_g_l = np.nanmean(NN_total_fine_flux_kg_s) / np.mean(Q_samplingg['Value'])
            
        elif 'Concentration_fine_g_l' in analysis_data.columns:
            # Calculate surface
            # left_edge_surface = (analysis_data['min_abscissa_NN_field'][0]*
            #                      np.max(analysis_data[analysis_data['Abscissa'] == abscissa_values[0]]['NN_field_bottom']))/2
            # maxx = analysis_data['max_abscissa_NN_field']
            # right_edge_surface = (maxx.iloc[-1]*
            #                      np.max(analysis_data[analysis_data['Abscissa'] == abscissa_values[0]]['NN_field_bottom']))/2
            total_surface = np.nansum(analysis_data['NN_area_m2']) #+ left_edge_surface + right_edge_surface) #* Edges???
           
            # Calculate cross-sectional discharge weighted mean concentration 
            C_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['Concentration_fine_g_l']             
            NN_mean_fine_concentration_g_l = np.nansum(analysis_data['Concentration_fine_g_l']*
                                                        (analysis_data['NN_area_m2']/total_surface)) 
            #+ analysis_data['Concentration_fine_g_l'][0] * (left_edge_surface / total_surface) # surface weighted
            #+ C_last_vertical *(right_edge_surface / total_surface)
             
            # Calculate total fine flux
            Q_samplingg = Q_sampling
            NN_total_fine_flux_kg_s = NN_mean_fine_concentration_g_l * np.mean(Q_samplingg['Value']) 
            NN_total_fine_flux_t_h = NN_total_fine_flux_kg_s *3.6
            
    else:
        if 'fine_flux_point_kg_s_m2' in analysis_data.columns:
            flux_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['NN_field_fine_flux_kg_s']
            
            # Calculate total fine flux
            NN_total_fine_flux_kg_s = np.nansum(analysis_data['NN_field_fine_flux_kg_s'])
            + analysis_data['fine_flux_point_kg_s_m2'][0]+ flux_last_vertical.iloc[0]  # add edges   
            NN_total_fine_flux_t_h = NN_total_fine_flux_kg_s * 3.6
       
        elif 'Concentration_fine_g_l' in analysis_data.columns:
            C_last_vertical = analysis_data[analysis_data['Abscissa'] == abscissa_values[-1]]['Concentration_fine_g_l']             
            NN_mean_fine_concentration_g_l = np.nanmean([analysis_data['NN_field_fine_concentration_g_l'],
            analysis_data['NN_field_fine_concentration_g_l'][0], C_last_vertical])
          
     
       

    #%% Plots  
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S', 'Niskin']
    forms_sampler = ['o', 'o','D', 's', 'P', '*', 'v']  
    size_sampler = [6, 6, 6, 6, 7, 8, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                     'darkorange', 'darkred', 'yellowgreen']
    samplers_used = np.unique(analysis_data['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    id_sampler_used = [sampler.index(analysis_data['Sampler'][i])
      for i in range(len(analysis_data))]

    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 14
    fontsize_legend_title = 14
    fontsize_text = 14
    fontsize_ticks = 12 
    
    #%% Plot sand concentration / flux
    #cmap = plt.get_cmap('viridis_r')
    
    fig, ax = plt.subplots(1, 1, figsize=(10, 6), dpi=100)

    if choice[3] == 1:
        ax.plot(track, track_depth, color='black')
        ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1)
    for i in index_samplers_used:
        ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
    
    if 'Sand_flux_point_kg_s_m2' in analysis_data.columns and np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1:
        cmap = "YlOrBr"
        color1=mcp.gen_color_normalized(cmap="YlOrBr", data_arr=analysis_data['NN_field_sand_flux_kg_s'])
    elif 'Concentration_sand_g_l' in analysis_data.columns:
        cmap = "YlOrBr"
        color1=mcp.gen_color_normalized(cmap="YlOrBr", data_arr=analysis_data['Concentration_sand_g_l'])

    for i, c in zip(range(len(analysis_data)), color1):
        ax.add_patch(mpl.patches.Rectangle((analysis_data['min_abscissa_NN_field'][i],
                                                   analysis_data['NN_field_top'][i]), (analysis_data['max_abscissa_NN_field'][i] -
                                                                                    analysis_data['min_abscissa_NN_field'][i]),
                                                  analysis_data['NN_field_height'][i],
                                                  ec='black', fc=c, alpha=1, linewidth = 0.5))
                                                                                       
    ax.invert_yaxis()                                                                                      
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
    legend = ax.legend(loc = 'upper right', fontsize=fontsize_legend, title = 'Sampler', bbox_to_anchor=(1.2, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
    
    if 'Sand_flux_point_kg_s_m2' in analysis_data.columns and np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1:
        norm = mpl.colors.Normalize(vmin=min(analysis_data['NN_field_sand_flux_kg_s']),vmax=max(analysis_data['NN_field_sand_flux_kg_s']))
        figname = sampling_date + '_NN_sand_flux_per_field_' + most_frequent_sampler
        ax.text(0.92, -0.13, '$\Sigma \Phi_{sand}$ = ' + str(np.round(NN_total_sand_flux_kg_s,2)) + ' kg/s', transform = ax.transAxes,
                fontsize = fontsize_text)
    elif 'Concentration_sand_g_l' in analysis_data.columns:
        norm = mpl.colors.Normalize(vmin=min(analysis_data['Concentration_sand_g_l']),vmax=max(analysis_data['Concentration_sand_g_l']))
        figname = sampling_date + '_NN_sand_concentration_per_field_' + most_frequent_sampler
        ax.text(0.92, -0.13, '$\overline{C}_{sand}$ = ' + str(np.round(NN_mean_sand_concentration_g_l,2)) + ' g/l', transform = ax.transAxes,
                fontsize = fontsize_text)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
   
    cb = plt.colorbar(sm, pad=0.04, shrink=0.5, anchor=(0, 0.1))
    if 'NN_field_sand_flux_kg_s' in analysis_data.columns:
        cb.set_label('$\mathregular{\Phi_{sand}}$ per field (kg/s)', fontsize = fontsize_legend_title, weight = 'bold')
    elif 'Concentration_sand_g_l' in analysis_data.columns: 
        cb.set_label('$\mathregular{\overline{C}_{sand}}$ per field (g/l) ', fontsize = fontsize_legend_title, weight = 'bold')             
       
    fig.tight_layout()    
    fig.savefig(outpath_figures + figname + '.png', dpi=300, bbox_inches='tight')
    
    #%% Plot fine concentration / flux
        
    if 'NN_field_fine_flux_kg_s' in analysis_data.columns or 'Concentration_fine_g_l' in analysis_data.columns:
        fig, ax = plt.subplots(1, 1, figsize=(10, 6), dpi=100)
    
        if choice[3] == 1:
            ax.plot(track, track_depth, color='black')
    
        for i in index_samplers_used:
            ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                    analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                    forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                    markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
        if 'NN_field_fine_flux_kg_s' in analysis_data.columns:
            cmap="YlOrBr"
            color1=mcp.gen_color_normalized(cmap="YlOrBr", data_arr=analysis_data['NN_field_fine_flux_kg_s'])
            for i, c in zip(range(len(analysis_data)), color1):
                 ax.add_patch(mpl.patches.Rectangle((analysis_data['min_abscissa_NN_field'][i],
                                                             analysis_data['NN_field_top'][i]), (analysis_data['max_abscissa_NN_field'][i] -
                                                                                             analysis_data['min_abscissa_NN_field'][i]),
                                                           analysis_data['NN_field_height'][i],
                                                           ec='black', fc=c, alpha=1, linewidth = 0.5))
        elif 'Concentration_fine_g_l' in analysis_data.columns:
           cmap="YlOrBr"
           color1=mcp.gen_color_normalized(cmap="YlOrBr", data_arr=analysis_data['Concentration_fine_g_l'])
           for i, c in zip(range(len(analysis_data)), color1):
                ax.add_patch(mpl.patches.Rectangle((analysis_data['min_abscissa_NN_field'][i],
                                                            analysis_data['NN_field_top'][i]), (analysis_data['max_abscissa_NN_field'][i] -
                                                                                            analysis_data['min_abscissa_NN_field'][i]),
                                                          analysis_data['NN_field_height'][i],
                                                          ec='black', fc=c, alpha=1, linewidth = 0.5))
                                                                                           
        ax.invert_yaxis()                                                                                      
        ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
        ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
        legend = ax.legend(loc = 'upper right', fontsize=fontsize_legend, title = 'Sampler', bbox_to_anchor=(1.2, 1))
        plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
       
        # cmap = plt.get_cmap('viridis_r',)
        if 'NN_field_fine_flux_kg_s' in analysis_data.columns:
            norm = mpl.colors.Normalize(vmin=min(analysis_data['NN_field_fine_flux_kg_s']),vmax=max(analysis_data['NN_field_fine_flux_kg_s']))
            figname = sampling_date + '_NN_fine_flux_per_field_' + most_frequent_sampler
            ax.text(0.92, -0.13, '$\Sigma \Phi_{fine}$ = ' + str(np.round(NN_total_fine_flux_kg_s,2)) + ' kg/s', 
                    transform = ax.transAxes, fontsize = fontsize_text)
        elif 'Concentration_fine_g_l' in analysis_data.columns:
            norm = mpl.colors.Normalize(vmin=min(analysis_data['Concentration_fine_g_l']),vmax=max(analysis_data['Concentration_fine_g_l']))
            figname = sampling_date + '_NN_fine_concentration_per_field_' + most_frequent_sampler
            ax.text(0.92, -0.13, '$\overline{C}_{fine}$ = ' + str(np.round(NN_mean_fine_concentration_g_l,2)) + ' g/l', 
                    transform = ax.transAxes, fontsize = fontsize_text)
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])
       
        cb = plt.colorbar(sm, pad=0.04, shrink=0.5, anchor=(0, 0.1))
        if 'NN_field_fine_flux_kg_s' in analysis_data.columns:
            cb.set_label('$\mathregular{\Phi_{fine}}$ per field (kg/s)', fontsize = fontsize_legend_title, weight = 'bold')
        elif 'Concentration_fine_g_l' in analysis_data.columns: 
            cb.set_label('$\mathregular{\overline{C}_{fine}}$ per field (g/l)', fontsize = fontsize_legend_title, weight = 'bold')             
           
        fig.tight_layout()    
        fig.savefig(outpath_figures + figname + '.png', dpi=300, bbox_inches='tight')
        
###################################################################################################

    #%% Export data
    
    # Sampling_data, sampler(s)
    # NN_mean_sand_concentration_g_l, NN_total_sand_flux_kg_s, NN_total_sand_flux_t_h,
    # NN_mean_fine_concentration_g_l, NN_total_fine_flux_kg_s
    
    if np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1:
            summary_NN = pd.DataFrame([NN_mean_sand_concentration_g_l, NN_total_sand_flux_kg_s, NN_total_sand_flux_t_h]).transpose()
            summary_NN.columns = ['NN_mean_sand_conc_g_l', 'NN_total_sand_flux_kg_s', 'NN_total_sand_flux_t_h']
    if 'Concentration_sand_g_l' in analysis_data.columns:
            if 'Concentration_fine_g_l' in analysis_data.columns:
                summary_NN = pd.DataFrame([NN_mean_sand_concentration_g_l, NN_total_sand_flux_kg_s, NN_total_sand_flux_t_h,
                       NN_mean_fine_concentration_g_l, NN_total_fine_flux_kg_s, NN_total_fine_flux_t_h]).transpose()
                summary_NN.columns = ['NN_mean_sand_conc_g_l', 'NN_total_sand_flux_kg_s', 'NN_total_sand_flux_t_h',
                                      'NN_mean_fine_conc_g_l', 'NN_total_fine_flux_kg_s', 'NN_total_fine_flux_t_h']
            else:
                summary_NN = pd.DataFrame([NN_mean_sand_concentration_g_l, NN_total_sand_flux_kg_s, NN_total_sand_flux_t_h]).transpose()
                summary_NN.columns = ['NN_mean_sand_conc_g_l', 'NN_total_sand_flux_kg_s', 'NN_total_sand_flux_t_h']         
    
    return (analysis_data, summary_NN)
 

    
    