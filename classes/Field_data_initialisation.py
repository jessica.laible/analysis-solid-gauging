"""
Prepares the analysis_data file througout the script, 
creates datetime format, determines the outpath and sampling date

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

# Load packages
import pandas as pd
import os
import datetime as dt
import numpy as np
import sys

def numbers_extrac(outpath) :
    copie_outpath = outpath.replace('\\', ' ').replace('_', ' ').replace('/', ' ')
    numbers_extrac = [int(s) for s in copie_outpath.split() if s.isdigit()]
    numbers_extrac = str(numbers_extrac)
    numbers_extrac = numbers_extrac.replace('[', '').replace(']', '')
    return (numbers_extrac)

def field_data_initialisation(path_prep_data, map_data=None) :

    # Load data
    field_data = pd.read_csv(path_prep_data, sep=";")

    # Creation of a copy dataframe to work with
    analysis_data = field_data

    # Define outpath
    split_string = path_prep_data.rsplit('/', 1)
    split_string[0] = split_string[0].replace('/', "\\") + '\\'

    outpath = split_string[0]
    outpath_figures = split_string[0] + 'Figures\\'

    if not os.path.exists(outpath_figures):
        os.mkdir(outpath_figures)

    # Determine sampling date
    # copie_outpath = outpath
    # copie_outpath = copie_outpath.replace('\\', ' ').replace('_', ' ')
    copie_outpath = os.path.basename(os.path.normpath(outpath))

    number_outpath = numbers_extrac(copie_outpath)
    # number_outpath = [int(s) for s in copie_outpath.split() if s.isdigit()]
    # number_outpath = str(number_outpath)
    # number_outpath = number_outpath.replace('[', '').replace(']', '')

    if len(number_outpath) >= 8:
        day = number_outpath[-2:]
        month = number_outpath[-4:-2]
        year = number_outpath[:4]
        sampling_date = year + month + day
    elif map_data is not None and hasattr(map_data, 'meas_date'):
        sampling_date = map_data.meas_date
    else:
        sampling_date = '19010101'
   
    if len(analysis_data['Time'][0]) == 4:
        sampling_time_datetime = pd.to_datetime(sampling_date + ' ' + analysis_data['Time'])
        sampling_time = dt.datetime.strptime(analysis_data['Time'][0], '%H:%M').time()
        analysis_data['Sampling_datetime'] = sampling_time_datetime
    elif len(analysis_data['Time'][0]) == 5:
        sampling_time_datetime = pd.to_datetime(sampling_date + ' ' + analysis_data['Time'])
        sampling_time = dt.datetime.strptime(analysis_data['Time'][0], '%H:%M').time()
        analysis_data['Sampling_datetime'] = sampling_time_datetime
    elif len(analysis_data['Time'][0]) == 8:
        sampling_time_datetime = pd.to_datetime(sampling_date + ' ' + analysis_data['Time'])
        sampling_time = dt.datetime.strptime(analysis_data['Time'][0], '%H:%M:%S').time()
        analysis_data['Sampling_datetime'] = sampling_time_datetime    
        
    # Define abscissas' name and add in column to analysis_data
    abscissa_values = np.unique(analysis_data['Abscissa']).tolist()
    # hh = 
    analysis_data.insert(2, 'Abscissa_Name', [abscissa_values.index(i) + 1
                                              for i in analysis_data['Abscissa']])    
    analysis_data['Sampling_date'] = sampling_date
       
    return (analysis_data, outpath, outpath_figures, sampling_date)


