"""
Export data as class and save class as pickle

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import pickle

class Export_data:
    def __init__(self, analysis_data, outpath, stage_sampling, spm_sampling, Q_sampling,summary_NN,summary_ISO, 
                 summary_Rouse, summary_SDC, summary_segment, summary_fine, summary_GSD_SDC, Conc_SDC_cell_export, 
                 sand_flux_SDC_kg_s_export, Conc_Rouse_cell_export, sand_flux_Rouse_kg_s_export, ISO_classes, ISO_mean_gsd, ISO_mean_gsd_number,
                 Conc_fine_cell_export, fine_flux_kg_s_export, ratio_cell, D50_SDC_cell_export, GSD_SDC_cell_export, 
                 mean_gsd_weighted, u_point_sand, u_point_fines, comparison_methods, HADCP_export, HADCP_export_BD, uncertainty):
        
        self.analysis_data = analysis_data
        self.outpath = outpath
        
        self.stage_sampling = stage_sampling
        self.spm_sampling = spm_sampling
        self.Q_sampling = Q_sampling
        
        self.summary_NN = summary_NN 
        self.summary_ISO = summary_ISO 
        self.summary_Rouse = summary_Rouse 
        self.summary_SDC = summary_SDC
        self.summary_segment = summary_segment
        self.summary_fine = summary_fine
        self.summary_GSD_SDC = summary_GSD_SDC
        
        self.Conc_SDC_cell_export = Conc_SDC_cell_export
        self.sand_flux_SDC_kg_s_export = sand_flux_SDC_kg_s_export
        self.Conc_Rouse_cell_export = Conc_Rouse_cell_export
        self.sand_flux_Rouse_kg_s_export = sand_flux_Rouse_kg_s_export
        self.ISO_classes = ISO_classes
        self.ISO_mean_gsd = ISO_mean_gsd
        self.ISO_mean_gsd_number = ISO_mean_gsd_number
        self.Conc_fine_cell_export = Conc_fine_cell_export
        self.fine_flux_kg_s_export = fine_flux_kg_s_export
        self.ratio_cell = ratio_cell
        self.D50_SDC_cell_export = D50_SDC_cell_export 
        self.GSD_SDC_cell_export = GSD_SDC_cell_export
        self.mean_gsd_weighted = mean_gsd_weighted 
        self.u_point_sand = u_point_sand
        self.u_point_fines = u_point_fines
        self.comparison_methods = comparison_methods
        self.HADCP_export = HADCP_export
        self.HADCP_export_BD = HADCP_export_BD
        self.uncertainty = uncertainty
        
    def save_data(self, sampling_date, most_frequent_sampler):
        with open(self.outpath + '\\' + sampling_date + '_Analysis_solid_gauging_' + most_frequent_sampler +'.txt', 'wb') as fp:
            pickle.dump(self, fp, protocol=pickle.HIGHEST_PROTOCOL)
