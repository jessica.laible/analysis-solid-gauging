"""
Calculate the sediment flux for samples taken with the Delft Bottle and the big bended nozzle

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.optimize import curve_fit

#############################################

x_vel75_big_bend = np.array([0.5, 0.6, 0.7,  0.8, 0.85])
y_alpha75_big_bend = np.array([1, 1.2, 1.45, 1.85, 2.25])
x_vel90_big_bend = np.array([0.5, 0.6, 0.7,  0.8, 0.9, 1.0, 1.05])
y_alpha90_big_bend = np.array([0.95, 1.05, 1.175, 1.35, 1.5, 1.9, 2.15])
x_vel100_big_bend = np.array([0.5, 0.6, 0.7,  0.8, 0.9, 1.0, 1.1, 1.2])
y_alpha100_big_bend = np.array([0.9, 0.95, 1, 1.1, 1.25, 1.5, 1.75, 2.25])
x_vel110_big_bend = np.array([0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4])
y_alpha110_big_bend = np.array([0.8, 0.825, 0.875, 0.925, 1, 1.125, 1.3, 1.55, 1.85, 2.25])
x_vel130_big_bend = np.array([0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5])
y_alpha130_big_bend = np.array([0.775, 0.75, 0.75, 0.775, 0.8, 0.85, 0.9, 0.975, 1.1, 1.25, 1.45])
x_vel150_big_bend = np.array([0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5])
y_alpha150_big_bend = np.array([0.775, 0.75, 0.725, 0.7, 0.7, 0.725, 0.75, 0.8, 0.9, 1, 1.15])
x_vel210_big_bend = np.array([0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5])
y_alpha210_big_bend = np.array([0.775, 0.75, 0.725, 0.7, 0.7, 0.725, 0.75, 0.775, 0.8, 0.9, 0.975])

#############################################

def exponential(x, a, b, c):
    return a*x**3+b*x**2+c

def sed_flux_big_bend(outpath_figures) :

    # Parametres
    BD_grain_size_steps = [75, 90, 100, 110, 130, 150, 210]
    name = '_big_bend'
    title = name.replace('_', ' ')
    title = title[1:].capitalize()
    colors = ['tab:blue','tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 'tab:pink']
    # Define and plot alpha

    # plt.figure(figsize=(9,6))
    k = 0
    for k in range(len(BD_grain_size_steps)) :
        x_range = np.arange(globals()['x_vel' + str(BD_grain_size_steps[k]) + str(name)][0],
                            globals()['x_vel' + str(BD_grain_size_steps[k]) + str(name)][-1], 0.01)
        x_range_extrap = np.arange(globals()['x_vel' + str(BD_grain_size_steps[k]) + str(name)][0], 1.8, 0.01)
        globals()['pars' + str(BD_grain_size_steps[k]) + str(name)], globals()['cov' + str(BD_grain_size_steps[k]) + str(name)] = curve_fit(
            f=exponential, xdata=globals()['x_vel' + str(BD_grain_size_steps[k]) + str(name)],
            ydata=globals()['y_alpha' + str(BD_grain_size_steps[k]) + str(name)], maxfev=5000)
        # plt.plot(x_range_extrap, exponential(x_range_extrap,
        #     globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][0],globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][1], globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][2]), linestyle='--',
        #     color=colors[k])
        # plt.plot(x_range, exponential(x_range,
        #     globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][0],globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][1], globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][2]), linestyle='-',
        #     color=colors[k], label=f' {BD_grain_size_steps[k]}')
        globals()['alpha' + str(BD_grain_size_steps[k]) + str(name)] = np.round(exponential(x_range,
            globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][0],globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][1], globals()['pars' + str(BD_grain_size_steps[k]) + str(name)][2]), 3)
    # plt.plot([], [], linestyle='--', color='black', label='extrapolation')

    # plt.legend(title='$D_{50}$ (\u03BCm)')
    # plt.grid(linewidth=0.2)
    # plt.xlim(0.4, 1.8)
    # plt.ylim(0.6, 2.5)
    # plt.xlabel('Velocity m/s')
    # plt.ylabel(r'$ \alpha $ (-)')
    # plt.title(str(title) + ' nozzle')

    # figname = 'Big_bend'
    # plt.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')

    # Define values out of lower range (velocity < 0.5 m/s)
    # Define values out of upper range (velocity > 2.4 m/s)
    # Small straight nozzle
    globals()['alpha' + str(name)] = []
    k = 0
    for k in range(len(BD_grain_size_steps)) :
        globals()['alpha' + str(BD_grain_size_steps[k]) + '_lower_outrange' + str(name)] = np.array(
            [np.min(globals()['alpha' + str(BD_grain_size_steps[k]) + str(name)])] * 120)
        globals()['alpha' + str(BD_grain_size_steps[k]) + '_upper_outrange' + str(name)] = np.array(
            [np.max(globals()['alpha' + str(BD_grain_size_steps[k]) + str(name)])] * 120)
        globals()['alpha' + str(BD_grain_size_steps[k]) + str(name)] = np.concatenate(
            (globals()['alpha' + str(BD_grain_size_steps[k]) + '_lower_outrange' + str(name)], globals()['alpha' + str(BD_grain_size_steps[k]) + str(name)]
             , globals()['alpha' + str(BD_grain_size_steps[k]) + '_upper_outrange' + str(name)]))
        globals()['alpha' + str(name)].append(globals()['alpha' + str(BD_grain_size_steps[k]) + str(name)])

    sed_flux_big_bend.alpha_big_bend = globals()['alpha' + str(name)]