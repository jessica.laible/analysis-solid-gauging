"""
Plots bathymetry, sampling points, concentration, flux, D50, fine/sand ratio per sample
depending on available data 

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import FixedLocator, FixedFormatter
from matplotlib.ticker import FuncFormatter
from matplotlib import cm
from matplotlib.colors import ListedColormap as mpl_colors_ListedColormap 

# Fontsizes
fontsize_axis = 14
fontsize_legend = 14
fontsize_legend_title = 14
fontsize_text = 14
fontsize_ticks = 12

def log_tick_formatter(val, pos=None):
    return r"$10^{:.0f}$".format(val)

def polygon_under_graph(xlist, ylist):
    """
    Construct the vertex list which defines the polygon filling the space under
    the (xlist, ylist) line graph.  Assumes the xs are in ascending order.
    """
    return [(xlist[0], 0.), *zip(xlist, ylist), (xlist[-1], 0.)]

formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))


def plot_data_A(analysis_data, outpath_figures, sampling_date, track, track_depth, cumsum) :
    
    abscissa_values = np.unique(analysis_data['Abscissa']).tolist()
    
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S', 'Niskin']
    forms_sampler = ['o', 'o','D', 's', 'P', '*', 'v']  
    size_sampler = [6, 6, 6, 6, 7, 8, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                     'darkorange', 'darkred', 'yellowgreen']
    samplers_used = np.unique(analysis_data['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 14
    fontsize_legend_title = 14
    fontsize_text = 14
    fontsize_ticks = 12 

    
    ##########################################################################
    # Bathymetry
    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)
    ax.plot(track, track_depth, color='black')

    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.invert_yaxis()
    ax.xaxis.label.set_size(fontsize_ticks)
    ax.yaxis.label.set_size(fontsize_ticks)
    ax.set_xlim(0,track[-1])

    fig.tight_layout()
    figname = sampling_date + '_Bathymetry'
    fig.savefig(outpath_figures + '\\' + figname + '.png', dpi = 300, bbox_inches='tight')

    ##########################################################################
    # Bathymetry and sampling points
    fig, ax = plt.subplots(1,1, figsize=(10, 6), dpi=100)
    ax.plot(track, track_depth, color='black')
    ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1, zorder = 0)
    for i in index_samplers_used:
        ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i], zorder = 30)
    
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.invert_yaxis()
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    legend = ax.legend(fontsize = fontsize_legend, title = 'Sampler', 
                       loc="upper right", bbox_to_anchor=(1.2, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)    
    ax.set_xlim(0,track[-1])
    
    fig.tight_layout()
    figname = sampling_date + '_Bathymetry_sampling_points_' + most_frequent_sampler 
    fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')

    ##########################################################################
    # Bathymetry and sampling points
    fig, ax = plt.subplots(1,1, figsize=(10, 6), dpi=100)
           
    #ax.hlines(track[0], track[-1], 1, color = 'darkblue', linewidth = 1)
    for i in index_samplers_used:
        ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['z_h'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])

    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('z/h (-)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylim(-0.05, 1.05)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    x_formatter = FixedFormatter(['0','0.2',"0.4","0.6",'0.8',"1"])
    x_locator = FixedLocator([0,0.2,0.4,0.6,0.8,1])
    ax.yaxis.set_major_formatter(x_formatter)
    ax.yaxis.set_major_locator(x_locator)
    #plt.gca().invert_yaxis()
       
    legend = ax.legend(fontsize = fontsize_legend, title = 'Sampler', 
                       loc="upper right", bbox_to_anchor=(1.2, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
   
    fig.tight_layout()
    figname = sampling_date + '_Z_h_sampling_points_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')
           
    #%% Plots  with GSD
    analysis_data_GSD = analysis_data.dropna(subset = ['D50'])       
    # Get size classes 
    colnames = analysis_data.columns.values
    index_num = []
    for name in colnames:
        index_num.append(not any([c.isalpha() for c in name]))
    size_classes = colnames[index_num]
    size_classes = [float(i) for i in size_classes]

    # Size classes for cumulated and classified distribution
    size_classes_cumsum = size_classes[1:]
    size_lower = np.array(size_classes[0:-1])* 1e-6 / 2
    size_upper = np.array(size_classes[1:])* 1e-6 / 2
    size_classes_classified = [10**((np.log10(size_lower[i]) + np.log10(size_upper[i]))/2) 
                         for i in range(len(size_lower))]
    size_classes_classified= np.array(size_classes_classified)

    start_GSD_idx = int(np.where(colnames==str(size_classes[1]))[0])
    end_GSD_idx = int(np.where(colnames==str(int(size_classes[-1])))[0])
    ##########################################################################
    # D50 and sampling points
    cmap = plt.cm.get_cmap('plasma_r')
    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)
    ax.plot(track, track_depth, color='black')
    ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1, zorder = 0)
    for i in index_samplers_used:
        sc_noGSD = ax.plot(analysis_data['Abscissa'][analysis_data['D10'].isnull()][analysis_data['Sampler']== sampler[i]], 
                           analysis_data['Sampling_depth_final'][analysis_data['D10'].isnull()][analysis_data['Sampler']== sampler[i]],
                      marker = forms_sampler[i], linestyle = '', markersize = 6, color = 'darkgrey', markeredgecolor = 'black', markeredgewidth=0.5,
                      zorder = 30, label = 'no GSD')
            
    for i in index_samplers_used:
        sc = ax.scatter(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                        analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                        c=analysis_data['D50'][analysis_data['Sampler']== sampler[i]], s=180, marker = forms_sampler[i],
                        cmap=cmap, edgecolor='black', linewidth=0.5, zorder = 30)

    plt.gca().invert_yaxis()
    cbar = fig.colorbar(sc, shrink = 0.9, ax=ax, extend='both')
    cbar.ax.yaxis.set_tick_params(pad=10, labelsize = fontsize_ticks)
    cbar.set_label('$\mathregular{D_{50}}$ (\u03BCm)', labelpad=10, fontsize = fontsize_axis, weight = 'bold')

    plt.legend(fontsize=fontsize_legend, loc = 'lower right', bbox_to_anchor = (1.27, -0.1))
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    ax.set_xlim(0,track[-1])

    fig.tight_layout()
    figname = sampling_date + '_Sampling_points_bathymetry_D50_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi=300, bbox_inches='tight')

    ##########################################################################
    # D50 abscissa z/h (normalized height)        
    cmap = plt.cm.get_cmap('plasma_r')    
    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)    
    for i in index_samplers_used:
        sc_noGSD = ax.plot(analysis_data['Abscissa'][analysis_data['D10'].isnull()][analysis_data['Sampler']== sampler[i]], 
                           analysis_data['z_h'][analysis_data['D10'].isnull()][analysis_data['Sampler']== sampler[i]],
                      marker = forms_sampler[i], linestyle = '', markersize = 6, color = 'darkgrey', markeredgecolor = 'black', markeredgewidth=0.5,
                      label = 'no GSD')
            
    for i in index_samplers_used:
        sc = ax.scatter(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], analysis_data['z_h'][analysis_data['Sampler']== sampler[i]],
                        c=analysis_data['D50'][analysis_data['Sampler']== sampler[i]], s=180, marker = forms_sampler[i],
                        cmap=cmap, edgecolor='black', linewidth=0.5)

    cbar = fig.colorbar(sc, shrink = 0.9, ax=ax, extend='both')
    cbar.ax.yaxis.set_tick_params(pad=10, labelsize = fontsize_ticks)
    cbar.set_label('$\mathregular{D_{50}}$ (\u03BCm)', labelpad=10, fontsize = fontsize_axis, weight = 'bold')

    plt.gca().invert_yaxis()
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('z/h (-)', fontsize=fontsize_axis, weight = 'bold')
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    x_formatter = FixedFormatter(['0','0.2',"0.4","0.6",'0.8',"1"])
    x_locator = FixedLocator([0,0.2,0.4,0.6,0.8,1])
    ax.yaxis.set_major_formatter(x_formatter)
    ax.yaxis.set_major_locator(x_locator)
    plt.legend(fontsize=fontsize_legend, loc = 'lower right', bbox_to_anchor = (1.27, -0.1))
    ax.set_ylim(-0.05,1.05)

    fig.tight_layout()
    figname = sampling_date + '_D50_abscissa_normalized_depth_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi=300, bbox_inches='tight')
    
    ##################################################################
    # Grain size distributions
    color = 'plasma_r'        
    # linestyles_abscissa = ['-', ':', '--', '-.', (0, (3, 5, 1, 5, 1, 5)), 'densely dotted', 'densely dashed', 'loosely dashed']
    # name_fig_graphe = ['Grainsize_Diameter_density_colours_all_' + sampling_date, 'Grainsize_Diameter_density_colours_abscissas_' + sampling_date]
    cmap = plt.get_cmap(color)
    colors = cmap(np.linspace(0,1,len(analysis_data_GSD)))

    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)

    ax.axvline(63, color='darkgrey', linewidth=1, linestyle='--')

    for i in range(len(analysis_data_GSD)):
        for j in range(len(abscissa_values)):
            ax.plot(size_classes_classified*2/1e-6, analysis_data_GSD.iloc[i,(start_GSD_idx-1):end_GSD_idx],color=colors[i], 
                    linestyle = '-',
                    label=analysis_data["Abscissa"].iloc[j])
            ax.set_xscale('log')
            ax.grid(which="both", linewidth=0.2)
            ax.set_xlim(10, 1000)
      
    # tick_values_all = np.around(analysis_data['z_h'], 2)
    # tick_values = np.around(np.linspace(tick_values_all.min(), tick_values_all.max(), 8, endpoint=True), 2)
    # norm = mpl.colors.Normalize(vmin=tick_values_all.iloc[0], vmax=tick_values_all.iloc[-1])
    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
    cmap.set_array([])
    cbar = plt.colorbar(cmap, ticks=[0, 0.2, 0.4, 0.6, 0.8, 1])
    cbar.ax.set_yticklabels([0, 0.2, 0.4, 0.6, 0.8, 1])
    cbar.ax.yaxis.set_tick_params(pad=10, labelsize = fontsize_ticks)
    cbar.set_label('z/h (-)', labelpad=10, fontsize = fontsize_axis, weight = 'bold')

    ax.set_xlabel('Diameter (\u03BCm)', fontsize = fontsize_axis, weight = 'bold')
    ax.set_ylabel('Density (%)', fontsize = fontsize_axis, weight = 'bold')
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    x_formatter = FixedFormatter(['10', '50', '100', '500', '1000'])
    x_locator = FixedLocator([10, 50, 100, 500, 1000])
    ax.xaxis.set_major_formatter(x_formatter)
    ax.xaxis.set_major_locator(x_locator)
    ax.set_ylim(0,)
    #plt.legend()

    plt.tight_layout()
    figname = sampling_date + '_Grain_size_distributions_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')

def plot_data_B(analysis_data, outpath_figures, sampling_date, cumsum) :
    
    abscissa_values = np.unique(analysis_data['Abscissa']).tolist()
    
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S', 'Niskin']
    forms_sampler = ['o', 'o','D', 's', 'P', '*', 'v']  
    size_sampler = [6, 6, 6, 6, 7, 8, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                     'darkorange', 'darkred', 'yellowgreen']
    samplers_used = np.unique(analysis_data['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 12
    fontsize_legend_title = 14
    fontsize_text = 12
    fontsize_ticks = 12 

     #%% Plots  with GSD
    analysis_data_GSD = analysis_data.dropna(subset = ['D50'])       
    # Get size classes 
    colnames = analysis_data.columns.values
    index_num = []
    for name in colnames:
        index_num.append(not any([c.isalpha() for c in name]))
    size_classes = colnames[index_num]
    size_classes = [float(i) for i in size_classes]

    # Size classes for cumulated and classified distribution
    size_classes_cumsum = size_classes[1:]
    size_lower = np.array(size_classes[0:-1])* 1e-6 / 2
    size_upper = np.array(size_classes[1:])* 1e-6 / 2
    size_classes_classified = [10**((np.log10(size_lower[i]) + np.log10(size_upper[i]))/2) 
                         for i in range(len(size_lower))]
    size_classes_classified= np.array(size_classes_classified)

    start_GSD_idx = int(np.where(colnames==str(size_classes[1]))[0])
    end_GSD_idx = int(np.where(colnames==str(int(size_classes[-1])))[0])
    
    ##########################################################################
    # D50 and sampling points
    cmap = plt.cm.get_cmap('plasma_r')
    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)
    
    sc_noGSD = ax.plot(analysis_data['Abscissa'][analysis_data['D10'].isnull()], analysis_data['Sampling_depth_final'][analysis_data['D10'].isnull()],
                  'o', markersize = 6, color = 'darkgrey', markeredgecolor = 'black', markeredgewidth=0.5,
                  label = 'no GSD')
                            
    sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                    c=analysis_data['D50'], s = 150,
                    cmap=cmap, edgecolor='black', linewidth=0.5)

    plt.gca().invert_yaxis()
    cbar = fig.colorbar(sc, shrink = 0.9, ax=ax, extend='both')
    cbar.ax.yaxis.set_tick_params(pad=10, labelsize = fontsize_ticks)
    cbar.set_label('$\mathregular{D_{50}}$ (\u03BCm)', labelpad=10, fontsize = fontsize_axis)

    legend = plt.legend(fontsize=fontsize_legend, loc = 'lower right', bbox_to_anchor = (1.27, -0.1))
    plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    
    fig.tight_layout()
    figname = sampling_date + '_Sampling_points_D50_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi=300, bbox_inches='tight')
   
    ##################################################################
    # Grain size distributions
    color = 'plasma_r'        
    #linestyles_abscissa = ['-', ':', '--', '-.', (0, (3, 5, 1, 5, 1, 5)), 'densely dotted', 'densely dashed', 'loosely dashed']
    name_fig_graphe = ['Grainsize_Diameter_density_colours_all_' + sampling_date, 'Grainsize_Diameter_density_colours_abscissas_' + sampling_date]
    cmap = plt.get_cmap(color)
    colors = cmap(np.linspace(0,1,len(analysis_data_GSD)))

    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)

    ax.axvline(63, color='darkgrey', linewidth=1, linestyle='--')

    for i in range(len(analysis_data_GSD)):
        for j in range(len(abscissa_values)):
            ax.plot(size_classes_classified*2/1e-6, analysis_data_GSD.iloc[i,(start_GSD_idx-1):end_GSD_idx],color=colors[i], 
                    linestyle = '-',
                    label=analysis_data["Abscissa"].iloc[j])
            ax.set_xscale('log')
            ax.grid(which="both", linewidth=0.2)
            ax.set_xlim(10, 1000)
      
    # tick_values_all = np.around(analysis_data['z_h'], 2)
    # tick_values = np.around(np.linspace(tick_values_all.min(), tick_values_all.max(), 8, endpoint=True), 2)
    # norm = mpl.colors.Normalize(vmin=tick_values_all.iloc[0], vmax=tick_values_all.iloc[-1])
    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
    cmap.set_array([])
    cbar = plt.colorbar(cmap, ticks=[0, 0.2, 0.4, 0.6, 0.8, 1])
    cbar.ax.set_yticklabels([0, 0.2, 0.4, 0.6, 0.8, 1])
    cbar.ax.yaxis.set_tick_params(pad=10, labelsize = fontsize_ticks)
    cbar.set_label('z/h (-)', labelpad=10, fontsize = fontsize_axis)

    ax.set_xlabel('Diameter (\u03BCm)', fontsize = fontsize_axis)
    ax.set_ylabel('Density (%)', fontsize = fontsize_axis)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    x_formatter = FixedFormatter(['10', '50', '100', '500', '1000'])
    x_locator = FixedLocator([10, 50, 100, 500, 1000])
    ax.xaxis.set_major_formatter(x_formatter)
    ax.xaxis.set_major_locator(x_locator)
    ax.set_ylim(0,)
    #plt.legend()

    plt.tight_layout()
    figname = sampling_date + '_Grain_size_distributions_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')
    
def plot_data_C(analysis_data, outpath_figures, sampling_date, track, track_depth) :
    
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S', 'Niskin']
    forms_sampler = ['o', 'o','D', 's', 'P', '*', 'v']  
    size_sampler = [6, 6, 6, 6, 7, 8, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                     'darkorange', 'darkred', 'yellowgreen']
    samplers_used = np.unique(analysis_data['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 12
    fontsize_legend_title = 14
    fontsize_text = 12
    fontsize_ticks = 12 
    
    ##########################################################################
    # Bathymetry
    fig, ax = plt.subplots(figsize=(10, 6), dpi=100)
    ax.plot(track, track_depth, color='black')

    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.invert_yaxis()
    ax.xaxis.label.set_size(fontsize_ticks)
    ax.yaxis.label.set_size(fontsize_ticks)
    ax.set_xlim(0,track[-1])

    fig.tight_layout()
    figname = sampling_date + '_Bathymetry'
    fig.savefig(outpath_figures + '\\' + figname + '.png', dpi = 300, bbox_inches='tight')

    ##########################################################################
    # Bathymetry and sampling points
    fig, ax = plt.subplots(1,1, figsize=(10, 6), dpi=100)
    ax.plot(track, track_depth, color='black')
    ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1)
    for i in index_samplers_used:
        ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
    
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.invert_yaxis()
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    legend = ax.legend(fontsize = fontsize_legend, title = 'Sampler', 
                       loc="upper right", bbox_to_anchor=(1.2, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)    
    ax.set_xlim(0,track[-1])
    
    fig.tight_layout()
    figname = sampling_date + '_Bathymetry_sampling_points_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')

    ##########################################################################
    # Bathymetry and sampling points
    fig, ax = plt.subplots(1,1, figsize=(10, 6), dpi=100)
    #ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1)
    for i in index_samplers_used:
        ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['z_h'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('z/h (-)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylim(-0.05, 1.05)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    x_formatter = FixedFormatter(['0','0.2',"0.4","0.6",'0.8',"1"])
    x_locator = FixedLocator([0,0.2,0.4,0.6,0.8,1])
    ax.yaxis.set_major_formatter(x_formatter)
    ax.yaxis.set_major_locator(x_locator)
    #plt.gca().invert_yaxis()
       
    legend = ax.legend(fontsize = fontsize_legend, title = 'Sampler', 
                       loc="upper right", bbox_to_anchor=(1.2, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
   
    fig.tight_layout()
    figname = sampling_date +'_Z_h_sampling_points_' + most_frequent_sampler 
    fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')
           
def plot_data_D(analysis_data, outpath_figures, sampling_date):
    
    sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S', 'Niskin']
    forms_sampler = ['o', 'o','D', 's', 'P', '*', 'v']  
    size_sampler = [6, 6, 6, 6, 7, 8, 8]
    color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'mediumvioletred',
                     'darkorange', 'darkred', 'yellowgreen']
    samplers_used = np.unique(analysis_data['Sampler'])
    index_samplers_used = [sampler.index(samplers_used[i])
                           for i in range(len(samplers_used))]
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 12
    fontsize_legend_title = 14
    fontsize_text = 12
    fontsize_ticks = 12 
  
    # Sampling points (z/h)
    fig, ax = plt.subplots(1,1, figsize=(10, 6), dpi=100)
   
    for i in index_samplers_used:        
        ax.plot(analysis_data['Abscissa'][analysis_data['Sampler']== sampler[i]], 
                analysis_data['Sampling_depth_final'][analysis_data['Sampler']== sampler[i]],
                forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
                markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
        
    ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.set_ylabel('Sampling depth (m)', fontsize=fontsize_axis, weight = 'bold')
    ax.invert_yaxis()
    #ax.set_ylim(,-0.05)
    ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
    # x_formatter = FixedFormatter(['0','0.2',"0.4","0.6",'0.8',"1"])
    # x_locator = FixedLocator([0,0.2,0.4,0.6,0.8,1])
    # ax.yaxis.set_major_formatter(x_formatter)
    # ax.yaxis.set_major_locator(x_locator)
    #plt.gca().invert_yaxis()
       
    legend = ax.legend(fontsize = fontsize_legend, title = 'Sampler', 
                       loc="upper right", bbox_to_anchor=(1.2, 1))
    plt.setp(legend.get_title(), fontsize = fontsize_legend_title)
   
    fig.tight_layout()
    figname = sampling_date + '_Elevation_sampling_points_' + most_frequent_sampler
    fig.savefig(outpath_figures + figname + '.png', dpi = 300, bbox_inches='tight')

#%%####################################################################################

def plot_conc_flux_D50(analysis_data, outpath_figures, sampling_date, track, track_depth):    
    # Fontsizes
    fontsize_axis = 14
    fontsize_legend = 12
    fontsize_legend_title = 14
    fontsize_text = 12
    fontsize_ticks = 12 
    
    if np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1: #'Sand_flux_point_kg_s_m2' in analysis_data.columns:
        #def plot_point_sand_flux(analysis_data, outpath_figures, track, track_depth, sampling_date):
        # Sand flux without D50 --> BD
        most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
       
        fig, ax = plt.subplots(figsize=(10, 6), dpi=400)
        ax.plot(track, track_depth, color='black')
        ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1)
        sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                        s = 150, c = analysis_data['Sand_flux_point_kg_s_m2'],
                        edgecolor='black', linewidth=0.2, cmap = 'YlOrBr')
        
        plt.gca().invert_yaxis()
        
        # Legend flux
        # Legend concentration
        cb = fig.colorbar(sc, pad=0.03, anchor=(0, 1))
        cb.ax.set_ylabel('$\mathregular{\Phi_{sand}}$ (kg/s/m²)', weight = 'bold')
        cb.ax.yaxis.label.set_fontsize(12)
        cb.ax.tick_params(labelsize=12)
         
        ax.set_xlim(0, track[-1])
        ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
        ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
        
        fig.tight_layout()
        figname = sampling_date + '_Point_sand_flux_' + most_frequent_sampler
        fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')
        
        if 'D50' in analysis_data.columns:
            #def plot_point_sand_flux_D50(analysis_data, outpath_figures, track, track_depth, sampling_date):
            # Sand flux with D50 --> BD
            analysis_data_GSD = analysis_data[analysis_data['D50'].notna()]
            analysis_data_GSD.reset_index(drop=True, inplace=True)
            size_factor = ((100/(np.min(analysis_data_GSD['Sand_flux_point_kg_s_m2'])))+
                           400/(np.max(analysis_data_GSD['Sand_flux_point_kg_s_m2'])))/2
            
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)
            ax.plot(track, track_depth, color='black')
            
            # sc_noGSD = ax.plot(analysis_data['Abscissa'][analysis_data['D10'].isnull()], analysis_data['Sampling_depth_final'][analysis_data['D10'].isnull()],
            #               'o', markersize = 6, color = 'darkgrey', markeredgecolor = 'black', markeredgewidth=0.5,
            #               label = 'no GSD')
            
            sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            s=analysis_data['Sand_flux_point_kg_s_m2']*size_factor,
                            color='darkgrey', edgecolor='black', linewidth=0.2)
            ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1)
            
            plt.gca().invert_yaxis()
            
            sc2 = ax.scatter(analysis_data_GSD['Abscissa'], analysis_data_GSD['Sampling_depth_final'],
                            s=analysis_data_GSD['Sand_flux_point_kg_s_m2']*size_factor,
                            c = analysis_data_GSD['D50'], 
                            edgecolor='black', linewidth=0.2, cmap = 'plasma_r')
                        
            # Legend flux
            handles, labels = sc2.legend_elements(prop="sizes", num=6, color='darkgrey', markeredgecolor='black', linewidth=0.5,
                                                      fmt="{x:.2f}", func=lambda s: s/size_factor)
            legend1 = ax.legend(handles, labels, bbox_to_anchor=(1.02, 1), borderaxespad=0, edgecolor = 'black',
                                title='$\Phi_{sand}$ (kg/s/m²)', fontsize=12)
            plt.setp(legend1.get_title(), fontsize=fontsize_legend)
            
            # Legend D50
            cb = fig.colorbar(sc2, pad=0.05, shrink=0.5, anchor=(0, 0))
            cb.ax.set_ylabel('$\mathregular{D_{50}}$ (µm)', weight = 'bold')
            cb.ax.yaxis.label.set_fontsize(12)
            cb.ax.tick_params(labelsize=12)          
            
            ax.set_xlim(0, track[-1])
            ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
            
            fig.tight_layout()
            figname = sampling_date + '_Point_sand_flux_D50_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')

    if 'Concentration_sand_g_l' in analysis_data.columns:
        #def plot_point_sand_conc(analysis_data, outpath_figures, track, track_depth, sampling_date):    
        # Sand concentration --> P6, P72, Pump   
        most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
       
        fig, ax = plt.subplots(figsize=(10, 6), dpi=400)
        ax.plot(track, track_depth, color='black')
        ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1, zorder = 0)
        sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                        s= 100, c = analysis_data['Concentration_sand_g_l'],
                        edgecolor='black', linewidth=0.2, cmap = 'YlOrBr', alpha = 1, zorder = 30)
        plt.gca().invert_yaxis()
        
        # Legend concentration
        cb = fig.colorbar(sc, pad=0.03, anchor=(0, 1))
        cb.ax.set_ylabel('$\mathregular{\overline{C}_{sand}}$ (g/l)', weight = 'bold')
        cb.ax.yaxis.label.set_fontsize(fontsize_legend_title)
        cb.ax.tick_params(labelsize=12)
         
        ax.set_xlim(0, track[-1])
        ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
        ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
        
        fig.tight_layout()
        figname = sampling_date + '_Point_sand_concentration_' + most_frequent_sampler
        fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')

        if 'D50' in analysis_data.columns:
            #  def plot_point_sand_conc_D50(analysis_data, outpath_figures, track, track_depth, sampling_date):    
            # Sand concentration with D50 --> P6, P72, Pump 
            # customize bubble size
            # arange sizes between 100 and 400
            size_factor = ((100/np.min(analysis_data['Concentration_sand_g_l']))+
                            400/np.max(analysis_data['Concentration_sand_g_l']))/2
            
            analysis_data_GSD = analysis_data[analysis_data['D50'].notna()]
            analysis_data_GSD.reset_index(drop=True, inplace=True)
            
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)
            ax.plot(track, track_depth, color='black')
            ax.hlines(track[0], track[-1], 0, color = 'darkblue', linewidth = 1)
            sc2 = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            s=analysis_data['Concentration_sand_g_l']*size_factor,
                            color='darkgrey', edgecolor='black', linewidth=0.2)
            
            plt.gca().invert_yaxis()
            
            sc2 = ax.scatter(analysis_data_GSD['Abscissa'], analysis_data_GSD['Sampling_depth_final'],
                            s=analysis_data_GSD['Concentration_sand_g_l']*size_factor,
                            c = analysis_data_GSD['D50'], 
                            edgecolor='black', linewidth=0.2, cmap = 'plasma_r')
               
            # Legend flux
            handles, labels = sc2.legend_elements(prop="sizes", num=6, color='darkgrey', markeredgecolor='black', linewidth=0.5,
                                                      fmt="{x:.2f}", func=lambda s: s/size_factor)
            legend1 = ax.legend(handles, labels, bbox_to_anchor=(1.02, 1), borderaxespad=0, edgecolor = 'black',
                                title='$\overline{C}_{sand}$ (g/l)', fontsize=fontsize_legend)
            plt.setp(legend1.get_title(), fontsize=fontsize_legend_title)
            
            # Legend D50
            cb = fig.colorbar(sc2, pad=0.03, shrink=0.5, anchor=(0, -0.1))
            cb.ax.set_ylabel('$\mathregular{D_{50}}$ (µm)', weight = 'bold')
            cb.ax.yaxis.label.set_fontsize(fontsize_legend_title)
            cb.ax.tick_params(labelsize=fontsize_legend)
            
            ax.set_xlim(0, track[-1])
            ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
            
            fig.tight_layout()
            figname = sampling_date + '_Point_sand_concentration_D50_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')
            
#%%
        if 'Concentration_fine_g_l' in analysis_data.columns: 
            most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()

            coef = 250 / np.median(analysis_data['Sampling_depth_final'])
           
            # def plot_point_sand_fine_conc(analysis_data, outpath_figures, track, track_depth, sampling_date):    
            # Sand concentration with fine concentration --> P6, P72
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)
            ax.plot(track, track_depth, color='black')
            ax.hlines(track[0], track[-1], 0, color='darkblue', linewidth=1, zorder=0)
            plt.gca().invert_yaxis()

            sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            s=analysis_data['Concentration_sand_g_l']*coef,
                            c=analysis_data['Concentration_fine_g_l'],
                            edgecolor='black', linewidth=0.2, cmap='Oranges', zorder=30)

            # Legend D50
            legend2 = ax.legend(*sc.legend_elements(num=5, markeredgecolor='black'),
                                title="$\overline{C}_{fines}$ (g/l)", edgecolor='black',
                                facecolor='white', loc="upper left", bbox_to_anchor=(1.02, 0.34), fontsize=fontsize_legend-2,
                                prop={'size': 9})
            plt.setp(legend2.get_title(), fontsize=fontsize_legend_title-4)
            ax.add_artist(legend2)
            
            # Legend flux
            handles, labels = sc.legend_elements(prop="sizes", num=5, color='darkgrey', markeredgecolor='black', linewidth=0.5,
                                                      fmt="{x:.2f}", func=lambda s: s/coef)
            legend1 = ax.legend(handles, labels, bbox_to_anchor=(1.02, 0.98), edgecolor='black',  # borderaxespad=0,
                                title='$\overline{C}_{sand}$ (g/l)', fontsize=fontsize_legend-2, prop={'size': 9})
            plt.setp(legend1.get_title(), fontsize=fontsize_legend_title-2)
            # ax.add_artist(legend2)

            ax.set_xlim(0, track[-1])
            ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
            
            fig.tight_layout()
            figname = sampling_date + '_Point_sand_fine_concentration_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')
            
            # Plot Cfines/Csand ratio (S)
            
            # Define colormap
            nb_couleurs = 11  # en combien de parties je découpe mon échelle colorimétrique
            viridis = cm.get_cmap('RdBu_r', nb_couleurs)  # _r signifie que je prends l'échelle dans l'ordre inverse, ici clair vers sombre
            echelle_colorimetrique = viridis(np.linspace(0, 1, nb_couleurs))  # j'extrais des couleurs discrètes
            vecteur_blanc = np.array([1, 1, 1, 1])
            echelle_colorimetrique[5:6,:] = vecteur_blanc  # je remplace la première couleur par du blanc (défini en RGB) : 0 ou 1 ?...
            cmap = mpl_colors_ListedColormap(echelle_colorimetrique)  # je crée la colormap à partir de l'échelle colorimétrique
            cmap.set_under('black')  # en dessous de la limite min : gris
            cmap.set_over('saddlebrown')  # 
            cbounds = [0.1, 0.2, 0.25, 0.33, 0.5, 0.8, 1.2, 2, 3, 4, 5, 10]
            norm = mpl.colors.BoundaryNorm(cbounds, cmap.N)

            # Plot
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)

            ax.plot(track, track_depth, color='black')
            ax.hlines(track[0], track[-1], 0, color='darkblue', linewidth=1, zorder=0)
            cax = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            c=analysis_data['Cfine_Csand'], 
                            s = 100, edgecolor = 'black', linewidth = 0.3, cmap=cmap,norm=norm, zorder = 30)
            cbar = fig.colorbar(cax, ax=ax, extend='both', ticks=[0.1, 0.2, 0.25, 0.33, 0.5, 0.8, 1.2, 2, 3, 4, 5, 10])
            cbar.ax.set_yticklabels(['0.1','0.2','0.25','0.33','0.5','0.8','1.2','2','3', '4', '5','10'],ha='right')
            cbar.ax.yaxis.set_tick_params(pad=35) 
            cbar.set_label(r'$\mathregular{C_{fines}/C_{sand}}$', labelpad= 10, fontsize = fontsize_legend, weight = 'bold')
            plt.gca().invert_yaxis()

            ax.set_xlim(0, track[-1])
            ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')

            fig.tight_layout()
            figname = sampling_date + '_Point_ratio_fine_sand_concentration_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi = 400, bbox_inches='tight')
            

#####################################################################################

def plot_conc_flux_D50_B(analysis_data, outpath_figures, sampling_date, choice):    
    if np.count_nonzero(~np.isnan(np.array(analysis_data['Sand_flux_point_kg_s_m2']))) >= 1: #'Sand_flux_point_kg_s_m2' in analysis_data.columns:
        #def plot_point_sand_flux(analysis_data, outpath_figures, track, track_depth, sampling_date):
        # Sand flux without D50 --> BD
        most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
       
        fig, ax = plt.subplots(figsize=(10, 6), dpi=400)        
        sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                        s = 150, c = analysis_data['Sand_flux_point_kg_s_m2'],
                        edgecolor='black', linewidth=0.2, cmap = 'YlOrBr')        
        plt.gca().invert_yaxis()
        
        # Legend flux
        # Legend concentration
        cb = fig.colorbar(sc, pad=0.03, anchor=(0, 1))
        cb.ax.set_ylabel('$\mathregular{\Phi_{sand}}$ (kg/s/m²)', weight = 'bold')
        cb.ax.yaxis.label.set_fontsize(12)
        cb.ax.tick_params(labelsize=12)
         
        ax.set_xlim(0,)
        ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
        ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
        
        fig.tight_layout()
        figname = sampling_date + '_Point_sand_flux_' + most_frequent_sampler
        fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')
        
        if choice[2] == 1:
            #def plot_point_sand_flux_D50(analysis_data, outpath_figures, track, track_depth, sampling_date):
            # Sand flux with D50 --> BD
            analysis_data_GSD = analysis_data[analysis_data['D50'].notna()]
            analysis_data_GSD.reset_index(drop=True, inplace=True)
            size_factor = ((100/(np.min(analysis_data_GSD['Sand_flux_point_kg_s_m2'])))+
                           400/(np.max(analysis_data_GSD['Sand_flux_point_kg_s_m2'])))/2
            
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)            
        
            sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            s=analysis_data['Sand_flux_point_kg_s_m2']*size_factor,
                            color='darkgrey', edgecolor='black', linewidth=0.2)            
            plt.gca().invert_yaxis()            
            sc2 = ax.scatter(analysis_data_GSD['Abscissa'], analysis_data_GSD['Sampling_depth_final'],
                            s=analysis_data_GSD['Sand_flux_point_kg_s_m2']*size_factor,
                            c = analysis_data_GSD['D50'], 
                            edgecolor='black', linewidth=0.2, cmap = 'plasma_r')
                        
            # Legend flux
            handles, labels = sc2.legend_elements(prop="sizes", num=6, color='darkgrey', markeredgecolor='black', linewidth=0.5,
                                                      fmt="{x:.2f}", func=lambda s: s/size_factor)
            legend1 = ax.legend(handles, labels, bbox_to_anchor=(1.02, 1), borderaxespad=0, edgecolor = 'black',
                                title='$\Phi_{sand}$ (kg/s/m²)', fontsize=12)
            plt.setp(legend1.get_title(), fontsize=fontsize_legend)
            
            # Legend D50
            cb = fig.colorbar(sc2, pad=0.05, shrink=0.5, anchor=(0, 0))
            cb.ax.set_ylabel('$\mathregular{D_{50}}$ (µm)', weight = 'bold')
            cb.ax.yaxis.label.set_fontsize(12)
            cb.ax.tick_params(labelsize=12)
          
            # Legend flux, but no grain size data
            # ax.legend(fontsize = fontsize_legend)
            
            ax.set_xlim(0,)
            ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
            
            fig.tight_layout()
            figname = sampling_date + '_Point_sand_flux_D50_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')

    if 'Concentration_sand_g_l' in analysis_data.columns:
        #def plot_point_sand_conc(analysis_data, outpath_figures, track, track_depth, sampling_date):    
        # Sand concentration --> P6, P72, Pump   
        most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
       
        fig, ax = plt.subplots(figsize=(10, 6), dpi=400)        
        sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                        s= 100, c = analysis_data['Concentration_sand_g_l'],
                       edgecolor='black', linewidth=0.2, cmap = 'YlOrBr', alpha = 1, zorder = 30)
        plt.gca().invert_yaxis()
        
        # Legend concentration
        cb = fig.colorbar(sc, pad=0.03, anchor=(0, 1))
        cb.ax.set_ylabel('$\mathregular{\overline{C}_{sand}}$ (g/l)', weight = 'bold')
        cb.ax.yaxis.label.set_fontsize(fontsize_legend_title)
        cb.ax.tick_params(labelsize=12)
         
        ax.set_xlim(0, )
        ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
        ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
        
        fig.tight_layout()
        figname = sampling_date + '_Point_sand_concentration_' + most_frequent_sampler
        fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')

        if choice[2] == 1:            
            size_factor = ((100/np.min(analysis_data['Concentration_sand_g_l']))+
                           400/np.max(analysis_data['Concentration_sand_g_l']))/2
            
            analysis_data_GSD = analysis_data[analysis_data['D50'].notna()]
            analysis_data_GSD.reset_index(drop=True, inplace=True)
            
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)            
            sc2 = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            s=analysis_data['Concentration_sand_g_l']*size_factor,
                            color='darkgrey', edgecolor='black', linewidth=0.2)
            plt.gca().invert_yaxis()
            
            sc2 = ax.scatter(analysis_data_GSD['Abscissa'], analysis_data_GSD['Sampling_depth_final'],
                            s=analysis_data_GSD['Concentration_sand_g_l']*size_factor,
                            c = analysis_data_GSD['D50'], 
                            edgecolor='black', linewidth=0.2, cmap = 'plasma_r')
               
            # Legend flux
            handles, labels = sc2.legend_elements(prop="sizes", num=6, color='darkgrey', markeredgecolor='black', linewidth=0.5,
                                                      fmt="{x:.2f}", func=lambda s: s/size_factor)
            legend1 = ax.legend(handles, labels, bbox_to_anchor=(1.02, 1), borderaxespad=0, edgecolor = 'black',
                                title='$\overline{C}_{sand}$ (g/l)', fontsize=fontsize_legend)
            plt.setp(legend1.get_title(), fontsize=fontsize_legend_title)
            
            # Legend D50
            cb = fig.colorbar(sc2, pad=0.03, shrink=0.5, anchor=(0, -0.1))
            cb.ax.set_ylabel('$\mathregular{D_{50}$ (µm)', weight = 'bold')
            cb.ax.yaxis.label.set_fontsize(fontsize_legend_title)
            cb.ax.tick_params(labelsize=fontsize_legend)
            
            ax.set_xlim(0, )
            ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
            
            fig.tight_layout()
            figname = sampling_date + '_Point_sand_concentration_D50_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')
            
#%%
        if 'Concentration_fine_g_l' in analysis_data.columns: 
            most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
           
            # def plot_point_sand_fine_conc(analysis_data, outpath_figures, track, track_depth, sampling_date):    
            # Sand concentration with fine concentration --> P6, P72
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)
           
            plt.gca().invert_yaxis()            
            sc = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            s=analysis_data['Concentration_sand_g_l']*1500,
                            c = analysis_data['Concentration_fine_g_l'], 
                            edgecolor='black', linewidth=0.2, cmap = 'Oranges', zorder = 30)
            
            # Legend D50
            legend2 = ax.legend(*sc.legend_elements(num=5, markeredgecolor = 'black'),
                                title="$\overline{C}_{fines}$ (g/l)", edgecolor = 'black', 
                                facecolor = 'white', bbox_to_anchor = (1.02, 0.6), fontsize = fontsize_legend)
            plt.setp(legend2.get_title(), fontsize=fontsize_legend_title)
            ax.add_artist(legend2)
            
            # Legend flux
            handles, labels = sc.legend_elements(prop="sizes", num=5, color='darkgrey', markeredgecolor='black', linewidth=0.5,
                                                      fmt="{x:.2f}", func=lambda s: s/1500)
            legend1 = ax.legend(handles, labels, bbox_to_anchor=(1.02, 1), borderaxespad=0, edgecolor = 'black',
                                title='$\overline{C}_{sand}$ (g/l)', fontsize=fontsize_legend)
            plt.setp(legend1.get_title(), fontsize=fontsize_legend_title)
            
            ax.set_xlim(0, )
            ax.set_xlabel('Distance (m)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Depth (m)', fontsize=fontsize_axis, weight = 'bold')
            
            fig.tight_layout()
            figname = sampling_date + '_Point_sand_fine_concentration_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi=400, bbox_inches='tight')
            
            # Plot Cfines/Csand ratio (S)
            
            # Define colormap
            nb_couleurs = 11  # en combien de parties je découpe mon échelle colorimétrique
            viridis = cm.get_cmap('RdBu_r', nb_couleurs)  # _r signifie que je prends l'échelle dans l'ordre inverse, ici clair vers sombre
            echelle_colorimetrique = viridis(np.linspace(0, 1, nb_couleurs))  # j'extrais des couleurs discrètes
            vecteur_blanc = np.array([1, 1, 1, 1])
            echelle_colorimetrique[5:6,:] = vecteur_blanc  # je remplace la première couleur par du blanc (défini en RGB) : 0 ou 1 ?...
            cmap = mpl_colors_ListedColormap(echelle_colorimetrique)  # je crée la colormap à partir de l'échelle colorimétrique
            cmap.set_under('black')  # en dessous de la limite min : gris
            cmap.set_over('saddlebrown')  # 
            cbounds = [0.1, 0.2, 0.25, 0.33, 0.5, 0.8, 1.2, 2, 3, 4, 5, 10]
            norm = mpl.colors.BoundaryNorm(cbounds, cmap.N)

            # Plot
            fig, ax = plt.subplots(figsize=(10, 6), dpi=400)            
            cax = ax.scatter(analysis_data['Abscissa'], analysis_data['Sampling_depth_final'],
                            c=analysis_data['Cfine_Csand'], 
                            s = 100, edgecolor = 'black', linewidth = 0.3, cmap=cmap,norm=norm)
            cbar = fig.colorbar(cax, ax=ax, extend='both', ticks=[0.1, 0.2, 0.25, 0.33, 0.5, 0.8, 1.2, 2, 3, 4, 5, 10])
            cbar.ax.set_yticklabels(['0.1','0.2','0.25','0.33','0.5','0.8','1.2','2','3', '4', '5','10'],ha='right')
            cbar.ax.yaxis.set_tick_params(pad=35) 
            cbar.set_label(r'$C_{fines}/C_{sand}$', labelpad= 10, fontsize = 12)
            plt.gca().invert_yaxis()

            ax.set_xlim(0, )
            ax.set_xlabel('Distance (m)', fontsize=12)
            ax.set_ylabel('Depth (m)', fontsize=12)

            fig.tight_layout()
            figname = sampling_date + '_Point_ratio_fine_sand_concentration_' + most_frequent_sampler
            fig.savefig(outpath_figures + figname + '.png', dpi = 400, bbox_inches='tight')
            
