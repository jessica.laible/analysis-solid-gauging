"""
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import numpy as np
import pandas as pd
import datetime as dt
import math
from collections import Counter
from itertools import groupby
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from Functions import get_sec

def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / float(N)


def ranges(nums):
    nums = sorted(set(nums))
    gaps = [[s, e] for s, e in zip(nums, nums[1:]) if s + 1 < e]
    edges = iter(nums[:1] + sum(gaps, []) + nums[-1:])
    return list(zip(edges, edges))

def define_sampling_depth(analysis_data):
    analysis_data['Sampling_depth_final'] = analysis_data['Depth_field']

    return analysis_data

def determine_sampling_depth(path_diver, analysis_data, choice, position_diver=0.12, plot_diver_data=False):
    #%% Load diver data
    diver_data = pd.read_csv(path_diver, sep=";")

    # Adapt diver data
    if 'Pressure' in diver_data.columns:
        diver_data = diver_data.rename({'Pressure': 'Value'}, axis='columns')
        if ',' in str(diver_data.Value[0]):
            diver_data.Value = diver_data.Value.replace(',', '', regex=True)
    elif 'Depth' in diver_data.columns:
        diver_data = diver_data.rename({'Depth': 'Value'}, axis='columns')

    # Adapt prep data
    if 'Depth_field' in analysis_data.columns:
        analysis_data_depth = analysis_data['Depth_field']
        #analysis_data = analysis_data.rename({'Depth_field': 'Sampling_depth'}, axis='columns')
    if 'Height_field' in analysis_data.columns:
        #analysis_data = analysis_data.rename({'Height_field': 'Sampling_height'}, axis='columns')
        analysis_data_heigth = analysis_data['Height_field']

    # Determine duration thresholds
    most_frequent_sampler = max(set(list(analysis_data['Sampler'])), key=list(analysis_data['Sampler']).count).strip()
    if most_frequent_sampler == 'BD' or most_frequent_sampler == 'BD_C':
        decrementation = dt.datetime.strptime('00:00:10', '%H:%M:%S') - dt.datetime.strptime('00:00:00', '%H:%M:%S')
        min_sampling_duration = dt.datetime.strptime('00:02:50', '%H:%M:%S') - dt.datetime.strptime('00:00:00',
                                                                                                    '%H:%M:%S')
        min_time_delta = dt.timedelta(seconds=120)
    if most_frequent_sampler == 'P6' or most_frequent_sampler == 'P72':
        decrementation = dt.datetime.strptime('00:00:01', '%H:%M:%S') - dt.datetime.strptime('00:00:00', '%H:%M:%S')
        min_sampling_duration = dt.datetime.strptime('00:00:10', '%H:%M:%S') - dt.datetime.strptime('00:00:00',
                                                                                                    '%H:%M:%S')
        min_time_delta = dt.timedelta(seconds=10)

    # Drop surface samples
    analysis_data_diver = analysis_data.drop(analysis_data[analysis_data.Sampler == 'S'].index)
    analysis_data_diver = analysis_data_diver.reset_index(drop=True)

    # Determine diver type (indicating pressure or depth)
    if isinstance(diver_data['Value'][0], str):
        diver_data['Value'] = [x.replace(',', '.') for x in diver_data['Value']]
        diver_data['Value'] = diver_data['Value'].astype(float)

    # Mean pressure/depth value
    diver_value_mean = np.nanmedian(diver_data['Value'])
    diver_value_ref = np.nanquantile(diver_data['Value'], 0.25)
    time_step_diver = dt.datetime.strptime(diver_data['Time'][1], '%H:%M:%S') - dt.datetime.strptime(
        diver_data['Time'][0], '%H:%M:%S')

    min_id_delta = int(min_time_delta / time_step_diver)

    if diver_value_mean >= 900:
        diver_type = 'Pressure'
        parameters = [2, max(1040, diver_value_mean + 20), min_time_delta,
                      int(1 + 10 / time_step_diver.total_seconds()),
                      min_id_delta]
    else:
        diver_type = 'Depth'
        parameters = [0.1, max(diver_value_mean + 0.08, 0.1), min_time_delta,
                      int(1 + 10 / time_step_diver.total_seconds()),
                      min_id_delta]

    t_analysis_data = pd.to_datetime(analysis_data_diver['Time']).dt.time
    t_diver_data = pd.to_datetime(diver_data['Time'], format='%H:%M:%S').dt.time  # .tolist()
    diver_data['Time_t'] = t_diver_data
    depth_analysis = analysis_data_depth

    # Moving average to smooth data
    value_running_mean = running_mean(list(diver_data['Value']), 2 * (parameters[3]) + 1)
    value_running_mean = np.append(np.insert(value_running_mean, 0, diver_data['Value'][:parameters[3]]),
                                   diver_data['Value'][-parameters[3]:])
    diver_data['value_running_mean'] = value_running_mean

    diver_range = diver_data[
        (diver_data['Time_t'] > (dt.datetime.combine(dt.date(1, 1, 1), min(t_analysis_data.dropna())) -
                                 dt.timedelta(hours=0, minutes=8)).time()) & (
                diver_data['Time_t'] <
                (dt.datetime.combine(dt.date(1, 1, 1),
                                     max(t_analysis_data.dropna())) + dt.timedelta(hours=0,
                                                                                   minutes=20)).time())]
    diver_range = diver_range.reset_index()

    # Plot diver data if necessary
    if plot_diver_data:
        diver_range['Id'] = [i for i in range(len(diver_range['Time']))]
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(diver_range['Id'], diver_range['Value'])
        # ax.plot(pd.to_datetime(diver_range['Time'], format='%H:%M:%S'), diver_range['Value'])
        plt.show()
#%%
    if diver_type == 'Depth':
        diver_depth = diver_range['value_running_mean'].values
        diver_depth[diver_depth < 0.08] = 0
        diver_range['depth'] = diver_depth
    elif diver_type == 'Pressure':
        default_pressure = np.nanquantile(diver_range['Value'], 0.2)
        diver_depth = (diver_range['value_running_mean'] - default_pressure) / 100
        diver_depth[diver_depth < 0.1] = 0
        diver_range['depth'] = diver_depth

    # Get main slots
    in_water_id = [i for i in range(len(diver_range)) if diver_range['value_running_mean'][i] >= parameters[1]]
    slot_index_range = ranges(in_water_id)
    slot_lag = [i[1] - i[0] >= parameters[4] for i in slot_index_range]
    slot_index_range = [i for (i, v) in zip(slot_index_range, slot_lag) if v]

    df = pd.DataFrame({
        'id_start': [i[0] for i in slot_index_range],
        'id_end': [i[1] for i in slot_index_range],
        'time_start': diver_range.loc[[i[0] for i in slot_index_range], 'Time_t'].values,
        'time_end': diver_range.loc[[i[1] for i in slot_index_range], 'Time_t'].values,
        # 'slot_depth': slot_depth
    })
    drop_row = []
    for i_df in range(len(df) - 1):
        if dt.datetime.combine(dt.date.min, df.loc[i_df + 1, 'time_start']) - \
                dt.datetime.combine(dt.date.min, df.loc[i_df, 'time_end']) < dt.timedelta(seconds=30):
            df.loc[i_df + 1, 'time_start'] = df.loc[i_df, 'time_start']
            df.loc[i_df + 1, 'id_start'] = df.loc[i_df, 'id_start']
            drop_row.append(i_df)
    df.drop(drop_row, inplace=True)
    df = df.reset_index()

    main_slot_index = list(zip(df.id_start, df.id_end))

    slot_depth = []
    for i in range(len(main_slot_index)):
        slot_depth.append(
            np.nanquantile(diver_range.loc[main_slot_index[i][0]:main_slot_index[i][1] + 1, 'depth'], 0.75))
    df['slot_depth'] = slot_depth

    # Find corresponding slot in main spot list
    slot_index = []
    lag_time = []
    for id_time in range(len(t_analysis_data)):
        t_time = t_analysis_data[id_time]
        depth_ref = depth_analysis[id_time]
        if str(t_time) != 'NaT' and (np.isnan(depth_analysis[id_time]) or depth_analysis[id_time] > 0.1):
            start_time = (dt.datetime.combine(dt.date(1, 1, 1), t_time) - dt.timedelta(hours=0, minutes=8)).time()
            end_time = (dt.datetime.combine(dt.date(1, 1, 1), t_time) + dt.timedelta(hours=0, minutes=8)).time()
            df_time = df[(df['time_start'] < end_time) & (df['time_end'] > start_time)]
            delta_min = dt.timedelta(hours=1)
            index_k = None
            for k in df_time.index:
                if np.isnan(depth_ref) or depth_ref - 1 < df_time.slot_depth.loc[k] <= depth_ref + 1:
                    delta_t = min(abs(dt.datetime.combine(dt.date.min, t_time) -
                                      dt.datetime.combine(dt.date.min, df_time.loc[k, 'time_start'])),
                                  abs(dt.datetime.combine(dt.date.min, t_time) -
                                      dt.datetime.combine(dt.date.min, df_time.loc[k, 'time_end'])))
                    if delta_t < delta_min:
                        delta_min = delta_t
                        index_k = k
            if index_k is not None:
                slot_index.append(index_k)
                lag_time.append(delta_min)
            else:
                slot_index.append(np.nan)
                lag_time.append(np.nan)
        else:
            slot_index.append(np.nan)
            lag_time.append(np.nan)

    # Group non nan values
    g = groupby(slot_index, key=type)  # group by type of the data, int != float
    groups = [list(a[1]) for a in g if a[0] != float]
    # Group nan
    g = groupby(slot_index, key=type)
    nan_groups = [list(a[1]) for a in g if a[0] != int]
    if np.isnan(slot_index[0]):
        is_start_nan = 0
    else:
        is_start_nan = 1

    # Fix incoherent result : same id duplicated
    index_fixed = []
    for i in range(len(groups)):
        if is_start_nan == 0:
            index_fixed.append(nan_groups[i])
        groups_i = groups[i]
        len_i = len(groups_i)
        if len_i != len(set(groups_i)):
            if len_i == groups_i[-1] - groups_i[0] + 1:
                groups[i] = list(range(groups_i[0], groups_i[-1] + 1))
            elif i != len(groups) - 1 and groups[i + 1][0] - groups[i][0] == len_i:
                groups[i] = list(range(groups_i[0], groups[i + 1][0]))
            elif len_i <= groups_i[-1] - groups_i[0]:
                range_i = list(range(groups_i[0], groups_i[-1] + 1))
                diff_i = list(set(range_i) - set(groups_i))
                keep_list = []
                for number in [item for item, count in Counter(groups_i).items() if count > 1]:
                    keep_list.append(min(diff_i, key=lambda x: abs(x - number)))
                if len([item for item, count in Counter(keep_list).items() if count > 1]) == 0:
                    for k in list(set(diff_i) - set(keep_list)):
                        range_i.remove(k)
                    if len(groups[i]) == len(range_i):
                        groups[i] = range_i
            elif len([item for item, count in Counter(groups_i).items() if count > 1]) > 0:
                count_i = [item for item, count in Counter(groups_i).items() if count > 1]
                groups[i] = [x if x not in count_i else np.nan for x in groups_i]
        index_fixed.append(groups[i])
        if is_start_nan == 1 and i < len(nan_groups):
            index_fixed.append(nan_groups[i])
    if len(groups) < len(nan_groups):
        index_fixed.append(nan_groups[-1])

    # Convert into diver dataframe index
    index_start = []
    index_end = []
    flat_list = [item for sublist in index_fixed for item in sublist]
    for value in flat_list:
        if not np.isnan(value):
            index_start.append(df.loc[value, 'id_start'])
            index_end.append(df.loc[value, 'id_end'])
        else:
            index_start.append(np.nan)
            index_end.append(np.nan)

    #%% Fix nan values which are still here
    # TODO optimize properly
    if np.isnan(flat_list).any():
        nan_array = np.where(np.isnan(flat_list))[0]
        nan_ranges = sorted(ranges(nan_array))
        for (nan_start, nan_end) in nan_ranges:
            print(nan_start, nan_end)
            if nan_start > 0:
                id_before = index_end[nan_start - 1] + 1
            else:
                id_before = 0

            if nan_end < len(t_analysis_data) - 1:
                id_after = index_start[nan_end + 1]
            else:
                id_after = -1

            depths_nan = depth_analysis[nan_start:nan_end + 1]
            time_nan = t_analysis_data[nan_start:nan_end + 1]

            if not all(v < 0.1 for v in depths_nan):
                diver_nan_time = diver_range.iloc[id_before:id_after]
                mean_nan_time = np.nanquantile(diver_nan_time.Value, 0.25)
                in_nan_water_id = [i for i in diver_nan_time.index if
                                   abs(diver_value_ref - diver_nan_time['value_running_mean'].loc[[i]].values[0]) >=
                                   parameters[0] and diver_nan_time.Value.loc[[i]].values[0] >= mean_nan_time]

                fill_range = ranges(in_nan_water_id)
                fill_valid = []
                for i in range(len(fill_range)):
                    if i != len(fill_range) - 1 and fill_range[i][1] + parameters[3] >= fill_range[i + 1][0]:
                        fill_range[i + 1] = (fill_range[i][0], fill_range[i + 1][1])
                    elif fill_range[i][1] - fill_range[i][0] >= parameters[3]:
                        fill_valid.append(fill_range[i])

                nan_values = [np.nan] * (nan_end - nan_start + 1)

                # Auto complete if perfect matching
                # if len(fill_valid)>0 and (len(fill_valid) == np.sum(depths_nan>0.1) or \
                #         len(fill_valid) == np.sum(np.isnan(depths_nan))):
                if len(fill_valid) > 0 and len(fill_valid) == np.sum(depths_nan > 0.1) + np.sum(np.isnan(depths_nan)):
                    inc = 0
                    for d in range(len(depths_nan)):
                        if np.isnan(depths_nan.iloc[d]) or (depths_nan.iloc[d] > 0.1 and \
                                                            (abs(depths_nan.iloc[d] - np.nanmedian(diver_range.loc[
                                                            fill_valid[inc][0]:fill_valid[inc][1] + 1][
                                                            'depth'])) < 1.2 or \
                                                            np.sum(depths_nan > 0.1) == len(fill_valid))):
                            nan_values[d] = fill_valid[inc]
                            inc += 1
                    # Remove values which are assigned
                    for value in nan_values:
                        if value in fill_valid:
                            fill_valid.remove(value)

                # Check with time
                if len(fill_valid) > 0 and np.sum([str(i) != 'NaT' for i in
                                                   time_nan]) > 0:  # len(fill_valid) >= np.sum([str(i)!= 'NaT' for i in time_nan]):
                    list_delta = [dt.timedelta(hours=1)] * len(depths_nan)
                    for t in range(len(time_nan)):
                        time_t = time_nan.iloc[t]
                        if type(nan_values[t]) == float and str(time_t) != 'NaT' and (np.isnan(depths_nan.iloc[t]) or \
                        depths_nan.iloc[t] > 0.1):

                            for (start, end) in fill_valid:
                                if dt.datetime.combine(dt.date.min, diver_range['Time_t'][end]) - dt.datetime.combine(
                                        dt.date.min, diver_range['Time_t'][start]) > parameters[2]:
                                    delta_t = min(
                                        abs(dt.datetime.combine(dt.date.min, time_t) - dt.datetime.combine(dt.date.min,
                                                                                                           diver_range[
                                                                                                               'Time_t'][
                                                                                                               start])),
                                        abs(dt.datetime.combine(dt.date.min, time_t) - dt.datetime.combine(dt.date.min,
                                                                                                           diver_range[
                                                                                                               'Time_t'][
                                                                                                               end])))
                                    if delta_t < list_delta[t]:
                                        list_delta[t] = delta_t
                                        nan_values[t] = (start, end)
                    # Keep closest value
                    for (start, end) in [item for item, count in Counter(nan_values).items() if
                                         count > 1 and type(item) == tuple]:
                        dup_index = [i for i, e in enumerate(nan_values) if e == (start, end)]
                        dup_delta = [list_delta[i] for i in dup_index]
                        _, idx = min((val, idx) for (idx, val) in enumerate(dup_delta))
                        del dup_index[idx]
                        for k in dup_index:
                            nan_values[k] = np.nan
                    # Remove values which are assigned
                    for value in nan_values:
                        if value in fill_valid:
                            fill_valid.remove(value)

                # Check with depth
                if len(fill_valid) > 0 and np.sum([~np.isnan(i) for i in depths_nan]) > 0:
                    list_delta = [999] * len(depths_nan)
                    for d in range(len(depths_nan)):
                        depth_d = depths_nan.iloc[d]
                        if type(nan_values[d]) == float and not np.isnan(depth_d) and depths_nan.iloc[d] > 0.1:
                            for (start, end) in fill_valid:
                                delta_depth = abs(depth_d - np.nanquantile(diver_range.loc[start:end + 1]['depth'], 0.75))
                                if delta_depth < list_delta[d] and delta_depth < 1.2:
                                    list_delta[d] = delta_depth
                                    nan_values[d] = (start, end)
                    # Keep closest value
                    for (start, end) in [item for item, count in Counter(nan_values).items() if
                                         count > 1 and type(item) == tuple]:
                        dup_index = [i for i, e in enumerate(nan_values) if e == (start, end)]
                        dup_delta = [list_delta[i] for i in dup_index]
                        _, idx = min((val, idx) for (idx, val) in enumerate(dup_delta))
                        del dup_index[idx]
                        for k in dup_index:
                            nan_values[k] = np.nan
                    # Remove values which are assigned
                    for value in nan_values:
                        if value in fill_valid:
                            fill_valid.remove(value)

                index_start[nan_start:nan_end + 1] = [i[0] if type(i) == tuple else np.nan for i in nan_values]
                index_end[nan_start:nan_end + 1] = [i[1] if type(i) == tuple else np.nan for i in nan_values]


    # Check limits of selection
    slot_start = []
    slot_end = []
    slot_stab_start = []
    slot_stab_end = []
    slot_depth = []
    for id_index in range(len(index_start)):
        # Define area of work
        start_valid = index_start[id_index] - 5
        end_valid = index_end[id_index] + 5
        if start_valid < 0:
            start_valid = 0
        if end_valid > len(diver_range):
            end_valid = len(diver_range)

        if not np.isnan(start_valid):
            diver_valid = diver_range.loc[start_valid:end_valid]
            # Find variation in diver data values
            diver_var = (diver_valid.Value[1:].values - diver_valid.Value[:-1].values)

            # Start of the slot
            start_slot = next((x[0] for x in enumerate(diver_var) if x[1] >= parameters[0]), None)
            end_slot = next((len(diver_var) - x[0] for x in enumerate(diver_var[::-1]) if
                             x[1] <= -parameters[0]), None)

            if start_slot is not None:
                slot_start.append(diver_valid.index[start_slot])
            else:
                slot_start.append(index_start[id_index])

            if end_slot is not None:
                slot_end.append(diver_valid.index[end_slot])
            else:
                slot_end.append(index_end[id_index])

            diver_slot = diver_range.loc[slot_start[-1]:slot_end[-1]]
            diver_ref = diver_slot[diver_slot.Value >= np.nanquantile(diver_slot.Value, 0.2)]

            func = {'cst': lambda x, b: b}
            x_diver_ref = list(range(len(diver_ref.Value)))
            f = curve_fit(func['cst'], x_diver_ref, diver_ref.Value.values)

            stab_start = next((x[0] for x in enumerate(diver_slot.Value) if x[1] >= f[0] - parameters[0]), None)
            stab_end = next((len(diver_slot) - x[0] - 1 for x in enumerate(diver_slot.Value[::-1]) if
                             x[1] >= f[0] - parameters[0]), None)

            if stab_start is not None:
                slot_stab_start.append(diver_slot.index[stab_start])
            else:
                slot_stab_start.append(index_start[id_index] + 1)

            if stab_end is not None:
                slot_stab_end.append(diver_slot.index[stab_end])
            else:
                slot_stab_end.append(index_end[id_index] - 1)

            slot_depth.append(np.nanmax(diver_slot.loc[slot_stab_start[-1]:slot_stab_end[-1] + 1, 'depth']))
        else:
            slot_start.append(np.nan)
            slot_end.append(np.nan)
            slot_stab_start.append(np.nan)
            slot_stab_end.append(np.nan)
            if not np.isnan(depth_analysis[id_index]) and depth_analysis[id_index] < 0.1:
                slot_depth.append(depth_analysis[id_index])
            else:
                slot_depth.append(np.nan)

    df_result = pd.DataFrame({'start': slot_start, 'stab_start': slot_stab_start, 'stab_end': slot_stab_end, 'end': slot_end})

    dive_duration = []
    stab_duration = []
    ascent_duration = []
    for i in range(len(slot_stab_end)):
        if not np.isnan(slot_stab_start[i]):
            dive_duration.append(str(dt.datetime.combine(dt.date.min, diver_range.loc[slot_stab_start[i], 'Time_t']) - \
                                     dt.datetime.combine(dt.date.min, diver_range.loc[slot_start[i], 'Time_t'])))

            stab_duration.append(str(dt.datetime.combine(dt.date.min, diver_range.loc[slot_stab_end[i], 'Time_t']) - \
                                     dt.datetime.combine(dt.date.min, diver_range.loc[slot_stab_start[i], 'Time_t'])))

            ascent_duration.append(str(dt.datetime.combine(dt.date.min, diver_range.loc[slot_end[i], 'Time_t']) - \
                                       dt.datetime.combine(dt.date.min, diver_range.loc[slot_stab_end[i], 'Time_t'])))
        else:
            dive_duration.append(np.nan)
            stab_duration.append(np.nan)
            ascent_duration.append(np.nan)

    sampling_depth = [np.nan] * len(slot_depth)
    for i in range(len(slot_depth)):
        if analysis_data_depth[i] < 0.1:
            sampling_depth[i] = 0.05
        elif analysis_data_diver['Sampler'][i] == 'BD':
            if math.isnan(analysis_data_heigth[i]):
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.5)
            elif analysis_data_heigth[i] == 0.1:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.1)
            elif analysis_data_heigth[i] == 0.08:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.1)
            elif analysis_data_heigth[i] == 0.2:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.2)
            elif analysis_data_heigth[i] == 0.18:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.2)
            elif analysis_data_heigth[i] == 0.4:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.4)
            elif analysis_data_heigth[i] == 0.5:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.5)
            elif analysis_data_heigth[i] == 1:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - 0.5)
            else:
                sampling_depth[i] = max(0.05, slot_depth[i] + position_diver - analysis_data_heigth[i])
        else:
            sampling_depth[i] = max(0.05, slot_depth[i] + position_diver)

    #%% Add to analysis data   
    analysis_data['Sampling_depth_final'] = [0] *len(analysis_data)
    analysis_data['Sampling_depth_final'][analysis_data['Sampler'] == most_frequent_sampler] = np.array(sampling_depth)
    
    dive_duration1 = []
    stab_duration1 = []
    ascent_duration1 = []
    if most_frequent_sampler == 'BD':
        for i in range(len(dive_duration)):           
            if isinstance(dive_duration[i],str) == True:
                if len(dive_duration[i]) == 7:
                    dive_durationn = '0' + dive_duration[i]
                    stab_durationn = '0' + stab_duration[i] 
                    ascent_durationn = '0' + ascent_duration[i] 
                else:
                    dive_durationn = dive_duration[i]
                    stab_durationn = stab_duration[i] 
                    ascent_durationn = ascent_duration[i] 
                    
                #dive_durationnu = get_sec(dive_durationn)
                
                dive_duration1.append(get_sec(dive_durationn))
                stab_duration1.append(get_sec(stab_durationn))
                ascent_duration1.append(get_sec(ascent_durationn))
            elif isinstance(dive_duration[i],str) == False:
                dive_durationn = np.nan
                stab_durationn = np.nan
                ascent_durationn = np.nan
                dive_duration1.append(dive_durationn)
                stab_duration1.append(stab_durationn)
                ascent_duration1.append(ascent_durationn)
       
        analysis_data['Dive_duration_s'] = [np.nan] *len(analysis_data)
        analysis_data['Dive_duration_s'][analysis_data['Sampler'] == most_frequent_sampler] = [
            (dive_duration1[i]) for i in range(len(dive_duration))] 
        analysis_data['Sampling_duration_s'] = [np.nan] *len(analysis_data)
        analysis_data['Sampling_duration_s'][analysis_data['Sampler'] == most_frequent_sampler] = [
            (stab_duration1[i]) for i in range(len(stab_duration))]   
        analysis_data['Ascent_duration_s'] = [np.nan] *len(analysis_data)
        analysis_data['Ascent_duration_s'][analysis_data['Sampler'] == most_frequent_sampler] = [
            (ascent_duration1[i]) for i in range(len(ascent_duration))]   
        
    # Normalized sampling depth
    if analysis_data['Height_field'] is not np.nan:
        analysis_data['vertical_depth'] = analysis_data['Sampling_depth_final'] + analysis_data['Height_field']
        for id in analysis_data.loc[analysis_data['vertical_depth'].isna()].index:
            x = analysis_data.iloc[id]['Abscissa']
            depth_value = analysis_data[analysis_data['Abscissa'] == x]['vertical_depth'].mean()
            analysis_data.loc[id, 'vertical_depth'] = depth_value 
            
     #%%       
    return analysis_data






