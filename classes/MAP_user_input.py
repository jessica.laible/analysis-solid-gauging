"""
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import tkinter as tk
from tkinter import messagebox

class map_user_input() :
    def __init__(self):
        self.input_map = [None, None, None, None, '']

        self.transect = None
        self.nav_ref = None
        self.node_horz = None
        self.node_vert = None
        self.folder = ''

    def open(self):
        text = ['Transect user [list, sep=,]', 'Navigation reference [BT, GPS]', 'Nodes horizontal [float]',
                'Nodes vertical [float]', 'Name MAP folder [string]']
        self.value = None
        self.ws = tk.Tk()
        self.ws.title('MAP Input')
        self.ws.geometry("400x250")
        self.ws.wm_attributes("-topmost", 1)
        self.ws.bind("<Return>", self.generate)

        self.tk_transect = tk.StringVar()
        self.tk_nav_ref = tk.StringVar()
        self.tk_node_horz = tk.StringVar()
        self.tk_node_vert = tk.StringVar()
        self.tk_folder = tk.StringVar()

        i = 0
        self.label_transect = tk.Label(self.ws, text=text[i])
        self.label_transect.grid(row=i, column=0, padx=3)
        self.entry_transect = tk.Entry(self.ws, textvariable=self.tk_transect)
        self.entry_transect.grid(row=i, column=1, padx=10, pady=10)
        i += 1
        self.label_nav_ref = tk.Label(self.ws, text=text[i])
        self.label_nav_ref.grid(row=i, column=0, padx=3)
        self.entry_nav_ref = tk.Entry(self.ws, textvariable=self.tk_nav_ref)
        self.entry_nav_ref.grid(row=i, column=1, padx=10, pady=10)
        i += 1
        self.label_node_horz = tk.Label(self.ws, text=text[i])
        self.label_node_horz.grid(row=i, column=0, padx=3)
        self.entry_node_horz = tk.Entry(self.ws, textvariable=self.tk_node_horz)
        self.entry_node_horz.grid(row=i, column=1, padx=10, pady=10)
        i += 1
        self.label_node_vert = tk.Label(self.ws, text=text[i])
        self.label_node_vert.grid(row=i, column=0, padx=3)
        self.entry_node_vert = tk.Entry(self.ws, textvariable=self.tk_node_vert)
        self.entry_node_vert.grid(row=i, column=1, padx=10, pady=10)
        i += 1
        self.label_folder = tk.Label(self.ws, text=text[i])
        self.label_folder.grid(row=i, column=0, padx=3)
        self.entry_folder = tk.Entry(self.ws, textvariable=self.tk_folder)
        self.entry_folder.grid(row=i, column=1, padx=10, pady=10)

        self.button = tk.Button(self.ws, text='Ok [Return]', command=self.generate, padx=50, pady=10)
        self.button.grid(row=i + 1, column=0, columnspan=2)
        self.ws.mainloop()

    def generate(self, event=None):
        try:
            if len(self.entry_transect.get())>0:
                transect_list = self.entry_transect.get()
                transect = [int(s) for s in transect_list.split(',')]
                self.transect = list(map(int, transect))
            if len(self.entry_nav_ref.get()) > 0:
                self.nav_ref = str(self.entry_nav_ref.get())
            if len(self.entry_node_horz.get()) > 0:
                self.node_horz = float(self.entry_node_horz.get())
            if len(self.entry_node_vert.get()) > 0:
                self.node_vert = float(self.entry_node_vert.get())
    
            self.folder = self.entry_folder.get()
    
            self.input_map = [self.transect, self.nav_ref, self.node_horz, self.node_vert, self.folder]
    
            self.ws.destroy()
            print(self.input_map)
        except Exception as ex:
            messagebox.showwarning('warning', ex)

# map_user_input()
#
# def map_user_input() :
#
#     master = tk.Tk()
#     master.geometry("400x250")
#     master.title('Information for user input')
#     master.wm_attributes("-topmost", 1)
#     # whatever_you_do = "Warning for datas you need to enter : \n" \
#     #                   " - If you want to apply MAP default values, let the box empty. \n" \
#     #                   " - For transect user, you need to separate transect number by comma. \n" \
#     #                   " - For navigation reference, two choices : BT or GPS. \n" \
#     #                   " - For nodes horizontal, MAP default value is a fine parametre in general. \n" \
#     #                   " - For nodes vertical, you can apply value = zmax * 0.05, for exemple if max depth is 4m you can put 0.2. \n" \
#     #                   " - For the MAP folder, a classic name may be Version_date_number of transects, care to separate with underscore"
#     # msg = tk.Message(master, text=whatever_you_do)
#     # msg.config(font=("Courier", 10))
#     # msg.pack()
#     # tk.mainloop()
#
#     text = ['Transect user (1,2,4888)', 'Navigation reference (BT, GPS)', 'Nodes horizontal (1)', 'Nodes vertical (0.2)', 'Name MAP folder (Version_27_07_2022_124)']
#
#     my_w = tk.Tk()
#     my_w.geometry("400x250")  # Size of the window
#     my_w.title("MAP_Input")  #  title
#     number_of_inputs=5 # Number of entries required
#     ref=[] # to store the references
#     for j in range(number_of_inputs):
#         l=tk.Label(my_w,text=text[j])
#         l.grid(row=j,column=0,padx=3)
#
#         e = tk.Entry(my_w)
#         e.grid(row=j, column=1,padx=10,pady=10)
#
#         ref.append(e) # store references
#
#     input_getted = []
#     def my_check():
#         my_flag=False
#         for w in ref:
#             input_getted.append(w.get())
#         my_w.destroy()
#
#     b1=tk.Button(my_w,text='Submit',command=lambda: my_check())
#     b1.grid(row=j+1,column=0,padx=10,pady=5)
#     my_w.mainloop()  # Keep the window open
#
#
#     # Treatment input_getted
#     for i in range(len(input_getted)) :
#         if input_getted[i] == '' :
#             input_getted[i] = None
#
#     if input_getted[0] != None :
#         input_getted[0] = eval(input_getted[0])
#     if input_getted[3] != None :
#         input_getted[3] = float(input_getted[3])
#
#     return(input_getted)