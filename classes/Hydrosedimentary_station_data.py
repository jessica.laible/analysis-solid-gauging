"""
Determine water discharge, stage and concentration measured 
by a hydrosedimentary station

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import pandas as pd
from datetime import date, timedelta, datetime

def hydrosedimentary_data(analysis_data, path):  
    data_raw = pd.read_csv(path, sep=";")
    data = data_raw.drop(data_raw[data_raw['Value'] == '-9999'].index, inplace = False)
    data.reset_index(drop = True)
     # Define timestamps
    data['Timestamp'] = pd.to_datetime(data.iloc[:,0],format='%d.%m.%Y %H:%M') # if diff format    
    sampling_datetime = analysis_data['Sampling_datetime']
    start_sampling = pd.to_datetime(analysis_data['Sampling_datetime'][0], format = '%Y-%m-%d %H:%M:%S') - timedelta (hours = 1)
    end_sampling = pd.to_datetime(sampling_datetime.iloc[-1], format = '%Y-%m-%d %H:%M:%S') + timedelta (hours = 1)
    # Get data during sampling time
    mask = (data['Timestamp'] >= start_sampling) & (data['Timestamp'] <= end_sampling)
    sampling = data.loc[mask] 
    sampling.reset_index(drop=True)
    
    return sampling

