"""
Determines and extracts the concentration, flux and grain size distribution of sand and fine sediments
at the height of the HADCPs depending on water stage 

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import numpy as np
import pandas as pd
import pickle
import math as math

# Only for cableway
def HADCP_function(analysis_data, map_data, stage_sampling, outpath, sampling_date, 
                              Conc_SDC_cell_export, sand_flux_SDC_kg_s_export, Conc_fine_cell_export, 
                              fine_flux_kg_s_export, ratio_cell, GSD_linear_cell_export, D50_linear_cell_export):

    #%% Use station data to determine HADCP height (depth)
    
    # Load HADCP data 
    path_folder = r'C:\Users\jessica.laible\Documents\Acoustics\3_TW16\Results\Total_beam_USGS'
    freq1 = 400
    freq2 = 1000
    with open(path_folder + '\celldist_along_beam_'+ str(freq1) + '.txt', "rb") as fp:    
       celldist_along_beam_freq1 = pickle.load(fp)
    with open(path_folder + '\celldist_along_beam_'+ str(freq2) + '.txt', "rb") as fp:    
        celldist_along_beam_freq2 = pickle.load(fp)
        
    # HADCP depth
    HADCP_depth = np.nanmean(stage_sampling['Value'] - 0.2)
    
    # HADCP range 
    range_freq1 = celldist_along_beam_freq1[-1]
    range_freq2 = celldist_along_beam_freq2[-1]
    
    # HADCP beam (triangle using beam width at maximum range)
    # Beam width, height at max range 
    beam_angle_freq1 = 1.7
    beam_angle_freq2 = 3.4
    beam_angle_freq1_rad = beam_angle_freq1*(math.pi/180)
    beam_angle_freq2_rad = beam_angle_freq2*(math.pi/180)
    
    max_height_range_freq1 = math.sin(beam_angle_freq1_rad)*celldist_along_beam_freq1[-1]
    max_height_range_freq2 = math.sin(beam_angle_freq2_rad)*celldist_along_beam_freq2[-1]
    
           
    #%% Define MAP grid
    cell_height = map_data.depth_cells_border[1][0]-map_data.depth_cells_border[0][0]  
    depth_cells = np.arange(map_data.depth_cells_border[0][0], (len(map_data.depth_cells_border)*cell_height), cell_height)
       
    # Calculate cell properties
    cell_height = map_data.border_depth[1] - map_data.border_depth[0]
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
    depth_cells = map_data.border_depth
    
    # Length and depth of transect (center of each distance cell, plus start (0) and end)
    track = (border_cells[1:] + border_cells[:-1]) / 2
    track = np.append(np.insert(track, 0, 0), border_cells[-1])
    
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    track_depth = np.append(np.insert(track_depth, 0, 0), 0)
    
    # Create height above bottom grid
    height_midpoints_cells=[]
    for i in range(len(track_depth)):
        heh = int(track_depth[i]//cell_height)
        height_last_cell = (track_depth[i]-depth_cells[heh])
        midpoint_last_cell = np.array([height_last_cell/2])   # use half-distance between bottom and bottom of lowest complete cell for bottom cell
        midpoint_height_cells = np.arange(0, heh)*cell_height + (height_last_cell + cell_height/2)
        height_midpoints_cells1 = np.concatenate([midpoint_last_cell,midpoint_height_cells], axis = 0)
        height_midpoints_cells1 = np.flip(height_midpoints_cells1)
        height_midpoints_cells.append(height_midpoints_cells1)
       
    height_midpoints_cells = pd.DataFrame(height_midpoints_cells).transpose()
    
    depth_midpoints_cells = [np.round((depth_cells[i+1]+depth_cells[i])/2,3) 
                             for i in range(len(depth_cells)-1)]
    
    #%% Determine indexes of MAP grid for HADCP beam 
    
    # HADCP depth
    idx_HADCP_depth = np.where(depth_cells<= HADCP_depth)[-1][-1]
    
    # HADCP range 
    idx_freq1_range = np.where(track <= range_freq1)[-1][-1]+1
    idx_freq2_range = np.where(track <= range_freq2)[-1][-1]+1
    
    # HADCP beam (triangle using beam width at maximum range)
    # 400 kHz
    # 1 MHz
    
    #%%     
    # Calculate start and end index of HADCP range
    Conc_SDC_HADCP_depth = Conc_SDC_cell_export.iloc[idx_HADCP_depth,:]
    MAP_distance_first_valid_value = Conc_SDC_HADCP_depth.first_valid_index()
    MAP_distance_first_valid_value = MAP_distance_first_valid_value[0]
    Conc_SDC_HADCP_depth = Conc_SDC_HADCP_depth.reset_index()    
    max_distance = MAP_distance_first_valid_value + range_freq1
    min_index = next(x-1 for x, val in enumerate(Conc_SDC_HADCP_depth.iloc[:,0]) if val > MAP_distance_first_valid_value)
    max_index = next(x+1 for x, val in enumerate(Conc_SDC_HADCP_depth.iloc[:,0]) if val > max_distance)
        
    # Get data at HADCP depth 
    # Concentration and flux using SDC (sand) and linear method (fines)
    Sand_conc_HADCP = Conc_SDC_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
    Sand_flux_HADCP = sand_flux_SDC_kg_s_export.iloc[idx_HADCP_depth,min_index:max_index]
    Fine_conc_HADCP = Conc_fine_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
    Fine_flux_HADCP = fine_flux_kg_s_export.iloc[idx_HADCP_depth, min_index:max_index]
    Fine_sand_ratio_HADCP = ratio_cell.iloc[idx_HADCP_depth, min_index:max_index]
    
    # GSD
    if GSD_linear_cell_export is not None:
        Gsd_HADCP = GSD_linear_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
        D50_HADCP = D50_linear_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
        fif = Fine_sand_ratio_HADCP.tolist()
        HADCP_export = pd.DataFrame([Sand_conc_HADCP, Sand_flux_HADCP, Fine_conc_HADCP, Fine_flux_HADCP,
                                     D50_HADCP, Gsd_HADCP])    
        HADCP_export = HADCP_export.append(pd.DataFrame([fif], columns=HADCP_export.columns))
        HADCP_export.index = ['Sand_conc_HADCP', 'Sand_flux_HADCP', 'Fine_conc_HADCP', 
                               'Fine_flux_HADCP', 'D50_HADCP', 'Gsd_HADCP', 'Fine_sand_ratio_HADCP']
   
    else: 
        Gsd_HADCP = None
        D50_HADCP = None        
        fif = Fine_sand_ratio_HADCP.tolist()
        HADCP_export = pd.DataFrame([Sand_conc_HADCP, Sand_flux_HADCP, Fine_conc_HADCP, Fine_flux_HADCP])    
        HADCP_export = HADCP_export.append(pd.DataFrame([fif], columns=HADCP_export.columns))
        HADCP_export.index = ['Sand_conc_HADCP', 'Sand_flux_HADCP', 'Fine_conc_HADCP', 
                               'Fine_flux_HADCP', 'Fine_sand_ratio_HADCP']

    # Export data
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
    HADCP_export.to_csv(outpath + sampling_date + '_HADCP_export_' + most_frequent_sampler +'.csv', sep = ';') 

    return HADCP_export


#%% Only for bridge
def HADCP_function_BD(analysis_data, map_data, stage_sampling, outpath, sampling_date, 
                              Conc_SDC_cell_export, sand_flux_SDC_kg_s_export, 
                              GSD_linear_cell_export, D50_linear_cell_export):

    #%% Use station data to determine HADCP height (depth)
    
    # Load HADCP data 
    path_folder = r'C:\Users\jessica.laible\Documents\Acoustics\3_TW16\Results\Total_beam_USGS'
    freq1 = 400
    freq2 = 1000
    with open(path_folder + '\celldist_along_beam_'+ str(freq1) + '.txt', "rb") as fp:    
       celldist_along_beam_freq1 = pickle.load(fp)
    with open(path_folder + '\celldist_along_beam_'+ str(freq2) + '.txt', "rb") as fp:    
        celldist_along_beam_freq2 = pickle.load(fp)
        
    # HADCP depth
    HADCP_depth = np.nanmean(stage_sampling['Value'] - 0.2)
    
    # HADCP range 
    range_freq1 = celldist_along_beam_freq1[-1]
    range_freq2 = celldist_along_beam_freq2[-1]
    
    # HADCP beam (triangle using beam width at maximum range)
    # Beam width, height at max range 
    beam_angle_freq1 = 1.7
    beam_angle_freq2 = 3.4
    beam_angle_freq1_rad = beam_angle_freq1*(math.pi/180)
    beam_angle_freq2_rad = beam_angle_freq2*(math.pi/180)
    
    max_height_range_freq1 = math.sin(beam_angle_freq1_rad)*celldist_along_beam_freq1[-1]
    max_height_range_freq2 = math.sin(beam_angle_freq2_rad)*celldist_along_beam_freq2[-1]
    
           
    #%% Define MAP grid
    cell_height = map_data.depth_cells_border[1][0]-map_data.depth_cells_border[0][0]  
    depth_cells = np.arange(map_data.depth_cells_border[0][0], (len(map_data.depth_cells_border)*cell_height), cell_height)
       
    # Calculate cell properties
    cell_height = map_data.border_depth[1] - map_data.border_depth[0]
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
    depth_cells = map_data.border_depth
    
    # Length and depth of transect (center of each distance cell, plus start (0) and end)
    track = (border_cells[1:] + border_cells[:-1]) / 2
    track = np.append(np.insert(track, 0, 0), border_cells[-1])
    
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    track_depth = np.append(np.insert(track_depth, 0, 0), 0)
    
    # Create height above bottom grid
    height_midpoints_cells=[]
    for i in range(len(track_depth)):
        heh = int(track_depth[i]//cell_height)
        height_last_cell = (track_depth[i]-depth_cells[heh])
        midpoint_last_cell = np.array([height_last_cell/2])   # use half-distance between bottom and bottom of lowest complete cell for bottom cell
        midpoint_height_cells = np.arange(0, heh)*cell_height + (height_last_cell + cell_height/2)
        height_midpoints_cells1 = np.concatenate([midpoint_last_cell,midpoint_height_cells], axis = 0)
        height_midpoints_cells1 = np.flip(height_midpoints_cells1)
        height_midpoints_cells.append(height_midpoints_cells1)
       
    height_midpoints_cells = pd.DataFrame(height_midpoints_cells).transpose()
    
    depth_midpoints_cells = [np.round((depth_cells[i+1]+depth_cells[i])/2,3) 
                             for i in range(len(depth_cells)-1)]
    
    #%% Determine indexes of MAP grid for HADCP beam 
    
    # HADCP depth
    idx_HADCP_depth = np.where(depth_cells<= HADCP_depth)[-1][-1]
    
    # HADCP range 
    idx_freq1_range = np.where(track <= range_freq1)[-1][-1]+1
    idx_freq2_range = np.where(track <= range_freq2)[-1][-1]+1
    
    # HADCP beam (triangle using beam width at maximum range)
    # 400 kHz
    # 1 MHz
    
    #%%     
    # Calculate start and end index of HADCP range
    Conc_SDC_HADCP_depth = Conc_SDC_cell_export.iloc[idx_HADCP_depth,:]
    MAP_distance_first_valid_value = Conc_SDC_HADCP_depth.first_valid_index()
    MAP_distance_first_valid_value = MAP_distance_first_valid_value[0]
    Conc_SDC_HADCP_depth = Conc_SDC_HADCP_depth.reset_index()    
    max_distance = MAP_distance_first_valid_value + range_freq1
    min_index = next(x-1 for x, val in enumerate(Conc_SDC_HADCP_depth.iloc[:,0]) if val > MAP_distance_first_valid_value)
    max_index = next(x+1 for x, val in enumerate(Conc_SDC_HADCP_depth.iloc[:,0]) if val > max_distance)
        
    # Get data at HADCP depth 
    # Concentration and flux using SDC (sand) and linear method (fines)
    Sand_conc_HADCP = Conc_SDC_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
    Sand_flux_HADCP = sand_flux_SDC_kg_s_export.iloc[idx_HADCP_depth,min_index:max_index]
     
    # GSD
    if GSD_linear_cell_export is not None:
        Gsd_HADCP = GSD_linear_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
        D50_HADCP = D50_linear_cell_export.iloc[idx_HADCP_depth, min_index:max_index]       
        HADCP_export = pd.DataFrame([Sand_conc_HADCP, Sand_flux_HADCP,
                                     D50_HADCP, Gsd_HADCP])   
        HADCP_export.index = ['Sand_conc_HADCP', 'Sand_flux_HADCP', 'D50_HADCP', 'Gsd_HADCP']
   
    else: 
        Gsd_HADCP = None
        D50_HADCP = None   
        HADCP_export = pd.DataFrame([Sand_conc_HADCP, Sand_flux_HADCP])    
        HADCP_export.index = ['Sand_conc_HADCP', 'Sand_flux_HADCP']

    # Export data
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
    HADCP_export.to_csv(outpath + sampling_date + '_HADCP_export_' + most_frequent_sampler +'.csv', sep = ';') 

    return HADCP_export






#%% if no ADCP-data available

def HADCP_function_without_ADCP(analysis_data, stage_sampling, outpath, sampling_date, 
                             ):

    #%% Use station data to determine HADCP height (depth)
    
    # Load HADCP data 
    path_folder = r'C:\Users\jessica.laible\Documents\Acoustics\3_TW16\Results\Total_beam_USGS'
    freq1 = 400
    freq2 = 1000
    with open(path_folder + '\celldist_along_beam'+ str(freq1) + '.txt', "rb") as fp:    
       celldist_along_beam_freq1 = pickle.load(fp)
    with open(path_folder + '\celldist_along_beam'+ str(freq2) + '.txt', "rb") as fp:    
        celldist_along_beam_freq2 = pickle.load(fp)
        
    # HADCP depth
    HADCP_depth = np.nanmean(stage_sampling['Value'] - 0.2)
    
    # HADCP range 
    range_freq1 = celldist_along_beam_freq1[-1]
    range_freq2 = celldist_along_beam_freq2[-1]
    
    # HADCP beam (triangle using beam width at maximum range)
    # Beam width, height at max range 
    beam_angle_freq1 = 1.7
    beam_angle_freq2 = 3.4
    beam_angle_freq1_rad = beam_angle_freq1*(math.pi/180)
    beam_angle_freq2_rad = beam_angle_freq2*(math.pi/180)
    
    max_height_range_freq1 = math.sin(beam_angle_freq1_rad)*celldist_along_beam_freq1[-1]
    max_height_range_freq2 = math.sin(beam_angle_freq2_rad)*celldist_along_beam_freq2[-1]
    
    #%%
    # Get point data closest to HADCP depth
           
    #%% Define MAP grid
    cell_height = map_data.depth_cells_border[1][0]-map_data.depth_cells_border[0][0]  
    depth_cells = np.arange(map_data.depth_cells_border[0][0], (len(map_data.depth_cells_border)*cell_height), cell_height)
       
    # Calculate cell properties
    cell_height = map_data.border_depth[1] - map_data.border_depth[0]
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
    depth_cells = map_data.border_depth
    
    # Length and depth of transect (center of each distance cell, plus start (0) and end)
    track = (border_cells[1:] + border_cells[:-1]) / 2
    track = np.append(np.insert(track, 0, 0), border_cells[-1])
    
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    track_depth = np.append(np.insert(track_depth, 0, 0), 0)
    
    # Create height above bottom grid
    height_midpoints_cells=[]
    for i in range(len(track_depth)):
        heh = int(track_depth[i]//cell_height)
        height_last_cell = (track_depth[i]-depth_cells[heh])
        midpoint_last_cell = np.array([height_last_cell/2])   # use half-distance between bottom and bottom of lowest complete cell for bottom cell
        midpoint_height_cells = np.arange(0, heh)*cell_height + (height_last_cell + cell_height/2)
        height_midpoints_cells1 = np.concatenate([midpoint_last_cell,midpoint_height_cells], axis = 0)
        height_midpoints_cells1 = np.flip(height_midpoints_cells1)
        height_midpoints_cells.append(height_midpoints_cells1)
       
    height_midpoints_cells = pd.DataFrame(height_midpoints_cells).transpose()
    
    depth_midpoints_cells = [np.round((depth_cells[i+1]+depth_cells[i])/2,3) 
                             for i in range(len(depth_cells)-1)]
    
    #%% Determine indexes of MAP grid for HADCP beam 
    
    # HADCP depth
    idx_HADCP_depth = np.where(depth_cells<= HADCP_depth)[-1][-1]
    
    # HADCP range 
    idx_freq1_range = np.where(track <= range_freq1)[-1][-1]+1
    idx_freq2_range = np.where(track <= range_freq2)[-1][-1]+1
    
    # HADCP beam (triangle using beam width at maximum range)
    # 400 kHz
    # 1 MHz
    
    #%%     
    # Calculate start and end index of HADCP range
    Conc_SDC_HADCP_depth = Conc_SDC_cell_export.iloc[idx_HADCP_depth,:]
    MAP_distance_first_valid_value = Conc_SDC_HADCP_depth.first_valid_index()
    MAP_distance_first_valid_value = MAP_distance_first_valid_value[0]
    Conc_SDC_HADCP_depth = Conc_SDC_HADCP_depth.reset_index()    
    max_distance = MAP_distance_first_valid_value + range_freq1
    min_index = next(x-1 for x, val in enumerate(Conc_SDC_HADCP_depth.iloc[:,0]) if val > MAP_distance_first_valid_value)
    max_index = next(x+1 for x, val in enumerate(Conc_SDC_HADCP_depth.iloc[:,0]) if val > max_distance)
        
    # Get data at HADCP depth 
    # Concentration and flux using SDC (sand) and linear method (fines)
    Sand_conc_HADCP = Conc_SDC_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
    Sand_flux_HADCP = sand_flux_SDC_kg_s_export.iloc[idx_HADCP_depth,min_index:max_index]
    Fine_conc_HADCP = Conc_fine_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
    Fine_flux_HADCP = fine_flux_kg_s_export.iloc[idx_HADCP_depth, min_index:max_index]
    Fine_sand_ratio_HADCP = ratio_cell.iloc[idx_HADCP_depth, min_index:max_index]
    
    # GSD
    if GSD_linear_cell_export is not None:
        Gsd_HADCP = GSD_linear_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
        D50_HADCP = D50_linear_cell_export.iloc[idx_HADCP_depth, min_index:max_index]
        fif = Fine_sand_ratio_HADCP.tolist()
        HADCP_export = pd.DataFrame([Sand_conc_HADCP, Sand_flux_HADCP, Fine_conc_HADCP, Fine_flux_HADCP,
                                     D50_HADCP, Gsd_HADCP])    
        HADCP_export = HADCP_export.append(pd.DataFrame([fif], columns=HADCP_export.columns))
        HADCP_export.index = ['Sand_conc_HADCP', 'Sand_flux_HADCP', 'Fine_conc_HADCP', 
                               'Fine_flux_HADCP', 'D50_HADCP', 'Gsd_HADCP', 'Fine_sand_ratio_HADCP']
   
    else: 
        Gsd_HADCP = None
        D50_HADCP = None        
        fif = Fine_sand_ratio_HADCP.tolist()
        HADCP_export = pd.DataFrame([Sand_conc_HADCP, Sand_flux_HADCP, Fine_conc_HADCP, Fine_flux_HADCP])    
        HADCP_export = HADCP_export.append(pd.DataFrame([fif], columns=HADCP_export.columns))
        HADCP_export.index = ['Sand_conc_HADCP', 'Sand_flux_HADCP', 'Fine_conc_HADCP', 
                               'Fine_flux_HADCP', 'Fine_sand_ratio_HADCP']

    # Export data
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
    HADCP_export.to_csv(outpath + sampling_date + '_HADCP_export_' + most_frequent_sampler +'.csv', sep = ';') 

    return HADCP_export


















