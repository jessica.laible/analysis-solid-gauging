# -*- coding: utf-8 -*-
"""
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import subprocess
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

def uncertainty_analysis(analysis_data, outpath, outpath_figures, sampling_date,
                    choice, v2_d_index, summary_ISO, summary_fine, map_data, cwd, path_R):
                            
    
    #%%
    abscissa_values = (analysis_data['Abscissa']).unique().tolist()
    most_frequent_sampler = max(set(list(analysis_data['Sampler'])), key=list(analysis_data['Sampler']).count).strip()
    
    #%% Uncertainty on sampling depth 
    # Accuracy component (Belaid 2006)
    u_d = 0.01 # m
    
    k = 2 
    U_d = k*u_d # expanded uncertainty
    
    
    ################################################################################
    
    ## Uncertainty on concentration 
    
    ################################################################################
    
    #%% 1. Errors on point concentrations
    # 1.1. Random error due to the used sampler (er,c)
    # includes isocinetics, imperfect orientation within the stream, etc.
    if most_frequent_sampler == 'Niskin':
        u_sampler = 16
    else:
        u_sampler = 8
    
    #%% 1.2. Random error due to laboratory analysis (er,s)
    # Uncertainty on laboratory measurements
    # Determine law based on Gordon et al. (2000) in mug/l and uncertainty in % 
    x_range = np.arange(1, 50000, 1) 
    lab_sand = [69*x_range[i] **(-0.5) for i in range(len(x_range))]
    lab_fines = [3.9*x_range[i] **(-0.06) for i in range(len(x_range))]
    
    # Determine uncertainty for each sample
    c_s_mg_l = [int(analysis_data['Concentration_sand_g_l'][i]*1000) for i in range(len(analysis_data))]
    u_lab_sand = [lab_sand[i]/2 for i in c_s_mg_l]
    
    # if 'Concentration_fine_g_l' in analysis_data.columns:
    #     if np.count_nonzero(~np.isnan(np.array(analysis_data['Concentration_fine_g_l']))) >= 1: 
    #         c_f_mg_l = [int(analysis_data['Concentration_fine_g_l'][i]*1000) for i in range(len(analysis_data))]
    #         u_lab_fines = [lab_fines[i]/2 for i in c_f_mg_l]
    
    #%% 1.3. Random error due to natural fluctuations (er,e) CI
    # Data obtained on repetitions at different hydro-sedimentary conditions at Grenoble Campus, Colorado & from Literature (Spicer 2019)
    sigma_i_sand = [11.873581500018904,7.0311367829958185, 8.977541563561369,6.922723731896255,0.0,
                     0.8174644869208717, 23.876332871234073, 4.561979233461601,33.941125496954285, 20.20305089104423,
                     25.898692676364565,47.14045207910317, 0.0,69.85874223770712, 64.28243465332251, 12.789989950935155,
                     3.3752798489154654,23.533299015881486,12.601901389861084, 28.163814204638893, 15.930840241342137,11.716427823148438,
                     0.5623541583015758,1.6027789662098724, 21.6384616983531, 7.9075397107449445,  9.392955240407497, 16.245712740341713, 16.444382670001957,
                     27.456854689777465,17.354129281205914, 12.430333082710405, 10.821148205121792, 14.197245231558187, 33.47071204653268, 4.318082807745946,
                     8.952564813392016, 17.20590931294529, 19.551857359594816,  15.49033672593352, 1.7251375686183428, 51.07828947192431, 17.89877510528149,
                     12.520466139745093, 7.910658151929864, 7.691137532164522, 28.156644557643677, 14.86200709498165,30.11773771384564, 14.455267544539058,
                     39.83436612651401,7.6345317049657595, 21.37464333621369,36.69624673835366, 14.275342023920118,18.464944714992704,
                     8.385783270428647, 9.641215393111782,9.856989305821802, 19.457666927009434,16.366635697276497, 2.802035572567217, 9.11379254172249,
                     9.622377954096091, 42.08318743310379, 21.571592230566274, 16.012808363117344, 47.63270195080383,  39.446443289164726, 51.65735712665954, 48.000365353632574,
                     13.433628587982168, 6.492101629639994,  5.811083208849999, 10.272176539492959, 9.484979460757142, 9.063421981334557, 14.110854258435273,
                     17.672544669179356, 8.387925368976742, 26.26591344050153, 32.57360099304961, 14.476228261672416, 9.514991399021499]
    sigma_i_fines = [3.97015514,  6.64227326,  4.64990038, 12.54195514,  6.63710336,
            6.5777375 ,  4.41941738, 23.09747079, 27.42717212, 17.41223906,
            9.76312445,  6.01447995,  7.27795014,  1.1945178 , 10.72413905,
           13.07785819,  5.11480921,  7.21079701,  3.78614409,  5.65583994,
           18.83568563,  1.76647081, 12.90747225, 17.18076828, 11.79427826,
           12.69760641,  5.90347623,  3.95976245,  2.90065026,  4.15760906,
            6.05804734,  5.70177825,  3.16608158,  3.4172844 ,  1.51493751,
            5.68522035, 16.22145603, 36.65623557, 15.31170877, 16.83741402,
           17.62635463, 39.21317547, 52.48685249,  5.20678237, 15.10502992,
           14.59002954, 31.44691079, 22.36740749, 70.08562768,  4.38999053,
            3.73817005, 24.22962798,  8.31477172,  4.88942048, 14.21053039,
           10.49464668, 21.86009644,  8.61539159, 15.43443115, 15.2249929 ,
            2.99955562,  3.88162668, 11.30504767,  2.35082088,  6.66917298,
            9.70389559,  1.97675905,  0.4706739 ,  0.94856678,  0.54483118,
            1.85932691,  3.00347822,  1.40056932,  1.26322067,  0.        ,
            3.32861988,  0.        ,  0.        ,  0.        ]
    
    # Apply threshold at 75 % of the distribution, supposing a normal distribution
    u_nf_sand = np.percentile(sigma_i_sand, 50)
    u_nf_fines = np.percentile(sigma_i_fines, 50)

    #%% 1.4. Total error on point 
    # Point uncertainty on sand concentration
    u_point_sand = [np.sqrt(u_sampler**2 + u_lab_sand[i]**2 + u_nf_sand**2) 
                    for i in range(len(u_lab_sand))]
    
    # Point uncertainty on fine concentration
    # if 'Concentration_fine_g_l' in analysis_data.columns:
    #     if np.count_nonzero(~np.isnan(np.array(analysis_data['Concentration_fine_g_l']))) >= 1: 
    #         u_point_fines = [np.sqrt(u_sampler**2 + u_lab_fines[i]**2 + u_nf_fines**2) 
    #                         for i in range(len(u_lab_fines))]
    #         analysis_data['u_C_fines_point'] = u_point_fines
    
    # Export data (for RBaM use)
    ulnCs = [u_point_sand[i]/100 for i in range(len(u_point_sand))]
    analysis_data['u_meas_sand_point'] = ulnCs
    analysis_data['u_meas_sand_point_g_l'] = analysis_data['u_meas_sand_point']*analysis_data['Concentration_sand_g_l']
    analysis_data['log_sand_mg_l'] = np.log(analysis_data['Concentration_sand_g_l'] * 1000)  # lnCs
    
    point = pd.DataFrame([analysis_data['Abscissa_Name'], analysis_data['Sampling_height_final'], 
                                 analysis_data['log_sand_mg_l']])
    point_errors = pd.concat([point, pd.DataFrame([ulnCs])], axis=0).transpose()
    point_errors.columns = ['Abscissa_Name', 'z', 'lnCs', 'ulnCs']
    point_errors.to_csv(outpath + sampling_date + '_Point_errors_' + most_frequent_sampler +'.csv', sep = ';', index=False)
    
    #%% 2. Random error due to the selection of a finite number of points per vertical (er,p) and 
    # the position of the points on the vertical using BaM (Renard et al. xx) 
    # Estimate a priori parameters for the model
    a_priori_est = []
    for i in range(len(abscissa_values)):
        vv = analysis_data[analysis_data['Abscissa'] == abscissa_values[i]]
    # Measured parameters
        if choice[2] == 1:
            if np.count_nonzero(~np.isnan(np.array(vv['D50']))) >= 2:   
                d50_vertical = np.mean(vv['D50'])*1e-6    
            else: 
                d50_vertical = 0.00013
        else: 
            d50_vertical = 0.00013
        d90_bed = 0.0004
        #u_d50_vertical = 0.2
        # velocity per vertical
        if choice[3] == 1:
            velocity_vert = vv['Mean_velocity_vertical'].iloc[0]   
        else: 
            velocity_vert = 1
        #u_velocity_vert = 0.05
        # water depth 
        if choice[3] == 1:
            h = vv['max_depth_vertical'].iloc[0]
        else:
            h = 1
        u_h = 0.05
        # roughness height
        kst = 0.05
        #u_kst = 0.5*kst
        ks = 2* d90_bed
        #u_ks = 0.3 *ks
        
        
        # Fixed parameters (errors u are ±)
        # kinematic viscosity of water
        nu = 0.000001
        #u_nu = 0.05
        # acceleration of the gravity
        g = 9.81
        #u_g = 0.01
        # relative density
        rho = 2.65
        #u_rho = 0.02
        # sedimentological diameter
        ds = d50_vertical*(g*(rho-1)/nu**2)**(1/3)  # use mean d50 per vertical
        #u_ds = ds*(1/9*((u_g/g)**2 + (u_rho/rho)**2+4*(u_nu/nu)**2)+ (u_d50_vertical/d50_vertical)**2)
        # settling velocity
        ws = (nu/d50_vertical)*((10.36**2 + 1.049 * ds**3)**0.5 - 10.36)
        u_ws = 0.05
        # Critical bed shear stress
        theta_cr = 0.3/(1 + 1.2*ds) + 0.055*(1-np.exp(-0.02*ds))
        #u_theta_cr = 0.1
        # van Karman constant
        kappa = 0.41
        u_kappa = 0
        # skin friction coefficient
        fc = 2*(kappa/(1+np.log(ks/30*h)))**2
        #u_fc = 0.05
        # Shields parameter
        theta = 0.5*fc*velocity_vert**2/((rho-1)*d50_vertical*g)
        #u_theta = 0.05
        #turbulant Schmidt number
        sigma = 1
        u_sigma = 0.2
        # skin friction coefficient for total shear velocity
        fc2 = 2*(kappa/(1+np.log(kst/30/h)))**2
        #u_fc2 = 0.05 *fc2
        
        # Reference concentration Cr
        Cr = 1.5*1000*theta*np.exp(-0.2*ds)*np.exp(-4.5*theta_cr/theta)
        # u_Cr = 0.3 * 1000 * Cr
        
        # Total shear velocity
        shear_vel = velocity_vert*np.sqrt(fc2/2)
        u_shear_vel = 0.05
        
        # Estimated a priori parameters
        alpha_est = -6*ws/(sigma*kappa*shear_vel*h)
        #u_alpha_est = np.sqrt((u_ws**2/ws**2 + u_sigma**2/sigma**2 + u_kappa**2/kappa**2 + u_shear_vel**2/shear_vel**2 + u_h**2/h**2)*alpha_est**2)
        u_alpha_est = np.sqrt(u_ws**2 + u_sigma**2 + u_kappa**2 + u_shear_vel**2 + u_h**2)
        
        lnCr_est = np.log(Cr)
        u_lnCr_est = 1 #np.log(u_Cr)
        
        # Prepare export data
        a_pri_est = pd.DataFrame([vv['Abscissa_Name'].iloc[0], alpha_est, u_alpha_est, lnCr_est, u_lnCr_est]).transpose()
        a_priori_est.append(a_pri_est)
    
    # Export a priori parameters for RBaM simulation
    a_priori_est = pd.concat(a_priori_est)
    a_priori_est = a_priori_est.reset_index(drop = True)
    a_priori_inf = pd.Series([0]*len(a_priori_est))
    a_priori_inf.loc[0] = sampling_date 
    a_priori_inf.loc[1] = most_frequent_sampler
    a_priori_inf.loc[2] = 2 # std for lnCr 
    a_priori_inf.loc[3] = 2  # std for alpha 
    a_priori_est = pd.concat([(a_priori_inf), a_priori_est], axis = 1)
    a_priori_est.columns = ['Info','Abscissa_Name', 'alpha_est', 'u_alpha_est', 'lnCr_est', 'u_lnCr_est']
    a_priori_est.to_csv(outpath + sampling_date + '_Estimated_a_priori_params_RBaM_' + most_frequent_sampler +'.csv', sep = ';', index=False)
    
    path_info = pd.DataFrame([outpath, sampling_date, most_frequent_sampler])
    path_info.to_csv(cwd + '\\Info_BaM.csv', sep = ';', index = False)
    
    #%% Perform BaM simulations in R 
    
    #### Run code in Rstudio
    R_script_paths = "\"" + path_R + "\" --vanilla --" + cwd.replace(" ", "§20%") + \
                     " \"" + cwd + "/Uncertainty_vertical_interpolation.R\""
    subprocess.run(R_script_paths, shell=True)
    
    # R_script_paths = "\"" + path_R + "\" CMD BATCH -- vanilla --" + cwd.replace(" ", "§20%") + \
    #                  " \"" + cwd + "/Uncertainty_vertical_interpolation.R\""
    # subprocess.run(R_script_paths, shell=True)
        
    
    #%% Calculate u_p due to the vertical integration
    no_abscissa = analysis_data['Abscissa_Name'].unique().tolist()
    u_param = []
    u_p = []
    mean_conc_rem_all = []
    for i in range(len(abscissa_values)):
        vv = analysis_data[analysis_data['Abscissa'] == abscissa_values[i]]
        vv_point_errors = point_errors[point_errors['Abscissa_Name'] == no_abscissa[i]]
    
        # Load data (results_cooking created by RBaM)
        results_cooking = pd.read_csv(outpath + 'BaM_workspace\\' + 'Abscissa_' + str(no_abscissa[i]) + '\\Results_Cooking.txt', sep = "  ")
        results_cooking = results_cooking.dropna(axis=1,how='all')
        results_cooking.columns = ['lnCr', 'alpha', 'u1', 'logPost']
        
        # Parametric uncertainty
        # Calculate C for each simulation, including only parametric uncertainty
        # Calculate concentration profiles for each simulation
        h = np.linspace(0, vv['max_depth_vertical'].iloc[0],100) 
        conc_df = []
        for i in range(len(results_cooking)):
            concc = [(results_cooking.iloc[i,0]) + (results_cooking.iloc[i,1]*h[j])
                for j in range(len(h))]    
            conc_df.append(concc)    
        conc_df = pd.DataFrame(conc_df)
    
        # Calculate mean concentration for each simulation
        mean_c = [np.trapz(conc_df.iloc[i,:],h)/h[-1] for i in range(len(conc_df))]
        # Calculate mean concentration (mean of distribution of depth-integrated concentrations)
        mean_conc = np.median(mean_c) 
        # Calculate standard deviation (std of distribution of depth-integrated concentrations)
        std_param = np.std(mean_c)
        u_param_i = std_param/mean_conc*100
        u_param.append(u_param_i)
    
    
        # Remnant uncertainty         
        # Get Conc at sampling height for each simulation
        sampling_heights_interp = vv['Sampling_height_final']
        conc_sampl = [[np.interp(sampling_heights_interp.iloc[i], h, conc_df.iloc[j,:])
                      for i in range(len(sampling_heights_interp))]
                      for j in range(len(results_cooking))]
        conc_sampl = pd.DataFrame(conc_sampl)
        
        # Draw errors from uncertainty distribution 
        unc_sample = []
        for i in range(len(results_cooking)):
            mu, sigma = 0, results_cooking.iloc[i,2] # mean and standard deviation
            s = np.random.normal(mu, sigma, len(sampling_heights_interp))
            unc_sample.append(s)
        unc_sample = pd.DataFrame(unc_sample)
        
        # Calculate point concentration with error 
        conc_sampl_unc = conc_sampl + unc_sample
                
        # Calculate concentration on bed and surface
        conc_bed = pd.DataFrame([np.interp(0, h, conc_df.iloc[j,:])             
                      for j in range(len(results_cooking))])
        conc_surface = pd.DataFrame([np.interp(vv['max_depth_vertical'].iloc[0], h, conc_df.iloc[j,:])             
                      for j in range(len(results_cooking))])
        conc_sampl_unc = pd.concat([conc_surface, conc_sampl_unc,conc_bed], axis = 1)
        sampling_heights_interp = pd.concat([pd.Series(h[-1]), sampling_heights_interp, pd.Series(0)])
                        
        # Calculate mean concentration for each simulation
        mean_c_rem = [(np.trapz(conc_sampl_unc.iloc[i,:],sampling_heights_interp)/h[-1]*(-1))
                      for i in range(len(conc_sampl_unc))]
        # Calculate mean concentration (mean of distribution of depth-integrated concentrations)
        mean_conc_rem = np.median(mean_c_rem) 
        mean_conc_rem_all.append(mean_conc_rem)
        # Calculate standard deviation (std of distribution of depth-integrated concentrations)
        std_conc_rem = np.std(mean_c_rem)
        u_p_i = std_conc_rem/mean_conc_rem*100
        u_p.append(u_p_i)
        
    # Determine Q_j and Q_total
    Q_j = []
    for j in range(len(abscissa_values)):
        Q_jj = analysis_data[analysis_data['Abscissa_Name'] == no_abscissa[j]]['NN_field_sand_flux_kg_s']
        Q_jjj = np.nansum(Q_jj)
        Q_j.append(Q_jjj)    
    Q_total = np.sum(Q_j)
        
    # u_p
    u2_p_final = np.sum([Q_j[j]**2/Q_total**2 *u_p[j]**2 
                  for j in range(len(abscissa_values))])
    U2_p = u2_p_final*4
    
    # u_rem - remnant uncertainty due to the vertical integration
    u_rem = [np.sqrt(u_p[i]**2 - u_param[i]**2)
              for i in range(len(u_param))]
    u2_rem_final = np.sum([Q_j[j]**2/Q_total**2 *u_rem[j]**2 
                  for j in range(len(abscissa_values))])
    U2_rem = u2_rem_final*4
    
    u2_param_final = np.sum([Q_j[j]**2/Q_total**2 *u_param[j]**2 
                  for j in range(len(abscissa_values))])
    U2_param = u2_param_final*4
    
    #%% 3. Uncertainty u_m due to lateral integration
    # Based on nomograph (Guy & Norman 1970, based on Hubbell 1960)
    
    if 'Concentration_fine_g_l' in analysis_data.columns:
        if np.count_nonzero(~np.isnan(np.array(analysis_data['Concentration_fine_g_l']))) >= 1: 
            per_sand = float(summary_ISO['total_sand_flux_kg_s']/(summary_ISO['total_sand_flux_kg_s']+
                                                  summary_fine['total_fine_flux_SDC_kg_s'])*100)
    else:
        per_sand = 100
        
    u_m = 0.4*per_sand*(1.43 * v2_d_index - 1.37) * len(abscissa_values) **(-0.7)
    
    #%% Total uncertainty on concentration
        
    # u'_r - random uncertainty 
    u_rand = np.sqrt(u_m**2 + u2_p_final) 
          
    # u'_sys - Total systematic uncertainty
    u_sys = np.sqrt(1.5**2 + 2.0**2 + 2.0**2 + 1.5**2)
    
    # total uncertainty
    u_C_perc = np.sqrt(u_rand**2 + u_sys**2)
    U_C_perc = 2*u_C_perc
    
        
    
    #%%###############################################################################
    
    ## Uncertainty on discharge 
    
    ################################################################################
    
    U_Q = map_data.u_Q_95
    
    
    
    #%%###############################################################################
    
    ## Total uncertainty on flux 
    
    ################################################################################
    
    U_F = np.sqrt(U_C_perc**2 + U_Q**2)
    
    
    #%% Plot Percentage contribution to total uncertainty u_F
    
    contribution_unc = [U_Q**2/U_F**2*100, 
                        (u_sys*2)**2/U_F**2*100, (u_m*2)**2/U_F**2*100,
                        U2_p/U_F**2*100]
    
    fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
    
    mylabels = ['$U\'_Q$', '$U\'_{sys}$', '$U\'_m$', '$U\'_p$'] 
    mycolors = ['blue', "sandybrown", 'forestgreen', "mediumorchid"]#, "skyblue"]
    patches, texts = ax.pie(contribution_unc, labels = mylabels, startangle = 90, 
            colors = mycolors, radius = 1.1,wedgeprops = {"edgecolor" : "black",
                          'linewidth': 1,
                          'antialiased': True})  
    plt.setp(texts, fontsize = 16)
    # ax.text(0.03, 0.9, 'b)', fontsize = 14)
    # labels = ['{0} - {1:1.1f} %'.format(i,j) for i,j in zip(mylabels, contribution_unc)]
      
    # plt.legend(patches, labels, loc='center left', bbox_to_anchor = (-0.5, 0.5),
    #            fontsize=14)
    
    fig.text(0.1, 0.9, '$U\'_\Phi$ = ' + str(np.round(U_F,1)) + ' %', fontsize = 14)
    
    fig.tight_layout()
    figname = '_Contribution_to_U_F_'
    fig.savefig(outpath_figures + '\\' + str(sampling_date) + figname + most_frequent_sampler + '.png', dpi = 200, bbox_inches='tight')
       
    #%% Plot Percentage contribution to total uncertainty u_F - U'p divided into param and rem
    
    contribution_unc = [U_Q**2/U_F**2*100, 
                    (u_sys*2)**2/U_F**2*100, (u_m*2)**2/U_F**2*100,
                    U2_param/U_F**2*100, U2_rem/U_F**2*100]
    
    fig, ax = plt.subplots(figsize=(8, 6), dpi = 100)
    
    mylabels = ['$U\'_Q$', '$U\'_{sys}$', '$U\'_m$', '$U\'_{param}$', '$U\'_{rem}$'] 
    mycolors = ['blue', "sandybrown", 'forestgreen', "indigo", "mediumorchid"]#, "skyblue"]
    patches, texts = ax.pie(contribution_unc, labels = mylabels, startangle = 90, 
        colors = mycolors, radius = 1.1,wedgeprops = {"edgecolor" : "black",
                      'linewidth': 1,
                      'antialiased': True})  
    plt.setp(texts, fontsize = 16)
    fig.text(0.1, 0.9, '$U\'_\Phi$ = ' + str(np.round(U_F,1)) + ' %', fontsize = 14)
    
    fig.tight_layout()
    figname = '_Contribution_to_U_F_2_'
    fig.savefig(outpath_figures + '\\' + str(sampling_date) + figname + most_frequent_sampler + '.png', dpi = 200, bbox_inches='tight')
       
    # #%% Plots for article
    # fig, ax = plt.subplots(figsize=(8, 6), dpi = 200)
    
    # mylabels = ['$U\'_Q$', '$U\'_{sys}$', '$U\'_m$', '$U\'_{param}$', '$U\'_{rem}$'] 
    # mycolors = ['blue', "sandybrown", 'forestgreen', "indigo", "mediumorchid"]
    # patches, texts = ax.pie(contribution_unc, startangle = 90, 
    #     colors = mycolors, radius = 1.1,wedgeprops = {"edgecolor" : "black",
    #                   'linewidth': 1,
    #                   'antialiased': True})  
    # fig.text(0.3, 0.95, '$U\'_\Phi$ = ' + str(np.round(U_F,1)) + ' %', fontsize = 26)
    
    # # plt.legend(patches, mylabels, ncol = 5, loc='lower center', bbox_to_anchor = (0.5, -0.5),
    # #             fontsize=22)
  
    # fig.tight_layout()
    # figname = '_Contribution_to_U_F_2_ps1'
    # fig.savefig(outpath_figures + '\\' + str(sampling_date) + figname + most_frequent_sampler + '.png', dpi = 400, bbox_inches='tight')
  
    #%% log-scale
    # cmap = plt.cm.get_cmap('nipy_spectral')
    # colorss = cmap(np.linspace(0,1,len(abscissa_values)))
    
    # fig, ax = plt.subplots(figsize=(12, 9), dpi = 100)
    
    # for i in range(len(abscissa_values)):
    #     ax.plot(Conc_vertical_profile.iloc[i,:], x_ranges_vertical.iloc[i,:],
    #             linestyle = '-', linewidth = 3, color = colorss[i],
    #             label = str(abscissa_values[i]))
           
    #     ax.errorbar(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa']== abscissa_values[i]], 
    #                 analysis_data['z_h'][analysis_data['Abscissa']== abscissa_values[i]],
    #                 xerr = np.array(analysis_data['u_meas_sand_point_g_l'][analysis_data['Abscissa']== abscissa_values[i]]),
    #                 color = colorss[i], linestyle = ' ',
    #                 markersize = 12, marker = markerss[i],
    #                 markeredgewidth = 0.2, markeredgecolor = 'black',
    #                 elinewidth = 1, capsize = 3)
    
    # legend = ax.legend(fontsize = 30, title = 'Abscissa', 
    #           loc = 'upper right',  facecolor = 'white', framealpha = 1)
    # plt.setp(legend.get_title(), fontsize=30)
    
    # ax.set_xscale('log')
    # ax.set_ylim(-0.05,1.05)
    # ax.set_xlim(0.01,0.2)
    # ax.set_ylabel('z/h (-)', fontsize = 34, weight = 'bold')
    # ax.set_xlabel('Concentration (g/l)', fontsize = 34, weight = 'bold')
    # ax.grid(linewidth = 0.2)
    # ax.tick_params(axis='both', which='major', labelsize = 24)
    # fig.tight_layout()
    # figname = '_Conc_profiles_log_SDC_unc_' + most_frequent_sampler
    # fig.savefig(outpath_figures + sampling_date + figname + '.png', dpi = 400, bbox_inches='tight')
    
    #%% Export data 
    
    uncertainty = pd.DataFrame([u_sys, u_m, np.sqrt(u2_p_final), U_C_perc, U_Q, U_F]).transpose()
    uncertainty.columns = ['u\'_sys', 'u\'_m', 'u\'_p', 'U\'_C', 'U\'_Q', 'U\'F']

    return (analysis_data, U_F, uncertainty)
    

