"""
Let the user choose the units of volume and mass used throughout the script
(mg and g; ml and l)

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import tkinter as tk

class choice_unit():
    def __init__(self):
        self.unit_volumes = None
        self.unit_masses = None

        self.ws = tk.Tk()
        self.ws.title('Define units')
        self.ws.geometry("175x130")
        self.ws.wm_attributes("-topmost", 1)
        self.ws.bind("<Return>", self.input_choice)

        label_volume = tk.Label(self.ws, text='Unit volumes')
        label_volume.grid(row=0, column=0)

        self.var_volume = tk.StringVar()
        rb_mL = tk.Radiobutton(self.ws, text='mL', variable=self.var_volume, value='mL')
        rb_mL.grid(row=1, column=0, sticky="W")
        rb_L = tk.Radiobutton(self.ws, text='L', variable=self.var_volume, value='L')
        rb_L.grid(row=2, column=0, sticky="W")
        rb_L.select()

        label_masses = tk.Label(self.ws, text='Unit masses')
        label_masses.grid(row=0, column=2)

        self.var_masses = tk.StringVar()
        rb_mg = tk.Radiobutton(self.ws, text='mg', variable=self.var_masses, value='mg')
        rb_mg.grid(row=1, column=2, sticky="W")
        rb_g = tk.Radiobutton(self.ws, text='g', variable=self.var_masses, value='g')
        rb_g.grid(row=2, column=2, sticky="W")
        rb_g.select()

        self.button = tk.Button(self.ws, text='Ok [Return]', command=self.input_choice, padx=20, pady=10)
        self.button.grid(row=3, column=0, columnspan=3)

        self.ws.grid_columnconfigure(1, minsize=30)
        self.ws.mainloop()

    def input_choice(self, event=None):
        self.unit_volume = self.var_volume.get()
        self.unit_masses = self.var_masses.get()

        print('unit_volume :', self.var_volume.get())
        print('unit_masses :', self.var_masses.get())
        self.ws.destroy()

