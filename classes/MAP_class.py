# -*- coding: utf-8 -*-
"""
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

#========================================
# External imports
#========================================
import copy
import os
import numpy as np
import scipy as sc
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize.minpack import curve_fit
import pickle
import math
from pathlib import Path
from datetime import datetime

#========================================
# Internal imports
#========================================

from MiscLibs.common_functions import cart2pol, pol2cart, nan_greater
from UI.MplCanvas import MplCanvas
import matplotlib.cm as cm
from Common_functions import show_figure, haversine, interpolation

class MAP:
    """Multitransect Averaged Profile (MAP) generates an average profile of selected transects.

    Attributes
    ----------
    primary_velocity: np.array(float)
        MAP primary velocity of each middle cell without extrapolation
    secondary_velocity: np.array(float)
        MAP secondary velocity of each middle cell without extrapolation
    vertical_velocity: np.array(float)
        MAP primary velocity of each middle cell without extrapolation
    depths: np.array(float) 1D
        MAP depths
    extrap_primary_velocity: np.array(float)
        MAP primary velocity with extrpolation on bottom/top part
    extrap_secondary_velocity: np.array(float)
        MAP secondary velocity with extrpolation on bottom/top part
    extrap_vertical_velocity: np.array(float)
        MAP vertical velocity with extrpolation on bottom/top part
    depth_cells_border: np.array(float)
        Depth borders of each MAP cell, last one of each vertical equal to vertical depth
    depth_cells_center: np.array(float)
        Depth center of each MAP cell
    borders_ens: np.array(float) 1D
        Borders of each MAP vertical
       
    left_distance/right_distance: float
        MAP edge distance from edge computation
    left_borders/right_borders: np.array(float) 1D
        Borders of each MAP vertical from edge computation
    left_coef/right_coef: float
        Shape coefficient of MAP edge
    left_primary_velocity/right_primary_velocity: np.array(float)
        MAP primary velocity for edge cells
    left_secondary_velocity/right_secondary_velocity: np.array(float)
        MAP secondary velocity for edge cells
    left_vertical_velocity/right_vertical_velocity: np.array(float)
        MAP vertical velocity for edge cells
    left_cells_discharge/right_cells_discharge: np.array(float)
        MAP edge cells discharge
    left_discharge/right_discharge: float
        MAP edge total discharge
        
    total_discharge: float
        MAP total discharge with current parameters
    middle_cells_discharge: np.array(float)
        MAP discharge of each middle cell (with top/bottom extrapolation if selected)
    middle_discharge: float
        MAP middle discharge with top/bottom extrapolation (if selected)
    """
    

    def __init__(self):
        """Initialize class and instance variables."""
        self.primary_velocity = None # MAP primary velocity of each middle cell without extrapolation
        self.secondary_velocity = None # MAP secondary velocity of each middle cell without extrapolation 
        self.vertical_velocity = None # MAP primary velocity of each middle cell without extrapolation
        self.depths = None # MAP depths
        self.extrap_primary_velocity = None # MAP primary velocity with extrapolation on bottom/top part
        self.extrap_secondary_velocity = None # MAP secondary velocity with extrapolation on bottom/top part
        self.extrap_vertical_velocity = None # MAP vertical velocity with extrapolation on bottom/top part
        self.depth_cells_border = None # Depth borders of each MAP cell, last one of each vertical equal to vertical depth
        self.depth_cells_center = None # Depth center of each MAP cell
        self.borders_ens = None # Borders of each MAP vertical
        self.border_depth = None # Raw depth grid

        self.left_distance = None # MAP edge distance from edge computation
        self.left_borders = None # Borders of each MAP vertical from edge computation
        self.left_coef = None # Shape coefficient of MAP edge
        self.left_primary_velocity = None # MAP primary velocity for edge cells
        self.left_secondary_velocity = None # MAP secondary velocity for edge cells
        self.left_vertical_velocity = None # MAP vertical velocity for edge cells
        self.left_cells_discharge = None # MAP edge cells discharge
        self.left_discharge = None # MAP edge total discharge
        self.right_distance = None # MAP edge distance from edge computation
        self.right_borders = None # Borders of each MAP vertical from edge computation
        self.right_coef = None # Shape coefficient of MAP edge
        self.right_primary_velocity = None # MAP primary velocity for edge cells
        self.right_secondary_velocity = None # MAP secondary velocity for edge cells
        self.right_vertical_velocity = None # MAP vertical velocity for edge cells
        self.right_cells_discharge = None # MAP edge cells discharge
        self.right_discharge = None # MAP edge total discharge

        self.left_mid_cells_x = None
        self.left_mid_cells_y = None
        self.left_vertical_depth = None
        self.right_mid_cells_x = None
        self.right_mid_cells_y = None
        self.right_vertical_depth = None
        
        self.total_discharge = None # MAP total discharge with current parameters
        self.middle_cells_discharge = None # MAP discharge of each middle cell (with top/bottom extrapolation if selected)
        self.middle_discharge = None # MAP middle discharge with top/bottom extrapolation (if selected)
        self.vertical_nodes = None        
        self.u_Q_95 = None #

        self.meas_date = None

    def populate_data(self, meas, node_horizontal_user, node_vertical_user, file_MAP = False, add_name_meas='unknown',
                      edge_constant = False, extrap_option = True, interp_option = True,
                      track_section = True, plot=False, name_meas='unknown', path_results=''):
        """Parameters
        ----------
        meas: Measurement
            Object of Measurement class
        node_horizontal_user: float
            Length of MAP cells, if None automatical value is selected
        node_vertical_user: float
            Depth of MAP cells, if None automatical value is selected
        edge_constant: bool
            Indicates if edge cells should all be the same size
        extrap_option: bool
            Indicates if top/bottom/edges extrapolation should be applied
        interp_option: bool
            Indicates if velocities interpolation should be applied
        track_section: bool
            Indicates if average cross-section should be computed on boat track (True) or 
            on mean velocities direction (False)
        plot: bool
            Indicates if graphics should be plotted and saved
        name_meas: str
            Indicates graphics name for save
        path_results: str
            Indicates the path to save graphics
        """
        # Get meas current parameters
        settings = meas.current_settings()

        self.meas_date = datetime.utcfromtimestamp(meas.transects[-1].date_time.start_serial_time).strftime('%Y%m%d')

        navigation_reference = settings['NavRef']
        checked_transect_idx = meas.checked_transect_idx
        self.u_Q_95 = meas.oursin.u_measurement['total_95'][0]
        
        # Get main data from selected transects
        x_raw_coordinates, y_raw_coordinates, w_vel_x, w_vel_y, w_vel_z, \
            depth_data, q_cells, orig_start_edge, invalid_data, cell_depth, \
                left_geometry, right_geometry = MAP.collect_data(meas, navigation_reference, checked_transect_idx, interp_option)
        
        # Compute coefficients of the average cross-section
        alpha, beta, direction_meas = MAP.compute_coef(x_raw_coordinates, y_raw_coordinates, 
                                                       w_vel_x, w_vel_y, q_cells, track_section)
        
        # Project raw coordinates on average cross-section
        x_projected, y_projected, acs_distance_raw = MAP.project_transect(alpha, beta, meas, x_raw_coordinates, 
                                                                      y_raw_coordinates, navigation_reference)
        
        # Compare bathymetry and translate transects on average cross-section if needed
        acs_distance = MAP.translated_transects(acs_distance_raw, depth_data)
        
        # Define horizontal and vertical mesh
        borders_ens_raw, nodes_depth_raw = MAP.compute_node_size(meas.transects[checked_transect_idx[0]], acs_distance, 
                                                                   depth_data, node_horizontal_user, node_vertical_user)

        self.border_depth = nodes_depth_raw

        # Compute transect median velocity on each mesh (North, East and vertical velocities) and depth on each vertical
        transects_node_x_velocity, transects_node_y_velocity, transects_node_vertical_velocity, \
                transects_node_depth, transects_nodes = MAP.compute_nodes_velocity(navigation_reference, checked_transect_idx, w_vel_x, w_vel_y, w_vel_z, 
                cell_depth, depth_data, acs_distance, borders_ens_raw, nodes_depth_raw, orig_start_edge)
        
        # Compute mesh mean value of selected transects
        MAP_x_velocity, MAP_y_velocity = self.compute_mean(transects_nodes, transects_node_x_velocity,
                                            transects_node_y_velocity, transects_node_vertical_velocity, 
                                            transects_node_depth, borders_ens_raw, nodes_depth_raw)
        
        # Compute primary and secondary velocity according Rozovskii projection
        mid_direction = self.compute_rozovskii(MAP_x_velocity, MAP_y_velocity)
        
        # Compute edges parameters
        if extrap_option:
            self.left_distance, self.left_coef = [x for x in left_geometry]
            self.right_distance, self.right_coef = [x for x in right_geometry]
        else:
            self.left_distance = 0
            self.right_distance = 0
         
        # Compute top/bottom extrapolation according QRevInt velocity exponent
        idx_bot, idx_top = self.compute_extrap_velocity(settings, extrap_option)
        
        # Compute edge extrapolation
        left_direction, right_direction, left_area, right_area, left_mid_cells_x, right_mid_cells_x, \
            left_mid_cells_y, right_mid_cells_y = self.compute_edges(borders_ens_raw, mid_direction, settings, edge_constant)

        self.left_area = left_area
        self.right_area = right_area

        # Compute discharge
        self.compute_discharge(direction_meas, mid_direction, extrap_option, left_direction, right_direction, left_area, right_area)
        
        if plot:
            MAP.plot_projected_data(alpha, beta, x_raw_coordinates, y_raw_coordinates, x_projected, y_projected, 
                                path_results, name_meas)
            self.plot_profile(borders_ens_raw, left_mid_cells_x, right_mid_cells_x,
                              left_mid_cells_y, right_mid_cells_y, path_results, name_meas)

        # if file_MAP:
        #     self.creation_MAP_file(path_results, add_name_meas)

    @staticmethod          
    def collect_data(meas, navigation_reference, checked_transect_idx, interp_option):
        """ Collect data of valid position and depth for each selected transect
        
        Parameters
        ----------
        meas: Measurement
            Object of Measurement class
        navigation_reference: str
            Indicated selected navigation reference 'bt_vel' (Bottom trakc) or 'gga_vel' (GGA)
        checked_transect_idx: list
            List of selected transects
        interp_option: bool
            Indicates if interpolated velocities should be used
            
        Returns
        -------
        x_raw_coordinates: list(np.array(float))
            List of 1D arrays of East boat coordinates from selected transects
        y_raw_coordinates: list(np.array(float))
            List of 1D arrays of North boat coordinates from selected transects
        w_vel_x: list(np.array(float))
            List of arrays of cells East velocity from selected transects
        w_vel_y: list(np.array(float))
            List of arrays of cells North velocity from selected transects
        w_vel_z: list(np.array(float))
            List of arrays of cells vertical velocity from selected transects
        depth_data: list(np.array(float))
            List of 1D arrays of depth from selected transects
        q_cells: list(np.array(float))
            List of arrays of cells discharge from selected transects
        orig_start_edge: list(str)
            List of original starting edge of transect looking downstream (Left or Right) from selected transects
        invalid_data: list(np.array(bool))
            List of arrays of invalid cells
        cell_depth: list(np.array(float))
            
        left_geometry/right_geometry: np.array(float)
            Array returning values of edge distance and shape coefficient
        """
        # Create empty lists to iterate
        depth_data = []
        x_raw_coordinates = []
        y_raw_coordinates = []
        w_vel_x = []
        w_vel_y = []
        w_vel_z = []
        q_cells = []
        orig_start_edge = []
        invalid_data = []
        cell_depth = []
        
        left_param = np.tile([np.nan], (len(checked_transect_idx), 2))
        right_param = np.tile([np.nan], (len(checked_transect_idx), 2))
        
        for id_transect in checked_transect_idx :
            transect = meas.transects[id_transect]
            index_transect = checked_transect_idx.index(id_transect)
            in_transect_idx = transect.in_transect_idx
            if navigation_reference=='bt_vel':
                ship_data = transect.boat_vel.compute_boat_track(transect, ref='bt_vel')
                if transect.orig_start_edge == 'Right':
                    # Reverse transects in ordred to start at 0 on left edge
                    dmg_ind = np.where(abs(ship_data['dmg_m']) == max(abs(ship_data['dmg_m'])))[0][0]
                    x_track = ship_data['track_x_m'] - ship_data['track_x_m'][dmg_ind]
                    y_track = ship_data['track_y_m'] - ship_data['track_y_m'][dmg_ind]
                    x_transect = x_track[::-1]
                    y_transect = y_track[::-1]
                    depth_transect = transect.depths.bt_depths.depth_processed_m[::-1]
                else :
                    x_transect = ship_data['track_x_m']
                    y_transect = ship_data['track_y_m']
                    depth_transect = transect.depths.bt_depths.depth_processed_m
                valid = transect.depths.bt_depths.valid_data
                x_transect = x_transect[valid]
                y_transect = y_transect[valid]
                d_transect = depth_transect[valid]
                x_raw_coordinates.append(x_transect)
                y_raw_coordinates.append(y_transect)
                
            else:  
                valid = ~np.isinf(transect.gps.gga_lon_ens_deg)*~np.isnan(transect.gps.gga_lon_ens_deg)
                x_raw_coordinates.append(transect.gps.gga_lon_ens_deg[valid])
                y_raw_coordinates.append(transect.gps.gga_lat_ens_deg[valid])
                d_transect = transect.depths.bt_depths.depth_processed_m[valid]
                
            # Edges parameters 
            left_param[index_transect,:] = [meas.transects[id_transect].edges.left.distance_m, 
                       meas.discharge[id_transect].edge_coef('left', meas.transects[id_transect])]
            right_param[index_transect,:] = [meas.transects[id_transect].edges.right.distance_m, 
                       meas.discharge[id_transect].edge_coef('right', meas.transects[id_transect])]
            left_geometry = np.nanmedian(left_param, axis = 0)
            right_geometry = np.nanmedian(right_param, axis = 0)
           
            invalid = np.logical_not(transect.w_vel.valid_data[0, :, in_transect_idx]).T
            vel_x = np.copy(transect.w_vel.u_processed_mps)
            vel_y = np.copy(transect.w_vel.v_processed_mps)
            vel_z = np.copy(transect.w_vel.w_mps[:, in_transect_idx])
            
            if not interp_option:
                vel_x[invalid] = np.nan
                vel_y[invalid] = np.nan
                vel_z[invalid] = np.nan
            
            w_vel_x.append(vel_x)
            w_vel_y.append(vel_y)
            w_vel_z.append(vel_z)
            invalid_data.append(invalid)
            depth_data.append(d_transect)   
            q_cells.append(meas.discharge[id_transect].middle_cells)
            orig_start_edge.append(transect.orig_start_edge)
            cell_depth.append(transect.depths.bt_depths.depth_cell_depth_m[:, in_transect_idx])
        
        return x_raw_coordinates, y_raw_coordinates, w_vel_x, w_vel_y, w_vel_z, \
            depth_data, q_cells, orig_start_edge, invalid_data, cell_depth, left_geometry, right_geometry
    
    @staticmethod 
    def compute_coef(x_raw_coordinates, y_raw_coordinates, w_vel_x, w_vel_y, q_cells, track_section):
        """ Compute average cross-section
        
        Parameters
        ----------
        x_raw_coordinates: list(np.array(float))
            List of 1D arrays of East boat coordinates from selected transects
        y_raw_coordinates: list(np.array(float))
            List of 1D arrays of North boat coordinates from selected transects
        w_vel_x: list(np.array(float))
            List of arrays of cells East velocity from selected transects
        w_vel_y: list(np.array(float))
            List of arrays of cells North velocity from selected transects
        q_cells: list(np.array(float))
            List of arrays of cells discharge from selected transects
        track_section: bool
            Indicates if average cross-section should be computed on boat track (True) or mean velocity (False)
        
        Returns
        -------
        alpha: float
            Slope of the line which define the average cross_section
        beta: float
            y-intercept of the line which define the average cross_section
        direction_meas: float
            Direction normal to the average cross-section
        """
        
        func = {'velocity': lambda x, b: x + b,
                'track': lambda x, a, b: a * x + b}
        # Compute average cross-section on boat track
        if track_section:
            coef_meas = np.tile(np.nan, (len(x_raw_coordinates),2))
            for i in range(len(x_raw_coordinates)):
                coef_meas[i,:], _ = curve_fit(func['track'], x_raw_coordinates[i], y_raw_coordinates[i]) 
            alpha = np.nanmedian(coef_meas[:,0])
            beta = np.nanmedian(coef_meas[:,1])
            # alpha, beta = coef_meas[coef_meas[:,0].argsort()][int(coef_meas.shape[0]/2),:]
            direction_meas = np.arctan2(-1,alpha)
        # Compute average cross-section on mean velocity direction
        else :
            mean_x = list()
            mean_y = list()
            for  i in range(len(w_vel_x)):
                mean_x.append(np.nansum(w_vel_x[i]*q_cells[i])/np.nansum(q_cells[i]))
                mean_y.append(np.nansum(w_vel_y[i]*q_cells[i])/np.nansum(q_cells[i]))
            v_x = np.nanmedian(mean_x)
            v_y = np.nanmedian(mean_y)
            direction_meas, _ = cart2pol(v_x, v_y)
            alpha = -1/np.tan(direction_meas)
            coef_meas = list()
            for i in range(len(w_vel_x)):
                coef_meas.append(curve_fit(func['velocity'], alpha*x_raw_coordinates[i], y_raw_coordinates[i])[0]) 
            beta = np.nanmean(coef_meas)
        
        return alpha, beta, direction_meas   
    
    @staticmethod
    def project_transect(alpha, beta, meas, x_raw_coordinates, y_raw_coordinates, navigation_reference):
        """ Project transects on the average cross-section
        
        Parameters
        ----------
        alpha: float
            Slope of the line which define the average cross_section
        beta: float
            y-intercept of the line which define the average cross_section
        meas: Measurement
            Object of Measurement class
        x_raw_coordinates: list(np.array(float))
            List of 1D arrays of East boat coordinates from selected transects
        y_raw_coordinates: list(np.array(float))
            List of 1D arrays of North boat coordinates from selected transects
        navigation_reference: str
            Indicated selected navigation reference 'bt_vel' (Bottom trakc) or 'gga_vel' (GGA)
            
        Returns
        -------
        x_projected: list(np.array(float))
            List of 1D arrays of the projection on the average cross-section of the East coordinates
        y_projected: list(np.array(float))
            List of 1D arrays of the projection on the average cross-section of the North coordinates
        acs_distance: list(np.array(float))
            List of 1D arrays to the left_most point distance on the average cross-section
        """
        x_projected = list()
        y_projected = list()
        distance_transect = list()
        x_left = list()
        y_left = list()
        
        # Project transect
        for i in range(len(x_raw_coordinates)): 
            # Project x and y coordinates on the cross-section
            x_projected.append((x_raw_coordinates[i] - alpha*beta + alpha*y_raw_coordinates[i])/(alpha**2+1))
            y_projected.append((beta + alpha*x_raw_coordinates[i] + alpha**2*y_raw_coordinates[i])/(alpha**2+1))
            
            # Determine where is the left side
            if navigation_reference == 'gga_vel' and meas.transects[i].orig_start_edge == 'Right':
                x_left.append(x_projected[i][-1])
                y_left.append(y_projected[i][-1])
    
            else:
                x_left.append(x_projected[i][0])
                y_left.append(y_projected[i][0])
            
            # Boundaries on x and y coordinates for the selected transects
            x_boundaries = [min([min(l) for l in x_projected]), max([max(l) for l in x_projected])]
            y_boundaries = [min([min(l) for l in y_projected]), max([max(l) for l in y_projected])]
        
            x_start = min(x_boundaries, key=lambda x:abs(x-np.nanmedian(x_left)))
            y_start = min(y_boundaries, key=lambda x:abs(x-np.nanmedian(y_left)))
        
        # Compute distance on the average cross-section
        acs_distance = list()
        if navigation_reference == 'gga_vel':
            for j in range(len(x_projected)):
                distance_transect = np.tile(np.nan, len(x_projected[j]))
                for i in range(len(x_projected[j])):
                    distance_transect[i] = haversine((y_projected[j][i], x_projected[j][i]), (y_start, x_start))
                acs_distance.append(distance_transect)
        elif navigation_reference == 'bt_vel':
            x_projected = [x - min(x_boundaries, key=abs) for x in x_projected]
            y_projected = [x - min(y_boundaries, key=abs) for x in y_projected]
            for i in range(len(x_projected)):
                acs_distance.append(np.sqrt(x_projected[i]**2 + y_projected[i]**2))
            
        return x_projected, y_projected, acs_distance
    
    @staticmethod 
    def translated_transects(acs_distance_raw, depth_data):
        """ Compare bathymetry and translate transects on average cross-section if needed
        
        Parameters
        ----------
        acs_distance_raw: list(np.array(float))
            List of 1D arrays of the distance on the average cross-section
        depth_data: list(np.array(float))
            List of 1D arrays of bathymetry from selected transects
       
        Returns
        -------
        acs_translated: list(np.array(float))
            List of 1D arrays of the corrected distance depending on the bathymetry
        """
        acs_translated = list()
        # Use the first transect as reference and define an homogeneous x grid across measurement
        acs_translated.append(acs_distance_raw[0])
        max_acs = max([max(l) for l in acs_distance_raw])
        min_acs = min([min(l) for l in acs_distance_raw])
        grid_acs = np.arange(min_acs, max_acs, (max_acs-min_acs)/100)
        acs_depth_ref = sc.interpolate.griddata(acs_distance_raw[0], depth_data[0], grid_acs)
        
        for i in range(1,len(acs_distance_raw)): 
            # Interpolate depth on x grid
            acs_depth = sc.interpolate.griddata(acs_distance_raw[i], depth_data[i], grid_acs)
            first_valid_acs = int(np.argwhere(~np.isnan(acs_depth))[0])
            last_valid_acs = int(np.argwhere(~np.isnan(acs_depth))[-1])
            valid_acs_depth = last_valid_acs-first_valid_acs+1
            acs_depth_valid = acs_depth[first_valid_acs:last_valid_acs+1]
            
            # Find the best position to minimize median of sum square
            default_acs_ssd = np.nanmedian((acs_depth_ref[:valid_acs_depth]-acs_depth_valid)**2)
            lag_acs_idx = 0
            for j in range(1,len(acs_depth_ref)-valid_acs_depth+1):
                acs_ssd = np.nanmean((acs_depth_ref[j:j+valid_acs_depth]-acs_depth_valid)**2)     
                if (acs_ssd < default_acs_ssd or np.isnan(default_acs_ssd)) and \
                    np.count_nonzero(np.isnan(acs_depth_ref[j:j+valid_acs_depth]))<0.5*valid_acs_depth:
                    default_acs_ssd = acs_ssd
                    lag_acs_idx = j
            # Correct average cross-section distance by translation
            acs_corrected = acs_distance_raw[i] - (grid_acs[first_valid_acs] - grid_acs[lag_acs_idx])
            acs_translated.append(acs_corrected)
        
        # Start from 0
        for i in range(len(acs_translated)):
            acs_translated[i]-= min([min(l) for l in acs_translated])
            
        return acs_translated
        
    
    @staticmethod
    def compute_node_size(transect_ref, acs_distance, depth_data, node_horizontal_user, 
                          node_vertical_user):
        """ Define horizontal and vertical mesh
        
        Parameters
        ----------
        transect_ref: TransectData
            Object TransectData
        acs_distance: list(np.array(float))
            List of 1D arrays of the corrected distance on the average cross-section
        depth_data: list(np.array(float))
            List of 1D arrays of bathymetry from selected transects
        node_horizontal_user: float
            Horizontal size of the mesh define by the user
        node_vertical_user: float
            Vertical size of the mesh define by the user
            
        Returns
        -------
        borders_ens: list(float)
            Horizontal grid length on the average cross-section
        nodes_depth: list(float)
            Vertical grid depth from the free-surface
        """
        # Define number of meshs on the average cross-section
        acs_total = max([max(l) for l in acs_distance])-min([min(l) for l in acs_distance])
        acs_distance[0][1:]-acs_distance[0][:-1]
        if node_horizontal_user == None: 
            all_acs_distance = np.array([item for subarray in acs_distance for item in subarray])
            all_acs_distance.sort()
            acs_diff = abs(all_acs_distance[1:] - all_acs_distance[:-1])
            node_horz = np.nanmax([acs_total/50, np.max(acs_diff)+np.std(acs_diff)])
        else:
            node_horz = node_horizontal_user
        
        # Divise total length in same size meshs
        nb_horz = int(acs_total/node_horz)
        borders_ens = np.linspace(min([min(l) for l in acs_distance]), 
                                        max([max(l) for l in acs_distance]), nb_horz)
        
        # Define mesh vertical size
        all_depth = np.array([item for subarray in depth_data for item in subarray])
        if node_vertical_user :
            nodes_depth = np.round(np.arange(0, np.nanmax(all_depth)+2*node_vertical_user, 
                                                     node_vertical_user).tolist(), 3)
        else :
            lag = np.nanmax([0.1, 2*(transect_ref.depths.bt_depths.depth_cell_depth_m[1,0]-\
                                    transect_ref.depths.bt_depths.depth_cell_depth_m[0,0])])           
            nodes_depth = np.arange(0, np.nanmax(all_depth)+lag, lag)

        return borders_ens, nodes_depth
   
    @staticmethod
    def compute_nodes_velocity(navigation_reference, checked_transect_idx, w_vel_x, w_vel_y, w_vel_z, 
                               cell_depth, depth_data, acs_distance, borders_ens_raw, 
                               nodes_depth_raw, orig_start_edge):
        """ Compute transect median velocity on each mesh (North, East and vertical velocities) 
        and depth on each vertical
        
        Parameters
        ----------
        navigation_reference: str
            Object TransectData
        checked_transect_idx: list(int)
            List of selected transects
        w_vel_x: list(np.array(float))
            List of arrays of cells East velocity from selected transects
        w_vel_y: list(np.array(float))
            List of arrays of cells North velocity from selected transects
        w_vel_z: list(np.array(float))
            List of arrays of cells vertical velocity from selected transects
        cell_depth:
            
        depth_data: list(np.array(float))
            List of 1D arrays of bathymetry from selected transects
        acs_distance: list(np.array(float))
            List of 1D arrays of the corrected distance on the average cross-section
        borders_ens_raw: list(float)
            Horizontal grid length on the average cross-section
        nodes_depth_raw: list(float)
            Vertical grid depth from the free-surface
        orig_start_edge: list(str)
            List of original starting edge of transect looking downstream (Left or Right) from 
            selected transects
            
        Returns
        -------
        borders_ens: list(float)
            Horizontal grid length on the average cross-section
        nodes_depth: list(float)
            Vertical grid depth from the free-surface
        """
        
        node_mid = (borders_ens_raw[1:]+borders_ens_raw[:-1])/2
        # Create list to save transects interpolated on mesh grid
        transects_nodes = list()
        transects_node_x_velocity = np.tile(np.nan, (len(checked_transect_idx), len(nodes_depth_raw)-1, len(node_mid)))
        transects_node_y_velocity = np.tile(np.nan, (len(checked_transect_idx), len(nodes_depth_raw)-1, len(node_mid)))
        transects_node_vertical_velocity = np.tile(np.nan, (len(checked_transect_idx), len(nodes_depth_raw)-1, len(node_mid)))
        transects_node_depth = np.tile(np.nan, (len(checked_transect_idx), len(node_mid)))
            
        for id_transect in checked_transect_idx :
            index_transect = checked_transect_idx.index(id_transect)
            print('========== '+str(index_transect)+' sur '+str(len(checked_transect_idx)-1)+' =========')
            w_vel_x_tr = w_vel_x[index_transect]
            w_vel_y_tr = w_vel_y[index_transect]
            w_vel_z_tr = w_vel_z[index_transect]
            cell_depth_tr = cell_depth[index_transect]
            depth_ens_tr = depth_data[index_transect]
            
            if navigation_reference=='bt_vel' and orig_start_edge[index_transect] == 'Right':
                cell_depth_tr = np.flip(cell_depth_tr, axis = 1)
                w_vel_x_tr = np.flip(w_vel_x_tr, axis = 1)
                w_vel_y_tr = np.flip(w_vel_y_tr, axis = 1)
                w_vel_z_tr = np.flip(w_vel_z_tr, axis = 1)
            
            # Find the representative mesh of each transect's vertical
            data = {'Index_ensemble': np.arange(len(acs_distance[index_transect])), 'Index_Node': None, 
                'Distance_node': None, 'Distance_ensemble': acs_distance[index_transect]}
            df = pd.DataFrame(data)      
            for i in range(len(acs_distance[index_transect])):
                    df.loc[[i], ['Index_Node', 'Distance_node']] = next(x for x in enumerate(borders_ens_raw[1:]) 
                                                                        if x[1] >= df.loc[i]['Distance_ensemble'])   
            
            # Transect's nodes
            id_proj = list(np.unique(df['Index_Node']))
            transects_nodes.append(id_proj)
            
            # Run through nodes to determine each parameters
            for node in id_proj:          
                index_node = np.array(df[df['Index_Node']==node]['Index_ensemble'])               
                w_vel_x_node = w_vel_x_tr[:,index_node]
                w_vel_y_node = w_vel_y_tr[:,index_node]
                w_vel_z_node = w_vel_z_tr[:,index_node]
                mid_cell_node = cell_depth_tr[:,index_node]
                depth_node = depth_ens_tr[index_node]    
                
                transects_node_depth[index_transect, node]  = np.nanmedian(depth_node) 
                # Determine every transect's cells in the mesh
                for id_vert in range(len(nodes_depth_raw)-1):
                        (id_x, id_y) = np.where(np.logical_and(mid_cell_node >= nodes_depth_raw[id_vert], \
                                                               mid_cell_node < nodes_depth_raw[id_vert+1]))    
                        w_vel_x_loc = w_vel_x_node[id_x, id_y]
                        w_vel_y_loc = w_vel_y_node[id_x, id_y]
                        w_vel_z_loc = w_vel_z_node[id_x, id_y]
    
                        transects_node_x_velocity[index_transect, id_vert, node] = np.nanmedian(w_vel_x_loc)
                        transects_node_y_velocity[index_transect, id_vert, node] = np.nanmedian(w_vel_y_loc)
                        transects_node_vertical_velocity[index_transect, id_vert, node] = np.nanmedian(w_vel_z_loc)

        return transects_node_x_velocity, transects_node_y_velocity, transects_node_vertical_velocity, \
                transects_node_depth, transects_nodes
   

    def compute_mean(self, transects_nodes, transects_node_x_velocity, transects_node_y_velocity,
                       transects_node_vertical_velocity, transects_node_depth, borders_ens_raw,
                       nodes_depth_raw):
        """ Compute mesh mean value of selected transects
        
        Parameters
        ----------
        transects_nodes: np.array(np.array(flot))
            Array of 1D arrays of nodes detected by each transect
        transects_node_x_velocity: np.array(np.array(flot))
            Median East velocity on each mesh of each transect
        transects_node_y_velocity: np.array(np.array(flot))
            Median North velocity on each mesh of each transect
        transects_node_vertical_velocity: np.array(np.array(flot))
            Median vertical velocity on each mesh of each transect
       transects_node_depth: np.array(np.array(float))
            Array of 1D arrays of depth values on each vertical for each transects
        borders_ens_raw: list(float)
            Horizontal grid length on the average cross-section
        nodes_depth_raw: list(float)
            Vertical grid depth from the free-surface
            
        Returns
        -------
        MAP_x_velocity: np.array
            East velocity on each cell of the MAP section
        MAP_y_velocity: np.array
            North velocity on each cell of the MAP section
        """
        # Define meshs detected by enough transects
        unique_node = list(np.unique([x for l in transects_nodes for x in l]))
        node_selected = copy.deepcopy(unique_node)
        valid_cell = max(int(len(transects_nodes)/3), 1)
        for node in unique_node :
            if sum(x.count(node) for x in transects_nodes) < valid_cell or \
            np.isnan(transects_node_depth[:,node]).all():
                node_selected.remove(node)  
    
        node_min = np.nanmin(node_selected)
        node_max = np.nanmax(node_selected)
        node_range = list(range(node_min, node_max+1))
        node_range_border = list(range(node_min, node_max+2))
        
        borders_ens = borders_ens_raw[node_range_border]
        borders_ens-= min(borders_ens)
        
        MAP_depth_cells_border = np.tile(np.nan, (len(nodes_depth_raw), len(node_range)))
        for i in range(len(node_range)):
            MAP_depth_cells_border[:,i] = nodes_depth_raw
        
        MAP_x_velocity = np.tile(np.nan, (len(nodes_depth_raw)-1,len(node_range)))
        MAP_y_velocity = np.tile(np.nan, (len(nodes_depth_raw)-1,len(node_range)))
        MAP_vertical_velocity = np.tile(np.nan, (len(nodes_depth_raw)-1,len(node_range)))
        MAP_depth = np.tile(np.nan, len(node_range))
        
        for node in node_selected :
            index_node = node_range.index(node)
            row = np.array([j for (j, sub) in enumerate(transects_nodes) if node in sub])        

            x_map_cell = transects_node_x_velocity[row,:, node]
            y_map_cell = transects_node_y_velocity[row,:, node]
            vertical_map_cell = transects_node_vertical_velocity[row,:, node]
            depth_map_cell = transects_node_depth[row, node]
            
            MAP_depth[index_node] = np.nanmean(depth_map_cell) 
            
            # Cut values under streambed    
            if np.isnan(MAP_depth[index_node]):
                depth_limit = 0
            else :
                depth_limit = next(x[0] for x in enumerate(nodes_depth_raw) if x[1] > MAP_depth[index_node])
                
            x_map_cell[:, depth_limit:] = np.nan
            y_map_cell[:, depth_limit:] = np.nan
            vertical_map_cell[:, depth_limit:] = np.nan
            
            # # Cut value if not detected by enough transects
            # MAP_x_velocity[np.count_nonzero(~np.isnan(x_map_cell), axis = 0) > valid_cell,index_node] = np.nanmedian(x_map_cell[:, 
            #                 np.count_nonzero(~np.isnan(x_map_cell), axis = 0) > valid_cell], axis = 0)
            # MAP_y_velocity[np.count_nonzero(~np.isnan(y_map_cell), axis = 0) > valid_cell,index_node] = np.nanmedian(y_map_cell[:,
            #                 np.count_nonzero(~np.isnan(y_map_cell), axis = 0) > valid_cell], axis = 0)
            # MAP_vertical_velocity[np.count_nonzero(~np.isnan(vertical_map_cell), axis = 0) > valid_cell,index_node] = np.nanmedian(vertical_map_cell[:, 
            #                 np.count_nonzero(~np.isnan(vertical_map_cell), axis = 0) > valid_cell], axis = 0)
            
            MAP_x_velocity[:,index_node] = np.nanmean(x_map_cell, axis = 0)
            MAP_y_velocity[:,index_node] = np.nanmean(y_map_cell, axis = 0)
            MAP_vertical_velocity[:,index_node] = np.nanmean(vertical_map_cell, axis = 0)
            MAP_depth_cells_border[depth_limit,index_node] = MAP_depth[index_node]
            MAP_depth_cells_border[depth_limit+1:,index_node] = np.nan

        deeper_border = [i for i, x in enumerate(self.border_depth > np.nanmax(MAP_depth)) if x]
        if len(deeper_border)>1:
            remove_row_idx = deeper_border[1:]
            self.border_depth = np.delete(self.border_depth, remove_row_idx)
            MAP_x_velocity = np.delete(MAP_x_velocity, [i - 1 for i in remove_row_idx], axis=0)
            MAP_y_velocity = np.delete(MAP_y_velocity, [i - 1 for i in remove_row_idx], axis=0)
            MAP_vertical_velocity = np.delete(MAP_vertical_velocity, [i - 1 for i in remove_row_idx], axis=0)
            MAP_depth_cells_border = np.delete(MAP_depth_cells_border, [i - 1 for i in remove_row_idx], axis=0)

        self.vertical_velocity = MAP_vertical_velocity
        self.depths = MAP_depth
        self.depth_cells_border = MAP_depth_cells_border
        self.borders_ens = borders_ens
            
        return MAP_x_velocity, MAP_y_velocity
    
    
    def compute_rozovskii(self, x_velocity, y_velocity):
        """ Compute primary and secondary velocity according Rozovskii projection
        
        Parameters
        ----------
        x_velocity: np.array
            East velocity on each cell of the MAP section
        y_velocity: np.array
            North velocity on each cell of the MAP section
            
        Returns
        -------
        direction: np.array
            1D array of mean velocity direction of each MAP vertical
        """
        # Compute mean velocity components in each ensemble
        w_vel_mean_1 = np.nanmean(x_velocity, 0)
        w_vel_mean_2 = np.nanmean(y_velocity, 0)

        # Compute a unit vector
        direction, _ = cart2pol(w_vel_mean_1, w_vel_mean_2)
        unit_vec_1, unit_vec_2 = pol2cart(direction, 1)
        unit_vec = np.vstack([unit_vec_1, unit_vec_2])

        # Compute the velocity magnitude in the direction of the mean velocity of each
        # ensemble using the dot product and unit vector
        w_vel_prim = np.tile([np.nan], x_velocity.shape)  
        w_vel_sec = np.tile([np.nan], x_velocity.shape)   
        for i in range(x_velocity.shape[0]):
            w_vel_prim[i, :] = np.sum(np.vstack([x_velocity[i, :], y_velocity[i, :]]) * unit_vec, 0)
            w_vel_sec[i, :] = unit_vec_2*x_velocity[i, :] - unit_vec_1* y_velocity[i, :]

        self.primary_velocity = w_vel_prim
        self.secondary_velocity = w_vel_sec
        
        return direction
        
        
    def compute_extrap_velocity(self, settings, extrap_option):
        """ Compute top/bottom extrapolation according QRevInt velocity exponent
        
        Parameters
        ----------
        nodes_depth_raw: list(float)
            Vertical grid depth from the free-surface
        settings: dict
            Measurement current settings
        extrap_option: bool
            Option to define if extrapolation should be applied
        Returns
        -------
        idx_bot: np.array(int)
            Index to the bottom most valid depth cell in each ensemble
        idx_top: np.array(int)
            Index to the top most valid depth cell in each ensemble
        """
        # Parameter to return nan in extrap value if extrapolation is not selected
        if extrap_option :
            units = 1
        else:
            units = np.nan
        
        depths = self.depths
        depth_cells_border = self.depth_cells_border
        w_vel_prim_extrap = np.copy(self.primary_velocity)
        w_vel_sec_extrap = np.copy(self.secondary_velocity)
        w_vel_z_extrap = np.copy(self.vertical_velocity)
        
        MAP_depth_cells_border = np.tile(np.nan, depth_cells_border.shape)
        for i in range(depth_cells_border.shape[1]):
            MAP_depth_cells_border[:,i] = self.border_depth
            
        MAP_depth_cells_center = (MAP_depth_cells_border[1:,:]+MAP_depth_cells_border[:-1,:])/2
        
        # Blanking on the bottom
        blanking_depth = depths * 0.9
        for i in range(MAP_depth_cells_center.shape[1]):
            invalid = MAP_depth_cells_center[:,i] > blanking_depth[i]
            w_vel_prim_extrap[invalid,i] = np.nan
            w_vel_sec_extrap[invalid,i] = np.nan
            w_vel_z_extrap[invalid,i] = np.nan
        
        # Identify valid data      
        valid_data = np.logical_not(np.isnan(w_vel_prim_extrap))
        # Preallocate variables
        n_ensembles = valid_data.shape[1]
        idx_bot = np.tile(-1, (valid_data.shape[1])).astype(int)
        idx_top = np.tile(-1, valid_data.shape[1]).astype(int)
        
        for n in range(n_ensembles):
            # Identifying bottom most valid cell
            idx_temp = np.where(np.logical_not(np.isnan(w_vel_prim_extrap[:, n])))[0]
            if len(idx_temp) > 0:
                idx_top[n] = idx_temp[0]
                idx_bot[n] = idx_temp[-1]               
            else:    
                idx_top[n] = 0
        
        # QRevInt extrapolation method
        idx_bed = copy.deepcopy(idx_bot)
        bot_method = settings['extrapBot']
        top_method = settings['extrapTop']
        exponent = settings['extrapExp']
        
        depth_cells_center = (depth_cells_border[1:]+depth_cells_border[:-1])/2
        mid_bed_cells = depths - depth_cells_center
        
        if bot_method == 'Power':
            coef_primary_bot = np.nanmean(w_vel_prim_extrap, 0)
        elif bot_method == 'No Slip':
            cutoff_depth = 0.8 * depths
            depth_ok = (nan_greater(depth_cells_center, np.tile(cutoff_depth, (depth_cells_center.shape[0], 1))))
            component_ok = np.logical_not(np.isnan(w_vel_prim_extrap))
            use_ns = depth_ok * component_ok
            for j in range(len(idx_bot)):
                if idx_bot[j] >= 0:
                    use_ns[idx_bot[j], j] = 1
            component_ns = np.copy(w_vel_prim_extrap)
            component_ns[np.logical_not(use_ns)] = np.nan
            
            coef_primary_bot = np.nanmean(component_ns, 0)
        

        # Extrapolation Bot velocity
        for n in range(len(idx_bed)):
            if idx_bed[n] > -1 :
                while idx_bed[n]<len(depth_cells_center[:, n]) and  depth_cells_border[idx_bed[n]+1, n]<= depths[n]:
                    idx_bed[n]+=1
                # Shape of bottom cells
                bot_depth = mid_bed_cells[idx_bot[n]+1:(idx_bed[n]),n]

                # Extrapolation for Primary velocity
                bot_prim_value = coef_primary_bot[n] * ((1+1/exponent)/(1/exponent)) * ((bot_depth/depths[n])**exponent)  
                w_vel_prim_extrap[(idx_bot[n]+1):(idx_bed[n]),n] = bot_prim_value * units
                
                # Constant extrapolation for Secondary velocity
                w_vel_sec_extrap[(idx_bot[n]+1):(idx_bed[n]),n] = w_vel_sec_extrap[idx_bot[n], n] * units
                # Linear extrapolation to 0 at streambed for Vertical velocity
                try :
                    w_vel_z_extrap[(idx_bot[n]+1):(idx_bed[n]),n] = units *sc.interpolate.griddata(np.append(mid_bed_cells[idx_bot[n],n], 0), np.append(self.vertical_velocity[idx_bot[n],n], 0),
                                                                                                  mid_bed_cells[(idx_bot[n]+1):(idx_bed[n]),n])
                except Exception : pass


        # Top power extrapolation (primary)
        if top_method == 'Power':
            coef_primary_top = np.nanmean(w_vel_prim_extrap, 0)
            for n in range(len(idx_top)):
                top_depth = mid_bed_cells[:idx_top[n],n]
                top_prim_value = coef_primary_top[n] * ((1+1/exponent)/(1/exponent)) * ((top_depth/depths[n])**exponent) 
                w_vel_prim_extrap[:idx_top[n],n] = top_prim_value * units
            # top_prim_value = coef_primary_top * ((1+1/exponent)/(1/exponent)) * (((depth_ens-top_rng/2)/depth_ens)**exponent)       

        # Top constant extrapolation (primary)
        elif top_method == 'Constant':
            n_ensembles = len(idx_top)
            top_prim_value = np.tile([np.nan], n_ensembles)
            for n in range(n_ensembles):
                if idx_top[n] >= 0:
                    w_vel_prim_extrap[:idx_top[n], n] = w_vel_prim_extrap[idx_top[n], n] * units
                      
        # Extrap top for second and vertical velocities
        for n in range(len(idx_top)):
            if idx_top[n] >= 0:
                w_vel_sec_extrap[:idx_top[n], n] = w_vel_sec_extrap[idx_top[n], n] * units
                try :
                    top_z_value = sc.interpolate.griddata(np.append(mid_bed_cells[idx_top[n],n], self.depths[n]), np.append(w_vel_z_extrap[idx_top[n],n], 0),
                                            mid_bed_cells[:idx_top[n],n])
                    w_vel_z_extrap[:idx_top[n], n] = top_z_value * units
                except Exception : pass
        
        self.depth_cells_center = depth_cells_center
        self.extrap_primary_velocity = w_vel_prim_extrap
        self.extrap_secondary_velocity = w_vel_sec_extrap
        self.extrap_vertical_velocity = w_vel_z_extrap 

        return idx_bot, idx_top
    
    def compute_edges(self, borders_ens_raw, mid_direction, settings, edge_constant):
        """ Compute edge extrapolation
        
        Parameters
        ----------
        borders_ens_raw: list(float)
            Horizontal grid length on the average cross-section
        mid_direction: np.array
            1D array of mean velocity direction of each MAP vertical
        settings: dict
            Measurement current settings
        edge_constant: bool
            Option to define if edge's meshs should share the exact same length or if they should be
            the same length as those in the middle (except the last vertical which is shorter)
        
        Returns
        -------
        left_direction/right_direction: np.array
            Direction of the first/last ensemble applied to edge
        left_area/right_area: np.array
            Area of edge's cells
        left_mid_cells_x/right_mid_cells_x: np.array
            Longitudinal position of the middle of each cell
        left_mid_cells_y/right_mid_cells_y: np.array
            Depth position of the middle of each cell
        """
        extrap_exp = 1/settings['extrapExp']
        self.left_primary_velocity, self.left_secondary_velocity, self.left_vertical_velocity, \
            self.left_borders, left_direction, left_mid_cells_x, left_mid_cells_y, \
            left_area, left_vertical_depth = self.edge_velocity(self.left_distance, self.left_coef, 'left', borders_ens_raw, mid_direction,
                                                                         extrap_exp, edge_constant)

        self.right_primary_velocity, self.right_secondary_velocity, self.right_vertical_velocity, \
           self.right_borders, right_direction, right_mid_cells_x, right_mid_cells_y, \
           right_area, right_vertical_depth = self.edge_velocity(self.right_distance, self.right_coef, 'right', borders_ens_raw, mid_direction,
                                                                         extrap_exp, edge_constant)

        self.left_mid_cells_x = left_mid_cells_x
        self.left_mid_cells_y = left_mid_cells_y
        self.left_vertical_depth = left_vertical_depth
        self.right_mid_cells_x = right_mid_cells_x
        self.right_mid_cells_y = right_mid_cells_y
        self.right_vertical_depth = right_vertical_depth

        return left_direction, right_direction, left_area, right_area, left_mid_cells_x, \
            right_mid_cells_x, left_mid_cells_y, right_mid_cells_y
        
    def edge_velocity(self, edge_distance, edge_coef, edge, borders_ens_raw, mid_direction,
                      extrap_exp, edge_constant):
        """ Compute edge extrapolation
        
        Parameters
        ----------
        edge_distance: float
            Edge distance
        edge_coef: float
            Shape coefficient of the edge
        edge: str
            'left' or 'right'
        borders_ens_raw: list(float)
            Horizontal grid length on the average cross-section
        mid_direction: np.array
            1D array of mean velocity direction of each MAP vertical
        extrap_exp: float
            QRevInt exponent power law value
        edge_constant: bool
            Option to define if edge's meshs should share the exact same length or if they should be
            the same length as those in the middle (except the last vertical which is shorter)
        
        Returns
        -------
        edge_primary_velocity: np.array
            Primary velocity of cells in edge
        edge_secondary_velocity: np.array
            Secondary velocity of cells in edge
        edge_vertical_velocity: np.array
            Vertical velocity of cells in edge
        nodes: np.array
            Length borders posiotion of ensemble
        edge_direction: np.array
            Direction of the first/last ensemble applied to edge
        mid_cells_x: np.array
            Longitudinal position of the middle of each cell
        mid_cells_y: np.array
            Depth position of the middle of each cell
        area: np.array
            Area of edge's cells
        """
        if edge == 'left':
            id_edge = 0
            node_size = abs(borders_ens_raw[1]-borders_ens_raw[0])
        elif edge == 'right':
            id_edge = -1
            node_size = abs(borders_ens_raw[-1]-borders_ens_raw[-2])
          
        if edge_constant:
            # Découpage de la berge
            nb_nodes = int(np.round(0.5+edge_distance/node_size))
            nodes = np.linspace(0, edge_distance, nb_nodes+1) # Découpage x (bords)
        else:
            nodes = edge_distance-np.arange(0,edge_distance, node_size)[::-1]
            nodes = np.insert(nodes, 0,0)
            nb_nodes = len(nodes)-1
            
            
        nodes_mid = (nodes[1:]+nodes[:-1])/2 # Decoupage x
        edge_size_raw = self.depth_cells_border[:,id_edge] # Decoupage y
        
        if edge_coef==0.3535 and edge_distance>0:
            # Depth arrays 
            border_depths = np.multiply(nodes, self.depths[id_edge]/edge_distance) # Profondeur sur les bords de chaque verticale
            cells_borders_depths_1 = np.transpose([edge_size_raw] * (len(border_depths)-1)) # Position en profondeur des bords de chaque cellule
            cells_borders_depths_2 = np.transpose([edge_size_raw] * (len(border_depths)))
            
            for i in range(len(border_depths)-1):
                sub_index = next(x[0] for x in enumerate(cells_borders_depths_1[:,i]) if x[1] >= int(1000*border_depths[i+1])/1000)
                cells_borders_depths_1[sub_index, i] = border_depths[i+1]
                cells_borders_depths_1[sub_index+1:, i] = np.nan
                cells_borders_depths_2[sub_index-1, i+1] = border_depths[i+1]
                cells_borders_depths_2[sub_index:, i] = np.nan
              
            # Distance arrays
            cut_x = edge_distance*edge_size_raw[edge_size_raw<=self.depths[id_edge]]/self.depths[id_edge] # Position couche coupe fond
            x_left = np.tile(nodes, (cells_borders_depths_1.shape[0],1))
                
            for j in range(np.count_nonzero(~np.isnan(cut_x))-1):     
                col, _ = next(x for x in enumerate(nodes) if x[1] > cut_x[j])
                row = np.where(edge_size_raw == edge_size_raw[j])[0][0]
                x_left[row, col-1] = cut_x[j]
                x_left[row+1:, col-1] = nodes[col]
           
            # Cells serparate in 2 rectangles and 1 triangle 
            area_rec2 = (x_left[:-1,1:] - x_left[1:,:-1])*(cells_borders_depths_1[1:,:] - cells_borders_depths_1[:-1,:])
            area_rec1 = (x_left[1:,:-1] - x_left[:-1,:-1])*(cells_borders_depths_2[:-1,:-1] - cells_borders_depths_1[:-1,:])
            area_tri1 = (x_left[1:,:-1] - x_left[:-1,:-1])*(cells_borders_depths_1[1:,:] - cells_borders_depths_2[:-1,:-1])/2
            area_tra1 = area_rec1 + area_tri1
            area = area_tra1 + area_rec2
            
            # Compute mid of every shape
            mid_rec2_x = (x_left[:-1,1:] + x_left[1:,:-1])/2
            mid_rec2_y = (cells_borders_depths_1[:-1,:] + cells_borders_depths_1[1:,:])/2
            
            mid_rec1_x = (x_left[:-1,:-1] + x_left[1:,:-1])/2
            mid_rec1_y = (cells_borders_depths_1[:-1,:] + cells_borders_depths_2[:-1,:-1])/2
            
            mid_tri1_x = (x_left[:-1,:-1] + 2*x_left[1:,:-1])/3
            mid_tri1_y = (2*cells_borders_depths_2[:-1,:-1] + cells_borders_depths_1[1:,:])/3
            
            mid_tra1_x = (area_rec1*mid_rec1_x + area_tri1*mid_tri1_x)/(area_tra1)
            mid_tra1_y = (area_rec1*mid_rec1_y + area_tri1*mid_tri1_y)/(area_tra1)
            mid_tra1_x[area_tra1==0] = 0
            mid_tra1_y[area_tra1==0] = 0
            
            # Compute cell's mid
            mid_cells_x = (area_rec2*mid_rec2_x + area_tra1*mid_tra1_x)/area
            mid_cells_y = (area_rec2*mid_rec2_y + area_tra1*mid_tra1_y)/area
            edge_exp = 2.41
            
            bed_distance = mid_cells_y*edge_distance/self.depths[id_edge]
            vertical_depth = mid_cells_x*self.depths[id_edge]/edge_distance
            
            is_edge = True
            
        elif edge_coef==0.91 and edge_distance>0:  
            mid_cells_x = np.tile([nodes_mid], (len(edge_size_raw)-1,1))
            mid_cells_y = np.transpose([self.depth_cells_center[:,id_edge]]*nb_nodes)
            
            size_x = np.tile([nodes[1:]-nodes[:-1]], (len(edge_size_raw)-1,1))
            size_y = np.transpose([edge_size_raw[1:]-edge_size_raw[:-1]]*nb_nodes)
            area = size_x * size_y
            bed_distance = np.tile(0, area.shape)
            # border_depths = np.tile([self.depths[id_edge]], nb_nodes+1)
            vertical_depth = np.tile([self.depths[id_edge]], area.shape)
            edge_exp = 10  
            
            is_edge = True
        
        else:
            mid_cells_x = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            mid_cells_y = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            
            # size_x = np.tile([nodes[1:]-nodes[:-1]], (len(edge_size_raw)-1,1))
            # size_y = np.transpose([edge_size_raw[1:]-edge_size_raw[:-1]]*nb_nodes)
            area = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            bed_distance = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            # border_depths = np.tile([np.nan], (nb_nodes+1))
            vertical_depth = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            edge_exp = np.nan  
            
            edge_primary_velocity = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            edge_secondary_velocity = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            edge_vertical_velocity = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            
            is_edge = False
            
        if np.all(np.isnan(self.primary_velocity[:, id_edge])) or not is_edge:
            edge_primary_velocity = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            edge_secondary_velocity = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
            edge_vertical_velocity = np.tile([np.nan], (len(edge_size_raw)-1,nb_nodes))
        
        else:
            ## Primary velocity : Power_power extrapolation from first ensemble
            # Mean velocity on the first valid ensemble
            primary_mean_valid = np.nanmean(self.extrap_primary_velocity[:,id_edge])
            # Compute mean velocity according power law at middle_distance position
            vp_mean = primary_mean_valid*(mid_cells_x/(edge_distance))**(1/edge_exp)
            # Compute velocity according power law on the chosen vertical
            edge_primary_velocity = vp_mean * ((extrap_exp+1)/extrap_exp) * ((vertical_depth-mid_cells_y)/vertical_depth)**(1/extrap_exp)
            
            ## Vertical velocity : linear extrapolation from first ensemble vertical distribution
            vertical_vel_first= np.insert(self.extrap_vertical_velocity[:,id_edge], 0, 0)
            vertical_vel_first = np.append(vertical_vel_first, 0)
            norm_depth_first = np.insert(self.depth_cells_center[:,id_edge]/self.depths[id_edge], 0, 0)
            norm_depth_first = np.append(norm_depth_first, 1)
            norm_depth_edge = mid_cells_y/vertical_depth
            edge_vertical_velocity = sc.interpolate.griddata(norm_depth_first, vertical_vel_first, norm_depth_edge)
            
            ## Secondary velocity : linear interpolation to 0 at edge
            secondary_vel_first= np.insert(self.extrap_secondary_velocity[:,id_edge], 0, self.secondary_velocity[0,id_edge])
            secondary_vel_first = np.append(secondary_vel_first, 
                                            secondary_vel_first[np.where(~np.isnan(secondary_vel_first))[0][-1]])
            depth_first = np.insert(self.depth_cells_center[:,id_edge], 0, 0)
            depth_first = np.append(depth_first, self.depths[id_edge])
            edge_secondary_vel_interp = sc.interpolate.griddata(depth_first, secondary_vel_first, mid_cells_y)
            
            edge_secondary_velocity = np.tile(np.nan, edge_primary_velocity.shape)
            
            for j in range(edge_primary_velocity.shape[1]):
                for i in range(np.count_nonzero(~np.isnan(edge_secondary_vel_interp[:,j]))):
                    edge_secondary_velocity[i,j] = interpolation(np.array([bed_distance[i,j], edge_distance]), 
                                                         np.array([0, edge_secondary_vel_interp[i,j]]), 
                                                         mid_cells_x[i,j])
           
        if edge=='right':
            edge_primary_velocity = edge_primary_velocity[:,::-1]
            edge_secondary_velocity = edge_secondary_velocity[:,::-1]
            edge_vertical_velocity = edge_vertical_velocity[:,::-1]
            mid_cells_x = abs(edge_distance - mid_cells_x[:,::-1])
            mid_cells_y = mid_cells_y[:,::-1]
            area = area[:,::-1]
            nodes = abs(edge_distance - nodes[::-1])
            vertical_depth = vertical_depth[:,::-1]
            
        edge_direction = np.tile(mid_direction[id_edge], edge_primary_velocity.shape[1])
            
        
        return edge_primary_velocity, edge_secondary_velocity, edge_vertical_velocity, nodes, \
                edge_direction, mid_cells_x, mid_cells_y, area, vertical_depth
            
    
    def compute_discharge(self, direction_meas, mid_direction, extrap_option, left_direction=0, right_direction=0, 
                          left_area=0, right_area=0):  
        """ Compute discharge
        
        Parameters
        ----------
        direction_meas: float
            Direction normal to the average cross-section
        mid_direction: np.array
            1D array of mean velocity direction of each MAP vertical
        extrap_option: bool
            Option to define if extrapolation should be applied
        left_direction: str
            Direction of the first ensemble applied to edge
        right_direction: list(float)
            Direction of the last ensemble applied to edge
        left_area: np.array
            Left area of edge's cells
        right_area: float
            Right area of edge's cells
        """
        
        distance = (self.borders_ens[1:]-self.borders_ens[:-1])
        depth = self.depth_cells_border[1:,:]-self.depth_cells_border[:-1,:]
        mid_area = distance*depth
        self.mid_area = mid_area
        
        if extrap_option:
            downstream_velocity_mid = self.extrap_primary_velocity * np.cos(mid_direction - direction_meas) + \
                                        self.extrap_secondary_velocity * np.sin(mid_direction - direction_meas)
                                        
            downstream_velocity_left = self.left_primary_velocity * np.cos(left_direction - direction_meas) + \
                                        self.left_secondary_velocity * np.sin(left_direction - direction_meas)
                                        
            downstream_velocity_right = self.right_primary_velocity * np.cos(right_direction - direction_meas) + \
                                        self.right_secondary_velocity * np.sin(right_direction - direction_meas)
                                
            middle_cells_discharge = mid_area*downstream_velocity_mid
            middle_discharge = np.nansum(middle_cells_discharge)
            
            if middle_discharge<0:
                unit = -1
            else:
                unit = 1
                
            # # No boundary
            depth_cells_no_boundary = np.tile(self.border_depth[1] - self.border_depth[0],
                                              (len(self.border_depth) - 1, len(distance)))
            
            no_boundary_area = distance * depth_cells_no_boundary
            
            if len(self.left_borders)>=2:
                left_distance_cells_no_boundary = np.tile(self.left_borders[1] - self.left_borders[0],
                                                  (len(self.border_depth) - 1, len(self.left_borders)-1))
                left_area_no_boundary = left_distance_cells_no_boundary * (
                        self.border_depth[1] - self.border_depth[0])
                self.no_boundary_left_cells_discharge = left_area_no_boundary * downstream_velocity_left * unit
            else:
                self.no_boundary_left_cells_discharge = []
            
            if len(self.right_borders) >= 2:
                right_distance_cells_no_boundary = np.tile(self.right_borders[-2] - self.right_borders[-1],
                                                  (len(self.border_depth) - 1, len(self.right_borders)-1))
                right_area_no_boundary = right_distance_cells_no_boundary * (
                        self.border_depth[1] - self.border_depth[0])
                self.no_boundary_right_cells_discharge = right_area_no_boundary * downstream_velocity_right * unit
            else:
                self.no_boundary_right_cells_discharge = []
            
            self.no_boundary_middle_cells_discharge = no_boundary_area * downstream_velocity_mid * unit

            
            self.middle_cells_discharge = middle_cells_discharge*unit
            self.middle_discharge = middle_discharge*unit
            
            self.left_cells_discharge = downstream_velocity_left*left_area*unit
            self.right_cells_discharge = downstream_velocity_right*right_area*unit
            
            self.left_discharge = np.nansum(self.left_cells_discharge)
            self.right_discharge = np.nansum(self.right_cells_discharge)
            
            self.total_discharge = self.left_discharge + self.middle_discharge + self.right_discharge
            
        else:
            downstream_velocity_mid = self.primary_velocity * np.cos(mid_direction - direction_meas) + \
                                        self.secondary_velocity * np.sin(mid_direction - direction_meas)
            
            middle_cells_discharge = mid_area*downstream_velocity_mid
            middle_discharge = np.nansum(middle_cells_discharge)
            
            if middle_discharge<0:
                unit = -1
            else:
                unit = 1
            
            self.middle_cells_discharge = middle_cells_discharge*unit
            self.middle_discharge = middle_discharge*unit
            self.total_discharge = middle_discharge*unit
            
        # # No boundary
        # depth_cells_no_boundary = np.tile(self.borders_depth[1] - self.borders_depth[0],
        #                                   (len(self.borders_depth) - 1, len(distance)))
        
        # no_boundary_area = distance * depth_cells_no_boundary
        
        # if len(self.left_borders)>=2:
        #     left_distance_cells_no_boundary = np.tile(self.left_borders[1] - self.left_borders[0],
        #                                       (len(self.borders_depth) - 1, len(self.left_borders)-1))
        #     left_area_no_boundary = left_distance_cells_no_boundary * (
        #             self.borders_depth[1] - self.borders_depth[0])
        #     self.no_boundary_left_cells_discharge = left_area_no_boundary * downstream_velocity_left * unit
        # else:
        #     self.no_boundary_left_cells_discharge = []
            
        # if len(self.right_borders) >= 2:
        #     right_distance_cells_no_boundary = np.tile(self.right_borders[-2] - self.right_borders[-1],
        #                                      (len(self.borders_depth) - 1, len(self.righ_borders)-1))
        #     right_area_no_boundary = right_distance_cells_no_boundary * (
        #             self.borders_depth[1] - self.borders_depth[0])
        #     self.no_boundary_right_cells_discharge = right_area_no_boundary * downstream_velocity_right * unit
        # else:
        #     self.no_boundary_right_cells_discharge = []
        
        # self.no_boundary_middle_cells_discharge = no_boundary_area * downstream_velocity_mid * unit
            
            


# =============================================================================
#     PLOT
# =============================================================================
    @staticmethod
    def plot_projected_data(alpha, beta, x_data, y_data, x_projected, y_projected, path_results, name_meas):

            Path(path_results).mkdir(parents=True, exist_ok=True)

            x_boundaries = [min([min(l) for l in x_projected]), max([max(l) for l in x_projected])]
            fig = plt.figure(figsize=(8, 6)) 
            plt.plot(x_boundaries, [alpha*l+beta for l in x_boundaries], color='firebrick', linewidth=2, label='Average cross-section', zorder = 2)
            for i in range(len(x_data)):
                plt.plot(x_data[i], y_data[i], color='grey', linewidth=1)
            plt.xlabel('Distance East (m)')
            plt.ylabel('Distance North (m)')
            plt.legend()
            fig.savefig(path_results+r'MAP_Average_cross_section_'+name_meas+'.png', dpi=240, bbox_inches='tight')
            fig.clear
    
    
    def plot_profile(self, borders_ens_raw, left_mid_cells_x, right_mid_cells_x,
                     left_mid_cells_y, right_mid_cells_y, path_results, name_meas, plot_extrap = True):
        
        left_distance = self.left_distance
        left_coef = self.left_coef
        right_coef = self.right_coef
        borders_ens = self.borders_ens

        # Define attributs
        plot_data = np.c_[self.left_primary_velocity, self.extrap_primary_velocity, self.right_primary_velocity]
        distance = left_distance + (borders_ens[1:]+borders_ens[:-1])/2
        x_axis = np.copy(distance)
        vertical_nodes = self.border_depth


        depths_plt = np.copy(self.depths)
        if left_coef == 0.3535:
            depths_plt = np.insert(depths_plt, 0, [0, self.depths[0]])
            x_axis = np.insert(x_axis, 0, self.left_borders[[0,-1]])
        elif left_coef == 0.91:
            depths_plt = np.insert(depths_plt, 0, [0, self.depths[0], self.depths[0]])
            x_axis = np.insert(x_axis, 0, self.left_borders[[0,0,-1]])

        if right_coef == 0.3535:
            depths_plt = np.append(depths_plt, [self.depths[-1], 0])
            x_axis = np.append(x_axis, self.left_distance + borders_ens[-1] + self.right_borders[[0, -1]])
        elif right_coef == 0.91:
            depths_plt = np.append(depths_plt, [self.depths[-1], self.depths[-1], 0])
            x_axis = np.append(x_axis, self.left_distance + borders_ens[-1] +self.right_borders[[0, -1, -1]])

        depth_cells_border = self.depth_cells_border
        middle_cells = np.c_[left_mid_cells_y,(depth_cells_border[1:]+depth_cells_border[:-1])/2, 
                                                          right_mid_cells_y]
        
        v = np.c_[self.left_secondary_velocity, np.c_[self.extrap_secondary_velocity, self.right_secondary_velocity]]
        w = np.c_[self.left_vertical_velocity, np.c_[self.extrap_vertical_velocity, self.right_vertical_velocity]]
        
        quiver_distance = np.tile([distance], (plot_data.shape[0], 1))
        quiver_distance = np.c_[left_mid_cells_x, quiver_distance, \
                                left_distance+borders_ens[-1]+right_mid_cells_x]

        quiver_scale = np.round(np.nanquantile(np.sqrt(v**2 + w**2), 0.9), 2)

        mid_dist = np.append(np.insert(borders_ens[1:]+left_distance, 0, self.left_borders), self.right_borders[1:]+
                   borders_ens[-1]+left_distance)
        
        max_limit=0

        x_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
        x_pand = np.array([val for val in mid_dist for _ in (0, 1)][1:-1])           
        for n in range(len(x_pand)):
            x_plt[:,n] = x_pand[n]
        
        cell_plt = np.tile(np.nan, (2 * plot_data.shape[0], 2 * (plot_data.shape[1])))
        cell_pand = np.array([val for val in vertical_nodes for _ in (0, 1)][1:-1])
        for p in range(cell_pand.shape[0]):
            cell_plt[p,:] = cell_pand[p]
        
        speed_xpand = np.tile(np.nan, (plot_data.shape[0], 2 * (plot_data.shape[1])))

        for j in range(plot_data.shape[1]):
            speed_xpand[:,2*j] = plot_data[:,j]
            speed_xpand[:,2*j+1] = plot_data[:,j]
            
        speed_plt = np.repeat(speed_xpand, 2, axis=0)

        # Main parameters
        plt.rcParams.update({'font.size': 14})
        
        ## Canvas
        main_wt_contour_canvas = MplCanvas(width=15, height=3, dpi=240)
        canvas = main_wt_contour_canvas
        fig = canvas.fig
        # Configure axis
        fig.ax = fig.add_subplot(1, 1, 1)
        fig.subplots_adjust(left=0.08, bottom=0.2, right=1, top=0.97, wspace=0.1, hspace=0)
        min_limit = np.nanmin(speed_plt)
        if max_limit == 0:
            if np.sum(speed_plt[speed_plt > -900]) > 0:
                max_limit = np.percentile(speed_plt[speed_plt > -900] , 99)
            else:
                max_limit = 1

        x_fill = np.insert(x_axis,0, -1)
        x_fill = np.append(x_fill, x_fill[-1]+1)
        depth_fill = np.insert(depths_plt,0,0)
        depth_fill = np.append(depth_fill, 0)

        # Create color map
        cmap = cm.get_cmap('viridis')
        cmap.set_under('white')
        # Generate color contour
        c = fig.ax.pcolormesh(x_plt, cell_plt, speed_plt, cmap=cmap, vmin=min_limit,
                               vmax=max_limit)       
        # Add color bar and axis labels
        cb = fig.colorbar(c, pad=0.02)
        cb.ax.set_ylabel(canvas.tr('Primary Water Velocity (m/s)'))
        cb.ax.yaxis.label.set_fontsize(12)
        cb.ax.tick_params(labelsize=12)
        fig.ax.invert_yaxis()
        fig.ax.fill_between(x_fill, np.nanmax(depths_plt)+2, depth_fill, color='w')
        fig.ax.plot(x_axis, depths_plt, color='k', linewidth=1.5)
        q = fig.ax.quiver(quiver_distance, middle_cells, v, w, units='xy', 
                      scale = quiver_scale, width=0.07)
        fig.ax.quiverkey(q, X=1.02, Y=-0.12, U=quiver_scale,label=str(quiver_scale)+'m/s', 
                          labelpos='E', coordinates='axes', fontproperties={'size': 12})
        fig.ax.set_xlabel(canvas.tr('Distance (m)'))
        fig.ax.set_ylabel(canvas.tr('Depth (m)'))
        fig.ax.xaxis.label.set_fontsize(12)
        fig.ax.yaxis.label.set_fontsize(12)
        fig.ax.tick_params(axis='both', direction='in', bottom=True, top=True, left=True, right=True)
        fig.ax.set_ylim(top=0, bottom=np.nanmax(depths_plt)+0.3)
        lower_limit = mid_dist[0]-0.5
        upper_limit = mid_dist[-1]+0.5
        
        fig.ax.set_xlim(left=lower_limit, right=upper_limit)
        canvas.draw()
        show_figure(fig)        
        
        fig.savefig(path_results+r'MAP_Profile_'+name_meas+'.png', dpi=300, bbox_inches='tight')


    def creation_MAP_file(self, path_results, add_name_meas) :

        # create folder if doesn't exist
        if not os.path.exists(path_results):
            os.makedirs(path_results)

        with open(path_results + 'MAP_file_' + add_name_meas + '.txt', 'wb') as fp:
            pickle.dump(self, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # discharge
        # data_discharge_test = self.middle_cells_discharge
        # with open(path_results + 'MAP_data_discharge' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_discharge_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # velocity
        # data_velocity_test = self.primary_velocity
        # with open(path_results + 'MAP_data_velocity' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_velocity_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # velocity_extrap
        # data_velocity_extrap_test = self.extrap_primary_velocity
        # with open(path_results + 'MAP_data_velocity_extrap' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_velocity_extrap_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Depth
        # data_depth_test = self.depths
        # with open(path_results + 'MAP_data_depth' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_depth_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Distance
        # data_distance_test = self.borders_ens[:-1]
        # with open(path_results + 'MAP_data_distance' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_distance_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Cells
        # i = 0
        # n = 2
        # while math.isnan(self.depth_cells_border[n, i]) == True and i < \
        #         (self.depth_cells_border).shape[1]:
        #     i = i + 1
        # incr_array = self.depth_cells_border[:n, i]
        # incr_float = incr_array[1] - incr_array[0]

        # data_cell_test = []
        # for i in range(len(data_discharge_test) + 1):
        #     data_cell_test.append(incr_float * i)
        # data_cell_test = np.array(data_cell_test)

        # with open(path_results + 'MAP_data_cell' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_cell_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Edges
        # velocity_borders = [np.nanmean(self.left_primary_velocity), np.nanmean(self.right_primary_velocity)]
        # discharge_borders = [np.nanmean(self.left_discharge), np.nanmean(self.right_discharge)]
        # distance_border = [np.nanmean(self.left_distance), np.nanmean(self.right_distance)]

        # data_edges_test = np.array([discharge_borders, velocity_borders, distance_border])
        # with open(path_results + 'MAP_data_edges' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_edges_test, fp, protocol=pickle.HIGHEST_PROTOCOL)


        # # For graphs
        # # Border_ens
        # data_border_ens_test = self.borders_ens
        # with open(path_results + 'MAP_data_border_ens' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_border_ens_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Nodes_depth_raw
        # data_nodes_depth_raw_test = self.vertical_nodes
        # with open(path_results + 'MAP_data_nodes_depth_raw' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_nodes_depth_raw_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Depth_cells_borders
        # data_depth_cells_border_test = self.depth_cells_border
        # with open(path_results + 'MAP_data_depth_cells_border' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_depth_cells_border_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Left_distance
        # data_left_distance_test = self.left_distance
        # with open(path_results + 'MAP_data_left_distance' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_left_distance_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Left_borders
        # data_left_borders_test = self.left_borders
        # with open(path_results + 'MAP_data_left_borders' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_left_borders_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # # Right_borders
        # data_right_borders_test = self.right_borders
        # with open(path_results + 'MAP_data_right_borders' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_right_borders_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # data_left_coef_test = self.left_coef
        # with open(path_results + 'MAP_data_left_coef' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_left_coef_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # data_right_coef_test = self.right_coef
        # with open(path_results + 'MAP_data_right_coef' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_right_coef_test, fp, protocol=pickle.HIGHEST_PROTOCOL)



        # data_velocity_discharge_test = self.middle_cells_discharge / self.mid_area
        # with open(path_results + 'MAP_velocity_discharge' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_velocity_discharge_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

        # data_mid_area_test = self.mid_area
        # with open(path_results + 'MAP_mid_area' + add_name_meas + '.txt', 'wb') as fp:
        #     pickle.dump(data_mid_area_test, fp, protocol=pickle.HIGHEST_PROTOCOL)

