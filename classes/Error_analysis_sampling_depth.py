# -*- coding: utf-8 -*-
"""
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas
import seaborn as sns
from statistics import mean
from sklearn.linear_model import * 
from sklearn.metrics import r2_score 
import decimal

#%% Accuracy component (Belaid 2006)
u_pj = 0.01 # m

#%% Reproducibility component 
## use P6 / P72 and BD? plot with different signs 
data = pd.read_csv(r'C:\Users\jessica.laible\Documents\Analyses\Flux_concentration\Analysis_flux_concentration\Sampling_analysis\Error_sampling_depth.csv', sep = ';')
outpath_figures = r'C:\Users\jessica.laible\Documents\Analyses\Flux_concentration\Analysis_flux_concentration'

# Prepare plots
sampler = ['BD', 'BD_C', 'P6', 'P72', 'Pump', 'S']
forms_sampler = ['o', 'o','D', 's', 'P', '*']  
size_sampler = [6, 6, 6, 6, 7, 8]
color_sampler = ['seagreen', 'seagreen', 'mediumblue', 'darkorange',
                 'mediumvioletred', 'darkred']
samplers_used = np.unique(data['Sampler'])
index_samplers_used = [sampler.index(samplers_used[i])
                       for i in range(len(samplers_used))]
# Fontsizes
fontsize_axis = 14
fontsize_legend = 12
fontsize_legend_title = 14
fontsize_text = 12
fontsize_ticks = 12 

u_pr = 0.05 # regress between field_data and diver 

# Plot field depth - diver depth
x_range = np.linspace(0,10,100)
fig, ax = plt.subplots(figsize=(8, 6), dpi=100)

for i in index_samplers_used:
    ax.plot(data['Real_depth_field'][data['Sampler']== sampler[i]], 
            data['Diver_depth'][data['Sampler']== sampler[i]],
            forms_sampler[i], markersize = size_sampler[i], color = color_sampler[i], 
            markeredgecolor = 'black', markeredgewidth=0.5, label = sampler[i])
       
ax.plot(x_range, x_range, '--', color = 'grey', zorder = 0)

ax.set_xlabel('Depth from field data (m)', fontsize=fontsize_axis)
ax.set_ylabel('Depth from diver data (m)', fontsize=fontsize_axis)
ax.xaxis.label.set_size(fontsize_ticks)
ax.yaxis.label.set_size(fontsize_ticks)
ax.set_xlim(0,2)
ax.set_ylim(0,2)

fig.tight_layout()
figname = 'Error_sampling_depth_1'
fig.savefig(outpath_figures + '\\' + figname + '.png', dpi = 300, bbox_inches='tight')




#%% Error on sampling depth 
u_p = np.sqrt(u_pj**2 + u_pr**2) # type error
k = 2 
U_p = k*u_p # expanded uncertainty