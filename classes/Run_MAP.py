# -*- coding: utf-8 -*-
"""
Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
#========================================
# External imports
#========================================
import sys
import numpy as np
from pathlib import Path
from Open_functions import select_file, open_measurement, new_settings
from MAP_class import MAP
import os
import pickle

def run_MAP(input_getted, filepath_ADCP, type_meas) :

    # =============================================================================
    # SETTINGS
    # =============================================================================
    """SETTINGS
            ----------
            path_data :
                Path of the window to open to select measurement
            path_results :
                Path of the folder in which the results are saved
            checked_transect_user :
                Selected transect, if None default QRev checked_transect_idx are selected
            navigation_reference_user :
                Navigation reference : 'BT', 'GPS' or None (select default QRev navigation reference)
            node_vertical_user :
                Size (in m) of the nodes along transects, if None a default value of 1 node by meter is selected
            node_horizontal_user :
                Size (in m) of the nodes on the vertical ensemble, if None the default QRev value is seleted
            plot :
                True if you want to plot profile in the selected folder
            run :
                True if you want to open a measurement
            """

    # Grenoble
    path_data = filepath_ADCP
    # path_results = filepath_ADCP + "MAP/"
    if type_meas == 'TRDI':
        path_results = '/'.join(path_data.split('/')[:-2]) + "/MAP/"
    else:
        path_results = '/'.join(path_data[0].split('/')[:-2]) + "/MAP/"
    add_name_meas = input_getted[4] # name for MAP file


    checked_transect_user = input_getted[0]
    # checked_transect_user = 0,1,2,3,4,5,13,14,15
    # navigation_reference_user in general BT
    navigation_reference_user = input_getted[1]
    # navigation_reference_user = 'BT'
    # node_horizontal_user in general None
    node_horizontal_user = input_getted[2]
    # node_horizontal_user = None
    # node_vertical_user = 0.2 to Grenoble, 0.6 to Perrache
    node_vertical_user = input_getted[3]
    # node_vertical_user = 0.2

    edge_constant = True
    track_section = True
    plot = False
    # activate to save MAP class
    file_MAP = True


    # =============================================================================

    # =============================================================================
    # Open measurement
    # =============================================================================
    # path_meas, type_meas, name_folders = select_file(path_data)
    meas, checked_transect, navigation_reference = open_measurement(path_data, type_meas, apply_settings=True,
                                                                    navigation_reference=navigation_reference_user,
                                                                    checked_transect=checked_transect_user,
                                                                    extrap_velocity=True, run_oursin=True)

    #%%
    # Apply new settings if needed (navigation reference / checked transect)
    # meas, checked_transect, navigation_reference = new_settings(meas, navigation_reference_user, checked_transect_user, extrap_velocity=True)

    # =============================================================================
    # Run MAP
    # =============================================================================
    average_profile = MAP()
    average_profile.populate_data(meas, node_horizontal_user, node_vertical_user, file_MAP=file_MAP,
                                  add_name_meas=add_name_meas,
                                  edge_constant=edge_constant, extrap_option=True, interp_option=True,
                                  track_section=track_section,
                                  plot=plot, path_results=path_results)
    
    # create folder if doesn't exist
    if not os.path.exists(path_results):
        os.makedirs(path_results)
    
    with open(path_results + 'MAP_file_' + add_name_meas + '.txt', 'wb') as fp:
        pickle.dump(average_profile, fp, protocol=pickle.HIGHEST_PROTOCOL)

    #################### comparaison debit ####################
    print(average_profile.total_discharge)
    print(meas.mean_discharges(meas))
    # print(meas.oursin.u_measurement['total_95'][0])
    #################### #################### #################

    return(path_results+'MAP_file_'+add_name_meas+'.txt')