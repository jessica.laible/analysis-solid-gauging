
"""
Calculation of mean cross-sectional sand concentration and grain size distribution
using the mid-section method (ISO 748) following ISO 4363

Copyright (C) 2023  INRAE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

def ISO_method(analysis_data, map_data, choice, sampling_date, outpath_figures, Q_sampling = None): 
    #%%
    abscissa_values = analysis_data['Abscissa'].unique().tolist()
    most_frequent_sampler= analysis_data['Sampler'].value_counts().idxmax()
   
    # Calculate cell properties
    cell_height = map_data.border_depth[1] - map_data.border_depth[0]
    mid = map_data.borders_ens + map_data.left_distance
    border_cells = np.append(np.insert(mid, 0, map_data.left_borders[:-1]), map_data.right_borders[1:] + mid[-1])
    depth_cells = map_data.border_depth
    
    # Length and depth of transect (center of each distance cell, plus start (0) and end)
    track = (border_cells[1:] + border_cells[:-1]) / 2
    track = np.append(np.insert(track, 0, 0), border_cells[-1])
    
    track_depth = np.append(np.insert(map_data.depths, 0, map_data.left_vertical_depth[0, :]),
                            map_data.right_vertical_depth[0, :])
    track_depth = np.append(np.insert(track_depth, 0, 0), 0)
               
    #%% # V1: Point method 
    # # Mean concentration calculated by calculating a mean concentration per vertical first
    
    # Calculate mean sand concentration per vertical 
    # mean_sand_conc_vertical_g_l_v1 = [np.nansum(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]*
    #       analysis_data['Velocity_sampling_point'][analysis_data['Abscissa'] == abscissa_values[i]])/np.nansum(
    #           analysis_data['Velocity_sampling_point'][analysis_data['Abscissa'] == abscissa_values[i]])
    #           for i in range(len(abscissa_values))]
    
    mean_sand_conc_vertical_g_l_v1 = [np.sum(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]*
           analysis_data['NN_field_height'][analysis_data['Abscissa'] == abscissa_values[i]])/analysis_data[
        'Mean_velocity_vertical'][i]/analysis_data['max_depth_vertical'][i]
        for i in range(len(abscissa_values))]
      
    # Determine liquid discharge per segment (stored in analysis_data)
    discharge_vertical_m3_s = [np.nansum(analysis_data['Discharge_NN_field_m3_s'][analysis_data['Abscissa'] == abscissa_values[i]])
                                for i in range(len(abscissa_values))]
    
    # Determine sand flux per vertical 
    sand_flux_vertical_kg_s_v1 = [mean_sand_conc_vertical_g_l_v1[i] * discharge_vertical_m3_s[i]
                      for i in range(len(abscissa_values))]
    
    # Determine cross-sectional sand discharge
    total_sand_flux_kg_s_v1 = np.nansum(sand_flux_vertical_kg_s_v1)
    total_sand_flux_t_h_v1 = total_sand_flux_kg_s_v1*3.6
    
    # Determine cross-sectional mean sand concentration
    mean_sand_conc_g_l_v1 = total_sand_flux_kg_s_v1/map_data.total_discharge
    
    #%% V2: Mean concentration calculated directly (especially for depth-integrative samples) (eq. 5 ISO4363)
    # needs NN fields (stored in analysis_data)
    mean_sand_conc_g_l_v2 = np.nansum([np.nansum(analysis_data['Concentration_sand_g_l'][analysis_data['Abscissa'] == abscissa_values[i]]*
          analysis_data['Velocity_sampling_point'][analysis_data['Abscissa'] == abscissa_values[i]]*
          analysis_data['NN_field_height'][analysis_data['Abscissa'] == abscissa_values[i]]*
          analysis_data['NN_field_length'][analysis_data['Abscissa'] == abscissa_values[i]], axis = 0)
                           for i in range(len(abscissa_values))], axis = 0
            )/np.nansum([np.nansum(analysis_data['Velocity_sampling_point'][analysis_data['Abscissa'] == abscissa_values[i]]*
              analysis_data['NN_field_height'][analysis_data['Abscissa'] == abscissa_values[i]]*
              analysis_data['NN_field_length'][analysis_data['Abscissa'] == abscissa_values[i]], axis = 0)
              for i in range(len(abscissa_values))], axis = 0)
    
    # Calculate total flux per cross-section
    # using ADCP (MAP) discharge
    if choice[3] == 1:
        total_sand_flux_kg_s_v2 = mean_sand_conc_g_l_v2*map_data.total_discharge
        total_sand_flux_t_h_v2 = total_sand_flux_kg_s_v2*3.6   
        
    #%% # Calculate mean grain size per cross-section    
    # Create df containing only samples with GSD
    if choice[2] == 1:
        analysis_data_gsd = analysis_data.dropna(subset=['D10'])
        analysis_data_gsd.reset_index(drop=True, inplace=True)
        
        abscissa_values_gsd = analysis_data_gsd['Abscissa'].unique().tolist()           
        analysis_data_gsd.insert(2, 'Abscissa_Name_gsd', [abscissa_values_gsd.index(i) + 1
                                                  for i in analysis_data_gsd['Abscissa']])
        ISO_mean_gsd_number = len(analysis_data_gsd)
      
        colnames = analysis_data_gsd.columns.values
        index_num = []
        for name in colnames:
            index_num.append(not any([c.isalpha() for c in name]))
        size_classes = colnames[index_num]
        size_classes = [float(i) for i in size_classes]
        
        # Size classes for cumulated and classified distribution
        size_classes_cumsum = size_classes[1:]
        size_classes_cumsum_mm = [size_classes_cumsum[i]*0.001 for i in range(len(size_classes_cumsum))]
        size_lower = np.array(size_classes[0:-1])* 1e-6
        size_upper = np.array(size_classes[1:])* 1e-6
        size_classes_classified = [10**((np.log10(size_lower[i]) + np.log10(size_upper[i]))/2) 
                             for i in range(len(size_lower))]
        size_classes_classified= np.array(size_classes_classified)
        phi_classes_cumsum = -np.log2(size_classes_cumsum_mm)
        
        # Cumulative distribution
        cumsum = [np.nancumsum(analysis_data_gsd.iloc[i,np.where(index_num)[0][0]:np.where(index_num)[0][-1]]) 
                  for i in range(len(analysis_data_gsd))]
        cumsum = pd.DataFrame(cumsum)
        
        if len(analysis_data_gsd) >= 2:
            # Calculate solid discharge per segment surrounding each GSD sample 
            abscissa_values = np.unique(analysis_data_gsd['Abscissa']).tolist()
            half_dis = [sum(abscissa_values[i:i + 2]) / 2
                        for i in range(len(abscissa_values) - 1)]
            abscissa_field = half_dis[:]
            abscissa_field.insert(0, abscissa_values[0]-(abscissa_field[0]-abscissa_values[0]))                              
            abscissa_field.append(abscissa_values[-1]+ abscissa_values[-1]- abscissa_field[-1])
                                
            # Calculate left and right limit (abscissa) for each field
            for i in np.unique(analysis_data_gsd['Abscissa_Name_gsd']):
                analysis_data_gsd.loc[analysis_data_gsd['Abscissa_Name_gsd'] ==
                                  i, 'min_abscissa_ISO_GSD_field'] = abscissa_field[i - 1]
            for i in np.unique(analysis_data_gsd['Abscissa_Name_gsd']):
                analysis_data_gsd.loc[analysis_data_gsd['Abscissa_Name_gsd']
                                  == i, 'max_abscissa_ISO_GSD_field'] = abscissa_field[i]
                
            # Define height, top and bottom of the fields
            field_top = []
            field_bottom = []
            field_height = []
            for k in np.unique(analysis_data_gsd['Abscissa_Name']):
                f = analysis_data_gsd[analysis_data_gsd['Abscissa_Name'] == k]
                f.reset_index(drop=True, inplace=True)
                l = len(f)
            
                # Calculate half height between sampling points
                half_height_per_vert = [(f['Sampling_depth_final'][i + 1] - f['Sampling_depth_final'][i]) / 2
                                        for i in range(len(f['Sampling_depth_final']) - 1)]
                half_height_per_vert.insert(0, f['Sampling_depth_final'][0])
            
                # Uncomment if need to rerun
                if f['max_depth_vertical'][0] < f['Sampling_depth_final'][l - 1]:
                    sys.exit(
                        "Error message: Deepest sampling point below ADCP bottom depth! Adjust last field depth for analysis")
            
                # If no error message:
                half_height_per_vert.append(
                    f['max_depth_vertical'][0] - f['Sampling_depth_final'][l - 1])
            
            
                # Calculating height of field around each sampling point
                field_height_vert = [round((half_height_per_vert[i + 1] + half_height_per_vert[i]), 2)
                                     for i in range(len(half_height_per_vert) - 1)]
                field_height.append(field_height_vert)
            
                # Field top
                field_top_vert = [f['Sampling_depth_final'][i] - half_height_per_vert[i]
                                  for i in range(len(f))]
                field_top.append(field_top_vert)
            
                # Field bottom
                field_bot_vert = [f['Sampling_depth_final'][i] + half_height_per_vert[i + 1]
                                  for i in range(len(f))]
                field_bottom.append(field_bot_vert)
            
            analysis_data_gsd['ISO_GSD_field_height'] = [np.round(item, 2)
                                             for sublist in field_height for item in sublist]
            analysis_data_gsd['ISO_GSD_field_top'] = [np.round(item, 2)
                                          for sublist in field_top for item in sublist]
            analysis_data_gsd['ISO_GSD_field_bottom'] = [np.round(item, 2)
                                             for sublist in field_bottom for item in sublist]
            analysis_data_gsd['ISO_GSD_field_length'] = analysis_data_gsd['max_abscissa_ISO_GSD_field'] - analysis_data_gsd['min_abscissa_ISO_GSD_field']
            analysis_data_gsd['ISO_GSD_area_m2'] = analysis_data_gsd['ISO_GSD_field_height'] * analysis_data_gsd['ISO_GSD_field_length']     
              
            mid_cell_x = (map_data.borders_ens[:-1] + (map_data.borders_ens[1:] - 
                                                       map_data.borders_ens[:-1])/2) + map_data.left_distance
            mid_cell_y = (map_data.depth_cells_border[:-1] + (map_data.depth_cells_border[1:] - 
                                                       map_data.depth_cells_border[:-1])/2)
                        
            list_discharge_sample_cells = []
            for m in range(len(analysis_data_gsd)) :
                list_interm_discharge_sample_cells = []
                for i in range(mid_cell_y.shape[1]) :
                    for k in range(mid_cell_y.shape[0]) :
                        if mid_cell_y[k][i] > analysis_data_gsd['ISO_GSD_field_top'][m] and mid_cell_y[k][i] <\
                        analysis_data_gsd['ISO_GSD_field_bottom'][m] and mid_cell_x[i] > analysis_data_gsd['min_abscissa_ISO_GSD_field'][m] \
                            and mid_cell_x[i] < analysis_data_gsd['max_abscissa_ISO_GSD_field'][m]:
                            list_interm_discharge_sample_cells.append(map_data.middle_cells_discharge[k][i])
                list_discharge_sample_cells.append(sum(list_interm_discharge_sample_cells))
            analysis_data_gsd['Discharge_ISO_GSD_field_m3_s'] = list_discharge_sample_cells
            
            # Determine liquid discharge in edges close to the bank
            discharge_left = np.nansum(map_data.left_cells_discharge)
            discharge_right = np.nansum(map_data.right_cells_discharge)  
            
            # Calculate flux and concentration if flux data are given
            if 'Sand_flux_point_kg_s_m2' in analysis_data_gsd.columns:
                # Calculate solid flux per field
                analysis_data_gsd['ISO_GSD_field_sand_flux_kg_s'] = analysis_data_gsd['Sand_flux_point_kg_s_m2'] * analysis_data_gsd['ISO_GSD_area_m2']
                  
            if 'Concentration_sand_g_l' in analysis_data_gsd.columns:
                # Calculate cross-sectional discharge weighted mean concentration 
                # ISO_GSD_mean_sand_concentration_g_l1 = np.nansum(analysis_data_gsd['Concentration_sand_g_l']*
                #                                            (analysis_data_gsd['ISO_GSD_area_m2']/total_surface)) # surface weighted
                C_last_vertical = analysis_data_gsd[analysis_data_gsd['Abscissa'] == abscissa_values[-1]]['Concentration_sand_g_l']
                
                ISO_GSD_mean_sand_concentration_g_l = np.nansum(analysis_data_gsd['Concentration_sand_g_l']*
                (analysis_data_gsd['Discharge_ISO_GSD_field_m3_s']/map_data.total_discharge)) # discharge weighted
                + analysis_data_gsd['Concentration_sand_g_l'][0] * (discharge_left/map_data.total_discharge)
                + C_last_vertical.iloc[0] *(discharge_right / map_data.total_discharge)
                
                # Calculate solid flux per field using MAP discharge
                analysis_data_gsd['ISO_GSD_field_sand_flux_kg_s'] = analysis_data_gsd['Concentration_sand_g_l']*analysis_data_gsd['Discharge_ISO_GSD_field_m3_s']
                
            
            # Calculate mean grain size distribution weighted per sediment discharge in segment
            mean_gsd_cumsum = np.nansum([cumsum.iloc[i,:]*analysis_data_gsd['ISO_GSD_field_sand_flux_kg_s'][i] 
                          for i in range(len(analysis_data_gsd))], axis = 0)/np.nansum(
                                  analysis_data_gsd['ISO_GSD_field_sand_flux_kg_s'])
                                  
            # Get grain size indices
            d10 = np.interp(10, mean_gsd_cumsum, size_classes_cumsum)
            d50 = np.interp(50, mean_gsd_cumsum, size_classes_cumsum)
            d90 = np.interp(90, mean_gsd_cumsum, size_classes_cumsum)
                        
            # Plot mean grain size distribution 
            # Fontsizes
            fontsize_axis = 14
            fontsize_legend = 12
            fontsize_legend_title = 14
            fontsize_text = 12
            fontsize_ticks = 12 
            
            # xlims
            xlim_left = next(x-2 for x, val in enumerate(mean_gsd_cumsum) if val > 2)
            if xlim_left < 0:
                xlim_left = 0
            xlim_right = next(x+4 for x, val in enumerate(mean_gsd_cumsum) if val > 99.9)
            if xlim_right >= len(size_classes_cumsum):
                xlim_right = len(size_classes_cumsum)-1
            
            fig, ax = plt.subplots(figsize=(10, 6), dpi=100)
                                  
            ax.plot(size_classes_cumsum, mean_gsd_cumsum, '-', lw = 2, color = 'darkred', 
                    zorder = 30, label = 'cross-sectional mean GSD')
            for i in range(len(cumsum)):
                ax.plot(size_classes_cumsum, cumsum.iloc[i,:], '-', lw = 1, color = 'lightgrey', zorder = 1)
            ax.plot(size_classes_cumsum, cumsum.iloc[0,:], '-', lw = 1, color = 'lightgrey', 
                    zorder = 1, label = 'GSD per sample')
            
            ax.set_ylim(-2, 102)
            ax.set_xlim(size_classes_cumsum[xlim_left], size_classes_cumsum[xlim_right])
            legend = plt.legend(fontsize=fontsize_legend, loc = 'lower right')
            plt.setp(legend.get_title(), fontsize=fontsize_legend_title)
            ax.grid(linewidth = 0.2)
            ax.grid(which = 'minor', axis = 'both', linewidth = 0.1)
            ax.set_xscale('log')
            ax.set_xlabel('Particle diameter (µm)', fontsize=fontsize_axis, weight = 'bold')
            ax.set_ylabel('Cumulative fraction (%)', fontsize=fontsize_axis, weight = 'bold')
            ax.tick_params(axis='both', which='major', labelsize = fontsize_ticks)
            
            fig.tight_layout()
            figname = sampling_date+ '_Mean_GSD_cross-section_ISO4363_' + str(analysis_data['Sampler'][0])
            fig.savefig(outpath_figures + figname + '.png', dpi=300, bbox_inches='tight')
            
        else:
            mean_gsd_cumsum = cumsum.iloc[0,:]    
            
    # ISO_D50 = [np.interp(50, ISO_mean_gsd.iloc[i,1:-1], size_classes[1:])
    #        for i in range(len(ISO_mean_gsd))]
           
    #%% # Export data
          
    summary_ISO = pd.DataFrame([mean_sand_conc_g_l_v2, total_sand_flux_kg_s_v2, total_sand_flux_t_h_v2]).transpose()
    summary_ISO.columns = ['mean_sand_conc_g_l', 'total_sand_flux_kg_s', 'total_sand_flux_t_h']
    if choice[2] == 1: 
        ISO_classes = size_classes_cumsum
        ISO_mean_gsd = pd.DataFrame([mean_gsd_cumsum])         
        return analysis_data, summary_ISO, ISO_classes, ISO_mean_gsd, ISO_mean_gsd_number  
    elif choice[2] == 0:       
        return analysis_data, summary_ISO

    # summary_ISO = pd.DataFrame([mean_sand_conc_g_l_v1, total_sand_flux_kg_s_v1, total_sand_flux_t_h_v1,
    #                             mean_sand_conc_g_l_v2, total_sand_flux_kg_s_v2, total_sand_flux_t_h_v2]).transpose()
    # summary_ISO.columns = ['mean_sand_conc_g_l_v1', 'total_sand_flux_kg_s_v1', 'total_sand_flux_t_h_v1', 
    #                       'mean_sand_conc_g_l_v2', 'total_sand_flux_kg_s_v2', 'total_sand_flux_t_h_v2']
    # if choice[2] == 1:
    #     ISO_mean_sand_conc_vertical = mean_sand_conc_vertical_g_l_v1
    #     ISO_mean_gsd = pd.DataFrame([size_classes_cumsum, mean_gsd_cumsum])         
    #     return analysis_data, summary_ISO, ISO_mean_gsd, ISO_mean_sand_conc_vertical    
    # elif choice[2] == 0:
    #     ISO_mean_sand_conc_vertical = mean_sand_conc_vertical_g_l_v1
    #     return analysis_data, summary_ISO, ISO_mean_sand_conc_vertical 
              
              
              
          
          
          
          
          
          
          
          
          
          
          
          
          
          


